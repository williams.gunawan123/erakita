//== Class definition

var ClipboardDemo = function () {
    
    //== Private functions
    var demos = function () {
        // basic example
        new Clipboard('[data-clipboard=true]').on('success', function(e) {
            e.clearSelection();
            Swal.fire({
                type: 'success',
                title: 'Sukses',
                text: 'Berhasil disalin ke Clipboard'
            });
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

jQuery(document).ready(function() {    
    ClipboardDemo.init();
});