function ajaxInit(){
    $(".ajax[data-fadein!=''][data-fadein]").each(function (index, element) {
        $(element).removeClass("ajax");
        // element == this
        $(element).click(function (e) { 
            e.preventDefault();
            $(this).toggleClass("active");
            if ($("#"+$(this).attr("data-fadein")).hasClass("active")) {
                setTimeout(() => {
                    $("#"+$(this).attr("data-fadein")).css("display","none"); 
                }, 100);
            } else {
                $("#"+$(this).attr("data-fadein")).css("display","flex"); 
            }
            $("#"+$(this).attr("data-fadein")).toggleClass("active");
            // $("#"+$(this).attr("data-fadein")).toggle(200);
        });
    });
}
$(document).ready(function(){ 
    
    $("[data-fadein!=''][data-fadein]").each(function (index, element) {
        // element == this
        $(element).click(function (e) { 
            e.preventDefault();
            $(this).toggleClass("active");
            if ($("#"+$(this).attr("data-fadein")).hasClass("active")) {
                setTimeout(() => {
                    $("#"+$(this).attr("data-fadein")).css("display","none"); 
                }, 100);
            } else {
                $("#"+$(this).attr("data-fadein")).css("display","flex"); 
            }
            $("#"+$(this).attr("data-fadein")).toggleClass("active");
            // $("#"+$(this).attr("data-fadein")).toggle(200);
        });
    });

    $("[data-slidein!=''][data-slidein]").each(function (index, element) {
        // element == this
        $(element).click(function (e) { 
            e.preventDefault();
            $(this).toggleClass("active");
            if ($("#"+$(this).attr("data-slidein")).hasClass("active")) {
                setTimeout(() => {
                    $("#"+$(this).attr("data-slidein")).css("display","none"); 
                }, 100);
            } else {
                $("#"+$(this).attr("data-slidein")).css("display","flex"); 
            }
            $("#"+$(this).attr("data-slidein")).toggleClass("active");
            // $("#"+$(this).attr("data-fadein")).toggle(200);
        });
    });
    $(".search-header .show-advance").click(function (e) { 
        e.preventDefault();
        $("header").toggleClass("active");
    });


    $('#status-active').click(function() {                              
        $("#statusTerjual").prop("checked", !$('#status-active').prop("checked"));
        hitungMatch();
        // updateCombox()
    });
    $("#statusTerjual").click(function (e) {                               
        $("#status-active").prop("checked", !$('#statusTerjual').prop("checked"));
        hitungMatch();
        // updateCombox();
    });
    // function updateCombox(){
        
    //     if (!$('#statusTerjual').prop("checked")) {
    //         console.log("matikan listing waktu terjual");
    //         $("#listingWaktuTerjual").attr("disabled", true);
    //     } else {
    //         console.log("nyalakan listing waktu terjual");
    //         $("#listingWaktuTerjual").removeAttr("disabled");
    //     }
        
        
    //     if (!$('#status-active').prop("checked")) {       
    //         $("#listingWaktuAktif").attr("disabled", true);
    //     } else {
    //         $("#listingWaktuAktif").removeAttr("disabled");
    //     }
    //     $('#listingWaktuTerjual').selectpicker('refresh');
    //     $('#listingWaktuAktif').selectpicker('refresh');
    // }

    $(".money").inputmask('Rp. 999.999.999.999,99', {
        numericInput: true
    });

    $('#priceSlider').ionRangeSlider({
        type: "double",
        grid: true,
        min: 0,
        max: 100000000000,
        from: 0,
        to: 100000000000,
        step : 10000000,
        grid_num: 4,
        grid_snap:true,
        prefix: "Rp. ",
        onStart: function (data) {
            // fired then range slider is ready
            // console.log("onStartPriceSlider : " + data.from + "-" + data.to);
            $(".priceInput.min").val(data.from);
            $(".priceInput.max").val(data.to);
            $(".priceInput").change(function(){
                priceSliderInstance.update({
                    from: $(".priceInput.min").val(),
                    to: $(".priceInput.max").val()
                });
            });
        },
        onChange: function (data) {
            // fired on every range slider update
            //console.log("onChange : " + data.from + "-" + data.to);
            $(".priceInput.min").val(data.from);
            $(".priceInput.max").val(data.to);
        },
        onFinish: function (data) {
            // fired on pointer release
            //console.log("onFinish : " + data.from + "-" + data.to);
            updateOptionBadge();
        },
        onUpdate: function (data) {
            // fired on changing slider with Update method
            //console.log("onUpdate : " + data.from + "-" + data.to);
        }
    });
    priceSliderInstance = $('#priceSlider').data("ionRangeSlider");

    $(".m_selectpicker").on("changed.bs.select", 
        function(e, clickedIndex, newValue, oldValue) {
        // console.log(this.value, clickedIndex, newValue, oldValue)
        updateOptionBadge();
    });

    // $('.m_selectpicker').selectpicker();
    updateOptionBadge();

    if (userID != null){
        loadNotifWishlist();
    }
});

function shortNumber(x){
    x = x/100000;
    if (x < 10000){
        return Math.floor(x)/10  + " jt";
    }else if (x < 10000000){
        return (Math.floor(x/1000) / 10) + " M";
    }else{
        return (Math.floor(x/1000000) / 10) + " T";
    }
}

function updateOptionBadge(){
    var active = 0;
    // Check Option Activate 
    $('.option-search-container').each(function (index, element) {
        if($(element).find(".option-search.active").length>0){
            active++;
        }
    });
    if ($(".priceInput.max").val() < 100000000000 || $(".priceInput.min").val() > 50000000) {
        active++;
    }
    $('.selectrange ').each(function (index, element) {
        //console.log($(this).find(".m_selectpicker").eq(0).val())
        //console.log($(this).find(".m_selectpicker[value=-1]").length);
        if (2 - ($(this).find(".m_selectpicker").filter(function(){return this.value=='-1'}).length)>0) {
            active++;                
        }        
    });

    $('.selectedOptionBadge').html(active);
    if($('.selectedOptionBadge').html()=="0"){
        $('.selectedOptionBadge').slideUp();
    } else {
        $('.selectedOptionBadge').slideDown();
    }
    hitungMatch();
}

$('#m_datetimepicker_2, #m_datetimepicker_1_validate, #m_datetimepicker_2_validate, #m_datetimepicker_3_validate').datetimepicker({
    autoclose: true,
    pickerPosition: 'bottom-left',
    startView: 1,
    minView: 0,
    maxView: 1,
    format: "hh:ii",
    startDate : new Date()
});

$(".option-search").click(function(){
    if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        $(this).find(".hidden-param").val("");
    } else {
        $(this).addClass("active");
        $(this).find(".hidden-param").val($(this).find(".mini-text").html());
    }
    updateOptionBadge();
});

var priceSliderInstance = "";

$(".activeable").click(function(){
    $(this).toggleClass("active");
});

$(".activeable-solo").click(function(){
    
        $(".activeable-solo[active-group='"+ $(this).attr("active-group") +"']").removeClass("active");
        $(this).toggleClass("active");    
    
});

// $("#header-select2").change(function(){
//     alert('a')
// });



// From Git
// function formatRepo (repo) {
//     if (repo.loading) {
//       return repo.text;
//     }
//     var markup = "<div class='select2-result-repository clearfix'>";
//     //   "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
//     markup += "<div class='select2-result-repository__meta'>" +
//         "<div class='select2-result-repository__title'>" + repo.nama + "</div>";
    
//     // if (repo.description) {
//     //   markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
//     // }
  
//     // markup += "<div class='select2-result-repository__statistics'>" +
//     //   "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.nama + " Forks</div>" +
//     //   "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.nama + " Stars</div>" +
//     //   "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.nama + " Watchers</div>" +
//     // "</div>" +
//     markup += "</div></div>";

//     return markup;
//   }
  

// function formatRepoSelection(repo) {
//     return repo.nama;
// }

// $("#header-select2").select2({
//     placeholder: "Search for git repositories",
//     allowClear : true,
//     ajax       : {
//         // url: "https://api.github.com/search/repositories",
//         url     : API_URL + "/api/get/listings/searchRekomendasi?token=public",
//         dataType: 'json',
//         delay   : 250,
//         data    : function(params) {
//             return {
//                 q   : params.term,   // search term
//                 page: params.page
//             };
//         },
//         processResults: function(data, params) {
//             // parse the results into the format expected by Select2
//             // since we are using custom formatting functions we do not need to
//             // alter the remote JSON data, except to indicate that infinite
//             // scrolling can be used
//                 params.page = params.page || 1;
//             $(data.items).each(function(e){
//                 this.id = data.items[e].Ket + "-" + data.items[e].nama;
//             });
//             return {
//                 results   : data.items,
//                 pagination: {
//                     more: (params.page * 30) < data.total_count
//                 }
//             };
//         },
//         cache: true
//     },
//     escapeMarkup: function(markup) {
//         return markup;
//     }, // let our custom formatter work
//     minimumInputLength: 1,
//     templateResult    : formatRepo,          // omitted for brevity, see the source of this page
//     templateSelection : formatRepoSelection  // omitted for brevity, see the source of this page
// });

function days_passed(dt) {
    var current = new Date();
    var previous = new Date(dt.getTime());
  
    var numbOfDays = (Math.ceil((current - previous + 1) / 86400000))-1;
    if (numbOfDays <= 30){
        if (numbOfDays == 0){
            return "Today";
        }else if (numbOfDays == 1){
            return "Yesterday";
        }else{
            return numbOfDays + " days ago";
        }
    }else{
        var numbOfMonths = Math.ceil(numbOfDays / 30)-1;
        if (numbOfMonths <= 12){
            if (numbOfMonths == 1){
                return "Last month";
            }else{
                return numbOfMonths + " months ago";
            }
        }else{
            var numbOfYears = Math.ceil(numbOfMonths / 12)-1;
            if (numbOfYears == 1){
                return "Last year";
            }else{
                return numbOfYears + " years ago";
            }
        }
    }
}

function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

$(document).on("mouseup", ".heart-animated", function() {
    $(this).toggleClass("heart-blast");
});

function diffDay(dateEnd) {
    let dateFuture = new Date(dateEnd);
    let dateNow = new Date();

    let delta = Math.abs(dateFuture - dateNow) / 1000;
    let days = (Math.floor(delta / 86400)) | 0;

    delta -= days * 86400;
    let hours = (Math.floor(delta / 3600) % 24) | 0;
    delta -= hours * 3600;
    let minutes = (Math.floor(delta / 60) % 60) | 0;
    delta -= minutes * 60;
    let seconds = (delta % 60) | 0;

    if(days > 6) {
        let timeNow = dateNow.getTime();
        let timeFuture = dateFuture.getTime();
        let weeks = parseInt((timeNow-timeFuture)/(24*3600*1000*7));
 
        if(weeks > 4) {
            let date1Year = dateFuture.getFullYear();
            let date2Year = dateNow.getFullYear();
            let date1Month = dateFuture.getMonth();
            let date2Month = dateNow.getMonth();
            let months = (date2Month+(12*date2Year))-(date1Month+(12*date1Year));

            if(months > 11) {
                return (dateNow.getFullYear()-dateFuture.getFullYear()) + " Tahun Yang Lalu";
            } else {
                return months + " Bulan Yang Lalu";
            }
        } else {
            return weeks + " Minggu Yang Lalu";
        }
    } else {
        if(days > 0) {
            return days + " Hari Yang Lalu";
        } else if(hours > 0) {
            return hours + " Jam Yang Lalu";
        } else if(minutes > 0) {
            return minutes + " Menit Yang Lalu";
        } else if(seconds > 0) {
            return seconds + " Detik Yang Lalu";
        }
    }
}

function hadapListing(hadap){
    var ret = "N/A";
    switch (hadap){
        case "0":
            ret = "Utara";
            break;
        case "1":
            ret = "Timur Laut";
            break;
        case "2":
            ret = "Timur";
            break;
        case "3":
            ret = "Tenggara";
            break;
        case "4":
            ret = "Selatan";
            break;
        case "5":
            ret = "Barat Daya";
            break;
        case "6":
            ret = "Barat";
            break;
        case "7":
            ret = "Barat Laut";
            break;
    }
    return ret;
}

var waitAjaxSearchDelay = 250;
var ajaxSearchPromise;
function changeDropSearch(){
    clearTimeout(ajaxSearchPromise)
    ajaxSearchPromise = setTimeout(() => {
        ajaxSearch();
    }, waitAjaxSearchDelay);
}

function ajaxSearch(){
    var q = $("#headSearch").val();
    $.ajax({
		"url" : API_URL + "/api/get/listings/searchRekomendasi?token=public&q=" + q,
        success : function(data){
		  data = JSON.parse(data);
          console.log(data);
          var lastSeparator = "";
          var row = "";
          data.items.forEach(e => {
                if (lastSeparator != e.Ket){
                    row += `<div class="dropdown-separator">`+e.Ket+`</div>`;
                    lastSeparator = e.Ket;
                }
                row += `<div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" onclick="insertMe(this);">`;
                row += `    <div style="width:75%">`;
                row += `        <div class="data-show truncate">`+e.nama+`</div>`;
                // row += `        <div class="data-hidden label">Surabaya, Jawa Timur</div>`;
                row += `    </div>`;
                row += `    <div class="data-hidden badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">`+e.Ket+`</div>`;
                row += `</div>`;
          });
          $(".dropdown-container").html(row);
        }
	});
}

function insertMe(ob){
    $("#headSearch").val($(ob).find(".data-show").text());
    $("#headSearch").attr("data-hidden", $(ob).find(".data-hidden").text());
}