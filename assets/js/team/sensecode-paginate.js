
class Paginate{
    constructor(container, maxIndex = 9, currentIndex = 1, callbackfunction, basicCss = true,){
        // Init Basic Variable
        this.baseUrl = "apiBaseUrl";
        this.currentIndex = currentIndex;
        this.maxIndex = maxIndex;
        this.containerId = container;
        
        // Initialize variable
        this.domContainer = "";
        this.domItem = "";
        this.dom = "";
        this.containerClass = [];
        this.containerCss = [];
        this.itemClass = [];
        this.itemCss = [];
        this.activeItemCss = [];
        this.activeItemClass = [];
        this.paginateFunction = callbackfunction;

        // Using simple Basic
        if (basicCss) {
            this.containerClass.push("sensePaginateContainer");
            this.itemClass.push("sensePaginateItem");
            this.activeItemClass.push("sensePaginateActive");
        }
    }

    viewExample(changeIndex = 0){
        this.dom = "";
        // Open Container
        this.dom += "<div class='"+ this.getClass(this.containerClass) + "' style='"+ this.getCss(this.containerCss) +"'>";
        if (changeIndex>0) {
            this.currentIndex = changeIndex;            
        }
        var startIndex = this.currentIndex-2;
        var lastIndex = this.currentIndex + 2;
        var offsetLast=0;
        var offsetStart=0;
        if (startIndex<1) {
            startIndex = 1;
        }
        if (lastIndex>this.maxIndex) {
            lastIndex = this.maxIndex;
        }

        if (this.currentIndex - startIndex  < 2) {
            offsetLast = 2 - (this.currentIndex - startIndex);
        }
        
        if (lastIndex - this.currentIndex < 2) {
            offsetStart = 2 - (lastIndex - this.currentIndex);
        }


        if (this.currentIndex>1) {
            this.dom += "<div data-index='1' class='"+ this.getClass(this.itemClass) +"' style='"+ this.getCss(this.itemCss) +"'>"+ "<<" +"</div>";                
            this.dom += "<div data-index='"+ ((this.currentIndex-1 > 0) ? this.currentIndex-1 : 1) +"' class='"+ this.getClass(this.itemClass) +"' style='"+ this.getCss(this.itemCss) +"'>"+ "&lt;" +"</div>";        
        } else {
            this.dom += "<div data-index='1' class='"+ this.getClass(this.itemClass) +" disabled' style='"+ this.getCss(this.itemCss) +"'>"+ "<<" +"</div>";
            this.dom += "<div data-index='"+ ((this.currentIndex-1 > 0) ? this.currentIndex-1 : 1) +"' class='"+ this.getClass(this.itemClass) +" disabled' style='"+ this.getCss(this.itemCss) +"'>"+ "&lt;" +"</div>";
        }
        for (let index = (startIndex - offsetStart < 1 ) ? 1 : startIndex-offsetStart; index <= ((lastIndex + offsetLast > this.maxIndex) ? this.maxIndex : lastIndex+offsetLast); index++) {


            if (index == this.currentIndex) {
                this.dom += "<div data-index='"+index+"' class='"+ this.getClass(this.activeItemClass) + this.getClass(this.itemClass) +"' style='"+ this.getCss(this.itemCss) +"'><span>"+ index +"<span></span></div>";
            } else {
                this.dom += "<div data-index='"+index+"' class='"+ this.getClass(this.itemClass) +"' style='"+ this.getCss(this.itemCss) +"'>"+ index +"</div>";
            }


            
        }
        if (this.currentIndex < this.maxIndex) {
            this.dom += "<div data-index='"+((this.currentIndex+1 > this.maxIndex) ? this.maxIndex : this.currentIndex+1) +"' class='"+ this.getClass(this.itemClass) +"' style='"+ this.getCss(this.itemCss) +"'>"+ ">" +"</div>";
            this.dom += "<div data-index='"+this.maxIndex+"' class='"+ this.getClass(this.itemClass) +"' style='"+ this.getCss(this.itemCss) +"'>"+ ">>" +"</div>";
        } else{
            this.dom += "<div data-index='"+((this.currentIndex+1 > this.maxIndex) ? this.maxIndex : this.currentIndex+1) +" disabled' class='"+ this.getClass(this.itemClass) +" disabled' style='"+ this.getCss(this.itemCss) +"'>"+ ">" +"</div>";
            this.dom += "<div data-index='"+this.maxIndex+"' class='"+ this.getClass(this.itemClass) +" disabled' style='"+ this.getCss(this.itemCss) +"'>"+ ">>" +"</div>";
        }            
        // Close Container
        this.dom += "</div>";
        // Append this dom
        
        $(this.containerId).html(this.dom);
        this.addEvent(this.paginateFunction);
    }

    addEvent(callbackFunction){
        var containerId = this.containerId;
        var currentIndex = 0;
        var me = this;
        $(this.containerId + " .sensePaginateItem").click(function (e) { 
            e.preventDefault();
            if(!$(this).hasClass("sensePaginateActive")){
                currentIndex = parseInt( $(this).attr("data-index"));
                me.viewExample(currentIndex);
                callbackFunction();
            }
        });        
    }

    appendPaginateTo(targetAppend){
        $(targetAppend).append(this.dom);
    }
    prependPaginateTo(targetAppend){
        $(targetAppend).prepend(this.dom);
    }
    getPaginateString(){
        return this.dom;
    }

    setCurrentPage(targetPage) {
        this.viewExample(targetPage);
        this.paginateFunction();
    }

    addContainerClass(newClass, important = false){
        if (important) {
            this.containerClass.unshift(newClass);
        } else {
            this.containerClass.push(newClass);
        }
    }
    
    addActiveItemClass(newClass, important = false){
        if (important) {
            this.activeItemClass.unshift(newClass);
        } else {
            this.activeItemClass.push(newClass);
        }
    }
    addActiveItemCss(newCss){
        this.activeItemCss.push(newCss);
    }
    addContainerCustomCss(newCss){
        this.containerCss.push(newCss);        
    }

    /**
     * 
     * @param {array} newClass 
     * @param {boolean} important 
     */
    addItemClass(newClass, important = false){
        if (important) {
            this.itemClass.unshift(newClass);
        } else {
            this.itemClass.push(newClass);
        }
    }

    /** Function to add Css
     * 
     * @param {Array} newCss it must be an array, and you don't need to specify ";"
     */
    addItemCustomCss(newCss){
        this.itemCss.concat(newCss);
    }
    
    getClass(variable){
        var myCss = "";
        variable.forEach(element => {
            myCss += element + " ";
        });
        return myCss;
    }

    getCss(variable){
        var myCss = "";
        variable.forEach(element => {
            myCss += element + ";";
        });
        return myCss;
    }
    getCurrentPage(){
        return this.currentIndex;
    }

}