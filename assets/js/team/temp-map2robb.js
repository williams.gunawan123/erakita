var listMarker = []
var listMarkerClusterer = new MarkerClusterer();
var map;
var markerSpiderfier;
var center = new google.maps.LatLng(-7.250445, 112.768845);
var bounds_changed = false;
var activeMarker = -1;
var lastColorMarker = "";

function initMap() {
    map = new google.maps.Map(document.getElementById('mapDiv'), {
        zoom: 13,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        fullscreenControl: false,
        mapTypeControl: false,
        streetViewControl: false
    });
    markerSpiderfier = new OverlappingMarkerSpiderfier(map, spiderConfig);
    google.maps.event.addListener(map, 'idle', function() {
        if (bounds_changed == true){
            bounds_changed = false;
            console.log("c!");
            var north = map.getBounds().getNorthEast().lat();
            var east = map.getBounds().getNorthEast().lng();
            var south = map.getBounds().getSouthWest().lat();
            var west = map.getBounds().getSouthWest().lng();
            boundData = "&north=" + north + "&east=" + east + "&south=" + south + "&west=" + west;
            loadList();
        }else{
            bounds_changed = true;
        }
    });
    google.maps.event.addListener(map, 'click', function() {
        if (activeMarker != -1){
            pinIcon.fillColor = color[data.data[activeMarker].jenis_transaksi];
            listMarker[activeMarker].setIcon(pinIcon);
        }
        activeMarker = -1;
    });
}

function clearMarker(){
    for (var i=0; i<listMarker.length; i++){
        listMarker[i].setMap(null);
        listMarker[i].setPosition(null);
        listMarker[i] = null;
    }
    listMarker = [];
    if (listMarkerClusterer != null){
        listMarkerClusterer.setMap(null);
        listMarkerClusterer = null;
    }
    markerSpiderfier = null;
}
var spiderConfig = {
	keepSpiderfied: true,
	event: 'mouseover',
	circleSpiralSwitchover:0
};
function initMarker(data){
    clearMarker();
    var infowindow = new google.maps.InfoWindow();
    markerSpiderfier = new OverlappingMarkerSpiderfier(map, spiderConfig);
    var color = ["#d32f2f", "#ff9800", "#ff5722"];
    var lat = []; var lng = [];
    data.data.forEach(e => {
      var latLng = new google.maps.LatLng(e.lat_listing, e.long_listing);
      lat.push(parseFloat(e.lat_listing)); lng.push(parseFloat(e.long_listing));
      var pinIcon = {
        path: "M 0,0 550,0 550,225 320,225 275,275 230,225 0,225 z",
        fillColor: color[e.jenis_transaksi],
        fillOpacity: 1,
        scale: .1,
        strokeColor: "white",
        strokeWeight: 1,
        labelOrigin : {x:275,y:112},
      };
      var pinLabel = {
        color: "white",
        fontSize: "11pt",
        fontWeight:"bold",
        text: shortNumber(e.harga_jual)
      };
      var marker = new google.maps.Marker({
          position: latLng,
          icon: pinIcon,
          label: pinLabel
      });
      markerSpiderfier.addMarker(marker);  // Adds the Marker to OverlappingMarkerSpiderfier

      google.maps.event.addListener(marker, 'spider_format', function(status) {
        var currColor = color[e.jenis_transaksi];
        var currLabel = shortNumber(e.harga_jual);
        if (status == OverlappingMarkerSpiderfier.markerStatus.SPIDERFIABLE){
            currColor = "grey";
            currLabel = "...";
        }
        pinIcon.fillColor = currColor;
        pinLabel.text = currLabel;
        marker.setIcon(pinIcon);
        marker.setLabel(pinLabel);
      });

      google.maps.event.addListener(marker, 'spider_click', function() {
        if (activeMarker != -1){
            var resetIcon = {
                path: "M 0,0 550,0 550,225 320,225 275,275 230,225 0,225 z",
                fillColor: lastColorMarker,
                fillOpacity: 1,
                scale: .1,
                strokeColor: "white",
                strokeWeight: 1,
                labelOrigin : {x:275,y:112},
              };
            console.log(activeMarker)
            var tempMarker = listMarker[activeMarker];
            tempMarker.setIcon(resetIcon);
        }
        lastColorMarker = pinIcon.fillColor;
        pinIcon.fillColor = "green";
        marker.setIcon(pinIcon);
        activeMarker = listMarker.length;
      });
      listMarker.push(marker);
    });

    if (data.data.length > 0){
        var minLat = Math.min(...lat);
        var maxLat = Math.max(...lat);
        var minLng = Math.min(...lng);
        var maxLng = Math.max(...lng);
        var north = map.getBounds().getNorthEast().lat();
        var east = map.getBounds().getNorthEast().lng();
        var south = map.getBounds().getSouthWest().lat();
        var west = map.getBounds().getSouthWest().lng();
        // var avgLat = lat.reduce(function(a,b){return a+b;}) / lat.length;
        // var avgLng = lng.reduce(function(a,b){return a+b;}) / lng.length;
    
        // if (Math.abs(avgLat - minLat) > 0.15 || Math.abs(avgLat - maxLat) > 0.15 
        //         || Math.abs(avgLng - minLng) > 0.15 || Math.abs(avgLng - maxLng) > 0.15){
        //     center = new google.maps.LatLng(avgLat, avgLng);
        //     map.setCenter(center);
        //     map.setZoom(13);
        //     console.log('a')
        // }else{
        if (forceFitBounds){
            forceFitBounds = false;
            bounds_changed = false;
            var southWest = new google.maps.LatLng(minLat,minLng);
            var northEast = new google.maps.LatLng(maxLat,maxLng);
            var bounds = new google.maps.LatLngBounds(southWest,northEast);
            map.fitBounds(bounds, 0);
        }

            // console.log('b')
        // }
        listMarkerClusterer = new MarkerClusterer(map, listMarker, {
            averageCenter: true
        });
        listMarkerClusterer.setMaxZoom(15);
    }
}