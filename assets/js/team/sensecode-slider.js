$(document).ready(function () {
    initSlider();
});


function initSlider(){
    $(".sensecode-slider.initiate").each(function (index, element) {
        var index = parseInt($(element).attr("data-index"));
        var listImage = JSON.parse($(element).attr("data-files"));
        $(element).css("background-image","url("+listImage[index]+")");
        $(element).toggleClass("initiate");
        // Tambalan sementara untuk erakita
        $(element).find(".listing-slider-counter").html((index+1) + " of " + (listImage.length));
    });       
    $('.sensecode-slider').css({
        "transition"          : "all .2s ease-in-out",
        "position"            : "relative",
        "background-size"     : "cover",
        "background-position" : "center",
        "background-repeat"   : "no-repeat",
        "padding"             : "0px"
    });
    $('.sensecode-slider').append(`<div class="flex flex-hor flex-separate flex-1 arrow-container" style='transition: all .2s ease-in-out'>
        <div onclick="next(this,-1);" class="controls unselectable" style="font-size:1em; display:block; margin:auto 0px; max-width:10%; display:none;  transition:all .2s ease-in-out; cursor:pointer; z-index:2;">
            <a style="color:white;" class="material-icons unselectable" style="cursor:pointer;">
                keyboard_arrow_left
            </a>
        </div>
        <div onclick="next(this,1);" class="controls unselectable" style="font-size:1em; display:block; margin:auto 0px; max-width:10%; display:none;  transition:all .2s ease-in-out; cursor:pointer; z-index:2">
            <a  style="color:white;" class="material-icons unselectable"  style="cursor:pointer;">
                keyboard_arrow_right
            </a>
        </div>
    </div>`);
    $('.sensecode-slider').mouseenter(function(){showControl(this)});
    $('.sensecode-slider').mouseleave(function(){hideControl(this)});
}

function showControl(ob){
    try {
        $(ob).find(".arrow-container").addClass("active");
        var listImage = JSON.parse($(ob).attr("data-files"));
        if (listImage.length>1) {
            $(ob).find(".controls").fadeIn(600);
        }   
    } catch (error) {}
}

function hideControl(ob){
    $(ob).find(".arrow-container").removeClass("active");
    $(ob).find(".controls").fadeOut(600);
}


function next(ob, to){
    var index = parseInt($(ob).parent().parent().attr("data-index"));
    var listImage = JSON.parse($(ob).parent().parent().attr("data-files"));
    if (listImage.length>1) {
        index+= to;
        index = index % listImage.length;
        if (index<0) {
            index=listImage.length-1;
        }
        $(ob).parent().parent().css("background-image","url("+listImage[index]+")");
    }
    $(ob).parent().parent().attr("data-index", index);
    // Tambalan sementara untuk erakita
    $(ob).parent().parent().find(".listing-slider-counter").html((index+1) + " of " + (listImage.length));
}

var loadFile = function(ob, event) {
    var image = document.getElementById('output');
    var output = "#" + $(ob).data("target");
    var listImage = [];
    image.style.backgroundImage = "url("+URL.createObjectURL(event.target.files[0])+")";
    for (var i = 0; i < event.target.files.length; i++){
        listImage[listImage.length] = URL.createObjectURL(event.target.files[i]);
    }
    $(output).attr("data-files", JSON.stringify(listImage));
    $(output).attr("data-index", 0);
};