class notif{
    constructor(modalTitle, modalBody, modalFooter){
        this.dom = "";
        this.dom += '<!-- Modal -->';
        this.dom += '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">';
        this.dom += '<div class="modal-dialog" role="document">';
        this.dom += '    <div class="modal-content">';
        this.dom += '    <div class="modal-header">';
        this.dom += '        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        this.dom += '        <h4 class="modal-title" style="position:absolute;" id="myModalLabel">'+modalTitle+'</h4>';
        this.dom += '    </div>';
        this.dom += '    <div class="modal-body" style="min-height:10vh;" id="myModalBody">';
        this.dom += modalBody;
        this.dom += '    </div>';
        this.dom += '    <div class="modal-footer" id="myModalFooter">';
        this.dom += '        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        this.dom += modalFooter;
        this.dom += '    </div>';
        this.dom += '    </div>';
        this.dom += '</div>';
        this.dom += '</div>';
    }

    appendModalTo(targetAppend){
        $(targetAppend).append(this.dom);
    }
    prependModalTo(targetAppend){
        $(targetAppend).prepend(this.dom);
    }
    getModalString(){
        return this.dom;
    }

    appendButtonTrigger(buttonDom,targetAppend){    
        buttonDom = buttonDom.replace("@modal",'data-toggle="modal" data-target="#myModal"');
        $(targetAppend).append(buttonDom);
    }
    prependButtonTrigger(buttonDom,targetAppend){    
        buttonDom = buttonDom.replace("@modal",'data-toggle="modal" data-target="#myModal"');
        $(targetAppend).prepend(buttonDom);
    }
    getButtonTriggerString(buttonDom){
        return buttonDom.replace("@modal",'data-toggle="modal" data-target="#myModal"');
    }

    static changeStructure(modalTitle, modalBody, modalFooter) {
        $("#myModalLabel").html(modalTitle);
        $("#myModalBody").html(modalBody);
        var footer = '        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        footer += modalFooter;
        $("#myModalFooter").html(footer);
    }

    static changeTitle(myDom) {
        $("#myModalLabel").html(myDom);
    }
    static changeBody(myDom) {
        $("#myModalBody").html(myDom);
    }
    static changeFoot(myDom) {
        var footer = '        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        footer += myDom;
        $("#myModalFooter").html(footer);
    }
    

}
