
class SensecodeMap{
    /** Start Using New Map
     * 
     */
    constructor(){
        // Init Basic Variable
        // Init First Center of Map
        this.centerLatlng = L.latLng(-7.29049, 112.72);

        this.clusterGroups = [];
        this.markersLists = [];
        // Set Map Tile (Background), Max Zoom, and Copyright
        // this.tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        //     maxZoom: 40,
        //     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        // });
        // Wikimedia
        this.tiles = L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png', {
	        attribution: '<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>',
	        minZoom: 1,
             maxZoom: 19
        });

        // Init Array of Buttons to be placed
        this.buttons = [];

        // Init Area Param for search
        // this.areaParam; 
        this.resetAreaParam();
    }

    initDrawable(){
        this.drawable = true;
        this.drawnStatus = false;
        // Initialise the FeatureGroup to store editable layers
        this.editableLayers = new L.FeatureGroup();
        this.map.addLayer(this.editableLayers);
        this.drawPluginOptions = {
            position: 'topright',
            draw: {
                polygon: {
                    allowIntersection: false, // Restricts shapes to simple polygons
                    drawError: {
                        color: '#e1e100', // Color the shape will turn when intersects
                        message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
                    },
                    shapeOptions: {
                        color: '#97009c'
                    }
                },
                // disable toolbar item by setting it to false
                polyline: false,
                circle: false, // Turns off this drawing tool
                rectangle: false,
                marker: false,
            },
            edit: {
                featureGroup: this.editableLayers, //REQUIRED!!
                remove: false
            }
        };
        this.drawControl = new L.Control.Draw(this.drawPluginOptions);
        this.map.addControl(this.drawControl);

        this.drawnLayers = [];
        let editableLayers = this.editableLayers;
        let drawnLayers = this.drawnLayers;
        this.map.on('draw:created', function(e) {
            var type = e.layerType,
            layer = e.layer;
            if (type === 'marker') {
                layer.bindPopup('A popup!');
            }
            drawnLayers.push(e.layer);

            editableLayers.addLayer(layer);
        });        
    }



    /**
     * Function to add New Button to map
     * @param {object} args you can place many options here
     * possible args
     *      position : string // One of these bottomright, topright, bottomleft, topleft
     *      innerHTML : string // text to be written in button
     *      onClick : function // function to be executed when button is clicked
     *      customClass : string // class for container button
     */
    addNewButtons(args){
        let className = 'sensecode-map-button leaflet-bar leaflet-control leaflet-control-custom leaflet-draw-toolbar leaflet-bar leaflet-draw-toolbar-top' + (('customClass' in args) ? " " + args.customClass : '');
        var newButton = L.Control.extend({        
            options: {
                position: ('position' in args) ? args.position : 'bottomright' // Default value is bottom right
            },        
            onAdd: function (map) {
                var container = L.DomUtil.create('div', className);
                container.style = ("customStyle" in args) ? args.customStyle : "";
                container.innerHTML = ('innerHTML' in args) ? args.innerHTML : "New Button"
                container.onclick = ('onClick' in args) ? args.onClick : function(){
                    console.log(innerHTML + " button is clicked");
                    console.log("Please set your event onClick in args param");
                }
                return container;
            }
        });
        //this.buttons.push(newButton);
        this.map.addControl(new newButton());
    }
    
        
    clearDrawnLayer(){
        if (this.drawable && this.drawnLayers.length > 0) {
            for (let index = this.drawnLayers.length-1; index >= 0; index--) {
                this.editableLayers.removeLayer(this.drawnLayers[index]);			
            }
        }
    }

    /**
     * function to "Add" more on click option, by default it will set area param and you can only "add" more, not erase this function
     * @param {Function} clickFunction Define your click function(e) you want to Add, if you dont need a variable, just give it null value
     * @param {Boolean} defaultOff Turn this true if you dont want to record area param
     */
    
    addOnClickMap(clickFunction = null , defaultOff = false){
        // Get Area
        let map = this.map;
        if (defaultOff) {
            if (typeof clickFunction != null) { 
                map.on('click', clickFunction(e))
            }
        } else {
            let ob = this;
            map.on('click', function(e) {
                //console.log("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng)
                if (e.latlng.lng>ob.areaParam[1]) {ob.areaParam[1] = e.latlng.lng;}
                if (e.latlng.lng<ob.areaParam[3]) {ob.areaParam[3] = e.latlng.lng;}
                if (e.latlng.lat<ob.areaParam[2]) {ob.areaParam[2] = e.latlng.lat;}
                if (e.latlng.lat>ob.areaParam[0]) {ob.areaParam[0] = e.latlng.lat;}
                if (typeof clickFunction != null) { clickFunction(); }
            });
        }
    }

     
    /**
     * function to "Add" function on hover cluster
     * @param {ClusterGroup} clusterGroup Cluster Group You can get from this instance.clusterGroups
     * @param {Function} eventHandler add eventHandler(c) for c is cluster instance
     */
    addOnHoverCluster(clusterGroup, eventHandler){
        clusterGroup.on('clustermouseover', function(c){
            eventHandler(c);
        })
        // Example
        //  function (c) {
        //     return console.log(c);
        //     console.log('cluster ' + a.layer.getAllChildMarkers().length);
        // });
    }
    
    /**
     * function to "Add" function on mouseout cluster
     * @param {ClusterGroup} clusterGroup Cluster Group You can get from this instance.clusterGroups
     * @param {Function} eventHandler add eventHandler(c) for c is cluster instance
     */
    addOnMouseOutCluster(clusterGroup, eventHandler){
        clusterGroup.on('clustermouseout', function(c){
            eventHandler(c);
        });
        // Example
        //  function(c){
        //     return console.log(c);
        //     if (childData.separate) {
        //         map.closePopup();
        //     }
        // })
    }

    /**
     * function to "Add" function on mouseout cluster
     * @param {ClusterGroup} clusterGroup Cluster Group You can get from this instance.clusterGroups
     * @param {Function} eventHandler add eventHandler(c) for c is cluster instance
     */
    addOnMouseClickCluster(clusterGroup, eventHandler){
        let map = this.map;
        clusterGroup.on('clusterclick',function(c){
            map.closePopup();
            if (!childData.separate) {
                let offsetY = -18;
                let domPopup = `<div class='flex flex-ver' style='width:320px;'>
                                    <div class="flex flex-ver" style='border-bottom: 2px solid var(--gray); margin-bottom: 10px;'>
                                        <div class='label' style='font-size:1.25em;'>`+ c.layer._childCount +` for sale</div>`
                                        // <a href='javascript:void(0)' class='my-2' style='font-size:1.25em;'><i class="fa fa-lock"></i> Sign in for more details</a>
                                +`</div>
                                    <div class='map-listing-item-container minimalistScrollbar' style='max-height:188px; overflow-y:auto;'>`;
                console.log(childData);
                childData.listings.forEach(listing => {			
                    domPopup +=			`<div class='flex flex-hor flex-vertical-center flex-separate py-2'>
                                            <div class='squared div-image ratio69' image='default' style='width:37%;'></div>
                                            <div class="flex flex-ver" style='width:58%;'>
                                                <div>Unit di`+ convertTransactionType(listing.jenis_transaksi) +`kan</div>
                                                <div class="hoverable truncate-line-2" onclick="clickUnit(this);" data-idx="`+listing.indexMarker+`" idx-showed="`+listing.idxShowed+`"><b>`+ listing.judul_listing +`</b></div>
                                                <div>Rp. `+  shortNumber(listing.harga_jual) +`</div>
                                                <div>`+listing.kamar_tidur+` bed.`+ listing.kamar_mandi +` bath. <i class="fa fa-lock"></i> sq.ft.</div>
                                            </div>
                                        </div>`
                });
                domPopup += 		`</div>
                                </div>`;
                new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: true, position : "s" })
                .setContent(domPopup)
                .setLatLng(c.latlng)
                .openOn(map);
                // c.originalEvent.preventDefault();
                // map.fitBounds(map.getBounds());
            }
        });
    }

    initMap(mapContainer){
        this.map = L.map(mapContainer, {center: this.centerLatlng, zoom: 15, layers: [this.tiles], drawControl: true});
        //this.turnOnClusterGroup();
    }

    // turnOnClusterGroup(){
    //     this.clusterGroups.forEach(clusterGroup => {
    //         this.map.addLayer(clusterGroup);
    //     });
    // }

    resetAreaParam(){
        this.areaParam = [-90, -180, 90, 180]; // Atas, Kanan, Bawah, Kiri, The value is inverted for searching maximum border
    }

    /**
     * Function to add New ClusterGrouping to map
     * @param {object} args you can place many options here
     * possible args
     *      spiderfyOnMaxZoom : bool // Let spiderfy do its job when map reach the max zoom level
     *      animateAddingMarkers : bool // Animation on adding new marker to cluster group
     *      chunkedLoading : bool // Chunked loading for better performance
     */    
    addClusterGroup(args = {}){
        let newMarkerList = [];
        let newClusterGroup = L.markerClusterGroup({
            animateAddingMarkers : ("animateAddingMarkers" in args) ? args.animateAddingMarkers : false,
            spiderfyOnMaxZoom: ("spiderfyOnMaxZoom" in args) ? args.spiderfyOnMaxZoom : false,
            // disableClusteringAtZoom: 2,
            chunkedLoading: ("chunkedLoading" in args) ? args.chunkedLoading : true,
            maxClusterRadius: function (zoom) {
                return (zoom <= 14) ? 80 : 1; // radius in pixels
            },
            iconCreateFunction: function(cluster) {
                
                    return ("iconCreateFunction" in args) ? args.iconCreateFunction(cluster) : 
                    L.divIcon({
                        html: "<div class='cluster-inner'><div>" + cluster.getChildCount() + "</div></div>",
                        className: 'cluster-container',
                        iconSize: L.point(40, 40)
                    });                
            }
        });
        this.clusterGroups.push(newClusterGroup);
        this.markersLists.push(newMarkerList);
        this.map.addLayer(newClusterGroup);
        return {clusterGroup : newClusterGroup, markerList : newMarkerList};
    }

    /**
     * function to make icon using div structure
     * @param {object} args
     * 
     * ### Possible Args
     * 
     * ------------
     * 
     *      html : string string for dom icon in map 
     *      className : string // add class to cluster container
     *      iconSize : {x : xValue, y : yValue} // icon size in cartesius coordinates
     * 
     */
    createIcon(args, noDefault){
        
        return (noDefault) ? L.divIcon(args) : L.divIcon({
            html: ("html" in args) ? args.html : "<div class='cluster-inner'><div>Testing</div></div>",
            className: 'cluster-container' + ("className" in args) ? " " + args.className : "",
            iconSize: L.point(("iconSize" in args) ? args.iconSize.x : 40, ("iconSize" in args) ? args.iconSize.y : 40)
        });
    }

    /**
     * Check markers if all markers in the same place (same long lat)
     * usually used to check marker inside clusterer
     * @param {array<marker>} childMarkers 
     */
    checkMarkerSeparate(childMarkers){
        let diffPosition = false; let lastPos = null;
        for (let index = 0; index < childMarkers.length; index++) {
            let el = childMarkers[index];
            if (lastPos != null && lastPos.lat != el._latlng.lat && lastPos.lng != el._latlng.lng) {
                return true;
            }
            lastPos = el._latlng;
        }        
        return diffPosition;
    }

    

    /**
     * 
     * @param {Array} datas Array of data you want to populate in map, must have lat and lng properties in each data
     * @param {*} args 
     */
    async addAllMarkers(datas, args = {}){
        if (!("markerList" in args)) { args.markersList = this.markersLists[0];}
        if (!("groupTargetLayer" in args)) { args.groupTargetLayer = this.clusterGroups[0];}
        
        args.markersList.forEach(element => {
            args.groupTargetLayer.removeLayer(element);
        });
        //let chunkSize = (getUrlVars()["chunkSize"] !== undefined) ? getUrlVars()["chunkSize"] : 10;
        let chunkSize = ("chunkSize" in args) ? args.chunkSize : 10;
        let chunkCount = datas.length / chunkSize;
        let promises = [];
        let indexes = [];
        for (let index = 0; index < chunkCount; index++) {
            let chunkData = datas.splice(0, chunkSize);
            promises.push(
                function(ob){
                    ob.addMarkers(chunkData, args);
                    // console.log(index);
                }
            )
            indexes.push({index: index});
        }
        let map = this.map;
        Promise.all(indexes.map(index=>promises[index.index](this))).then(function(){
            map.fitBounds(args.groupTargetLayer.getBounds());
            if ("callbackOnFinish" in args) {
                args.callbackOnFinish();
            }
        });
    }
    
    addMarkers(datas, args = {}){
        // console.log("promise executed");
        datas.forEach(data => {
            if ("additionalData" in data) {
                args.additionalData = data.additionalData;
            }
            if ("icon" in data) {
                args.icon = data.icon;
            }
            this.addMarker(data.lat, data.lng, args);
        });
    }

    
    addMarker(lat, lng, args = {}) {
        if (!("markerList" in args)) { args.markersList = this.markersLists[0];}
        if (!("groupTargetLayer" in args)) { args.groupTargetLayer = this.clusterGroups[0];}
        var m;
        if ("icon" in args) {
            m = L.marker(new L.latLng(lat, lng), {icon : args.icon});
        } else {
            m = L.marker(new L.latLng(lat, lng));
        }

        if ("additionalData" in args) {
            for(let property in args.additionalData) {
                m[property] = args.additionalData[property];
            }
        }

        if ("popupHtml" in args) {
            m.bindPopup(args.popupHtml).openPopup();
        } else {
            if ( !("defaultPopup" in args) || ( "defaultPopup" in args && args.defaultPopup != false)) {
                m.bindPopup("<b>lat :"+ args.lat_listing +"; lng : "+ args.long_listing+"</b><br>I am a popup.").openPopup(); 
            }
        }
        args.markersList.push(m);
        args.groupTargetLayer.addLayer(m);
    }
    

}