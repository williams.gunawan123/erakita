/**
 * Google Map Markers Init
 */
var map;
var allMarkers = [];
var markerCluster;
var mapMode = 0;
var mapPolygon = [];
var polygonMarker = [];
var areaParam = [-90, -180, 90, 180]; // Atas, Kanan, Bawah, Kiri, The value is inverted for searching maximum border

var mapPolygonArea;

function initMap(markers, centerMap) {
    var center = new google.maps.LatLng(centerMap[0], centerMap[1]);

    map = new google.maps.Map(document.getElementById('mapListing'), {
        zoom: 13,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        fullscreenControl: false,
        mapTypeControl: false,
        streetViewControl: false
    });

    initMarker(markers);
    initButton();
    initDrawer();
    // var customControlDiv = document.createElement("div");
    // var customControl = new generateCustom(customControlDiv, "Layers");
    // customControlDiv.index = 1;
    // map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(customControlDiv);

}


function initButton(){

    var saveSearchDiv = document.createElement("div");
    var saveSearchInner = new generateSearch(saveSearchDiv, map);
    saveSearchDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(saveSearchDiv);
    
    var customControlRemoveOutlineDiv = document.createElement("div");
    var customControlRemoveOutline = new generateCustom(customControlRemoveOutlineDiv, "Remove Outline");
    customControlRemoveOutlineDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(customControlRemoveOutlineDiv);
    
    customControlRemoveOutlineDiv.addEventListener('click', function() {
        clearDrawer();
    });

    var customControlDrawDiv = document.createElement("div");
    customControlDrawDiv.id = 'drawsearch';
    var customControlDraw = new generateCustom(customControlDrawDiv, "Draw");
    
    customControlDrawDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(customControlDrawDiv);
    customControlDrawDiv.addEventListener('click', function() {
        mapMode = 1 - mapMode;
        if (mapMode==1) {
            $("#drawsearch>div>div").html("Search");    
            
        } else {
            // areaParam  this is your parameter for ajax, in array clockwise from top
            // console.log("Atas : " + areaParam[0]);
            // console.log("Kanan : " + areaParam[1]);
            // console.log("Bawah : " + areaParam[2]);
            // console.log("Kiri : " + areaParam[3]);
            var sendData = {
                atas: areaParam[0],
                kanan: areaParam[1],
                bawah: areaParam[2],
                kiri: areaParam[3]
            }
            
            $.ajax({
                "url" : API_URL + "/api/get/listings/searchArea?token=public",
                "method" : "GET",
                "data" : sendData,
                success : function(data){
                    data = JSON.parse(data);
                    console.log(data);
                    var temp = [];
                    data.forEach(element => {
                        if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(element.lat_listing, element.long_listing), mapPolygonArea) ){
                            temp.push(element);
                        }    
                    });
                    // Total ini data yang sudah menghilangkan data2 yg tidak diperlukan
                    console.log(temp);    
                    data['data'] = temp;                
                    $("#drawsearch>div>div").html("Draw");     
                    clearMarker();
                    showListing(data, 1); 
                }
            });
        }
    });
}

function initDrawer(){
    mapPolygon = new google.maps.Polyline({
        strokeColor: '#00DB00',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        fillColor: 'green',
        fillOpacity: 0.05
      });
    mapPolygon.setMap(map);
    map.addListener('click', addLatLng);
}

function clearDrawer() {
    areaParam = [-90, -180, 90, 180];
    var path = mapPolygon.getPath();
    path.clear();
    for (let index = 0; index < polygonMarker.length; index++) {
        polygonMarker[index].setMap(null);
    }
    polygonMarker.length = 0;
}

function addLatLng(event) {//debugger
    if (mapMode==1) {
        var path = mapPolygon.getPath();
        if(path.length==2){
            var polygonOptions={path:path,strokeColor:"#00DB00",fillColor:"green"};
            mapPolygonArea=new google.maps.Polygon(polygonOptions);
            mapPolygonArea.setMap(map);
        }
        //console.log("lat : " + event.latLng.lat());
        //console.log("lng : " + event.latLng.lng());
        if (event.latLng.lng()>areaParam[1]) {
            areaParam[1] = event.latLng.lng();
        }
        if (event.latLng.lng()<areaParam[3]) {
            areaParam[3] = event.latLng.lng();
        }
        if (event.latLng.lat()<areaParam[2]) {
            areaParam[2] = event.latLng.lat();
        }
        if (event.latLng.lat()>areaParam[0]) {
            areaParam[0] = event.latLng.lat();
        }

        path.push(event.latLng);
        var pinIcon = new google.maps.MarkerImage(
            "/assets/img/marker/circle.png",
            null, /* size is determined at runtime */
            null, /* origin is 0,0 */
            new google.maps.Point(12.5, 12.5), /* anchor is bottom center of the scaled image */
            new google.maps.Size(25, 25));  /* Size given in CSS Length units */
        
        polygonMarker.push(new google.maps.Marker({
            position: event.latLng,
            title: '#' + path.getLength(),
            map: map,
            icon: pinIcon
        }));
    }

    
}

function clearMarker(){
    for (var i=0; i<allMarkers.length; i++){
        allMarkers[i].setMap(null);
        allMarkers[i].setPosition(null);
        allMarkers[i] = null;
    }
    allMarkers = [];
}

function initMarker(markers){
    allMarkers = allMarkers.concat(markers); 

    
    if (markerCluster != null){
        markerCluster.setMap(null);
    }
    markerCluster = new MarkerClusterer(map, allMarkers, {
        averageCenter: true
    });
    minClusterZoom = 15;
    markerCluster.setMaxZoom(minClusterZoom);

    google.maps.event.addListener(markerCluster, "click", function (c) {
        // console.log("click: ");
        // console.log("&mdash;Center of cluster: " + c.getCenter());
        // console.log("&mdash;Number of managed markers in cluster: " + c.getSize());
        var m = c.getMarkers();
        var p = [];
        for (var i = 0; i < m.length; i++ ){
            p.push(m[i].getPosition());
        }
        // console.log("&mdash;Locations of managed markers: " + p.join(", "));
    });
    google.maps.event.addListener(markerCluster, "mouseover", function (c) {
        // console.log("mouseover: ");
        // console.log("&mdash;Center of cluster: " + c.getCenter());
        // console.log("&mdash;Number of managed markers in cluster: " + c.getSize());
    });
    google.maps.event.addListener(markerCluster, "mouseout", function (c) {
        // console.log("mouseout: ");
        // console.log("&mdash;Center of cluster: " + c.getCenter());
        // console.log("&mdash;Number of managed markers in cluster: " + c.getSize());
    });
}

function generateSearch(controlDiv, map){
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#d32f2f';
    controlUI.style.borderRadius = "5px";
    controlUI.style.cursor = 'pointer';
    controlUI.style.margin = "10px";
    controlUI.style.textAlign = 'center';
    controlDiv.append(controlUI);

    var controlText = document.createElement('div');
    controlText.style.color = 'white';
    controlText.style.fontWeight = "bold";
    controlText.style.fontSize = '16px';
    controlText.style.padding = "10px";
    controlText.innerHTML = 'Save Search';
    controlUI.appendChild(controlText);

    // controlUI.addEventListener('click', function() {
    //     map.setCenter(chicago);
    // });
}

function generateCustom(controlDiv, text){
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = 'white';
    controlUI.style.borderRadius = "5px";
    controlUI.style.cursor = 'pointer';
    controlUI.style.margin = "10px";
    controlUI.style.marginBottom = "20px";
    controlUI.style.textAlign = 'center';
    controlDiv.append(controlUI);

    var controlText = document.createElement('div');
    controlText.style.fontWeight = "bold";
    controlText.style.fontSize = '16px';
    controlText.style.padding = "10px";
    controlText.innerHTML = text;
    controlUI.appendChild(controlText);
}