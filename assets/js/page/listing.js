var limit = 10;
var page = 1;
var activeListing = -1;
var selectedCenterMap = -1;
var buildingsMarkersArray = [];
var drawData = "";


// when you are done creating markers then
var clusterOptions = {
     imagePath: '/assets/img/marker/m'
};

var pagination = null;
var forceFitBounds = false;

$(document).ready(function () {
	var url = window.location.href.split('?');
	if (url.length > 1){
		var param = url[1];
		var splitSendAndSort = param.split("&sort=");
		sendData = splitSendAndSort[0];
		var splitSendAndDraw = sendData.split("&draw=");
		if (splitSendAndDraw.length > 1){
			sendData = splitSendAndDraw[0];
			drawData = "&draw = " + splitSendAndDraw[1];
		}
		if (splitSendAndSort.length > 1){
			var splitSortAndBound = splitSendAndSort[1].split("&north=");
			sortData = splitSortAndBound[0]!="" ? "&sort=" + splitSortAndBound[0] : "";
			boundData = splitSortAndBound[1]!=null ? "&north=" + splitSortAndBound[1] : "";
		}
	}
	loadSenseMapMarker();
	if (drawData != ""){

	}
});

function updateTableScroll(){
	if (listingActive != -1){
		var tempCardActive = $(".listing-item[data-idx='"+listingActive+"']");
		$('#tabs-foto>.content').animate({
			scrollTop: $(tempCardActive).offset().top +  $('#tabs-foto>.content').scrollTop() - $('#tabs-foto>.content').offset().top
		}, 500);
	}
}

var pageHandler = function pageHandling(){
	page = pagination.getCurrentPage();
	updateShowedListing();
}

function updateShowedListing(){
	var dataShowed = senseMap.markersLists[1].slice((page-1)*limit, (page-1)*limit+limit);
	var row = "";
	var row2 = "";
	dataShowed.forEach(el => {
		let e = el.listing;
		let gambar = [];
		for (var i=1; i<=e.gambar; i++){
			gambar.push("/assets/img/listing/"+e.id_listing+"/"+i+".jpg");
		}
		gambar = JSON.stringify(gambar);

		let jenis = "";
		let bgJenis = "";
		let harga = "";
		let icon = e.tipe_listing == "1" ? "home" : (e.tipe_listing == "2" ? "building-o" : (e.tipe_listing == "3" ? "archive" : (e.tipe_listing == "4" ? "industry" : (e.tipe_listing == "5" ? "road" : "industry"))));

		if (e.jenis_transaksi == "0"){
			jenis = "Jual";
			bgJenis = "jual";
			harga = shortNumber(e.harga_jual);
		}else{
			jenis = "Sewa";
			bgJenis = "sewa";
			harga = shortNumber(e.harga_sewa) + "/thn";
		}

		let favorite = e.favorite == 1 ? "favorite" : "";

		row += `	<div class="col-lg-6 col-sm-12 mb-3 px-2">`;
		row += `		<div onclick='selectListingCard(this);' class=" mb-0 listing-item m-portlet m-portlet--rounded" data-idx='`+el.indexMarker+`' data-id='`+e.id_listing+`' data-telp='`+e.telp_broker+`' data-judul='`+e.judul_listing+`' data-lat='`+e.lat_listing+`' data-lng='`+e.long_listing+`' data-tipe='`+e.nama_tipe+`' data-jenis='`+jenis+`'>`;
		row += `			<div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='`+gambar+`'>`;
		row += `				<div class="listing-slider-badge jenis-`+bgJenis+`">` + jenis + `</div>`;
		row += `				<div class="listing-slider-icon jenis-`+bgJenis+`"><i class="fas fa-`+icon+`"></i></div>`;
		row += `				<div class="listing-slider-counter" style="z-index:2;">1 of 3</div>`;
		row += `			</div>`;
		row += `			<div class="m-portlet__body" style="padding:0px;">`;
		row += `				<div class="col-lg-10 col-md-10 col-sm-12" style="padding: 10px 5px 10px 10px;">`;
		row += `					<div class="listing-price listing-info">` + harga + `</div>`;
		row += `					<div class="listing-title listing-info truncate hoverable" onclick="viewDetail(this);"><b>` + e.judul_listing + `</b></div>`;
		row += `					<div class="listing-jalan listing-info truncate">` + e.alamat_listing + `</div>`;
		row += `					<div class="listing-kota listing-info truncate">` + e.nama_kecamatan + `, ` + e.nama_kota + `</div>`;
		row += `					<div class="listing-detail">`;
		if (e.kamar_tidur != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>`+e.kamar_tidur+`</div>`;
		}
		if (e.kamar_mandi != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>`+e.kamar_mandi+`</div>`;
		}
		if (e.luas_tanah != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>` + e.luas_tanah + `m<sup>2</sup></div>`;
		}
		if (e.luas_bangunan != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>` + e.luas_bangunan + `m<sup>2</sup></div>`;
		}
		row += `					</div>`;
		row += `				</div>`;
		row += `				<div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding:10px 10px 10px 5px">`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='sendWA(this);' class="fab fa-whatsapp fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Pesan WhatsApp"></i></div>`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='favorite(this);' class="fas fa-heart fa-lg ` + favorite + `" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Favorit"></i></div>`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='shareLink(this);' class="fas fa-share fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Bagikan"></i></div>`;
		row += `					<a href="/detaillisting/view/`+e.id_listing+`" style="margin:auto;" class="listing-content-icon txt-black"><i class="fas fa-ellipsis-h fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Tampilkan Lebih Banyak"></i></a>`;
		row += `				</div>`;
		row += `			</div>`;
		row += `		</div>`;
		row += `	</div>`;

		row2 += `<tr class="hoverable listing-item" onclick="selectListingCard(this);" data-idx="`+el.indexMarker+`" data-id="`+e.id_listing+`">`;
		row2 += `<td> <i class="fas fa-`+icon+` fa-xs txt-white p-2" style='background-color:var(--bg-`+jenis.toLowerCase()+`)'></i></td>`;
		row2 += `<td> <div class='truncate-line-3'>`+e.alamat_listing+`</div></td>`;
		row2 += `<td> <div class='truncate-line-3'>`+e.nama_kota+`</div></td>`;
		row2 += `<td> <div class='truncate-line-3'>`+shortNumber(e.harga_jual)+`</div></td>`;
		row2 += `<td> <div class='truncate-line-3'>`+e.kamar_tidur+`</div></td>`;
		row2 += `<td> <div class='truncate-line-3'>`+e.kamar_mandi+`</div></td>`;
		row2 += `<td nowrap> <div class='truncate-line-3'>`+e.luas_bangunan+` m<sup>2</sup></div></td>`;
		row2 += `<td nowrap> <div class='truncate-line-3'>`+e.luas_tanah+` m<sup>2</sup></div></td>`;
		row2 += `<td nowrap> <div class='truncate-line-3'>`+shortNumber(e.harga_jual/e.luas_tanah)+`</div></td>`;
		row2 += `</tr>`;
	});
	$("#tabs-foto>.content").html(row);
	$("#table-data").html(row2);
	initSlider();
	mApp.initPopovers();
}

var listingActiveID = null;
function viewDetail(element){
	var idlisting = $(element).parent().parent().parent().attr('data-id');
	if (idlisting == listingActiveID){
		$("#my-chevron").click();    
	}else{
		listingActiveID = idlisting;
			mApp.block("#detailListing", {});
            $.post({
				"url" : API_URL + "/api/get/listings/getById/"+idlisting+"?token=public",
				success : function(data){
					data = JSON.parse(data);
					if(data.jenis_transaksi == 0){
						var jenis = "Jual";
						var harga = "Rp " + numberWithCommas(data.harga_jual);
					}else if(data.jenis_transaksi == 1){
						var jenis = "Sewa";
						var harga = "Rp " + numberWithCommas(data.harga_sewa) + "/thn";
					}else{
						var jenis = "Jual - Sewa";
						var harga = "Rp " + numberWithCommas(data.harga_jual) + " - Rp " + numberWithCommas(data.harga_sewa) + "/thn";
					}
					var gambar= "";
					var thumb = "";
					var dom = `<div class="single-list-slider owl-carousel" id="sl-slider">`;
					for (var i=1; i<=data.gambar; i++){
						gambar += `<div class="sl-item set-bg" data-setbg="/assets/img/listing/`+data.id_listing+`/`+i+`.jpg">`;
						gambar += `	<div class="sale-notic">FOR SALE</div>`;
						gambar += `</div>`;
						thumb += `<div class="sl-thumb set-bg" data-setbg="/assets/img/listing/`+data.id_listing+`/`+i+`.jpg"></div>`;
					}
					dom += gambar + `</div>`;
					dom += `<div class="owl-carousel sl-thumb-slider" id="sl-slider-thumb">`;
					dom += thumb + `</div>`;

					$("#my-slider").html(dom);
					initSlSlider();
					$("#detailJudul").html(data.judul_listing);
					$("#detailAlamat").html('<i class="fa fa-map-marker"></i> ' + data.alamat_listing);
					$("#detailHarga").html(harga);
					$("#detailLuas").html('<i class="fa fa-th-large"></i> ' + data.luas_tanah + " m<sup>2</sup>")
					$("#detailKTidur").html('<i class="fa fa-bed"></i> ' + data.kamar_tidur + " Kamar Tidur");
					$("#detailBroker").html('<i class="fa fa-user"></i> ' + data.nama_broker);
					$("#detailGarasi").html('<i class="fa fa-car"></i> ' + data.jml_garasi + " Garasi");
					$("#detailLantai").html('<i class="fa fa-building-o"></i> ' +data.jml_lantai +' Tingkat');
					$("#detailKMandi").html('<i class="fa fa-bath"></i> ' + data.kamar_mandi + " Kamar Mandi");
					$("#detailUmur").html('<i class="fa fa-trophy"></i> Tahun ' + data.tahun_bangunan);
					$(".description").html('<p>'+data.deskripsi_listing+'</p>');
					if (data.url_video != null){
						var video = '<iframe width="100%" height="500px" src="'+data.url_video + '"></iframe>';
					}else{
						var video = "No Video Preview";
					}
					$(".perview-video").html(video);
					mApp.unblock("#detailListing", {});
				}
			});          
		if ($("#my-chevron").attr("data-show")=="hide") {
			$("#my-chevron").click();
		} 
	}
}

function favorite(element){
	if (userID == null){
		let action = "like";
		let idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		let url = window.location.href;
		Swal.fire({
			type: 'error',
			title: 'Oops...',
			text: 'Harap Login Terlebih Dahulu',
			footer: '<a href="/login?last_url='+url+'&last_action='+action+'#'+idlisting+'">LOGIN DISINI</a>'
		});
	}else{
		var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		var status = !$(element).hasClass('favorite');
		$(element).toggleClass("favorite");
		if (status){
			var url = API_URL + "/api/get/customers/logFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
			if (typeof cookieBroker !== 'undefined'){
				url = API_URL + "/api/get/customers/logFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
			}
			$.post({
				"url" : url
			});
		}
		$.post({
			"url" : API_URL + "/api/get/customers/updateFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&status=" + Number(status),
			success : function(data){
				data = JSON.parse(data);
				if (!data.status){
					$(element).toggleClass("favorite");
				}
			}
		});
	}
}

function sendWA(element){
	var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
	var judul = $(element).parent().parent().parent().parent().attr('data-judul');
	var telp = $(element).parent().parent().parent().parent().attr('data-telp');
	var pesan = "Halo,%20Saya%20mau%20menanyakan%20mengenai%20listingan%20" + judul;
	var win = window.open("https://api.whatsapp.com/send?phone="+telp+"&text="+pesan, '_blank');
	if (userID == null){
		
	} else {
		var url = API_URL + "/api/get/customers/logSendWA?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
		if (typeof cookieBroker !== 'undefined'){
			url = API_URL + "/api/get/customers/logSendWA?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
		}
		$.post({
			"url" : url
		});
	}
	win.focus();
}

function shareLink(element) {
	/* Get the text field */
	var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
	var text = BASE_URL + "/detaillisting/view/" + idlisting;

	var body = document.getElementsByTagName('body')[0];
	var tempInput = document.createElement('INPUT');
	body.appendChild(tempInput);
	tempInput.setAttribute('value', text)
	tempInput.select();
	document.execCommand('copy');
	body.removeChild(tempInput);

	/* Alert the copied text */
	Swal.fire({
		type: 'success',
		title: 'Sukses',
		text: 'Berhasil disalin ke Clipboard'
	});
	if (userID != null){
		var url = API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
		if (cookieBroker != null){
			url = API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
		}
		$.post({
			"url" : url
		});
	}
}

$(".chevron").click(function (e) {
	if ($(this).attr("data-show") == "hide") {
		$(this).attr("data-show", "show");
		$(this).removeClass("chevron-flip");
		$("#detailListing").css("transform", "translateX(0)");
	} else {
		$(this).attr("data-show", "hide");
		$(this).addClass("chevron-flip");
		$("#detailListing").css("transform", "translateX(-100%)");
	}
});

function selectListingCard(ob, unit=false){
	$(".listing-item.active").removeClass("active");
	let idx = $(ob).attr("data-idx");
	$(".listing-item[data-idx='"+idx+"']").addClass("active");
	if (!unit){
		showSeparateMarker(idx);
		changeTabTableDetail(idx);
	}
}

function changeTabTableDetail(idx){
	var idlisting = senseMap.markersLists[0][idx].listing.id_listing;
	mApp.block(".tableDetail", {});
	$.post({
		"url" : API_URL + "/api/get/listings/getById/"+idlisting+"?token=public",
		success : function(data){
			e = JSON.parse(data);
			$("#tablePrice").html(shortNumber(e.harga_jual));
			$("#tableAlamat").html(e.alamat_listing);
			$("#tableKota").html(e.nama_kecamatan + ", " + e.nama_kota);
			$("#tableBed").html(`<i class="fas fa-bed"></i><br>` + e.kamar_tidur);
			$("#tableBath").html(`<i class="fas fa-bath"></i><br>` + e.kamar_mandi);
			$("#tableLB").html(`<i class="fas fa-square"></i><br>` + e.luas_bangunan + `m<sup>2</sup>`);
			$("#tableLT").html(`<i class="fas fa-building"></i><br>` + e.luas_tanah + `m<sup>2</sup>`);
			$("#tablePanjang").html(e.panjang_tanah + " m");
			$("#tableLebar").html(e.lebar_tanah + " m");
			$("#tableSertifikat").html(e.sertifikat_listing);
			$("#tableHadap").html(hadapListing(e.hadap_listing));
			$("#tableDeveloper").html(e.nama_developer);
			$("#tableProyek").html(e.nama_proyek);
			$("#tableBroker").html(e.nama_broker);
			$("#tableCabang").html(e.nama_cabang);
			$("#tablePhotoBroker").css("background-image", "url('/assets/img/profile-broker/" + e.foto_broker + "')");
			mApp.unblock(".tableDetail", {});
		}
	});

}

/** Using Sensecode-Map.js */
var mapMode = 0;
var senseMap = new SensecodeMap();

senseMap.initMap("mapDiv");
senseMap.addOnClickMap(function(){
	// this is a function i think
	// console.log("mapCLicked");
});


/**
 * SenseMap Ke - 0 == All Marker
 */
senseMap.addClusterGroup({
	"iconCreateFunction" : function(cluster){
		var childMarkers = cluster.getAllChildMarkers();
		if (senseMap.checkMarkerSeparate(childMarkers)) {
			return L.divIcon({
				html: "<div class='cluster-inner'><div>" + cluster.getChildCount() + "</div></div>",
				className: 'cluster-container',
				iconSize: L.point(40, 40)
			});
		} else {
			let domIcon =  `<div class='erakita-marker-label'>
								<b>`+ cluster.getChildCount() +` Unit</b>
							</div>
						`;
			return L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing-cluster', html: domIcon });
		}
	}
});
/**
 * SenseMap Ke - 1 == Showed Marker
 */
senseMap.addClusterGroup();
var privateMarkerContainer = senseMap.addClusterGroup();

senseMap.addOnHoverCluster(senseMap.clusterGroups[0], clusterOnHover);
senseMap.addOnMouseOutCluster(senseMap.clusterGroups[0], clusterOnMouseOut);
senseMap.addOnMouseClickCluster(senseMap.clusterGroups[0], clusterOnMouseClick);
senseMap.initDrawable();
senseMap.addNewButtons({
	"customClass" : "hoverable",
	"customStyle" : "cursor : pointer;",
	"innerHTML" : "Reset",
	"onClick" : function(){
		senseMap.resetAreaParam();
		senseMap.clearDrawnLayer();
		loadSenseMapMarker();
		senseMap.drawnStatus = false;
	}
});


senseMap.addNewButtons({
	"innerHTML" : `<div><div class='hoverable' onclick="$(this).parent().find('.my-popover').fadeToggle();">Add to Wishlist</div><div class='my-popover flex flex-hor flex-equal' style='display:none;'><div class='btn btn-secondary'>Save</div><input type='text'></div></div>`,
	"position" : "topright",
	"onClick" : function(){
		//$(this).find(".my-popover").fadeToggle();
	}
});


/** Customized function for listing Page */
var clusterGroupIndividualMarkers = senseMap.addClusterGroup();
var lastSeparateMarket = undefined;
function showSeparateMarker(markerIndex){
	if (listingActive != -1){
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
	}
	if (lastSeparateMarket !== undefined || markerIndex == null) {
		//publicMarkerContainer.clusterGroup.addLayer(senseMap.markersLists[0][lastSeparateMarket]);
		if (clusterGroupIndividualMarkers.markerList[0] != null){
			privateMarkerContainer.clusterGroup.removeLayer(clusterGroupIndividualMarkers.markerList[0]);
		}	
		clusterGroupIndividualMarkers.markerList = [];
	} 
	if (markerIndex != null) {
		let marker = senseMap.markersLists[0][markerIndex];
		clusterGroupIndividualMarkers.markerList.push(L.marker(marker._latlng, {icon : marker.options.icon}))
		privateMarkerContainer.clusterGroup.addLayer(clusterGroupIndividualMarkers.markerList[0]);
		// publicMarkerContainer.clusterGroup.removeLayer(senseMap.markersLists[0][markerIndex]);
		lastSeparateMarket = markerIndex;
		listingActive = markerIndex;
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().addClass("activeMarker");
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().css("z-index", 9999);
	}
}

function clusterIconCreate(cluster){
	var childMarkers = cluster.getAllChildMarkers();
	var n = 0;
	if (senseMap.checkMarkerSeparate(childMarkers)) {
		return L.divIcon({
			html: "<div class='cluster-inner'><div>" + cluster.getChildCount() + "</div></div>",
			className: 'cluster-container',
			iconSize: L.point(40, 40)
		});
	} else {
		let domIcon =  `<div class='erakita-marker-label'>
							<b>`+ cluster.getChildCount() +` Unit</b>
						</div>
					`;
		return L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing-cluster', html: domIcon });
	}
}

function clickUnit(e){
	if (listingActive != -1){
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
	}
	listingActive = $(e).attr("data-idx");
	let idxShowed = $(e).attr("idx-showed");;
	pagination.setCurrentPage(Math.floor((idxShowed)/10)+1);
	var tempCardActive = $(".listing-item[data-idx='"+listingActive+"']");
	selectListingCard(tempCardActive, true);
	listingActive = -1;
	$('#tabs-foto>.content').animate({
		scrollTop: $(tempCardActive).offset().top +  $('#tabs-foto>.content').scrollTop() - $('#tabs-foto>.content').offset().top
	}, 500);
}

function clusterOnHover(cluster){
	childData = (getChildData(cluster.layer.getAllChildMarkers()));
	let offsetY = -10;
	if (childData.separate) {
		senseMap.clusterGroups[0].options.zoomToBoundsOnClick = true;
		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: false, position : "s" })
		.setContent("Melihat " + cluster.layer._childCount +' Listing <br> Listing dari harga ' + shortNumber(childData.minPrice) + " - " + shortNumber(childData.maxPrice) + " dengan rata-rata harga " + shortNumber(childData.avgPrice))
		.setLatLng(cluster.latlng)
		.openOn(senseMap.map);
	} else {
		senseMap.clusterGroups[0].options.zoomToBoundsOnClick = false;
	}
}

function clusterOnMouseOut(cluster){
	if (childData.separate) {
		senseMap.map.closePopup();
	}
}
function clusterOnMouseClick(cluster){
	senseMap.map.closePopup();
	if (!childData.separate) {
		offsetY = -18;
		let domPopup = `<div class='flex flex-ver' style='width:320px;'>
							<div class="flex flex-ver" style='border-bottom: 2px solid var(--gray); margin-bottom: 10px;'>
								<div class='label' style='font-size:1.25em;'>`+ c.layer._childCount +` for sale</div>`;
								// <a href='javascript:void(0)' class='my-2' style='font-size:1.25em;'><i class="fa fa-lock"></i> Sign in for more details</a>
		domPopup += `	</div>
							<div class='map-listing-item-container minimalistScrollbar' style='max-height:188px; overflow-y:auto;'>`;

		childData.listings.forEach(listing => {	
			domPopup +=			`<div class='flex flex-hor flex-vertical-center flex-separate py-2'>
									<div class='squared div-image ratio69' image='default' style='width:37%;'></div>
									<div class="flex flex-ver" style='width:58%;'>
										<div>Unit di`+ convertTransactionType(listing.jenis_transaksi) +`kan</div>
										<div><b>`+ listing.judul_listing +`</b></div>
										<div>Rp. `+  shortNumber(listing.harga_jual) +`</div>
										<div>`+listing.kamar_tidur+` bed.`+ listing.kamar_mandi +` bath. <i class="fa fa-lock"></i> sq.ft.</div>
									</div>
								</div>`;
		});
		domPopup += 		`</div>
						</div>`;
		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: true, position : "s" })
		.setContent(domPopup)
		.setLatLng(cluster.latlng)
		.openOn(senseMap.map);
		// c.originalEvent.preventDefault();
		// map.fitBounds(map.getBounds());
	}
}


function getChildData(childs){
	let childData = {maxPrice: 0, minPrice : null, listings : []};
	let sumPrice = 0;
	childs.forEach(el => {
		el.listing.indexMarker = el.indexMarker;
		el.listing.idxShowed = el.idxShowed;
		childData.listings.push(el.listing);
		if (childData.minPrice == null || el.listing.harga_jual < childData.minPrice ) {
			childData.minPrice = el.listing.harga_jual;
		}
		if (el.listing.harga_jual > childData.maxPrice ) {
			childData.maxPrice = el.listing.harga_jual;
		}
		sumPrice += parseFloat(el.listing.harga_jual);
	});
	console.log(sumPrice, childs.length);
	childData.avgPrice = sumPrice / childs.length;
	childData.separate = senseMap.checkMarkerSeparate(childs);
	return childData;
}

var requestPage = null;
var listingActive = -1;

var waitZoom = 500;
var zoomPromise;
function updateZoomView(){
	if (senseMap.drawnStatus == false){
		showSeparateMarker(null);
		page = 1;
		senseMap.markersLists[1] = [];
		var idxShowed = 0;
		senseMap.markersLists[0].forEach(e => {
			if(senseMap.map.getBounds().contains(e.getLatLng())){
				senseMap.markersLists[1].push(e);
				e.idxShowed = idxShowed++;
			}
		});
		pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
		pagination.addActiveItemClass(["bg-red txt-white"]);
		pagination.addItemClass(["clickable"]);
		pagination.viewExample(0);
		$("#showedListing").html(senseMap.markersLists[1].length);
		updateShowedListing();
	}
}

senseMap.map.on('draw:created', function(e) {
	let polygon = L.polygon(senseMap.drawnLayers[senseMap.drawnLayers.length-1].getLatLngs());
	showSeparateMarker(null);
	page = 1;
	senseMap.markersLists[1] = [];
	var idxShowed = 0;
	senseMap.markersLists[0].forEach(e => {
		if(polygon.contains(e.getLatLng())) {
			e.idxShowed = idxShowed++;
			senseMap.markersLists[1].push(e);
		}else{
			senseMap.clusterGroups[0].removeLayer(e);
		}
	});
	senseMap.drawnStatus = true;
	senseMap.map.fitBounds(polygon.getBounds());

	// drawData = "&draw=";
	// polygon.getLatLngs()[0].forEach(e =>{
	// 	drawData += e.lat + "," + e.lng + ";"; 
	// });
	// var newUrl = "/listing?"+sendData+drawData+sortData+boundData;
	// window.history.pushState(data, "EraKita | Listing", newUrl);

	pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
	pagination.addActiveItemClass(["bg-red txt-white"]);
	pagination.addItemClass(["clickable"]);
	pagination.viewExample(0);
	$("#totalListing").html(senseMap.markersLists[1].length);
	$("#showedListing").html(senseMap.markersLists[1].length);
	updateShowedListing();
});

senseMap.map.on('zoomend', function(e) {
	updateZoomView();
	clearTimeout(zoomPromise);
	zoomPromise = setTimeout(updateZoomView(), waitZoom);
});

senseMap.map.on('dragend', function(e) {
	updateZoomView();
	clearTimeout(zoomPromise);
	zoomPromise = setTimeout(updateZoomView(), waitZoom);
});

function addEventClickMarker(){
	senseMap.markersLists[0].forEach(element =>{
		$(element).click(function (e){
			if (listingActive != -1){
				$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
			}
			listingActive = $(this)[0].indexMarker;
			let idxShowed = listingActive;
			if ($(this)[0].idxShowed != null){idxShowed=$(this)[0].idxShowed}
			pagination.setCurrentPage(Math.floor((idxShowed)/10)+1);
			var tempCardActive = $(".listing-item[data-idx='"+listingActive+"']");
			selectListingCard(tempCardActive);
			$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().addClass("activeMarker");
			$('#tabs-foto>.content').animate({
				scrollTop: $(tempCardActive).offset().top +  $('#tabs-foto>.content').scrollTop() - $('#tabs-foto>.content').offset().top
			}, 500);
		})
	});
}

function loadSenseMapMarker(){
	page = 1;
	senseMap.markersLists[0].forEach(element => {
		senseMap.clusterGroups[0].removeLayer(element);
	});
	senseMap.markersLists[0] = []; 
	senseMap.markersLists[1] = []; 
	showSeparateMarker(null);
	mApp.block("#mapDiv", {});
	
	if(userID == null) {
		$.ajax({
			"url" : API_URL + "/api/get/listings/searchLatLng?token=public" + "&save=" + saveSearch + sendData + sortData + boundData,
			success : function(data){
				data = JSON.parse(data);
	
				var newUrl = "/listing?"+sendData+drawData+sortData+boundData;
				window.history.pushState(data, "EraKita | Listing", newUrl);
				$("#totalListing").html(data.data.length);
				$("#showedListing").html(data.data.length);
	
				let preparedData = prepareDataforMarker(data.data);
				senseMap.addAllMarkers(preparedData, {defaultPopup:false});
				senseMap.markersLists[1] = senseMap.markersLists[0];
				addEventClickMarker();
				pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
				pagination.addActiveItemClass(["bg-red txt-white"]);
				pagination.addItemClass(["clickable"]);
				pagination.viewExample(0);
				pagination.setCurrentPage(1);
				mApp.unblock("#mapDiv", {});
				updateShowedListing();
			}
		});
	} else {
		$.ajax({
			"url" : API_URL + "/api/get/listings/searchLatLng?token=public&user_cust=" + userID + "&save=" + saveSearch + sendData + sortData + boundData,
			success : function(data){
				data = JSON.parse(data);
	
				var newUrl = "/listing?"+sendData+drawData+sortData+boundData;
				window.history.pushState(data, "EraKita | Listing", newUrl);
				$("#totalListing").html(data.data.length);
				$("#showedListing").html(data.data.length);
	
				let preparedData = prepareDataforMarker(data.data);
				senseMap.addAllMarkers(preparedData, {defaultPopup:false});
				senseMap.markersLists[1] = senseMap.markersLists[0];
				addEventClickMarker();
				pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
				pagination.addActiveItemClass(["bg-red txt-white"]);
				pagination.addItemClass(["clickable"]);
				pagination.viewExample(0);
				pagination.setCurrentPage(1);
				mApp.unblock("#mapDiv", {});
				updateShowedListing();
			}
		});
	}
}

function prepareDataforMarker(data){
	let targetData = ("data" in data) ? data.data : data;
	var ctrCard = 0;
	targetData.forEach(element => {
		let domIcon =  `<div class='erakita-marker-label' indexMarker='`+ctrCard+`'>`+shortNumber(element.harga_jual)+`</div>`
		let temp = JSON.stringify({listing : element, idxShowed : ctrCard, indexMarker : ctrCard++});
		element.additionalData = JSON.parse(temp);
		element.lat = element.lat_listing;
		element.lng = element.long_listing;
		element.icon = senseMap.createIcon({
			html: domIcon,
			className : "bg-red txt-white erakita-marker-container leaflet-div-icon tipe-listing"+ element.jenis_transaksi}, true)
	});
	return targetData;
}

function loadListingbySearch(){
	var sendData = {
		atas: senseMap.areaParam[0],
		kanan: senseMap.areaParam[1],
		bawah: senseMap.areaParam[2],
		kiri: senseMap.areaParam[3]
	}
	$.ajax({
		"url" : API_URL + "/api/get/listings/searchArea?token=public",
		"method" : "GET",
		"data" : sendData,
		success : function(data){
			data = JSON.parse(data);
			let preparedData = prepareDataforMarker(data);
			senseMap.addAllMarkers(preparedData, {defaultPopup:false});
			// data.forEach(element => {
			// 	if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(element.lat_listing, element.long_listing), mapPolygonArea) ){
			// 		temp.push(element);
			// 	}    
			// });
			// Total ini data yang sudah menghilangkan data2 yg tidak diperlukan
			// console.log(temp);    
			// data['data'] = temp;                
			// $("#drawsearch>div>div").html("Draw");     
			// clearMarker();
			// showListing(data, 1); 
		}
	});
}

function convertTransactionType(kode_jenis_transaksi){
	switch (kode_jenis_transaksi) {
		case "0":
			return  "jual";
			break;
		case "1":
			return  "sewa";
			break;
		case "2":
			return  "jual/sewa";
			break;
		
		default:
			return "";
			break;
	}
}

$(".leaflet-draw-draw-polygon").click(function (e) { 
	e.preventDefault();
	// console.log("start drawing");
	senseMap.resetAreaParam();
	senseMap.clearDrawnLayer();
	senseMap.drawnStatus = false;
});

var sort = "startlisting";
var urutan = "desc";
var sortData = "";
var sortKata = "";
function urutkan(e){
    $("#sort-" + sort).find(".fa.fa-check").remove();
	sort = $(e).attr("data-value");
	sortKata = $(e).html();
    $("#sort-" + sort).append(" <i class='fa fa-check'></i>");
	urutan = $("#urutan").find(".active").attr("data-value");
	
	let parent = $(e).parent().parent().parent();
	sortKata = (sortKata.split(" ").length > 1 ? sortKata.split(" ")[0][0] + sortKata.split(" ")[1][0] : sortKata);
	$($(parent).find("div")[0]).html('<i class="fa fa-sort-alpha-'+urutan+'"></i> Urutkan - '+ sortKata);
    sortData = "&sort=" + sort + "-" + urutan;

    refreshData();
}
function urutkanAZ(element){
    let urutan = $(element).attr("data-value");
    sortData = "&sort=" + sort + "-" + urutan;
    
    let parent = $(element).parent().parent().parent();
    $($(parent).find("div")[0]).html('<i class="fa fa-sort-alpha-'+urutan+'"></i> Urutkan - '+ sortKata);
    refreshData();
}
