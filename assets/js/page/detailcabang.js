function updateListing(id){
    $.ajax({
        "url" : API_URL + "/api/get/listings/getActive?token=public&user_broker=" + id,
        success : function(data){
          data = JSON.parse(data);
		      var row = "";
          data.forEach(e => {
            row += `<a href="#" class="flex-2 rounded" style="border: dotted #9999 0.5px; color: black;">`;
            row += `    <div class="flex flex-ver" style="height:100%; padding: 10px;">`;
            row += `        <div style="background-image:url('/assets/img/listing/`+e.id_listing+`/1.jpg'); background-size:cover; min-height: 200px; background-position: center;">`;
            row += `            <div style="background-color:seagreen; padding: 2px; min-width: 196px; width: 50%; margin-top: 20px; margin-left: 10px; color: white; font-weight: bold; border-radius: 20px; text-align: center;">[BadgeInfo]</div>`;
            row += `        </div>`;
            row += `        <div class="flex flex-hor" style="margin-top: 5px;">`;
            row += `            <div class="flex flex-ver flex-equal" style="width: 90%;">`;
            row += `                <div style="font-weight: bold; font-size: 1.5em;">500 jt</div>`;
            row += `                <div>`+e.judul_listing+`</div>`;
            row += `                <div>`+e.alamat_listing+`, `+e.nama_kecamatan+`, `+e.nama_kota+`</div>`;
            row += `                <div>`+e.kamar_tidur+` <i class="fas fa-bed"></i>, `+e.kamar_mandi+` <i class="fas fa-bath"></i>, `+e.luas_tanah+` <i class="fas fa-square"></i>, `+e.luas_bangunan+` <i class="fas fa-building"></i></div>`;
            row += `            </div>`;
            row += `            <div class="flex flex-ver" style="font-size: 1.8em; width: 10%;">`;
            row += `                <div><i class="fa fa-whatsapp"></i></div>`;
            row += `                <div><i class="fa fa-heart"></i></div>`;
            row += `                <div><i class="fa fa-share"></i></div>`;
            row += `                <div><i class="fa fa-ellipsis-h"></i></div>`;
            row += `            </div>`;
            row += `        </div>`;
            row += `    </div>`;
            row += `</a>`;
          });
          $("#listListing").html(row);
        }
    });
}