let options = {
  startStep: 1,
  manualStepForward: false
};
let wizard1 = new mWizard('m_wizard1',options);
wizard1.on("change", function(e) {
  $('html, body').animate({
  scrollTop: 0
  }, 500);
  $("#saveAsDraft").fadeOut();
  if(e.currentStep == 2) {
      let judul     = $('#judulListing').val();
      let deskripsi = $('#deskripsiListing').summernote("code");
      let transaksi = $('#transaksiListing').val();

      let validPrice = false;
      if(transaksi == "0") {
          let hargaJual = parseInt($('#hargaJualListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, ''));
              validPrice = hargaJual > 0;
      } else if(transaksi == "1") {
          let hargaSewa  = parseInt($('#hargaSewaListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, ''));
          let minimSewa  = parseInt($('#minimSewaListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, ''));
              validPrice = hargaSewa > 0 && minimSewa > 0;
      } else {
          let hargaJual = parseInt($('#hargaJualListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, ''));
          let hargaSewa  = parseInt($('#hargaSewaListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, ''));
          let minimSewa  = parseInt($('#minimSewaListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, ''));
              validPrice = hargaJual > 0 && hargaSewa > 0 && minimSewa > 0;
      }

      let notValid = judul.length < 10 || deskripsi == "" || !validPrice;
      if(notValid) {
          wizard1.goTo(e.currentStep-1);
      }
  } else if(e.currentStep == 3) {
      let alamat    = $('#alamatListing').val();
      let provinsi  = $('#provinsiListing').val();
      let kota      = $('#kotaListing').val();
      let kecamatan = $('#kecamatanListing').val();
      let kelurahan = $('#kelurahanListing').val();
      let kodepos   = $('#kodeposListing').val();

      let notValid = alamat == "" || provinsi == "" || kota == "" || kecamatan == "" || kelurahan == "" || kodepos == "";
      if(notValid) {
          wizard1.goTo(e.currentStep-1);
      }
  } else if(e.currentStep == e.totalSteps) {
      let panjangTanah    = parseInt($('#ptanahListing').val());
      let lebarTanah      = parseInt($('#ltanahListing').val());
      let luasTanah       = parseInt($('#lutanahListing').val());

      let notValid = panjangTanah <= 0 || lebarTanah <= 0 || luasTanah <= 0;
      if(notValid) {
          wizard1.goTo(e.currentStep-1);
      } else {
          $("#saveAsDraft").fadeIn();
          $('#judulListing_preview').html(": " + $('#judulListing').val());
          $('#deskripsiListing_preview').html($('#deskripsiListing').summernote("code"));
          $('#tipeListing_preview').html(": " + $('#tipeListing option:selected').text());
          $('#transaksiListing_preview').html(": " + $('#transaksiListing option:selected').text());
          if($('#transaksiListing').val() == "0") {
            $('#jualStat_Preview').show();
            $('#sewaStat_Preview').hide();
            $('#hargajualListing_preview').html(": " + shortNumber(parseInt($('#hargaJualListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, ''))));
          } else if($('#transaksiListing').val() == "1") {
            $('#sewaStat_Preview').show();
            $('#jualStat_Preview').hide();
            $('#hargasewaListing_preview').html(": " + shortNumber(parseInt($('#hargaSewaListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, ''))));
            $('#minimsewaListing_preview').html(": " + $('#minimSewaListing').val());
          }
          $('#negoListing_preview').html(": " + ($('#negoListing').is(':checked') ? "Ya" : "Tidak"));
          $('#videoListing_preview').html(`<iframe width="500" height="300" src="`+$('#urlListing').val()+`"></iframe>`);

          $('#alamatListing_preview').html(": " + $('#alamatListing').val());
          $('#blokListing_preview').html(": " + $('#blokListing').val());
          $('#nomorListing_preview').html(": " + $('#nomorListing').val());
          $('#provinsiListing_preview').html(": " + $('#provinsiListing').val());
          $('#kotaListing_preview').html(": " + $('#kotaListing').val());
          $('#kecamatanListing_preview').html(": " + $('#kecamatanListing').val());
          $('#kelurahanListing_preview').html(": " + $('#kelurahanListing').val());
          $('#kodeposListing_preview').html(": " + $('#kodeposListing').val());
          let dataArea = "";
          $.each($('#areaListing').select2('data'), function(i, e){
              if(i == 0) {
                  dataArea += e.text;
              } else {
                  dataArea += ", " + e.text;
              }
          });
          $('#areaListing_preview').html(dataArea);

          $('#ptanahListing_preview').html(": " + $('#ptanahListing').val() + "m");
          $('#ltanahListing_preview').html(": " + $('#ltanahListing').val()  + "m");
          $('#lutanahListing_preview').html(": " + $('#lutanahListing').val() + "m<sup>2</sup>");
          $('#lubangunanListing_preview').html(": " + $('#lubangunanListing').val() + "m<sup>2</sup>");
          $('#ktidurListing_preview').html(": " + $('#ktidurListing').val());
          $('#kmandiListing_preview').html(": " + $('#kmandiListing').val());
          $('#garasiListing_preview').html(": " + $('#garasiListing').val());
          $('#lantaiListing_preview').html(": " + $('#lantaiListing').val());
          $('#listrikListing_preview').html(": " + $('#listrikListing').val() + "W");
          $('#tahunListing_preview').html(": " + $('#tahunListing').val());
          $("#orientasiListing_preview").html(": " + $('#orientasiListing option:selected').text());
          $("#sertifikatListing_preview").html(": " + $('#sertifikatListing option:selected').text());
          $('#hoekListing_preview').html(": " + ($('#hoekListing').is(':checked') ? "Ya" : "Tidak"));
          $('#interiorListing_preview').html(": " + ($('#interiorListing').is(':checked') ? "Ya" : "Tidak"));
      }
  }
});

jQuery.validator.addMethod("greater", function(value, element) {
  return parseInt(value.replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, '')) > 0; 
}, "Value must greater than 0");

let idUploaded = "";
let idx = 0;

$(document).ready(function () {
  $("body").addClass("m-brand--minimize m-aside-left--minimize");

  /** Initialize Input Element */
  $('.summernote').summernote({
      height: 300
  });
  $("#hargaJualListing, #hargaSewaListing").inputmask(
      "Rp. 999.999.999.999", {
      numericInput: !0
  });
  $('#transaksiListing').on('change', function(e){
      if(this.value == "0") {
          $("#jualStat").fadeIn();
          $("#sewaStat").fadeOut();
          let rulesHarga = {
              required: true,
              greater: true
          };
          $('#hargaSewaListing').rules("remove", rulesHarga);
          $('#minimSewaListing').rules("remove", rulesHarga);

          $('#hargaJualListing').rules("add", rulesHarga);
          $('#hargaJualListing').val();
      } else if(this.value == "1") {
          $("#sewaStat").fadeIn();
          $("#jualStat").fadeOut();
          let rulesHarga = {
              required: true,
              greater: true
          };
          $('#hargaJualListing').rules("remove", rulesHarga);

          $('#minimSewaListing').rules("add", rulesHarga);
          $('#hargaSewaListing').rules("add", rulesHarga);

          $('#hargaSewaListing, #minimSewaListing').val();
      }
  });
  Dropzone.forElement("#m-dropzone-three").destroy();
  Dropzone.autoDiscover = false;
  $(".dropzone").dropzone({
      autoProcessQueue: false,
      addRemoveLinks: true,
      parallelUploads: 10,
      sending: function(file, xhr, formData) {
          idx++;
          formData.append("idx", idx);
          formData.append("id", idUploaded);
      },
      queuecomplete : function() {
          Dropzone.forElement("#m-dropzone-three").options.autoProcessQueue = false;
          mApp.unblock('#m_wizard1');
          Swal.fire({
              type : 'success',
              title: 'Sukses',
              text : 'Sukses Menambahkan Listing!'
          }).then(function(result){
              window.location.href = BASE_URL+"/listingsaya/detail/" + idUploaded;
          });
      },
      removedfile: function(file) {
          var _ref;
          return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
      }
  });
  $("#areaListing").select2({
      tags: true
  });
  $("#tahunListing").datepicker({
      format: "yyyy",
      viewMode: "years", 
      minViewMode: "years",
      todayHighlight:!0,
      orientation:"bottom left",
      templates:{
          leftArrow:'<i class="la la-angle-left"></i>',
          rightArrow:'<i class="la la-angle-right"></i>'
      }
  });

  $('#actionListing').click(function(e) {
      e.preventDefault();
      Swal.fire({
          title            : 'Apakah Yakin Data Sudah Benar?',
          text             : "Cek Data Listing Sekali Lagi!",
          type             : 'warning',
          showCancelButton : true,
          confirmButtonText: "Ya!",
          cancelButtonText : 'Batal!',
          reverseButtons   : true
      }).then(function(result){
          if (result.value) {
              let dataArea = [];
              $.each($('#areaListing').select2('data'), function(i, e){
                  dataArea.push(e.id);
              });
              let harga = $('#hargaJualListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, '') > 0 ? $('#hargaJualListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, '') : $('#hargaSewaListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, '')

              let sendData = {
                  judul         : $('#judulListing').val(),
                  customer      : userID,
                  proyek        : "",
                  broker        : "",
                  deskripsi     : $('#deskripsiListing').summernote('code'),
                  tipe          : $('#tipeListing').val(),
                  jenis         : $('#transaksiListing').val(),
                  harga_jual    : harga,
                  harga_sewa    : harga,
                  minim_sewa    : $('#minimSewaListing').val(),
                  nego          : $('#negoListing').is(':checked') ? "1" : "0",
                  url_video     : $('#urlListing').val().replace("watch?v=","embed/"),
                  jalan         : $('#alamatListing').val(),
                  blok          : $('#blokListing').val(),
                  nomor         : $('#nomorListing').val(),
                  provinsi      : $('#provinsiListing').val(),
                  kota          : $('#kotaListing').val().replace("Kota ",""),
                  kecamatan     : $('#kecamatanListing').val().replace("Kecamatan ",""),
                  kelurahan     : $('#kelurahanListing').val().replace("Kelurahan ",""),
                  kodepos       : $('#kodeposListing').val(),
                  area          : dataArea,
                  lat           : $("#latListing").val(),
                  lng           : $("#lngListing").val(),
                  panjang_tanah : $('#ptanahListing').val(),
                  lebar_tanah   : $('#ltanahListing').val(),
                  luas_tanah    : $('#lutanahListing').val(),
                  luas_bangunan : $('#lubangunanListing').val(),
                  kamar_tidur   : $('#ktidurListing').val(),
                  kamar_mandi   : $('#kmandiListing').val(),
                  jumlah_garasi : $('#garasiListing').val(),
                  jumlah_lantai : $('#lantaiListing').val(),
                  listrik       : $('#listrikListing').val(),
                  tahun_dibangun: $('#tahunListing').val(),
                  hoek          : $('#hoekListing').is(':checked') ? "1" : "0",
                  interior      : $('#interiorListing').is(':checked') ? "1" : "0",
                  hadap         : $("#orientasiListing").val(),
                  sertifikat    : $("#sertifikatListing").val(),
                  gambar        : Dropzone.forElement("#m-dropzone-three").files.length
              }
              $.post(API_URL + "/api/post/listings/addListing?token=junior", JSON.stringify(sendData), function (data, status) {
                  if (data.stat){
                      idx = 0;
                      idUploaded = data.id;

                      Dropzone.forElement("#m-dropzone-three").options.autoProcessQueue = true;
                      Dropzone.forElement("#m-dropzone-three").processQueue();
                  }
              }, 'json');
          }
      });
  });

  $('#saveAsDraft').click(function(e) {
      e.preventDefault();
      Swal.fire({
          title            : 'Apakah Yakin Data Sudah Benar?',
          text             : "Cek Data Listing Sekali Lagi! Data Akan Disimpan Sebagai Draft",
          type             : 'warning',
          showCancelButton : true,
          confirmButtonText: "Ya!",
          cancelButtonText : 'Batal!',
          reverseButtons   : true
      }).then(function(result){
          if (result.value) {
              mApp.block('#m_wizard1', {});
              let dataArea = [];
              $.each($('#areaListing').select2('data'), function(i, e){
                  dataArea.push(e.id);
              });
              let harga = $('#hargaJualListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, '') > 0 ? $('#hargaJualListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, '') : $('#hargaSewaListing').val().replace("Rp. ", "").replace(/\_/g, '').replace(/\./g, '')

              let sendData = {
                judul         : $('#judulListing').val(),
                customer      : userID,
                proyek        : "",
                broker        : "",
                deskripsi     : $('#deskripsiListing').summernote('code'),
                tipe          : $('#tipeListing').val(),
                jenis         : $('#transaksiListing').val(),
                harga_jual    : harga,
                harga_sewa    : harga,
                minim_sewa    : $('#minimSewaListing').val(),
                nego          : $('#negoListing').is(':checked') ? "1" : "0",
                url_video     : $('#urlListing').val().replace("watch?v=","embed/"),
                jalan         : $('#alamatListing').val(),
                blok          : $('#blokListing').val(),
                nomor         : $('#nomorListing').val(),
                provinsi      : $('#provinsiListing').val(),
                kota          : $('#kotaListing').val().replace("Kota ",""),
                kecamatan     : $('#kecamatanListing').val().replace("Kecamatan ",""),
                kelurahan     : $('#kelurahanListing').val().replace("Kelurahan ",""),
                kodepos       : $('#kodeposListing').val(),
                area          : dataArea,
                lat           : $("#latListing").val(),
                lng           : $("#lngListing").val(),
                panjang_tanah : $('#ptanahListing').val(),
                lebar_tanah   : $('#ltanahListing').val(),
                luas_tanah    : $('#lutanahListing').val(),
                luas_bangunan : $('#lubangunanListing').val(),
                kamar_tidur   : $('#ktidurListing').val(),
                kamar_mandi   : $('#kmandiListing').val(),
                jumlah_garasi : $('#garasiListing').val(),
                jumlah_lantai : $('#lantaiListing').val(),
                listrik       : $('#listrikListing').val(),
                tahun_dibangun: $('#tahunListing').val(),
                hoek          : $('#hoekListing').is(':checked') ? "1" : "0",
                interior      : $('#interiorListing').is(':checked') ? "1" : "0",
                hadap         : $("#orientasiListing").val(),
                sertifikat    : $("#sertifikatListing").val(),
                gambar        : Dropzone.forElement("#m-dropzone-three").files.length,
                status        : "-"
              }
              $.post(API_URL + "/api/post/listings/addListing?token=customer", JSON.stringify(sendData), function (data, status) {
                  if (data.stat){
                      idx = 0;
                      idUploaded = data.id;

                      Dropzone.forElement("#m-dropzone-three").options.autoProcessQueue = true;
                      Dropzone.forElement("#m-dropzone-three").processQueue();
                  }
              }, 'json');
          }
      });
  });

  loadArea();
  loadSertifikat();
  resetListing();
  $("#m_form1").validate({
      rules: {
          judulListing: {
              required: true, minlength: 10, maxlength: 100
          },
          deskripsiListing: {
              required: true
          },
          alamatListing: {
              required: true
          },
          provinsiListing: {
            required: true
          },
          kotaListing: {
            required: true
          },
          kecamatanListing: {
            required: true
          },
          kelurahanListing: {
            required: true
          }
      },
      invalidHandler:function(e, r) {
          var i=$("#m_form_1_msg");
          i.removeClass("m--hide").show(), mApp.scrollTo(i, -200)
      },
      onfocusout: function(element) {
          return $(element).valid();
      }
  });
  $("#m_form1").validate().settings.ignore = ':not(select:hidden, input:visible, textarea:visible), .bs-searchbox>input, .m-select2, .select2-search__field';
  let hargaJualListing = {
      required: true,
      greater: true
  }
  $('#hargaJualListing').rules("add", hargaJualListing);
  /*********************************************************/

  $('#custListing').parent().find("[data-id='custListing']").css("height", "72px");
});

/**                                 Handle Reset Input Form                         */
function resetListing() {
  $('#judulListing, #proyekListing, #urlListing, #alamatListing, #blokListing, #nomorListing, #provinsiListing, #kotaListing, #kecamatanListing, #kelurahanListing, #kodeposListing').val("");
  $('#deskripsiListing').summernote("code", "");
  $('#transaksiListing').val("0");
  $('#tipeListing').val("1");
  $('#hargaJualListing, #hargaSewaListing, #minimSewaListing').val();
  $('#negoListing').bootstrapSwitch('state', true);
  $('#areaListing').val([]).trigger("change");

  $('#ptanahListing, #ltanahListing, #lutanahListing, #lubangunanListing, #ktidurListing, #kmandiListing, #garasiListing, #lantaiListing, #listrikListing').val(0);
  $('#tahunListing').datepicker("update", new Date());
  $('#hoekListing').bootstrapSwitch('state', true);
  $('#interiorListing').bootstrapSwitch('state', true);
  $('.m_selectpicker').selectpicker('refresh');
}
/************************************************************************************/

function handleModalCustomer() {
  $('#modalTambahCustomer').modal('show');
}

function loadArea() {
  $.get(API_URL + "/api/get/areas/getAll?token=junior",  function (data) {
      $('#areaListing').html("");
      $.each(data, function(i, e) {
          let row = `<option value="`+e.id_area+`">`+e.nama_area+`</option>`;
          $('#areaListing').append(row);
          if(i == data.length - 1) {
              $("#areaListing").select2("destroy");
              $("#areaListing").select2({
                  multiple: true,
                  placeholder: "Select an area", maximumSelectionLength: 3,
                  tags:true                  
              });
          }
      });
  },'json');
}

function loadSertifikat() {
  $.get(API_URL + "/api/get/tipes/getSertifikat?token=junior",  function (data) {
      $('#sertifikatListing').html("");
      $.each(data, function(i, e) {
          let row = `<option value="`+e.id_hak+`">`+e.nama_hak+`</option>`;
          $('#sertifikatListing').append(row);
          $('#sertifikatListing').selectpicker('refresh');
      });
  },'json');
}