var limit = 10;
var page = 1;

// Wills
var idDetail = 0;
var flag = 0;
var ctrListing = -1;
var selectedCenterMap = -1;
var buildingsMarkersArray = [];

var spiderConfig = {
	keepSpiderfied: true,
	event: 'mouseover',
	circleSpiralSwitchover:0
};

// when you are done creating markers then
// Wills
var clusterOptions = {
     imagePath: '/assets/img/marker/m'
};


var pagination = null;
var boundData = "";
var forceFitBounds = false;



$(document).ready(function () {
	// var url = window.location.href;
    // var arrUrl = url.split('/');
	// var arrUrl2 = arrUrl[3].split('?');
	// if (arrUrl2.length > 1){
	// 	var param = arrUrl2[1];
	// 	var sendSort = param.split("&sort=");
	// 	sendData = sendSort[0];
	// 	var sortBound = sendSort[1].split("&north=");
	// 	sortData = "&sort=" + sortBound[0];
	// 	boundData = "&north=" + sortBound[1];
	// }
	// //initMap();
	// forceFitBounds = true;
	// boundData = "";
	// //loadList();

	// // Wills
	// map.addLayer(markers);
	// map.addLayer(markersAlone);
	// loadList();
	loadListSenseMap();
});

var pageHandler = function pageHandling(){
	page = pagination.getCurrentPage();

	//Robb
	// loadList();

	// Wills
	loadList();
}

function showListing(data){
	var row = "";
	var ctr = 1;
	var ctrListing = -1;
	
	data.data.forEach(e => {
		// ctrListing++;
		var gambar = [];
		for (var i=1; i<=e.gambar; i++){
			gambar.push("/assets/img/listing/"+e.id_listing+"/"+i+".jpg");
		}
		gambar = JSON.stringify(gambar);

		// if (ctr % 2 == 1){
		// 	row += `<div class="row">`;
		// }
		if (e.jenis_transaksi == "0"){
			var jenis = "Jual";
			var bgJenis = "jual";
			var harga = shortNumber(e.harga_jual);
		}else if (e.jenis_transaksi == "1"){
			var jenis = "Sewa";
			var bgJenis = "sewa";
			var harga = shortNumber(e.harga_sewa) + "/thn";
		}else{
			var jenis = "Jual Sewa";
			var bgJenis = "jual_sewa";
			var harga = shortNumber(e.harga_jual) + " - " + shortNumber(e.harga_sewa) + "/thn";;
		}
		var favorite = "";
		if (e.favorite==1){
			favorite = "favorite";
		}
		row += `	<div class="col-lg-6 col-sm-12 mb-3 px-2">`;
		row += `		<div onclick='selectListingCard(this);' class=" mb-0 listing-item m-portlet m-portlet--rounded" id='listing`+ctrListing+`' data-ctr='`+ctrListing+`' data-id='`+e.id_listing+`' data-telp='`+e.telp_broker+`' data-judul='`+e.judul_listing+`' data-lat='`+e.lat_listing+`' data-lng='`+e.long_listing+`' data-tipe='`+e.nama_tipe+`' data-jenis='`+jenis+`'>`;
		row += `			<div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='`+gambar+`'>`;
		row += `				<div class="listing-slider-badge jenis-`+bgJenis+`">` + jenis + `</div>`;
		row += `				<div class="listing-slider-icon jenis-`+bgJenis+`"><i class="fas fa-home"></i></div>`;
		row += `				<div class="listing-slider-counter" style="z-index:2;">1 of 3</div>`;
		row += `			</div>`;
		row += `			<div class="m-portlet__body" style="padding:0px;">`;
		row += `				<div class="col-lg-10 col-md-10 col-sm-12" style="padding: 10px 5px 10px 10px;">`;
		row += `					<div class="listing-price listing-info">` + harga + `</div>`;
		row += `					<div class="listing-title listing-info truncate hoverable" onclick="viewDetail(this);"><b>` + e.judul_listing + `</b></div>`;
		row += `					<div class="listing-jalan listing-info truncate hoverable" onclick="updateCenterMap(this);">` + e.alamat_listing + `</div>`;
		row += `					<div class="listing-kota listing-info truncate">` + e.nama_kecamatan + `, ` + e.nama_kota + `</div>`;
		row += `					<div class="listing-detail">`;
		if (e.kamar_tidur != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>`+e.kamar_tidur+`</div>`;
		}
		if (e.kamar_mandi != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>`+e.kamar_mandi+`</div>`;
		}
		if (e.luas_tanah != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>` + e.luas_tanah + `m<sup>2</sup></div>`;
		}
		if (e.luas_bangunan != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>` + e.luas_bangunan + `m<sup>2</sup></div>`;
		}
		row += `					</div>`;
		row += `				</div>`;
		row += `				<div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding:10px 10px 10px 5px">`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='sendWA(this);' class="fab fa-whatsapp fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Pesan WhatsApp"></i></div>`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='favorite(this);' class="fas fa-heart fa-lg ` + favorite + `" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Favorit"></i></div>`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='shareLink(this);' class="fas fa-share fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Bagikan"></i></div>`;
		row += `					<a href="/detaillisting/view/`+e.id_listing+`" style="margin:auto;" class="listing-content-icon txt-black"><i class="fas fa-ellipsis-h fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Tampilkan Lebih Banyak"></i></a>`;
		row += `				</div>`;
		row += `			</div>`;
		row += `		</div>`;
		row += `	</div>`;
		if (ctr % 2 == 0){
			row += `</div>`;
		}
		ctr++;
	});
	// if (ctr % 2 == 0){
	// 	row += `</div>`;
	// }
	$("#tabs-foto").html(row);
	initSlider();
	mApp.initPopovers();
}
function loadListingbyLoad(){
	$.ajax({
		"url" : API_URL + "/api/get/listings/getActive?token=public&user_cust=" + userID + "&limit=10&offset=" + page,
        success : function(data){
          data = JSON.parse(data);
		  populateListing(data["data"]);
        }
	});
}
function loadList(requestPage = null){
	if (requestPage != null){
		page = requestPage;
	}
	mApp.block("#tabs-foto", {});
    $.ajax({
		"url" : API_URL + "/api/get/listings/search?token=public&user_cust=" + userID + "&limit=10&offset=" + (page-1)*limit + "&save=" + saveSearch + sendData + sortData + boundData,
        success : function(data){

		  data = JSON.parse(data);
		  if (page == 1){
			var newUrl = "/listing?"+sendData+sortData+boundData;
			window.history.pushState(data, "EraKita | Listing", newUrl);

			$("#showedListing").html(data.count);
			$("#totalListing").html(data.count);
		
			loadLatLngPin();
		  }
		  showListing(data);
		  mApp.unblock("#tabs-foto", {});
        }
	});
}

function loadLatLngPin(){
	mApp.block("#mapDiv", {});
    $.ajax({
		"url" : API_URL + "/api/get/listings/searchLatLng?token=public&user_cust=" + userID + "&limit=10&offset=" + (page-1)*limit + "&save=" + saveSearch + sendData + sortData + boundData,
        success : function(data){
		  data = JSON.parse(data);
		  pagination = new Paginate(".sensecode-paginate", (data.data.length-1)/limit+1, 1, pageHandler);
		  pagination.addActiveItemClass(["bg-red txt-white"]);
		  pagination.addItemClass(["clickable"]);
		  pagination.viewExample(0);
		  
		  $("#showedListing").html(data.data.length);
		  populateListing(data.data);
		//   initMarker(data);
		  mApp.unblock("#mapDiv", {});
        }
	});
}

function viewDetail(element){
	var idlisting = $(element).parent().parent().parent().attr('data-id');
	if (idlisting == idDetail){
		$("#my-chevron").click();    
	}else{
		idDetail = idlisting;
		$("#detailListing>div").fadeOut(100, function(){
            $.post({
				"url" : API_URL + "/api/get/listings/getById/"+idlisting+"?token=public",
				success : function(data){
					data = JSON.parse(data);
					if(data.jenis_transaksi == 0){
						var jenis = "Jual";
						var harga = "Rp " + numberWithCommas(data.harga_jual);
					}else if(data.jenis_transaksi == 1){
						var jenis = "Sewa";
						var harga = "Rp " + numberWithCommas(data.harga_sewa) + "/thn";
					}else{
						var jenis = "Jual - Sewa";
						var harga = "Rp " + numberWithCommas(data.harga_jual) + " - Rp " + numberWithCommas(data.harga_sewa) + "/thn";
					}
					var gambar= "";
					var thumb = "";
					var dom = `<div class="single-list-slider owl-carousel" id="sl-slider">`;
					for (var i=1; i<=data.gambar; i++){
						gambar += `<div class="sl-item set-bg" data-setbg="/assets/img/listing/`+data.id_listing+`/`+i+`.jpg">`;
						gambar += `	<div class="sale-notic">FOR SALE</div>`;
						gambar += `</div>`;
						thumb += `<div class="sl-thumb set-bg" data-setbg="/assets/img/listing/`+data.id_listing+`/`+i+`.jpg"></div>`;
					}
					dom += gambar + `</div>`;
					dom += `<div class="owl-carousel sl-thumb-slider" id="sl-slider-thumb">`;
					dom += thumb + `</div>`;

					$("#my-slider").html(dom);
					initSlSlider();
					$("#detailJudul").html(data.judul_listing);
					$("#detailAlamat").html('<i class="fa fa-map-marker"></i> ' + data.alamat_listing);
					$("#detailHarga").html(harga);
					$("#detailLuas").html('<i class="fa fa-th-large"></i> ' + data.luas_tanah + " m<sup>2</sup>")
					$("#detailKTidur").html('<i class="fa fa-bed"></i> ' + data.kamar_tidur + " Kamar Tidur");
					$("#detailBroker").html('<i class="fa fa-user"></i> ' + data.nama_broker);
					$("#detailGarasi").html('<i class="fa fa-car"></i> ' + data.jml_garasi + " Garasi");
					$("#detailLantai").html('<i class="fa fa-building-o"></i> ' +data.jml_lantai +' Tingkat');
					$("#detailKMandi").html('<i class="fa fa-bath"></i> ' + data.kamar_mandi + " Kamar Mandi");
					$("#detailUmur").html('<i class="fa fa-trophy"></i> Tahun ' + data.tahun_bangunan);
					$(".description").html('<p>'+data.deskripsi_listing+'</p>');
					if (data.url_video != null){
						var video = '<iframe width="100%" height="500px" src="'+data.url_video + '"></iframe>';
					}else{
						var video = "No Video Preview";
					}
					$(".perview-video").html(video);
				}
			}); 
            $("#detailListing>div").fadeIn(1000);
        });
		if ($("#my-chevron").attr("data-show")=="hide") {
			$("#my-chevron").click();
		} 
	}
}

function favorite(element){
	if (userID == null){
		Swal.fire({
			type: 'error',
			title: 'Oops...',
			text: 'Harap Login Terlebih Dahulu',
			footer: '<a href="/login">LOGIN DISINI</a>'
		});
	}else{
		var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		var status = !$(element).hasClass('favorite');
		$(element).toggleClass("favorite");
		if (status){
			var url = API_URL + "/api/get/customers/logFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
			if ($cookieBroker != null){
				url = API_URL + "/api/get/customers/logFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
			}
			$.post({
				"url" : url
			});
		}
		$.post({
			"url" : API_URL + "/api/get/customers/updateFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&status=" + Number(status),
			success : function(data){
				data = JSON.parse(data);
				if (!data.status){
					$(element).toggleClass("favorite");
				}
			}
		});
	}
}

function sendWA(element){
	var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
	var judul = $(element).parent().parent().parent().parent().attr('data-judul');
	var telp = $(element).parent().parent().parent().parent().attr('data-telp');
	var pesan = "Halo,%20Saya%20mau%20menanyakan%20mengenai%20listingan%20" + judul;
	var win = window.open("https://api.whatsapp.com/send?phone="+telp+"&text="+pesan, '_blank');
	if (userID != null){
		var url = API_URL + "/api/get/customers/logSendWA?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
		if (cookieBroker != null){
			url = API_URL + "/api/get/customers/logSendWA?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
		}
		$.post({
			"url" : url
		});
	}
	win.focus();
}

function shareLink(element) {
	/* Get the text field */
	var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
	var text = BASE_URL + "/detaillisting/view/" + idlisting;

	var body = document.getElementsByTagName('body')[0];
	var tempInput = document.createElement('INPUT');
	body.appendChild(tempInput);
	tempInput.setAttribute('value', text)
	tempInput.select();
	document.execCommand('copy');
	body.removeChild(tempInput);

	/* Alert the copied text */
	Swal.fire({
		type: 'success',
		title: 'Sukses',
		text: 'Berhasil disalin ke Clipboard'
	});
	if (userID != null){
		var url = API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
		if (cookieBroker != null){
			url = API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
		}
		$.post({
			"url" : url
		});
	}
}

$(".chevron").click(function (e) {
	if ($(this).attr("data-show") == "hide") {
		$(this).attr("data-show", "show");
		$(this).removeClass("chevron-flip");
		$("#detailListing").css("transform", "translateX(0)");
	} else {
		$(this).attr("data-show", "hide");
		$(this).addClass("chevron-flip");
		$("#detailListing").css("transform", "translateX(-100%)");
	}
});

function selectListingCard(ob){
	$(".listing-item.active").removeClass("active");
	$(ob).addClass("active");
}




















// /** Start Using New Map */
// var tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
// 	maxZoom: 18,
// 	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
// }),
// latlng = L.latLng(-7.29049, 112.72);

// var map = L.map('mapListing', {center: latlng, zoom: 15, layers: [tiles], drawControl: true});
// var areaParam; resetAreaParam();
// // Get Area
// map.on('click', function(e) {
// 	//console.log("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng)
// 	if (e.latlng.lng>areaParam[1]) {
// 		areaParam[1] = e.latlng.lng;
// 	}
// 	if (e.latlng.lng<areaParam[3]) {
// 		areaParam[3] = e.latlng.lng;
// 	}
// 	if (e.latlng.lat<areaParam[2]) {
// 		areaParam[2] = e.latlng.lat;
// 	}
// 	if (e.latlng.lat>areaParam[0]) {
// 		areaParam[0] = e.latlng.lat;
// 	}
// });

// var markersAlone = L.markerClusterGroup({ animateAddingMarkers : true });


// //var markers = L.markerClusterGroup({ animateAddingMarkers : true });
// var markers = L.markerClusterGroup({
// 	//animateAddingMarkers : true,
// 	spiderfyOnMaxZoom: false,
// 	// disableClusteringAtZoom: 2,
// 	chunkedLoading: true,
// 	maxClusterRadius: function (zoom) {
//         return (zoom <= 14) ? 80 : 1; // radius in pixels
//     },
// 	iconCreateFunction: function(cluster) {
// 		//erakita-marker-container
// 		var childMarkers = cluster.getAllChildMarkers();
		
// 		var n = 0;
// 		if (checkMarkerSeparate(childMarkers)) {
// 			cluster.childTogether = false;
// 			return L.divIcon({
// 				html: "<div class='cluster-inner'><div>"+cluster.getChildCount()+"</div></div>",
// 				className: 'cluster-container',
// 				iconSize: L.point(40, 40)
// 			});
// 		} else {
// 			clusterku = cluster;
// 			cluster.childTogether = true;
// 			let domIcon =  `<div class='erakita-marker-label'>
// 								<b>`+ cluster.getChildCount() +` Unit</b>
// 							</div>
// 						`;
// 			return L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing-cluster', html: domIcon });
// 		}

// 	}
// });
// var clusterku;

// function checkMarkerSeparate(childMarkers){
// 	let diffPosition = false;
// 	let lastPos = null;
// 	childMarkers.forEach(el => {
// 		if (lastPos != null && lastPos.lat != el._latlng.lat && lastPos.lng != el._latlng.lng) {
// 			diffPosition = true;
// 		}
// 		lastPos = el._latlng;
// 	});
// 	return diffPosition;
// }






// var childData;
// markers.on('clustermouseover', function (c) {
// 	// a.layer is actually a cluster
// 	// console.log('cluster ' + a.layer.getAllChildMarkers().length);
// 	childData = (getChildData(c.layer.getAllChildMarkers()));
// 	let offsetY = -10;
// 	//
// 	if (childData.separate) {
// 		markers.options.zoomToBoundsOnClick = true;
// 		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: true, autoPan: false, position : "s" })
// 		.setContent("Melihat " + c.layer._childCount +' Listing <br> Listing dari harga ' + shortNumber(childData.minPrice) + " - " + shortNumber(childData.maxPrice) + " dengan rata-rata harga " + shortNumber(childData.avgPrice))
// 		.setLatLng(c.latlng)
// 		.openOn(map);
// 	} else {
// 		markers.options.zoomToBoundsOnClick = false;
// 	}
// 	// var popup = L.popup()
//     //               .setLatLng(c.layer.getLatLng())
//     //               .setContent("Melihat " + c.layer._childCount +' Listing <br> Listing dari harga ' + shortNumber(childData.minPrice) + " - " + shortNumber(childData.maxPrice) + " dengan rata-rata harga " + shortNumber(childData.avgPrice))
// 	// 			  .openOn(map);
// }).on('clustermouseout',function(c){
// 	if (childData.separate) {
// 		map.closePopup();
// 	}
// }).on('clusterclick',function(c){
// 	map.closePopup();
// 	if (!childData.separate) {
// 		offsetY = -18;
// 		let domPopup = `<div class='flex flex-ver' style='width:320px;'>
// 							<div class="flex flex-ver" style='border-bottom: 2px solid var(--gray); margin-bottom: 10px;'>
// 								<div class='label' style='font-size:1.25em;'>`+ c.layer._childCount +` for sale</div>
// 								<a href='javascript:void(0)' class='my-2' style='font-size:1.25em;'><i class="fa fa-lock"></i> Sign in for more details</a>
// 							</div>
// 							<div class='map-listing-item-container minimalistScrollbar' style='max-height:188px; overflow-y:auto;'>`;

// 		childData.listings.forEach(listing => {	
// 			// console.log(listing);			
// 			domPopup +=			`<div class='flex flex-hor flex-vertical-center flex-separate py-2'>
// 									<div class='squared div-image ratio69' image='default' style='width:37%;'></div>
// 									<div class="flex flex-ver" style='width:58%;'>
// 										<div>Unit di`+ convertTransactionType(listing.jenis_transaksi) +`kan</div>
// 										<div><b>`+ listing.judul_listing +`</b></div>
// 										<div>Rp. `+  shortNumber(listing.harga_jual) +`</div>
// 										<div>`+listing.kamar_tidur+` bed.`+ listing.kamar_mandi +` bath. <i class="fa fa-lock"></i> sq.ft.</div>
// 									</div>
// 								</div>`
// 		});
// 		domPopup += 		`</div>
// 						</div>`;
// 		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: true, position : "s" })
// 		.setContent(domPopup)
// 		.setLatLng(c.latlng)
// 		.openOn(map);
// 		// c.originalEvent.preventDefault();
// 		// map.fitBounds(map.getBounds());
// 	}
// });

// function convertTransactionType(kode_jenis_transaksi){
// 	switch (kode_jenis_transaksi) {
// 		case "0":
// 			return  "jual";
// 			break;
// 		case "1":
// 			return  "sewa";
// 			break;
// 		case "2":
// 			return  "jual/sewa";
// 			break;
		
// 		default:
// 			return "";
// 			break;
// 	}
// }

// var markersList = [];

// // Initialise the FeatureGroup to store editable layers
// var editableLayers = new L.FeatureGroup();
// map.addLayer(editableLayers);

// var drawPluginOptions = {
// 	position: 'topright',
// 	draw: {
// 		polygon: {
// 			allowIntersection: false, // Restricts shapes to simple polygons
// 			drawError: {
// 				color: '#e1e100', // Color the shape will turn when intersects
// 				message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
// 			},
// 			shapeOptions: {
// 				color: '#97009c'
// 			}
// 		},
// 		// disable toolbar item by setting it to false
// 		polyline: false,
// 		circle: false, // Turns off this drawing tool
// 		rectangle: false,
// 		marker: false,
// 	},
// 	edit: {
// 		featureGroup: editableLayers, //REQUIRED!!
// 		remove: false
// 	}
// };
  
// // Initialise the draw control and pass it the FeatureGroup of editable layers
// var drawControl = new L.Control.Draw(drawPluginOptions);
// map.addControl(drawControl);

// var editableLayers = new L.FeatureGroup();
// map.addLayer(editableLayers);

// var drawnLayers = [];
// map.on('draw:created', function(e) {
//  	var type = e.layerType,
//     layer = e.layer;

// 	if (type === 'marker') {
// 		layer.bindPopup('A popup!');
// 	}
// 	drawnLayers.push(e.layer);

// 	  editableLayers.addLayer(layer);
// 	  console.log("selesai");
// });




// var btnSearchControl = L.Control.extend({        
// 	options: {
// 	  position: 'bottomright'
// 	},

// 	onAdd: function (map) {
// 		var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom leaflet-draw-toolbar leaflet-bar leaflet-draw-toolbar-top activeable');

// 		container.style.backgroundColor = 'white';     
// 		container.style.display = 'block';
// 		container.style.cursor = 'pointer';
// 		container.style.padding = '5px 10px';
// 		container.innerHTML = "Search"
		
// 		container.onclick = function(){
// 			// Search Disini
// 			// Load Listing disini
// 			mapMode = 0;
// 			//console.log('SearchClicked', areaParam);
// 			loadListingbySearch();
// 		}

// 	  return container;
// 	}
// });

// var btnSaveSearchControl = L.Control.extend({        
// 	options: {
// 	  position: 'bottomright'
// 	},

// 	onAdd: function (map) {
// 		var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom leaflet-draw-toolbar leaflet-bar leaflet-draw-toolbar-top activeable');

// 		container.style.backgroundColor = 'white';     
// 		container.style.display = 'block';
// 		container.style.cursor = 'pointer';
// 		container.style.padding = '5px 10px';
// 		container.innerHTML = "Save Search"
		
// 		container.onclick = function(){
// 			// Search Disini
// 			// Load Listing disini
// 			mapMode = 0;
// 			//console.log('SearchClicked', areaParam);
// 			loadListingbySearch();
// 		}

// 	  return container;
// 	}
// });
// var btnResetControl = L.Control.extend({
// 	options: {
// 	  position: 'bottomright'
// 	},

// 	onAdd: function (map) {
// 		var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom leaflet-draw-toolbar leaflet-bar leaflet-draw-toolbar-top activeable');

// 		container.style.backgroundColor = 'white';     
// 		container.style.display = 'block';
// 		container.style.cursor = 'pointer';
// 		container.style.padding = '5px 10px';
// 		container.innerHTML = "Reset"
// 		container.onclick = function(){
// 			if (mapMode == 0) {
// 				resetAreaParam();
// 				clearDrawn();
// 				console.log('ResetClicked');
// 			}
// 		}

// 	  return container;
// 	}
// });

// function randomPopulate() {
// 	for (var i = 0; i < 100; i++) {
// 		console.log(getRandomLatLng(map));
// 		var m = L.marker(getRandomLatLng(map));
// 		markersList.push(m);
// 		markers.addLayer(m);
// 	}
// 	return false;
// }
// function getRandomLatLng(map) {
// 	var bounds = map.getBounds(),
// 		southWest = bounds.getSouthWest(),
// 		northEast = bounds.getNorthEast(),
// 		lngSpan = northEast.lng - southWest.lng,
// 		latSpan = northEast.lat - southWest.lat;

// 	return L.latLng(
// 			southWest.lat + latSpan * Math.random(),
// 			southWest.lng + lngSpan * Math.random());
// }

// //randomPopulate();
// //map.addLayer(markers);
// //map.addLayer(markers);
// // for (var i = 0; i < 100; i++) {
// // 	markers.addLayer(markersList[i]);
// // }
// map.addControl(new btnSaveSearchControl());
// map.addControl(new btnSearchControl());
// map.addControl(new btnResetControl());

// //Ugly add/remove code
// // L.DomUtil.get('populate').onclick = function () {
// // 	var bounds = map.getBounds(),
// // 	southWest = bounds.getSouthWest(),
// // 	northEast = bounds.getNorthEast(),
// // 	lngSpan = northEast.lng - southWest.lng,
// // 	latSpan = northEast.lat - southWest.lat;
// // 	var m = L.marker([
// // 			southWest.lat + latSpan * 0.5,
// // 			southWest.lng + lngSpan * 0.5]);
// // 	markersList.push(m);
// // 	markers.addLayer(m);
// // };
// // L.DomUtil.get('remove').onclick = function () {
// // 	markers.removeLayer(markersList.pop());
// // };



// // map.on('editable:drawing:end', function() {
// //     console.log("selesai");
// // });
// // map.on('editable:drawing:start', function() {
// //     console.log("mulai");
// // });
// // map.editTools.on('editable:drawing:start', function() {
// // 	console.log("mulai");
// // });
// // map.editTools.on('editable:drawing:end', function() {
// // 	console.log("selesai");
// // });
// $(".leaflet-draw-draw-polygon").click(function (e) { 
// 	e.preventDefault();
// 	//console.log("start drawing");
// 	resetAreaParam();
// 	clearDrawn();
// 	mapMode = 1;
// });



// function clearDrawn(){
// 	if (drawnLayers.length>0) {
// 		for (let index = drawnLayers.length-1; index >= 0; index--) {
// 			editableLayers.removeLayer(drawnLayers[index]);			
// 		}
// 	}
// }
// function resetAreaParam(){
// 	areaParam = [-90, -180, 90, 180]; // Atas, Kanan, Bawah, Kiri, The value is inverted for searching maximum border
// }

// function loadListingbySearch(){
// 	var sendData = {
// 		atas: areaParam[0],
// 		kanan: areaParam[1],
// 		bawah: areaParam[2],
// 		kiri: areaParam[3]
// 	}

// 	$.ajax({
// 		"url" : API_URL + "/api/get/listings/searchArea?token=public",
// 		"method" : "GET",
// 		"data" : sendData,
// 		success : function(data){
// 			data = JSON.parse(data);
// 			// console.log(data);
// 			populateListing(data);
// 			// data.forEach(element => {
// 			// 	if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(element.lat_listing, element.long_listing), mapPolygonArea) ){
// 			// 		temp.push(element);
// 			// 	}    
// 			// });
// 			// Total ini data yang sudah menghilangkan data2 yg tidak diperlukan
// 			// console.log(temp);    
// 			// data['data'] = temp;                
// 			// $("#drawsearch>div>div").html("Draw");     
// 			// clearMarker();
// 			// showListing(data, 1); 
// 		}
// 	});
// }

// function getUrlVars() {
//     var vars = {};
//     var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
//         vars[key] = value;
//     });
//     return vars;
// }
// async function populateListing(data){
// 	//console.log(data);
// 	var temp = [];
// 	markersList.forEach(element => {
// 		markers.removeLayer(element);
// 	});
// 	// Individual add pin
// 	// data.forEach(element => {
// 	// 	addListingPopulation(element.lat_listing, element.long_listing, element);
// 	// });
// 	let chunkSize = (getUrlVars()["chunkSize"] !== undefined) ? getUrlVars()["chunkSize"] : 10;
// 	// console.log("chunkSize ", chunkSize);
// 	let chunkCount = data.length / chunkSize;
// 	// console.log("SleepTime ", sleepTime);
// 	let promises = [];
// 	let indexes = [];
// 	for (let index = 0; index < chunkCount; index++) {
// 		let chunkListing = data.splice(0, chunkSize);
// 		promises.push(
// 			function(){
// 					addListingsPopulation(chunkListing);
// 					console.log(index);
// 			}
// 		)
// 		indexes.push({index: index});
// 	}
// 	Promise.all(indexes.map(index=>promises[index.index]())).then(function(){
// 		//console.log("done");
// 	});
// 	//addCardPopulation(data);
// 	//map.fitBounds(markers.getBounds());
// }

// var x = " ";
// function addListingsPopulation(listings){
	
// 	// console.log("promise executed");
// 	listings.forEach(listing => {
// 		addListingPopulation(listing.lat_listing, listing.long_listing, listing);
// 	});
// }

// function addListingPopulation(lat, lng, args) {
// 	//console.log(args);
// 	let domIcon =  `<div class='erakita-marker-label'>
// 						`+shortNumber(args.harga_jual)+`
// 					</div>`;
// 	var markerIcon = L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing' + args.jenis_transaksi, html : domIcon});
// 	var m = L.marker(new L.latLng(lat, lng), {icon : markerIcon});
// 	m.listing = args
// 	//m.bindPopup("<b>lat :"+ args.lat_listing +"; lng : "+ args.long_listing+"</b><br>I am a popup.").openPopup();
// 	markersList.push(m);
// 	markers.addLayer(m);
// 	return false;
// }

// function addCardPopulation(listings){
// 	// dimana rob variable aslinya
// 	//if (clear == 1){
// 	//	ctrListing = -1;
// 	//	$("#tabs-foto").html("");
// 	//}
// 	var row = "";
// 	var ctr = 1;	   
	
// 	listings.forEach(e => {
// 		// ctrListing++;
// 		var gambar = [];
// 		for (var i=1; i<=e.gambar; i++){
// 			gambar.push("/assets/img/listing/"+e.id_listing+"/"+i+".jpg");
// 		}
// 		gambar = JSON.stringify(gambar);

// 		if (ctr % 2 == 1){
// 			row += `<div class="row">`;
// 		}
// 		if (e.jenis_transaksi == "0"){
// 			var jenis = "Jual";
// 			var bgJenis = "jual";
// 			var harga = shortNumber(e.harga_jual);
// 		}else if (e.jenis_transaksi == "1"){
// 			var jenis = "Sewa";
// 			var bgJenis = "sewa";
// 			var harga = shortNumber(e.harga_sewa) + "/thn";
// 		}else{
// 			var jenis = "Jual Sewa";
// 			var bgJenis = "jual_sewa";
// 			var harga = shortNumber(e.harga_jual) + " - " + shortNumber(e.harga_sewa) + "/thn";;
// 		}
// 		var favorite = "";
// 		if (e.favorite==1){
// 			favorite = "favorite";
// 		}
		
		

// 		row += `	<div class="col-lg-6 col-sm-12">`;
// 		row += `		<div style='margin-bottom: 10px;' onclick='selectListingCard(this);' class="mb-0 listing-item m-portlet m-portlet--rounded" id='listing`+ctrListing+`' data-ctr='`+ctrListing+`' data-id='`+e.id_listing+`' data-telp='`+e.telp_broker+`' data-judul='`+e.judul_listing+`' data-lat='`+e.lat_listing+`' data-lng='`+e.long_listing+`' data-tipe='`+e.nama_tipe+`' data-jenis='`+jenis+`'>`;
// 		row += `			<div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='`+gambar+`'>`;
// 		row += `				<div class="listing-slider-badge jenis-`+bgJenis+`">` + jenis + `</div>`;
// 		row += `				<div class="listing-slider-icon jenis-`+bgJenis+`"><i class="fas fa-home"></i></div>`;
// 		row += `				<div class="listing-slider-counter" style="z-index:2;">1 of 3</div>`;
// 		row += `			</div>`;
// 		row += `			<div class="m-portlet__body" style="padding:0px;">`;
// 		row += `				<div class="col-lg-10 col-md-10 col-sm-12" style="padding: 10px 5px 10px 10px;">`;
// 		row += `					<div class="listing-price listing-info">` + harga + `</div>`;
// 		row += `					<div class="listing-title listing-info truncate hoverable" onclick="viewDetail(this);"><b>` + e.judul_listing + `</b></div>`;
// 		row += `					<div class="listing-jalan listing-info truncate hoverable" onclick="updateCenterMap(this);">` + e.alamat_listing + `</div>`;
// 		row += `					<div class="listing-kota listing-info truncate">` + e.nama_kecamatan + `, ` + e.nama_kota + `</div>`;
// 		row += `					<div class="listing-detail">`;
// 		if (e.kamar_tidur != null){
// 			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>`+e.kamar_tidur+`</div>`;
// 		}
// 		if (e.kamar_mandi != null){
// 			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>`+e.kamar_mandi+`</div>`;
// 		}
// 		if (e.luas_tanah != null){
// 			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>` + e.luas_tanah + `m<sup>2</sup></div>`;
// 		}
// 		if (e.luas_bangunan != null){
// 			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>` + e.luas_bangunan + `m<sup>2</sup></div>`;
// 		}
// 		row += `					</div>`;
// 		row += `				</div>`;
// 		row += `				<div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding:10px 10px 10px 5px">`;
// 		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='sendWA(this);' class="fab fa-whatsapp fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Pesan WhatsApp"></i></div>`;
// 		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='favorite(this);' class="fas fa-heart fa-lg ` + favorite + `" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Favorit"></i></div>`;
// 		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='shareLink(this);' class="fas fa-share fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Bagikan"></i></div>`;
// 		row += `					<a href="/detaillisting/view/`+e.id_listing+`" style="margin:auto;" class="listing-content-icon txt-black"><i class="fas fa-ellipsis-h fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Tampilkan Lebih Banyak"></i></a>`;
// 		row += `				</div>`;
// 		row += `			</div>`;
// 		row += `		</div>`;
// 		row += `	</div>`;
// 		if (ctr % 2 == 0){
// 			row += `</div>`;
// 		}
// 		ctr++;
// 	});

// 	// Gapaham maksudmu @robby
// 	// if (flag != 0){
// 	// 		initMarker(markers);
// 	// }else{
// 	// 		var centerMap = [data.data[0].lat_listing, data.data[0].long_listing];
// 	// 		initMap(markers, centerMap);
// 	// }
// 	$("#showedListing").html(ctrListing+1);
// 	$("#totalListing").html(ctrListing+1);
// 	flag += 10;
// 	if (ctr % 2 == 0){
// 		row += `</div>`;
// 	}
// 	$("#tabs-foto").html(row);
// 	initSlider();
// 	mApp.initPopovers();
// }

// // cluster.on('clustermouseover', function(c) {
// // 	var popup = L.popup()
// // 		.setLatLng(c.layer.getLatLng())
// // 		.setContent(c.layer._childCount +' Locations(click to Zoom)')
// // 		.openOn(map);
// // 	}).on('clustermouseout',function(c){
// // 		 map.closePopup();
// // 	}).on('clusterclick',function(c){
// // 		 map.closePopup();
// // }); 











// // Ini Robby
// var lastSeparateMarket = undefined;
// function addSeparateCluster(targetMarker){
// 	if (lastSeparateMarket !== undefined || targetMarker == null) {
// 		markersAlone.removeLayer(lastSeparateMarket);
// 	} 
// 	if (targetMarker != null) {
// 		markersAlone.addLayer(targetMarker);
// 		lastSeparateMarket = targetMarker;
// 	}
// }

















/** Using Sensecode-Map.js */
var mapMode = 0;
var senseMap = new SensecodeMap();

senseMap.initMap("mapDiv");
senseMap.addOnClickMap(function(){
	// this is a function i think
	console.log("mapCLicked");
});

var publicMarkerContainer = senseMap.addClusterGroup({
	"iconCreateFunction" : function(cluster){
		var childMarkers = cluster.getAllChildMarkers();
		if (senseMap.checkMarkerSeparate(childMarkers)) {
			return L.divIcon({
				html: "<div class='cluster-inner'><div>" + cluster.getChildCount() + "</div></div>",
				className: 'cluster-container',
				iconSize: L.point(40, 40)
			});
		} else {
			let domIcon =  `<div class='erakita-marker-label'>
								<b>`+ cluster.getChildCount() +` Unit</b>
							</div>
						`;
			return L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing-cluster', html: domIcon });
		}
	}
});
var privateMarkerContainer = senseMap.addClusterGroup();

senseMap.addOnHoverCluster(publicMarkerContainer.clusterGroup, clusterOnHover);
senseMap.addOnMouseOutCluster(publicMarkerContainer.clusterGroup, clusterOnMouseOut);
senseMap.addOnMouseClickCluster(publicMarkerContainer.clusterGroup, clusterOnMouseClick);
senseMap.initDrawable();
senseMap.addNewButtons({
	"innerHTML" : "Search",
	"onClick" : loadListingbySearch
});
senseMap.addNewButtons({
	"innerHTML" : "Save Search",
	"onClick" : loadListingbySearch // Save search param here
});
senseMap.addNewButtons({
	"innerHTML" : "Reset",
	"onClick" : function(){
		senseMap.resetAreaParam();
		senseMap.clearDrawnLayer();
	}
});








/** Customized function for listing Page */
var clusterGroupIndividualMarkers = senseMap.addClusterGroup();
function showSeparateMarker(markerIndex){
	privateMarkerContainer.clusterGroup.addLayer(senseMap.markersLists[0][markerIndex]);
}

function clusterIconCreate(cluster){
	var childMarkers = cluster.getAllChildMarkers();
	var n = 0;
	if (senseMap.checkMarkerSeparate(childMarkers)) {
		return L.divIcon({
			html: "<div class='cluster-inner'><div>" + cluster.getChildCount() + "</div></div>",
			className: 'cluster-container',
			iconSize: L.point(40, 40)
		});
	} else {
		let domIcon =  `<div class='erakita-marker-label'>
							<b>`+ cluster.getChildCount() +` Unit</b>
						</div>
					`;
		return L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing-cluster', html: domIcon });
	}
}

function clusterOnHover(cluster){
	childData = (getChildData(cluster.layer.getAllChildMarkers()));
	let offsetY = -10;
	if (childData.separate) {
		publicMarkerContainer.clusterGroup.options.zoomToBoundsOnClick = true;
		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: false, position : "s" })
		.setContent("Melihat " + cluster.layer._childCount +' Listing <br> Listing dari harga ' + shortNumber(childData.minPrice) + " - " + shortNumber(childData.maxPrice) + " dengan rata-rata harga " + shortNumber(childData.avgPrice))
		.setLatLng(cluster.latlng)
		.openOn(senseMap.map);
	} else {
		publicMarkerContainer.clusterGroup.options.zoomToBoundsOnClick = false;
	}
}

function clusterOnMouseOut(cluster){
	if (childData.separate) {
		senseMap.map.closePopup();
	}
}
function clusterOnMouseClick(cluster){
	senseMap.map.closePopup();
	if (!childData.separate) {
		offsetY = -18;
		let domPopup = `<div class='flex flex-ver' style='width:320px;'>
							<div class="flex flex-ver" style='border-bottom: 2px solid var(--gray); margin-bottom: 10px;'>
								<div class='label' style='font-size:1.25em;'>`+ c.layer._childCount +` for sale</div>
								<a href='javascript:void(0)' class='my-2' style='font-size:1.25em;'><i class="fa fa-lock"></i> Sign in for more details</a>
							</div>
							<div class='map-listing-item-container minimalistScrollbar' style='max-height:188px; overflow-y:auto;'>`;

		childData.listings.forEach(listing => {	
			domPopup +=			`<div class='flex flex-hor flex-vertical-center flex-separate py-2'>
									<div class='squared div-image ratio69' image='default' style='width:37%;'></div>
									<div class="flex flex-ver" style='width:58%;'>
										<div>Unit di`+ convertTransactionType(listing.jenis_transaksi) +`kan</div>
										<div><b>`+ listing.judul_listing +`</b></div>
										<div>Rp. `+  shortNumber(listing.harga_jual) +`</div>
										<div>`+listing.kamar_tidur+` bed.`+ listing.kamar_mandi +` bath. <i class="fa fa-lock"></i> sq.ft.</div>
									</div>
								</div>`;
		});
		domPopup += 		`</div>
						</div>`;
		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: true, position : "s" })
		.setContent(domPopup)
		.setLatLng(cluster.latlng)
		.openOn(senseMap.map);
		// c.originalEvent.preventDefault();
		// map.fitBounds(map.getBounds());
	}
}


function getChildData(childs){
	let childData = {maxPrice: 0, minPrice : null, listings : []};
	let sumPrice = 0;
	childs.forEach(el => {
		childData.listings.push(el.listing);
		if (childData.minPrice == null || el.listing.harga_jual < childData.minPrice ) {
			childData.minPrice = el.listing.harga_jual;
		}
		if (el.listing.harga_jual > childData.maxPrice ) {
			childData.maxPrice = el.listing.harga_jual;
		}
		sumPrice += parseFloat(el.listing.harga_jual);
	});
	console.log(sumPrice, childs.length);
	childData.avgPrice = sumPrice / childs.length;
	childData.separate = senseMap.checkMarkerSeparate(childs);
	return childData;
}

var requestPage = null;
function loadListSenseMap(){
	if (requestPage != null){
		page = requestPage;
	}
	mApp.block("#tabs-foto", {});
	$.ajax({
		"url" : API_URL + "/api/get/listings/search?token=public&user_cust=" + userID + "&limit=10&offset=" + (page-1)*limit + "&save=" + saveSearch + sendData + sortData + boundData,
		success : function(data){

			data = JSON.parse(data);
			if (page == 1){
				var newUrl = "/listing?"+sendData+sortData+boundData;
				window.history.pushState(data, "EraKita | Listing", newUrl);
				$("#showedListing").html(data.count);
				$("#totalListing").html(data.count);
				loadSenseMapMarker();
			}
			showListing(data);
			mApp.unblock("#tabs-foto", {});
		}
	});
}
var listingActive = -1;
function loadSenseMapMarker(){
	mApp.block("#mapDiv", {});
    $.ajax({
		"url" : API_URL + "/api/get/listings/searchLatLng?token=public&user_cust=" + userID + "&save=" + saveSearch + sendData + sortData + boundData,
        success : function(data){
			data = JSON.parse(data);
			pagination = new Paginate(".sensecode-paginate", (data.data.length-1)/limit+1, 1, pageHandler);
			pagination.addActiveItemClass(["bg-red txt-white"]);
			pagination.addItemClass(["clickable"]);
			pagination.viewExample(0);

			$("#showedListing").html(data.data.length);
			let preparedData = prepareDataforMarker(data.data);
			senseMap.addAllMarkers(preparedData, {defaultPopup:false});
			publicMarkerContainer.markerList.forEach(element =>{
				$(element).click(function (e){
					if (listingActive != -1){
						$(".erakita-marker-label[indexCard='"+listingActive+"']").parent().removeClass("activeMarker");
					}
					listingActive = $(this)[0].listing.id_listing;
					$(".erakita-marker-label[indexCard='"+listingActive+"']").parent().addClass("activeMarker");
				})
			});
			mApp.unblock("#mapDiv", {});
        }
	});
}

function prepareDataforMarker(data){
	let targetData = ("data" in data) ? data.data : data;
	targetData.forEach(element => {
		let domIcon =  `<div class='erakita-marker-label' indexCard='`+element.id_listing+`'>`+shortNumber(element.harga_jual)+`</div>`
		let temp = JSON.stringify({listing : element});
		element.additionalData = JSON.parse(temp);
		element.lat = element.lat_listing;
		element.lng = element.long_listing;
		element.icon = senseMap.createIcon({
			html: domIcon,
			className : "bg-red txt-white erakita-marker-container leaflet-div-icon tipe-listing"+ element.jenis_transaksi}, true)
	});
	return targetData;
}

function loadListingbySearch(){
	var sendData = {
		atas: senseMap.areaParam[0],
		kanan: senseMap.areaParam[1],
		bawah: senseMap.areaParam[2],
		kiri: senseMap.areaParam[3]
	}
	console.log(sendData);
	$.ajax({
		"url" : API_URL + "/api/get/listings/searchArea?token=public",
		"method" : "GET",
		"data" : sendData,
		success : function(data){
			data = JSON.parse(data);
			let preparedData = prepareDataforMarker(data);
			senseMap.addAllMarkers(preparedData, {defaultPopup:false});
			// data.forEach(element => {
			// 	if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(element.lat_listing, element.long_listing), mapPolygonArea) ){
			// 		temp.push(element);
			// 	}    
			// });
			// Total ini data yang sudah menghilangkan data2 yg tidak diperlukan
			// console.log(temp);    
			// data['data'] = temp;                
			// $("#drawsearch>div>div").html("Draw");     
			// clearMarker();
			// showListing(data, 1); 
		}
	});
}

function convertTransactionType(kode_jenis_transaksi){
	switch (kode_jenis_transaksi) {
		case "0":
			return  "jual";
			break;
		case "1":
			return  "sewa";
			break;
		case "2":
			return  "jual/sewa";
			break;
		
		default:
			return "";
			break;
	}
}

$(".leaflet-draw-draw-polygon").click(function (e) { 
	e.preventDefault();
	// console.log("start drawing");
	senseMap.resetAreaParam();
	senseMap.clearDrawnLayer();
});