$(document).ready(function(){
    $("#createlisting").addClass("active");
    loadTipe();
    loadArea();
    // watermarkAll(".marked");
});

  /**
   * Function to watermark images, all src will be reloaded as base64 image
   * 
   * @param {*} myselector use this param to select which image u will 
   * change the image consider using "marked" class for standarization
   */
  function watermarkAll(myselector){
    $(myselector).each(function() {  
      myObject = this;
      //console.log(imgsrc);
      var oriImgSrc = this.src;
      var label = $(this).attr("label");
  
      watermark([this.src])
      .dataUrl(watermark.text.center(label, '48px sans-serif', '#fff', 0.5))
      .load(['/assets/img/logo.png'])
      .dataUrl(watermark.image.lowerRight(0.5))
      .then(function (url) {
        imgsrc=url;
        //alert(url);
        console.log($("img[src$='"+oriImgSrc+"']").attr("src",url));
        //console.log(oriImgSrc);
      });
      
      //console.log(imgsrc);
     });  
  }
  
  var listImage = [];
  var index = 0;
  
  function next(to){
    if (listImage.length>1) {
        index+= to;
        index = index % listImage.length;
        if (index<0) {
            index=listImage.length-1;
        }
        $("#carousel-preview").css("background-image","url("+listImage[index]+")");
    }
  }
  
  function changeGambar(target){
    index=target;
    $("#carousel-preview").css("background-image","url("+listImage[index]+")");
  }
  
  var loadFile = function(ob,event) {
    $(ob).parent().removeClass("active");
    
    listImage=[];
    var image = document.getElementById('carousel-preview');
    // console.log(event.target.files);
    if (event.target.files.length>0) {
      $("#preview").fadeIn();
    } else {
      $("#preview").fadeOut();
    }
    var dom ="";
    var domCarousel = "";
    
    for (var i = 0; i < event.target.files.length; i++)
    {
      //dom+='<div style="position:relative; height:100px; width:100px; display:inline-flex;">';
      dom+='<img class="flex-4" style="object-fit: cover; padding-right:2px;" onclick="changeGambar('+i+')" src="'+URL.createObjectURL(event.target.files[i])+'" alt="'+event.target.files[i].name+'"  height="100px" width="100px">';
      //dom+= '<span class="bg-red-hover" style="color:Gainsboro; cursor:pointer; position:absolute; right:0px; top:0px; background-color:gray; padding:1px 5px;">X</span>';
      //dom+='</div>';
      
      
                        
                       
                      
  
        //dom += '<img style="cursor: pointer;" onclick="changeGambar('+i+')" src="'+URL.createObjectURL(event.target.files[i])+'" alt="'+event.target.files[i].name+'" height="100px" width="100px">'
        //dom += "<div style='width:100%;height:100%;  ;background-image:url("+URL.createObjectURL(event.target.files[i])+")' class='multiple-items post-image-part' >";
        listImage[listImage.length] = URL.createObjectURL(event.target.files[i]);                      
    }
    $("#thumbnail-preview").html(dom);
    $("#thumbnail-preview").attr("target",0);
    image.style.backgroundImage = "url("+URL.createObjectURL(event.target.files[0])+")";
  };

  function updateTipe(element){
      var idx = $(element).val();
      if (idx == 5){
        $(".non-tanah").attr('hidden', 'hidden');
      }else{
        $(".non-tanah").removeAttr('hidden');
      }
  }

    function updateJenis(element){
        var idx = $(element).val();
        if (idx == 0){
          $("#sec-harga").children().eq(0).removeAttr('hidden');
          $("#sec-harga").children().eq(1).attr('hidden','hidden');
          $("#sec-minim-sewa").attr('hidden','hidden');
        }else if (idx == 1){
          $("#sec-harga").children().eq(0).attr('hidden','hidden');
          $("#sec-harga").children().eq(1).removeAttr('hidden');
          $("#sec-minim-sewa").removeAttr('hidden');
        }else{
          $("#sec-harga").children().eq(0).removeAttr('hidden');
          $("#sec-harga").children().eq(1).removeAttr('hidden');
          $("#sec-minim-sewa").removeAttr('hidden');
        }
    }

    function updateLuas(){
      var lbr = $("#lebar_tanah").val();
      var pjg = $("#panjang_tanah").val();
      if (lbr!="" && pjg != ""){
        var luas = lbr*pjg;
        $("#luas_tanah").val(luas);
      }
  }

    function updateVideo(element){
 
        var str = $(element).val();       
        str = str.replace("watch?v=","embed/");
        $(element).val(str);
    }
  
  function loadArea(){
    $.post({
        "url" : API_URL + "/api/get/areas/getAll?token=public",
        success : function(data){
            data = JSON.parse(data);
            console.log(data);
            data.forEach(e => {
                $('#sec-area').append('<option value="'+e.id_area+'">'+e.nama_area+'</option>');
            });
            $('#sec-area').selectpicker('refresh');
        }
    });
  }

  function loadTipe(){
    $.post({
        "url" : API_URL + "/api/get/tipes/getAll?token=public",
        success : function(data){
            data = JSON.parse(data);
            console.log(data);
            data.forEach(e => {
                $('#sec-tipe').append('<option value="'+e.id_tipe+'">'+e.nama_tipe+'</option>');
            });
            $('#sec-tipe').selectpicker('refresh');
        }
    });
  }

  

  function dropRelease(el, e){
    
    if($(el).hasClass("active")){
      $(el).removeClass("active");
    }
  }
  function dropHere(el, e){
    
    if(!$(el).hasClass("active")){
      $(el).addClass("active");
    }
  }

  /*
  function insertListing(){
    $("form#createListing").submit(function(event){
      event.preventDefault();
      var sendData = $(this).serialize();
      $.post({
        "url" : "/api/get/listing/insert?token=Admin",
        data: {data:JSON.stringify(sendData)},
        success : function(data){
          // window.location.href="home";
        }
      });
    })
  }*/