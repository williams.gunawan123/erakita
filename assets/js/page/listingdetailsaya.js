$(document).ready(function(){
    initChart();
    initSlider();
    
	pagination = new Paginate(".paginate", 10, 1, function(){});
	pagination.addActiveItemClass(["bg-red txt-white"]);
	pagination.addItemClass(["clickable"]);
    pagination.viewExample(0);
    
    loadMedia();
});

function readPostMedia(){
    let idlisting = $("#id-listing").val();
    $.ajax({
        "url" : API_URL + "/api/get/medias/readCustomer?token=customer&listing="+idlisting,
        success : function(data){

        }
    });
}

function loadMedia(){
    let idlisting = $("#id-listing").val();
    $("#badge-admin").attr("hidden", "hidden");
    let ctrMedia = 0;
    $.ajax({
        "url" : API_URL + "/api/get/medias/getPostByListing?token=customer&listing="+idlisting,
        success : function(data){
            data = JSON.parse(data);
            let row = "";

            data.data.forEach(sosmed =>{
                if (sosmed.status_read_cust_pemilik == 0){
                    ctrMedia++;
                }
                row += `<div class="py-2 px-2 mx-0 row border-bottom" style="background-color:var(--bg-read-`+sosmed.status_read_cust_pemilik+`)">`;
                row += `    <div class="col-1 squared div-image" style="width:50px; background-image:url('/assets/img/media-logo/`+sosmed.gambar+`')">`;
                row += `        <!-- <a href="javascript:void(0)" class="show-on-hoverParent"><i class="fa fa-pencil w-100 txt-center"></i></a> -->`;
                row += `    </div>`;
                row += `    <div class="col-9 px-1">`;
                row += `        <div>`;
                row += sosmed.nama;
                row += `            <!-- <i class="fa fa-pencil" aria-hidden="true"></i> -->`;
                row += `        </div> `;
                row += `        <div>`;
                row += `            <a href="http://`+sosmed.url_post+`" target="_blank">`+sosmed.url_post+`</a> `;
                row += `            <!-- <i class="fa fa-pencil" aria-hidden="true"></i> -->`;
                row += `        </div>`;
                row += `    </div>`;
                row += `    <div class="col-2 text-right label px-1 align-self-center">`+diffDay(sosmed.waktu_post)+`</div>`;
                row += `</div>    `;

            });
            $("#listMedia").html(row);
            if (ctrMedia > 0){
                $("#badge-admin").removeAttr("hidden");
                $("#badge-admin").html(ctrMedia);
            }
        }
    });
    
}

function initChart() {
    if ($('#m_chart_trends_stats').length == 0) {
        return;
    }

    var ctx = document.getElementById("m_chart_trends_stats").getContext("2d");

    var gradient = ctx.createLinearGradient(0, 0, 0, 240);
    gradient.addColorStop(0, Chart.helpers.color('#00c5dc').alpha(0.7).rgbString());
    gradient.addColorStop(1, Chart.helpers.color('#f2feff').alpha(0).rgbString());

    var config = {
        type: 'line',
        data: {
            labels: [
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
                "January", "February", "March", "April"
            ],
            datasets: [{
                label: "Sales Stats",
                backgroundColor: gradient, // Put the gradient here as a fill color
                borderColor: '#0dc8de',

                pointBackgroundColor: Chart.helpers.color('#ffffff').alpha(0).rgbString(),
                pointBorderColor: Chart.helpers.color('#ffffff').alpha(0).rgbString(),
                pointHoverBackgroundColor: mApp.getColor('danger'),
                pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.2).rgbString(),

                //fill: 'start',
                data: [
                    20, 10, 18, 15, 26, 18, 15, 22, 16, 12,
                    12, 13, 10, 18, 14, 24, 16, 12, 19, 21,
                    16, 14, 21, 21, 13, 15, 22, 24, 21, 11,
                    14, 19, 21, 17
                ]
            }]
        },
        options: {
            title: {
                display: false,
            },
            tooltips: {
                intersect: false,
                mode: 'nearest',
                xPadding: 10,
                yPadding: 10,
                caretPadding: 10
            },
            legend: {
                display: false
            },
            responsive: true,
            maintainAspectRatio: false,
            hover: {
                mode: 'index'
            },
            scales: {
                xAxes: [{
                    display: false,
                    gridLines: false,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: false,
                    gridLines: false,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    },
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            elements: {
                line: {
                    tension: 0.19
                },
                point: {
                    radius: 4,
                    borderWidth: 12
                }
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 5,
                    bottom: 0
                }
            }
        }
    };

    var chart = new Chart(ctx, config);
}