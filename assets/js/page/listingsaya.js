$(document).ready(function(){

    initChart();
    
});
function initChart() {
    // Init chart instances
    _initSparklineChart($('#m_chart_view'), [10, 20, -5, 8, -20, -2, -4, 15, 5, 8], mApp.getColor('danger'), 2);
    _initSparklineChart($('#m_chart_wa'), [20, 10, 15, 8, 20, 21, 7, 0, 25, 18], mApp.getColor('danger'), 2);
    _initSparklineChart($('#m_chart_telp'), [10, 20, -5, 8, 20, 21, 7, 15, 5, 8], mApp.getColor('danger'), 2);
    _initSparklineChart($('#m_chart_fav'), [10, 15, 8, 20, 21, 20, -5, 8, -20, 8], mApp.getColor('danger'), 2);
    _initSparklineChart($('#m_chart_wishlist'), [10, 20, -5, 8, -20, -2, -4, 15, 5, 8], mApp.getColor('danger'), 2);
    _initSparklineChart($('#m_chart_share'), [10, 20, -5, 8, -20, -2, -4, 15, 5, 8], mApp.getColor('danger'), 2);
    _initSparklineChart($('#m_chart_xout'), [20, 10, 15, 8, 20, 21, 7, 0, 25, 18], mApp.getColor('danger'), 2);
    _initSparklineChart($('#m_chart_sales_by_apps_2_1'), [10, 20, -5, 8, -20, -2, -4, 15, 5, 8], mApp.getColor('danger'), 2);
    _initSparklineChart($('#m_chart_sales_by_apps_2_2'), [2, 16, 0, 12, 22, 5, -10, 5, 15, 2], mApp.getColor('metal'), 2);
    _initSparklineChart($('#m_chart_sales_by_apps_2_3'), [15, 5, -10, 5, 16, 22, 6, -6, -12, 5], mApp.getColor('brand'), 2);
    _initSparklineChart($('#m_chart_sales_by_apps_2_4'), [8, 18, -12, 12, 22, -2, -14, 16, 18, 2], mApp.getColor('info'), 2);
}
var _initSparklineChart = function(src, data, color, border) {
    if (src.length == 0) {
        return;
    }

    var config = {
        type: 'line',
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October"],
            datasets: [{
                label: "",
                borderColor: color,
                borderWidth: border,

                pointHoverRadius: 4,
                pointHoverBorderWidth: 12,
                pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                pointHoverBackgroundColor: mApp.getColor('danger'),
                pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                fill: false,
                data: data,
            }]
        },
        options: {
            title: {
                display: false,
            },
            tooltips: {
                enabled: false,
                intersect: false,
                mode: 'nearest',
                xPadding: 10,
                yPadding: 10,
                caretPadding: 10
            },
            legend: {
                display: false,
                labels: {
                    usePointStyle: false
                }
            },
            responsive: true,
            maintainAspectRatio: true,
            hover: {
                mode: 'index'
            },
            scales: {
                xAxes: [{
                    display: false,
                    gridLines: false,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: false,
                    gridLines: false,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    },
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },

            elements: {
                point: {
                    radius: 4,
                    borderWidth: 12
                },
            },

            layout: {
                padding: {
                    left: 0,
                    right: 10,
                    top: 5,
                    bottom: 0
                }
            }
        }
    };

    return new Chart(src, config);
}
var page = 0;
function loadListing(status){
    $.ajax({
		"url" : API_URL + "/api/get/listings/getByCust?token=customer&cust=" + userID + "&limit=10&offset=" + page + "&status=" + status,
        success : function(data){
          data = JSON.parse(data);
          var row = "";
		  data.data.forEach(e => {
                row += `<div class="listing-list row" style='border: 1px solid #00000012;'>`;
                row += `    <div class="flex flex-hor" data-slidein="x1">`;
                row += `        <div class="listing-list-photo col-sm-12 col-md-2" style="background-image:url('/assets/img/listing/`+e.id_listing+`/1.jpg');"></div>`;
                row += `        <div class="listing-list-contain col-sm-12 col-md-10 row" style="margin-left:0;padding:10px 0px;">`;
                row += `            <div class="col-lg-3 col-sm-12 col-md-3 listing-list-contain-part part-main">`;
                row += `                <a href="/listingsaya/detail/`+e.id_listing+`" class="main-part-title truncate hoverable txt-black">`+e.judul_listing+`</a>`;
                row += `                <div class="main-part-content">`;
                row += `                    <div class="main-part-address">`+e.alamat_listing+`</div>`;
                row += `                    <div class="main-part-information">`;
                row += `                        <div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>`+e.kamar_tidur+`</div>`;
                row += `                        <div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>`+e.kamar_mandi+`</div>`;
                row += `                        <div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>`+e.luas_tanah+`m<sup>2</sup></div>`;
                row += `                        <div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>`+e.luas_bangunan+`m<sup>2</sup></div>`;
                row += `                    </div>`;
                row += `                </div>`;
                row += `            </div>`;
                row += `            <div class="col-lg-2 col-sm-12 col-md-2 listing-list-contain-part text-center">`;
                row += e.alamat_listing;
                row += `            </div>`;
                row += `            <div class="col-lg-1 col-sm-12 col-md-1 listing-list-contain-part">`;
                row += `                Jakarta Selatan`;
                row += `            </div>`;
                row += `            <div class="col-lg-1 col-sm-12 col-md-1 listing-list-contain-part">`;
                row += shortNumber(e.harga_jual);
                row += `            </div>`;
                row += `            <div class="col-lg-2 col-sm-12 col-md-2 listing-list-contain-part text-center">`;
                row += `                3 Dec 2018`;
                row += `            </div>`;
                row += `            <div class="col-lg-2 col-sm-12 col-md-2 listing-list-contain-part text-center">`;
                row += e.start_listing;
                row += `            </div>`;
                // row += `            <div class="col-lg-1 listing-list-contain-part part-dropdown col-sm-12 col-md-1">`;
                // row += `                <div class="dropdown">	`;
                // row += `                    <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"> `;
                // row += `                        <i class="la la-ellipsis-h"></i>`;
                // row += `                    </a>`;
                // row += `                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(-186px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">						    	`;
                // row += `                        <a class="dropdown-item" href="#"><i class="la la-edit"></i> Preview</a>`;
                // row += `                        <a class="dropdown-item" href="#"><i class="fab fa-whatsapp"></i> Whatsapp</a>`;
                // row += `                        <a class="dropdown-item" href="#"><i class="la la-phone"></i> Telepon</a>`;
                // row += `                    </div>						`;
                // row += `                </div>`;
                // row += `            </div>`;
                row += `        </div>`;
                row += `    </div>`;
                row += `    <div style="display:none;" id='x1' class="minimalistScrollbar flex flex-hor txt-bold padding-s flex-equal flex-1" style="overflow-x:auto;">`;
                row += `        <div class="flex flex-hor padding-s" >`;
                row += `            <div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">`;
                row += `                <span class="fa fa-eye">`;
                row += `            </div>`;
                row += `            <div class="flex flex-hor flex-equal">`;
                row += `                <div class="flex flex-ver" style="flex-grow:1" align="center">`;
                row += `                    <div style="color: rgb(175, 175, 175);">View</div>`;
                row += `                    <div class="txt-bold" style="font-size: 1.5em;">1678</div>`;
                row += `                    <div><a href="#">(43 baru)</a></div>`;
                row += `                    <div class="padding-m"> `;
                row += `                        <div class="m-widget11__chart" style="height:50px; width: 100px">`;
                row += `                            <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">`;
                row += `                                <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
                row += `                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>`;
                row += `                                </div>`;
                row += `                                <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
                row += `                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>`;
                row += `                                </div>`;
                row += `                            </div>`;
                row += `                            <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>`;
                row += `                            <canvas id="m_chart_view" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>`;
                row += `                        </div>									`;
                row += `                    </div>`;
                row += `                </div>`;
                row += `            </div>`;
                row += `        </div>`;
                row += `        <div class="flex flex-hor padding-s">`;
                row += `            <div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">`;
                row += `                <span class="fa fa-whatsapp">`;
                row += `            </div>`;
                row += `            <div class="flex flex-hor flex-equal">`;
                row += `                <div class="flex flex-ver" style="flex-grow:1" align="center">`;
                row += `                    <div style="color: rgb(175, 175, 175);">Whatsapp</div>`;
                row += `                    <div class="txt-bold" style="font-size: 1.5em;">1678</div>`;
                row += `                    <div><a href="#">(43 baru)</a></div>`;
                row += `                    <div class="padding-m"> `;
                row += `                        <div class="m-widget11__chart" style="height:50px; width: 100px">`;
                row += `                            <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">`;
                row += `                                <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
                row += `                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>`;
                row += `                                </div>`;
                row += `                                <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
                row += `                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>`;
                row += `                                </div>`;
                row += `                            </div>`;
                row += `                            <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>`;
                row += `                            <canvas id="m_chart_wa" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>`;
                row += `                        </div>									`;
                row += `                    </div>`;
                row += `                </div>`;
                row += `            </div>`;
                row += `        </div>`;
                row += `        <div class="flex flex-hor padding-s">`;
                row += `            <div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">`;
                row += `                <span class="fa fa-phone">`;
                row += `            </div>`;
                row += `            <div class="flex flex-hor flex-equal">`;
                row += `                <div class="flex flex-ver" style="flex-grow:1" align="center">`;
                row += `                    <div style="color: rgb(175, 175, 175);">Telepon</div>`;
                row += `                    <div class="txt-bold" style="font-size: 1.5em;">1678</div>`;
                row += `                    <div><a href="#">(43 baru)</a></div>`;
                row += `                    <div class="padding-m"> `;
                row += `                        <div class="m-widget11__chart" style="height:50px; width: 100px">`;
                row += `                            <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">`;
                row += `                                <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
                row += `                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>`;
                row += `                                </div>`;
                row += `                                <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
                row += `                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>`;
                row += `                                </div>`;
                row += `                            </div>`;
                row += `                            <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>`;
                row += `                            <canvas id="m_chart_telp" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>`;
                row += `                        </div>									`;
                row += `                    </div>`;
                row += `                </div>`;
                row += `            </div>`;
                row += `        </div>`;
                row += `        <div class="flex flex-hor padding-s">`;
                row += `            <div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">`;
                row += `                <span class="fa fa-heart">`;
                row += `            </div>`;
                row += `            <div class="flex flex-hor flex-equal">`;
                row += `                <div class="flex flex-ver" style="flex-grow:1" align="center">`;
                row += `                    <div style="color: rgb(175, 175, 175);">Favorit</div>`;
                row += `                    <div class="txt-bold" style="font-size: 1.5em;">1678</div>`;
                row += `                    <div><a href="#">(43 baru)</a></div>`;
                row += `                    <div class="padding-m"> `;
                row += `                        <div class="m-widget11__chart" style="height:50px; width: 100px">`;
                row += `                            <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">`;
                row += `                                <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
                row += `                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>`;
                row += `                                </div>`;
                row += `                                <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
                row += `                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>`;
                row += `                                </div>`;
                row += `                            </div>`;
                row += `                            <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>`;
                row += `                            <canvas id="m_chart_fav" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>`;
                row += `                        </div>									`;
                row += `                    </div>`;
                row += `                </div>`;
                row += `            </div>`;
                row += `        </div>`;
                row += `    </div>`;
                row += `</div>`;
          });
          $("#listListing").html(row);
        }
	});
}

$(document).ready(function(){
    loadListing(1);
});