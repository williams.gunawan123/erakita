var limit = 9;
var page = 1;
var activeListing = -1;
var selectedCenterMap = -1;
var buildingsMarkersArray = [];

// when you are done creating markers then
var clusterOptions = {
     imagePath: '/assets/img/marker/m'
};
var clusterGroupIndividualMarkers;
var pagination = null;
var forceFitBounds = false;
var mapMode = 0;
var senseMap = new SensecodeMap();
/** Using Sensecode-Map.js */
senseMap.initMap("mapDiv");
senseMap.addOnClickMap(function(){
  // this is a function i think
  // console.log("mapCLicked");
});

/**
 * SenseMap Ke - 0 == All Marker
 */
senseMap.addClusterGroup({
  "iconCreateFunction" : function(cluster){
    var childMarkers = cluster.getAllChildMarkers();
    if (senseMap.checkMarkerSeparate(childMarkers)) {
      return L.divIcon({
        html: "<div class='cluster-inner'><div>" + cluster.getChildCount() + "</div></div>",
        className: 'cluster-container',
        iconSize: L.point(40, 40)
      });
    } else {
      let domIcon =  `<div class='erakita-marker-label'>
                <b>`+ cluster.getChildCount() +` Unit</b>
              </div>
            `;
      return L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing-cluster', html: domIcon });
    }
  }
});
/**
 * SenseMap Ke - 1 == Showed Marker
 */
senseMap.addClusterGroup();
var privateMarkerContainer = senseMap.addClusterGroup();

senseMap.addOnHoverCluster(senseMap.clusterGroups[0], clusterOnHover);
senseMap.addOnMouseOutCluster(senseMap.clusterGroups[0], clusterOnMouseOut);
senseMap.addOnMouseClickCluster(senseMap.clusterGroups[0], clusterOnMouseClick);
senseMap.initDrawable();
senseMap.addNewButtons({
  "innerHTML" : "Reset",
  "onClick" : function(){
    senseMap.resetAreaParam();
    senseMap.clearDrawnLayer();
    loadSenseMapMarker();
	senseMap.drawnStatus = false;
  }
});

/** Customized function for listing Page */
clusterGroupIndividualMarkers = senseMap.addClusterGroup(); 

$(document).ready(function () {
  loadSenseMapMarker();
});
      
var pageHandler = function pageHandling(){
  page = pagination.getCurrentPage();
  console.log(page)
	updateShowedListing();
}

function updateShowedListing(){
  var dataShowed = senseMap.markersLists[1].slice((page-1)*limit, (page-1)*limit+limit);
  console.log(dataShowed)
	var row = "";
	var row2 = "";
	dataShowed.forEach(el => {
		var e = el.listing;
		var gambar = [];
		for (var i=1; i<=e.gambar; i++){
			gambar.push("/assets/img/listing/"+e.id_listing+"/"+i+".jpg");
		}
		gambar = JSON.stringify(gambar);
		if (e.jenis_transaksi == "0"){
			var jenis = "Jual";
			var bgJenis = "jual";
			var harga = shortNumber(e.harga_jual);
		}else if (e.jenis_transaksi == "1"){
			var jenis = "Sewa";
			var bgJenis = "sewa";
			var harga = shortNumber(e.harga_sewa) + "/thn";
		}else{
			var jenis = "Jual Sewa";
			var bgJenis = "jual_sewa";
			var harga = shortNumber(e.harga_jual) + " - " + shortNumber(e.harga_sewa) + "/thn";;
		}
		var favorite = "";
		if (e.favorite==1){
			favorite = "favorite";
		}
		row += `	<div class="col-lg-4 col-sm-12 mb-3 px-2">`;
		row += `		<div onclick='selectListingCard(this);' class=" mb-0 listing-item m-portlet m-portlet--rounded hoverable" data-idx='`+el.indexMarker+`' data-id='`+e.id_listing+`' data-telp='`+e.telp_broker+`' data-judul='`+e.judul_listing+`' data-lat='`+e.lat_listing+`' data-lng='`+e.long_listing+`' data-tipe='`+e.nama_tipe+`' data-jenis='`+jenis+`'>`;
		row += `			<div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='`+gambar+`'>`;
		row += `				<div class="listing-slider-badge jenis-`+bgJenis+`">` + jenis + `</div>`;
		row += `				<div class="listing-slider-icon jenis-`+bgJenis+`"><i class="fas fa-home"></i></div>`;
		row += `				<div class="listing-slider-counter" style="z-index:2;">1 of 3</div>`;
		row += `			</div>`;
		row += `			<div class="m-portlet__body" style="padding:0px;">`;
		row += `				<div class="col-lg-10 col-md-10 col-sm-12" style="padding: 10px 5px 10px 10px;">`;
		row += `					<div class="listing-price listing-info">` + harga + `</div>`;
		row += `					<a href="/detaillisting/view/`+e.id_listing+`" class="listing-title listing-info truncate hoverable txt-black"><b>` + e.judul_listing + `</b></a>`;
		row += `					<div class="listing-jalan listing-info truncate">` + e.alamat_listing + `</div>`;
		row += `					<div class="listing-kota listing-info truncate">` + e.nama_kecamatan + `, ` + e.nama_kota + `</div>`;
		row += `					<div class="listing-detail">`;
		if (e.kamar_tidur != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>`+e.kamar_tidur+`</div>`;
		}
		if (e.kamar_mandi != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>`+e.kamar_mandi+`</div>`;
		}
		if (e.luas_tanah != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>` + e.luas_tanah + `m<sup>2</sup></div>`;
		}
		if (e.luas_bangunan != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>` + e.luas_bangunan + `m<sup>2</sup></div>`;
		}
		row += `					</div>`;
		row += `				</div>`;
		row += `				<div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding:10px 10px 10px 5px">`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='sendWA(this);' class="fab fa-whatsapp fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Pesan WhatsApp"></i></div>`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='favorite(this);' class="fas fa-heart fa-lg ` + favorite + `" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Favorit"></i></div>`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='shareLink(this);' class="fas fa-share fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Bagikan"></i></div>`;
		row += `					<a href="/detaillisting/view/`+e.id_listing+`" style="margin:auto;" class="listing-content-icon txt-black"><i class="fas fa-ellipsis-h fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Tampilkan Lebih Banyak"></i></a>`;
		row += `				</div>`;
		row += `			</div>`;
		row += `		</div>`;
		row += `</div>`

		row2 += `<tr class="hoverable listing-item" onclick="selectListingCard(this);" data-idx="`+el.indexMarker+`" data-id="`+e.id_listing+`">`;
		row2 += `<td style="text-align:left;">`+e.judul_listing+`</td>`;
		row2 += `<td style="text-align:left;">`+e.alamat_listing+`</td>`;
		row2 += `<td>`+shortNumber(e.harga_jual)+`</td>`;
		row2 += `<td>`+e.kamar_tidur+`</td>`;
		row2 += `<td>`+e.kamar_mandi+`</td>`;
		row2 += `<td>`+e.luas_bangunan+` m<sup>2</sup></td>`;
		row2 += `<td>`+e.luas_tanah+` m<sup>2</sup></td>`;
		row2 += `</tr>`;
	});
	$("#tabs-foto").html(row);
	$("#table-data").html(row2);
	initSlider();
	mApp.initPopovers();
}

function selectListingCard(ob, unit=false){
	$(".listing-item.active").removeClass("active");
	let idx = $(ob).attr("data-idx");
	$(".listing-item[data-idx='"+idx+"']").addClass("active");
	if (!unit){
		showSeparateMarker(idx);
		// changeTabTableDetail(idx);
	}
}

var lastSeparateMarket = undefined;
function showSeparateMarker(markerIndex){
	if (listingActive != -1){
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
	}
	if (lastSeparateMarket !== undefined || markerIndex == null) {
		//publicMarkerContainer.clusterGroup.addLayer(senseMap.markersLists[0][lastSeparateMarket]);
		if (clusterGroupIndividualMarkers.markerList[0] != null){
			privateMarkerContainer.clusterGroup.removeLayer(clusterGroupIndividualMarkers.markerList[0]);
		}	
		clusterGroupIndividualMarkers.markerList = [];
	} 
	if (markerIndex != null) {
		let marker = senseMap.markersLists[0][markerIndex];
		clusterGroupIndividualMarkers.markerList.push(L.marker(marker._latlng, {icon : marker.options.icon}))
		privateMarkerContainer.clusterGroup.addLayer(clusterGroupIndividualMarkers.markerList[0]);
		// publicMarkerContainer.clusterGroup.removeLayer(senseMap.markersLists[0][markerIndex]);
		lastSeparateMarket = markerIndex;
		listingActive = markerIndex;
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().addClass("activeMarker");
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().css("z-index", 9999);
	}
}

function clusterIconCreate(cluster){
	var childMarkers = cluster.getAllChildMarkers();
	var n = 0;
	if (senseMap.checkMarkerSeparate(childMarkers)) {
		return L.divIcon({
			html: "<div class='cluster-inner'><div>" + cluster.getChildCount() + "</div></div>",
			className: 'cluster-container',
			iconSize: L.point(40, 40)
		});
	} else {
		let domIcon =  `<div class='erakita-marker-label'>
							<b>`+ cluster.getChildCount() +` Unit</b>
						</div>
					`;
		return L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing-cluster', html: domIcon });
	}
}

function clickUnit(e){
	if (listingActive != -1){
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
	}
	listingActive = $(e).attr("data-idx");
	let idxShowed = $(e).attr("idx-showed");;
	pagination.setCurrentPage(Math.floor((idxShowed)/10)+1);
	var tempCardActive = $(".listing-item[data-idx='"+listingActive+"']");
	selectListingCard(tempCardActive, true);
	listingActive = -1;
	$('#tabs-foto').animate({
		scrollTop: $(tempCardActive).offset().top + $('#tabs-foto').scrollTop() - $(tempCardActive).offset().bottom
	}, 500);
}

function clusterOnHover(cluster){
	childData = (getChildData(cluster.layer.getAllChildMarkers()));
	let offsetY = -10;
	if (childData.separate) {
		senseMap.clusterGroups[0].options.zoomToBoundsOnClick = true;
		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: false, position : "s" })
		.setContent("Melihat " + cluster.layer._childCount +' Listing <br> Listing dari harga ' + shortNumber(childData.minPrice) + " - " + shortNumber(childData.maxPrice) + " dengan rata-rata harga " + shortNumber(childData.avgPrice))
		.setLatLng(cluster.latlng)
		.openOn(senseMap.map);
	} else {
		senseMap.clusterGroups[0].options.zoomToBoundsOnClick = false;
	}
}

function clusterOnMouseOut(cluster){
	if (childData.separate) {
		senseMap.map.closePopup();
	}
}
function clusterOnMouseClick(cluster){
	senseMap.map.closePopup();
	if (!childData.separate) {
		offsetY = -18;
		let domPopup = `<div class='flex flex-ver' style='width:320px;'>
							<div class="flex flex-ver" style='border-bottom: 2px solid var(--gray); margin-bottom: 10px;'>
								<div class='label' style='font-size:1.25em;'>`+ c.layer._childCount +` for sale</div>`;
								// <a href='javascript:void(0)' class='my-2' style='font-size:1.25em;'><i class="fa fa-lock"></i> Sign in for more details</a>
		domPopup += `	</div>
							<div class='map-listing-item-container minimalistScrollbar' style='max-height:188px; overflow-y:auto;'>`;

		childData.listings.forEach(listing => {	
			domPopup +=			`<div class='flex flex-hor flex-vertical-center flex-separate py-2'>
									<div class='squared div-image ratio69' image='default' style='width:37%;'></div>
									<div class="flex flex-ver" style='width:58%;'>
										<div>Unit di`+ convertTransactionType(listing.jenis_transaksi) +`kan</div>
										<div><b>`+ listing.judul_listing +`</b></div>
										<div>Rp. `+  shortNumber(listing.harga_jual) +`</div>
										<div>`+listing.kamar_tidur+` bed.`+ listing.kamar_mandi +` bath. <i class="fa fa-lock"></i> sq.ft.</div>
									</div>
								</div>`;
		});
		domPopup += 		`</div>
						</div>`;
		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: true, position : "s" })
		.setContent(domPopup)
		.setLatLng(cluster.latlng)
		.openOn(senseMap.map);
		// c.originalEvent.preventDefault();
		// map.fitBounds(map.getBounds());
	}
}


function getChildData(childs){
	let childData = {maxPrice: 0, minPrice : null, listings : []};
	let sumPrice = 0;
	childs.forEach(el => {
		el.listing.indexMarker = el.indexMarker;
		el.listing.idxShowed = el.idxShowed;
		childData.listings.push(el.listing);
		if (childData.minPrice == null || el.listing.harga_jual < childData.minPrice ) {
			childData.minPrice = el.listing.harga_jual;
		}
		if (el.listing.harga_jual > childData.maxPrice ) {
			childData.maxPrice = el.listing.harga_jual;
		}
		sumPrice += parseFloat(el.listing.harga_jual);
	});
	console.log(sumPrice, childs.length);
	childData.avgPrice = sumPrice / childs.length;
	childData.separate = senseMap.checkMarkerSeparate(childs);
	return childData;
}

var requestPage = null;
var listingActive = -1;

var waitZoom = 500;
var zoomPromise;
function updateZoomView(){
	if (senseMap.drawnStatus == false){
		showSeparateMarker(null);
		page = 1;
		senseMap.markersLists[1] = [];
		var idxShowed = 0;
		senseMap.markersLists[0].forEach(e => {
			if(senseMap.map.getBounds().contains(e.getLatLng())){
				senseMap.markersLists[1].push(e);
				e.idxShowed = idxShowed++;
			}
		});
		pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
		pagination.addActiveItemClass(["bg-red txt-white"]);
		pagination.addItemClass(["clickable"]);
		pagination.viewExample(0);
		$("#showedListing").html(senseMap.markersLists[1].length);
		updateShowedListing();
	}
}

senseMap.map.on('draw:created', function(e) {
	let polygon = L.polygon(senseMap.drawnLayers[senseMap.drawnLayers.length-1].getLatLngs());
	showSeparateMarker(null);
	page = 1;
	senseMap.markersLists[1] = [];
	var idxShowed = 0;
	senseMap.markersLists[0].forEach(e => {
		if(polygon.contains(e.getLatLng())) {
			e.idxShowed = idxShowed++;
			senseMap.markersLists[1].push(e);
		}else{
			senseMap.clusterGroups[0].removeLayer(e);
		}
	});
	senseMap.drawnStatus = true;
	senseMap.map.fitBounds(polygon.getBounds());

	// drawData = "&draw=";
	// polygon.getLatLngs()[0].forEach(e =>{
	// 	drawData += e.lat + "," + e.lng + ";"; 
	// });
	// var newUrl = "/listing?"+sendData+drawData+sortData+boundData;
	// window.history.pushState(data, "EraKita | Listing", newUrl);

	pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
	pagination.addActiveItemClass(["bg-red txt-white"]);
	pagination.addItemClass(["clickable"]);
	pagination.viewExample(0);
	$("#totalListing").html(senseMap.markersLists[1].length);
	$("#showedListing").html(senseMap.markersLists[1].length);
	updateShowedListing();
});

senseMap.map.on('zoomend', function(e) {
	updateZoomView();
	clearTimeout(zoomPromise);
	zoomPromise = setTimeout(updateZoomView(), waitZoom);
});

senseMap.map.on('dragend', function(e) {
	updateZoomView();
	clearTimeout(zoomPromise);
	zoomPromise = setTimeout(updateZoomView(), waitZoom);
});

function addEventClickMarker(){
	senseMap.markersLists[0].forEach(element =>{
		$(element).click(function (e){
			if (listingActive != -1){
				$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
			}
			listingActive = $(this)[0].indexMarker;
			let idxShowed = listingActive;
			if ($(this)[0].idxShowed != null){idxShowed=$(this)[0].idxShowed}
			pagination.setCurrentPage(Math.floor((idxShowed)/limit)+1);
			var tempCardActive = $(".listing-item[data-idx='"+listingActive+"']");
			selectListingCard(tempCardActive);
			$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().addClass("activeMarker");
			$('#tabs-foto').animate({
				scrollTop: $(tempCardActive).offset().top +  $('#tabs-foto').scrollTop() - $('#tabs-foto').offset().top
			}, 500);
		})
	});
}

function loadSenseMapMarker(){
	page = 1;
	senseMap.markersLists[0].forEach(element => {
		senseMap.clusterGroups[0].removeLayer(element);
	});
	senseMap.markersLists[0] = []; 
	senseMap.markersLists[1] = []; 
  showSeparateMarker(null);
  var broker = $("#databroker").attr("data-id");
	mApp.block("#mapDiv", {});
    $.ajax({
		"url" : API_URL + "/api/get/listings/getActive?token=public&user_cust=" + userID + "&user_broker="+broker + sortData,
        success : function(data){
			data = JSON.parse(data);

			// var newUrl = "/listing?"+sortData;
			// window.history.pushState(data, "EraKita | Listing", newUrl);
			$("#totalListing").html(data.data.length);
			$("#showedListing").html(data.data.length);

			let preparedData = prepareDataforMarker(data.data);
			senseMap.addAllMarkers(preparedData, {defaultPopup:false});
			senseMap.markersLists[1] = senseMap.markersLists[0];
			addEventClickMarker();
			pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
			pagination.addActiveItemClass(["bg-red txt-white"]);
			pagination.addItemClass(["clickable"]);
			pagination.viewExample(0);
			pagination.setCurrentPage(1);
			mApp.unblock("#mapDiv", {});
			updateShowedListing();
      }
	});
}

function prepareDataforMarker(data){
	let targetData = ("data" in data) ? data.data : data;
	var ctrCard = 0;
	targetData.forEach(element => {
		let domIcon =  `<div class='erakita-marker-label' indexMarker='`+ctrCard+`'>`+shortNumber(element.harga_jual)+`</div>`
		let temp = JSON.stringify({listing : element, idxShowed : ctrCard, indexMarker : ctrCard++});
		element.additionalData = JSON.parse(temp);
		element.lat = element.lat_listing;
		element.lng = element.long_listing;
		element.icon = senseMap.createIcon({
			html: domIcon,
			className : "bg-red txt-white erakita-marker-container leaflet-div-icon tipe-listing"+ element.jenis_transaksi}, true)
	});
	return targetData;
}

function loadListingbySearch(){
	var sendData = {
		atas: senseMap.areaParam[0],
		kanan: senseMap.areaParam[1],
		bawah: senseMap.areaParam[2],
		kiri: senseMap.areaParam[3]
	}
	$.ajax({
		"url" : API_URL + "/api/get/listings/searchArea?token=public",
		"method" : "GET",
		"data" : sendData,
		success : function(data){
			data = JSON.parse(data);
			let preparedData = prepareDataforMarker(data);
			senseMap.addAllMarkers(preparedData, {defaultPopup:false});
			// data.forEach(element => {
			// 	if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(element.lat_listing, element.long_listing), mapPolygonArea) ){
			// 		temp.push(element);
			// 	}    
			// });
			// Total ini data yang sudah menghilangkan data2 yg tidak diperlukan
			// console.log(temp);    
			// data['data'] = temp;                
			// $("#drawsearch>div>div").html("Draw");     
			// clearMarker();
			// showListing(data, 1); 
		}
	});
}

function convertTransactionType(kode_jenis_transaksi){
	switch (kode_jenis_transaksi) {
		case "0":
			return  "jual";
			break;
		case "1":
			return  "sewa";
			break;
		case "2":
			return  "jual/sewa";
			break;
		
		default:
			return "";
			break;
	}
}

$(".leaflet-draw-draw-polygon").click(function (e) { 
	e.preventDefault();
	// console.log("start drawing");
	senseMap.resetAreaParam();
	senseMap.clearDrawnLayer();
	senseMap.drawnStatus = false;
});

function favorite(element){
  if (userID == null){
    Swal.fire({
      type: 'error',
      title: 'Oops...',
      text: 'Harap Login Terlebih Dahulu',
      footer: '<a href="/login">LOGIN DISINI</a>'
    });
  }else{
      var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
      var status = !$(element).hasClass('favorite');
      $(element).toggleClass("favorite");
      $.post({
        "url" : API_URL + "/api/get/customers/updateFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&status=" + Number(status),
        success : function(data){
          data = JSON.parse(data);
          if (!data.status){
            $(element).toggleClass("favorite");
          }
        }
      });
  }
}

function xout(element){
  if (userID == null){
    Swal.fire({
      type: 'error',
      title: 'Oops...',
      text: 'Harap Login Terlebih Dahulu',
      footer: '<a href="/login">LOGIN DISINI</a>'
    });
  }else{
    var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
    var status = !$(element).hasClass('xout');
    $(element).toggleClass("xout");
    $.post({
      "url" : API_URL + "/api/get/customers/updateXout?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&status=" + Number(status),
      success : function(data){
        data = JSON.parse(data);
        if (!data.status){
          $(element).toggleClass("xout");
        }
      }
    });
  }
}

function sendWA(element){
    var judul = $(element).parent().parent().parent().parent().attr('data-judul');
    var telp = $(element).parent().parent().parent().parent().attr('data-telp');
    var pesan = "Halo,%20Saya%20mau%20menanyakan%20mengenai%20listingan%20" + judul;
    var win = window.open("https://api.whatsapp.com/send?phone="+telp+"&text="+pesan, '_blank');
    win.focus();
}

function shareLink(element) {
    /* Get the text field */
    var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
    var text = BASE_URL + "/detaillisting/view/" + idlisting;

    var body = document.getElementsByTagName('body')[0];
    var tempInput = document.createElement('INPUT');
    body.appendChild(tempInput);
    tempInput.setAttribute('value', text)
    tempInput.select();
    document.execCommand('copy');
    body.removeChild(tempInput);

    /* Alert the copied text */
    Swal.fire({
        type: 'success',
        title: 'Sukses',
        text: 'Berhasil disalin ke Clipboard'
    });
    if (userID != null){
      $.post({
        "url" : API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting
      });
    }
}
function loadList(){
    var broker = $("#databroker").attr("data-id");
    mApp.block('#tabs-foto', {});
    $.ajax({
		"url" : API_URL + "/api/get/listings/getActive?token=public&user_cust=" + userID + "&user_broker="+broker+"&limit="+limit+"&offset=" + (page-1)*limit + sortData,
      success : function(data){
        data = JSON.parse(data);
        if (page == 1){
          pagination = new Paginate(".sensecode-paginate", (data.count-1)/limit+1, 1, pageHandler);
          pagination.addActiveItemClass(["bg-red txt-white"]);
          pagination.addItemClass(["clickable"]);
          pagination.viewExample(0);
          $("#totalListing").html(data.count);
        }
        var row="";
        data.data.forEach(e => {
          var gambar = [];
          for (var i=1; i<=e.gambar; i++){
            gambar.push("/assets/img/listing/"+e.id_listing+"/"+i+".jpg");
          }
          gambar = JSON.stringify(gambar);
          if (e.jenis_transaksi == "0"){
            var jenis = "Jual";
            var bgJenis = "jual";
            var harga = shortNumber(e.harga_jual);
          }else if (e.jenis_transaksi == "1"){
            var jenis = "Sewa";
            var bgJenis = "sewa";
            var harga = shortNumber(e.harga_sewa) + "/thn";
          }else{
            var jenis = "Jual Sewa";
            var bgJenis = "jual_sewa";
            var harga = shortNumber(e.harga_jual) + " - " + shortNumber(e.harga_sewa) + "/thn";;
          }
          var favorite = "";
          if (e.favorite==1){
            favorite = "favorite";
          }
          row += `    <div class="col-lg-4 col-sm-12 ">`;
          row += `        <div class="m-portlet m-portlet--rounded" data-id='`+e.id_listing+`' data-telp='`+e.telp_broker+`' data-judul='`+e.judul_listing+`'>`;
          row += `            <div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='`+gambar+`'>`;
          row += `                <div class="listing-slider-badge">`+jenis+`</div>`;
          row += `                <div class="listing-slider-icon"><i class="fas fa-home"></i></div>`;
          row += `                <div class="listing-slider-counter" style="z-index:2;">1 of 3</div>`;
          row += `            </div>`;
          row += `            <div class="m-portlet__body" style="padding:0px;">`;
          row += `                <div class="col-lg-10 col-md-10 col-sm-12" style="padding: 10px 5px 10px 10px;">`;
          row += `                    <div class="listing-price listing-info"><?=$harga?></div>`;
          row += `                    <div class="listing-title listing-info truncate"><b>`+e.judul_listing+`</b></div>`;
          row += `                    <div class="listing-jalan listing-info truncate">`+e.alamat_listing+`</div>`;
          row += `                    <div class="listing-kota listing-info truncate">`+e.nama_kecamatan+`, `+e.nama_kota+`</div>`;
          row += `                    <div class="listing-detail">`;
          if (e.kamar_tidur != null){
            row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>`+e.kamar_tidur+`</div>`;
          }
          if (e.kamar_mandi != null){
            row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>`+e.kamar_mandi+`</div>`;
          }
          if (e.luas_tanah != null){
            row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>` + e.luas_tanah + `m<sup>2</sup></div>`;
          }
          if (e.luas_bangunan != null){
            row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>` + e.luas_bangunan + `m<sup>2</sup></div>`;
          }
          row += `                    </div>`;
          row += `                </div>`;
          row += `                <div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding:10px 10px 10px 5px">`;
          row += `                    <div class="listing-content-icon" style="margin:auto;"><i onclick='sendWA(this);' style="font-size: 1.3em;" class="fab fa-whatsapp" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Pesan WhatsApp"></i></div>`;
          row += `                    <div class="listing-content-icon" style="margin:auto;"><i onclick='favorite(this);' style="font-size: 1.3em;" class="fas fa-heart `+favorite+`" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Favorit"></i></div>`;
          row += `                    <div class="listing-content-icon" style="margin:auto;"><i onclick='shareLink(this);' style="font-size: 1.3em;" class="fas fa-share" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Bagikan"></i></div>`;
          row += `                    <a href="/detaillisting/view/<?=$listingBroker->data[$i]->id_listing?>" style="margin:auto;" class="listing-content-icon txt-black"><i style="font-size: 1.3em;" class="fas fa-ellipsis-h" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Tampilkan Lebih Banyak"></i></a>`;
          row += `                </div>`;
          row += `            </div>`;
          row += `        </div>`;
          row += `    </div>`;
          row += `</div>`;
        });   	
        // $("#showedListing").html(ctrListing+1);
        // $("#totalListing").html(ctrListing+1);
        $("#tabs-foto").html(row);
        mApp.unblock('#tabs-foto', {});
        initSlider();
        mApp.initPopovers();
      }
	});
}
var sort = "startlisting";
var urutan = "desc";
var sortData = "&sort=startlisting-desc";
function urutkanListingBroker(e){
    $("#sort-" + sort).find(".fa.fa-check").remove();
    sort = $(e).attr("data-value");
    $("#sort-" + sort).append(" <i class='fa fa-check'></i>");
    urutan = $("#urutan").find(".active").attr("data-value");
    
    sortData = "&sort=" + sort + "-" + urutan;

    loadSenseMapMarker();
}
function urutkanListingBrokerAZ(temp){
    urutan = temp;
    sortData = "&sort=" + sort + "-" + urutan;

    loadSenseMapMarker();
}