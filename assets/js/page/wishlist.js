$(document).ready(function(){
    // loadList();
});

function loadList(){
    $.ajax({
        "url" :API_URL + "/api/get/wishlists/getByCustomer?token=customer&customer=" + userID,
        success : function(data){
            data = JSON.parse(data);
            // if (page == 1){
            //     pagination = new Paginate(".sensecode-paginate", (data.count-1)/limit+1, 1, pageHandler);
            //     pagination.addActiveItemClass(["bg-red txt-white"]);
            //     pagination.addItemClass(["clickable"]);
            //     pagination.viewExample(0);
            // }
            var row = "";
            var row2 = "";
            var ctr = 1;
            data.data.forEach(e => {
                row += `   <tr>`;
                row += `   <td>`;
                row += `       <a href="#">`+e.label+`</a>`;
                row += `   </td>`;
                row += `   <td>`;
                row += `Rp ` + numberWithCommas(e.minPrice) + ` sd Rp ` + numberWithCommas(e.maxPrice);
                row += `   </td>`;
                row += `   <td>`;
                row += e.minLT + ` sd ` + e.maxLT
                row += `   </td>`;
                row += `   <td>`;
                row += e.minLB + ` sd ` + e.maxLB
                row += `   </td>`;
                row += `   <td>`;
                row += e.minKT + ` sd ` + e.maxKT
                row += `   </td>`;
                row += `   <td>`;
                row += e.minKM + ` sd ` + e.maxKM
                row += `   </td>`;
                row += `   <td>Anda</td>`;
                row += `   <td><i class="txt-md fas fa-pencil-alt margin-col-md"></i> <i class="txt-md fas fa-trash-alt"></i></td>`;
                row += `</tr>   `;


            });
        $("#listWishlist").html(row);
        $("#data-wishlist").html(row2);
        $('.m-accordion').accordion();
      }
    });
}