$(document).ready(function () {
	loadList();
});

function loadList(){
    $.ajax({
        "url" : API_URL + "/api/get/cabangs/getActive?token=public",
        success : function(data){
          data = JSON.parse(data);
          var row = "";
          var ctr = 0;
          data.forEach(e => {
            row += `<tr onclick="updateSideContent('`+e.id_cabang+`');">`;
            row += `    <td class="flex flex-hor txt-bold padding-s flex-equal flex-1">`;
            row += `        <div class="col-sm-3"`;
            row += `            style="vertical-align:middle; background-image: url('/assets/img/erakita-logo-square.png'); background-size: contain; background-repeat: no-repeat;">`;
            row += `        </div>`;
            row += `        <div class="flex flex-ver padding-m" style="flex-grow:1">`;
            row += `            <a href='/cabang/detail/`+e.id_cabang+`' class='txt-black'>`+e.nama_cabang+`</a>`;
            row += `            <table>`;
            row += `            <tr>`;
            row += `                <td><span style="color: #128C7E; font-size: 1.3em;" class="fa fa-whatsapp"></span></td><td>:</td><td style="color: #666F;">+`+e.telp_cabang+`</td>`;
            row += `            </tr>`;
            row += `            <tr>`;
            row += `                <td><span style="color: #2e7d32; font-size: 1.3em;" class="fa fa-phone"></span></td><td>:</td><td style="color: #666F;">+`+e.telp_cabang+`</td>`;
            row += `            </tr>`;
            row += `            </table>`;
            row += `        </div>`;
            row += `    </td>`;
            row += `    <td style="vertical-align:middle;" align="center">`;
            row += `        <div>`+e.jml_listing+`</div>`;
            row += `    </td>`;
            row += `    <td style="vertical-align:middle;">`;

            row += `<div class="m-widget11__chart" style="height:50px; width: 100px">`;
            row += `  <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">`;
            row += `    <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
            row += `      <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>`;
            row += `    </div>`;
            row += `    <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
            row += `      <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>`;
            row += `    </div>`;
            row += `  </div>`;
            row += `  <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>`;
            row += `  <canvas id="m_chart_listing_`+ctr+`" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>`;
            row += `</div>	`;

            row += `    </td>`;
            row += `    <td align="center" style="vertical-align:middle;">`;
            row += `        <div>35</div>`;
            row += `    </td>`;
            row += `    <td style="vertical-align:middle;">`;

            row += `<div class="m-widget11__chart" style="height:50px; width: 100px">`;
            row += `  <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">`;
            row += `    <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
            row += `      <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>`;
            row += `    </div>`;
            row += `    <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">`;
            row += `      <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>`;
            row += `    </div>`;
            row += `  </div>`;
            row += `  <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>`;
            row += `  <canvas id="m_chart_transaksi_`+ctr+`" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>`;
            row += `</div>	`;

            row += `    </td>`;
            row += `    <td align="center" style="vertical-align:middle;">`;
            row += `        <div>`+e.jml_broker+`</div>`;
            row += `    </td>`;
            row += `</tr>`;
            ctr++;
		      });
        $("#listCabang").html(row);
        $("#showedCabang").html(data.length);
        $("#totalCabang").html(data.length);
        initChart(data.length);
      }
    });
}

function updateSideContent(id){
    $.ajax({
      "url" : API_URL + "/api/get/cabangs/getById/"+id+"?token=public",
      success : function(data){
        data = JSON.parse(data);
        $("#side-nama").html(data.nama_cabang);
        $("#side-alamat").html(data.alamat_cabang);
    }
  });
}

function initChart(jml){
    // Init chart instances
    for (var i=0; i<jml; i++){
      _initSparklineChart($('#m_chart_transaksi_'+i), [20, 10, 15, 8, 20, 21, 7, 0, 25, 18], mApp.getColor('danger'), 2);
      _initSparklineChart($('#m_chart_listing_'+i), [10, 20, -5, 8, 20, 21, 7, 15, 5, 8], mApp.getColor('danger'), 2);
    }
}

var _initSparklineChart = function(src, data, color, border) {
  if (src.length == 0) {
      return;
  }

  var config = {
      type: 'line',
      data: {
          labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October"],
          datasets: [{
              label: "",
              borderColor: color,
              borderWidth: border,

              pointHoverRadius: 4,
              pointHoverBorderWidth: 12,
              pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
              pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
              pointHoverBackgroundColor: mApp.getColor('danger'),
              pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
              fill: false,
              data: data,
          }]
      },
      options: {
          title: {
              display: false,
          },
          tooltips: {
              enabled: false,
              intersect: false,
              mode: 'nearest',
              xPadding: 10,
              yPadding: 10,
              caretPadding: 10
          },
          legend: {
              display: false,
              labels: {
                  usePointStyle: false
              }
          },
          responsive: true,
          maintainAspectRatio: true,
          hover: {
              mode: 'index'
          },
          scales: {
              xAxes: [{
                  display: false,
                  gridLines: false,
                  scaleLabel: {
                      display: true,
                      labelString: 'Month'
                  }
              }],
              yAxes: [{
                  display: false,
                  gridLines: false,
                  scaleLabel: {
                      display: true,
                      labelString: 'Value'
                  },
                  ticks: {
                      beginAtZero: true
                  }
              }]
          },

          elements: {
              point: {
                  radius: 4,
                  borderWidth: 12
              },
          },

          layout: {
              padding: {
                  left: 0,
                  right: 10,
                  top: 5,
                  bottom: 0
              }
          }
      }
  };

  return new Chart(src, config);
}