$(document).ready(function () {
	loadList();
});

function loadList(){
    $.ajax({
        "url" : API_URL + "/api/get/developers/getActive?token=public",
        success : function(data){
          data = JSON.parse(data);
		  var row = "";
          data.forEach(e => {
            
            $.ajax({
                "url" : API_URL + "/api/get/proyeks/getActive?token=public&id_dev=" + e.id_developer,
                success : function(dataProyek){
                    dataProyek = JSON.parse(dataProyek);
                    row += `<a href="/developer/detail/`+e.id_developer+`" class="box-developer transition flex-3">`;
                    row += `    <div class="flex-container flex flex-ver">`;
                    row += `        <div class="header flex flex-hor">`;
                    row += `            <div class="brand" style="background-image: url('/assets/img/erakita-logo-square.png');" ></div>`;
                    row += `            <div class="brand-container">`;
                    row += `                <div class="brand-title">`+e.nama_developer+`</div>`;
                    row += `                <div>`+e.alamat_developer+`</div>`;
                    row += `            </div>`;
                    row += `        </div>`;
                    row += `        <div class="desc">`;
                    row += e.deskripsi_developer;
                    row += `        </div>`;
                    row += `        <div class="flex flex-ver project-container">`;
                    dataProyek.forEach(e2 => {
                        row += `            <div class="flex flex-hor project-box">`;
                        row += `                <div class="project-icon" style="background-image: url('https://techcrunch.com/wp-content/uploads/2018/07/logo-2.png');" ></div>`;
                        row += `                <div class="project-title-container">`;
                        row += `                    <div class="sub-brand">`+e2.nama_proyek+`</div>`;
                        row += `                    <div class="sub-brand-desc">`+e2.deskripsi_proyek+`</div>`;
                        row += `                </div>`;
                        row += `                <div class="project-price"></div>`;
                        row += `            </div>`;
                    });
                    row += `        </div>`;
                    row += `    </div>`;
                    row += `</a>`;
                    $("#listDeveloper").html(row);
                }
            });
            
		  });
        }
    });
}