var limit = 9;
var page = 1;
var activeListing = -1;
var selectedCenterMap = -1;
var buildingsMarkersArray = [];

// when you are done creating markers then
var clusterOptions = {
     imagePath: '/assets/img/marker/m'
};
var clusterGroupIndividualMarkers;
var pagination = null;
var forceFitBounds = false;
var mapMode = 0;
var senseMap = new SensecodeMap();
/** Using Sensecode-Map.js */
senseMap.initMap("mapDiv");
senseMap.addOnClickMap(function(){
  // this is a function i think
  // console.log("mapCLicked");
});

/**
 * SenseMap Ke - 0 == All Marker
 */
senseMap.addClusterGroup({
  "iconCreateFunction" : function(cluster){
    var childMarkers = cluster.getAllChildMarkers();
    if (senseMap.checkMarkerSeparate(childMarkers)) {
      return L.divIcon({
        html: "<div class='cluster-inner'><div>" + cluster.getChildCount() + "</div></div>",
        className: 'cluster-container',
        iconSize: L.point(40, 40)
      });
    } else {
      let domIcon =  `<div class='erakita-marker-label'>
                <b>`+ cluster.getChildCount() +` Unit</b>
              </div>
            `;
      return L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing-cluster', html: domIcon });
    }
  }
});
/**
 * SenseMap Ke - 1 == Showed Marker
 */
senseMap.addClusterGroup();
var privateMarkerContainer = senseMap.addClusterGroup();

senseMap.addOnHoverCluster(senseMap.clusterGroups[0], clusterOnHover);
senseMap.addOnMouseOutCluster(senseMap.clusterGroups[0], clusterOnMouseOut);
senseMap.addOnMouseClickCluster(senseMap.clusterGroups[0], clusterOnMouseClick);
senseMap.initDrawable();
senseMap.addNewButtons({
  "innerHTML" : "Reset",
  "onClick" : function(){
    senseMap.resetAreaParam();
    senseMap.clearDrawnLayer();
    loadSenseMapMarker();
	senseMap.drawnStatus = false;
  }
});

/** Customized function for listing Page */
clusterGroupIndividualMarkers = senseMap.addClusterGroup(); 
      
var pageHandler = function pageHandling(){
  page = pagination.getCurrentPage();
  console.log(page)
    updateShowedListing();
}

function updateShowedListing(){
  var dataShowed = senseMap.markersLists[1].slice((page-1)*limit, (page-1)*limit+limit);
  console.log(dataShowed)
	var row = "";
    var ctr = 0;
	dataShowed.forEach(el => {
		let e = el.listing;
		if (e.jenis_transaksi == "0"){
			var jenis = "Jual";
			var bgJenis = "jual";
			var harga = shortNumber(e.harga_jual);
		}else if (e.jenis_transaksi == "1"){
			var jenis = "Sewa";
			var bgJenis = "sewa";
			var harga = shortNumber(e.harga_sewa) + "/thn";
		}else{
			var jenis = "Jual Sewa";
			var bgJenis = "jual_sewa";
			var harga = shortNumber(e.harga_jual) + " - " + shortNumber(e.harga_sewa) + "/thn";;
		}
		var favorite = "";
		if (e.favorite==1){
			favorite = "favorite";
		}
	    row += `	<div onclick='selectListingCard(this);' class="col-lg-4 col-md-6 col-sm-12 mb-3 px-2 listing-item hoverable" data-idx='`+el.indexMarker+`' data-id='`+e.id_proyek+`'>`;
        row += `		<div class="mb-0 listing-item m-portlet m-portlet--rounded">`;
        row += `			<div class="m-portlet__head sensecode-slider" style="background-size: cover; background-position:center; background-repeat: no-repeat; background-image:url('/assets/img/proyek/`+e.photo_proyek+`');">`;
        row += `                <div class="harga-badge"><span class="fa fa-check-circle">&nbsp;</span>`+shortNumber(e.min_jual)+`</div>`
		row += `			</div>`;
		row += `			<div class="m-portlet__body" style="padding:0px;">`;
        row += `				<div class="col-lg-12 col-md-12 col-sm-12" style="padding: 10px;">`;
		row += `					<a href="/proyek/detail/`+e.id_proyek+`" class="listing-price listing-info txt-black hoverable">` + e.nama_proyek + `</a>`;
		row += `					<div class="listing-jalan listing-info truncate hoverable">by ` + e.nama_developer + `</div>`;
		row += `					<div class="listing-kota listing-info truncate"><span class="fa fa-map-marker"></span>&nbsp;` + e.nama_kecamatan + `, ` + e.nama_kota + `</div>`;
        row += `					<div class="listing-detail row m-0">`;
        if (e.min_tidur == e.max_tidur){
            var tidur = e.min_tidur;
        }else{
            var tidur = e.min_tidur+` - `+e.max_tidur;
        }
        row += `						<div class='p-2 col-6'><div class="listing-detail-pills col-12 m-0 txt-center"><i class="fas fa-bed"></i><br>`+tidur+`</div></div>`;

        if (e.min_tanah == e.max_tanah){
            var tanah = e.min_tanah+`m<sup>2</sup>`;
        }else{
            var tanah = e.min_tanah+`m<sup>2</sup> - `+ e.max_tanah+`m<sup>2</sup>`;
        }
        row += `						<div class='p-2 col-6'><div class="listing-detail-pills col-12 m-0 txt-center"><i class="fas fa-square"></i><br>` + tanah + `</div></div>`;


        if (e.min_mandi == e.max_mandi){
            var mandi = e.min_mandi;
        }else{
            var mandi = e.min_mandi+` - `+e.max_mandi;
        } 
        row += `						<div class='p-2 col-6'><div class="listing-detail-pills col-12 m-0 txt-center"><i class="fas fa-bath"></i><br>`+mandi+`</div></div>`;

        if (e.min_bangunan == e.max_bangunan){
            var bangunan = e.min_bangunan+`m<sup>2</sup>`;
        }else{
            var bangunan = e.min_bangunan+`m<sup>2</sup> - `+e.max_bangunan+`m<sup>2</sup>`;
        } 
		row += `						<div class='p-2 col-6'><div class="listing-detail-pills col-12 m-0 txt-center"><i class="fas fa-building"></i><br>` + bangunan + `</div></div>`;
		row += `					</div>`;
		row += `				</div>`;
		row += `			</div>`;
		row += `		</div>`;
		row += `	</div>`;
		if (ctr % 2 == 0){
			row += `</div>`;
		}
		ctr++;
	});
	$("#listProyeks").html(row);
	mApp.initPopovers();
}

function selectListingCard(ob, unit=false){
	$(".listing-item.active").removeClass("active");
	let idx = $(ob).attr("data-idx");
	$(".listing-item[data-idx='"+idx+"']").addClass("active");
	if (!unit){
		showSeparateMarker(idx);
		// changeTabTableDetail(idx);
	}
}

var lastSeparateMarket = undefined;
function showSeparateMarker(markerIndex){
	if (listingActive != -1){
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
	}
	if (lastSeparateMarket !== undefined || markerIndex == null) {
		//publicMarkerContainer.clusterGroup.addLayer(senseMap.markersLists[0][lastSeparateMarket]);
		if (clusterGroupIndividualMarkers.markerList[0] != null){
			privateMarkerContainer.clusterGroup.removeLayer(clusterGroupIndividualMarkers.markerList[0]);
		}	
		clusterGroupIndividualMarkers.markerList = [];
	} 
	if (markerIndex != null) {
		let marker = senseMap.markersLists[0][markerIndex];
		clusterGroupIndividualMarkers.markerList.push(L.marker(marker._latlng, {icon : marker.options.icon}))
		privateMarkerContainer.clusterGroup.addLayer(clusterGroupIndividualMarkers.markerList[0]);
		// publicMarkerContainer.clusterGroup.removeLayer(senseMap.markersLists[0][markerIndex]);
		lastSeparateMarket = markerIndex;
		listingActive = markerIndex;
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().addClass("activeMarker");
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().css("z-index", 9999);
	}
}

function clusterIconCreate(cluster){
	var childMarkers = cluster.getAllChildMarkers();
	var n = 0;
	if (senseMap.checkMarkerSeparate(childMarkers)) {
		return L.divIcon({
			html: "<div class='cluster-inner'><div>" + cluster.getChildCount() + "</div></div>",
			className: 'cluster-container',
			iconSize: L.point(40, 40)
		});
	} else {
		let domIcon =  `<div class='erakita-marker-label'>
							<b>`+ cluster.getChildCount() +` Unit</b>
						</div>
					`;
		return L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing-cluster', html: domIcon });
	}
}

function clickUnit(e){
	if (listingActive != -1){
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
	}
	listingActive = $(e).attr("data-idx");
	let idxShowed = $(e).attr("idx-showed");;
	pagination.setCurrentPage(Math.floor((idxShowed)/10)+1);
	var tempCardActive = $(".listing-item[data-idx='"+listingActive+"']");
	selectListingCard(tempCardActive, true);
	listingActive = -1;
	$('#listProyeks').animate({
		scrollTop: $(tempCardActive).offset().top +  $('#listProyeks').scrollTop() - $('#listProyeks').offset().top
	}, 500);
}

function clusterOnHover(cluster){
	childData = (getChildData(cluster.layer.getAllChildMarkers()));
	let offsetY = -10;
	if (childData.separate) {
		senseMap.clusterGroups[0].options.zoomToBoundsOnClick = true;
		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: false, position : "s" })
		.setContent("Melihat " + cluster.layer._childCount +' Listing <br> Listing dari harga ' + shortNumber(childData.minPrice) + " - " + shortNumber(childData.maxPrice) + " dengan rata-rata harga " + shortNumber(childData.avgPrice))
		.setLatLng(cluster.latlng)
		.openOn(senseMap.map);
	} else {
		senseMap.clusterGroups[0].options.zoomToBoundsOnClick = false;
	}
}

function clusterOnMouseOut(cluster){
	if (childData.separate) {
		senseMap.map.closePopup();
	}
}
function clusterOnMouseClick(cluster){
	senseMap.map.closePopup();
	if (!childData.separate) {
		offsetY = -18;
		let domPopup = `<div class='flex flex-ver' style='width:320px;'>
							<div class="flex flex-ver" style='border-bottom: 2px solid var(--gray); margin-bottom: 10px;'>
								<div class='label' style='font-size:1.25em;'>`+ c.layer._childCount +` for sale</div>`;
								// <a href='javascript:void(0)' class='my-2' style='font-size:1.25em;'><i class="fa fa-lock"></i> Sign in for more details</a>
		domPopup += `	</div>
							<div class='map-listing-item-container minimalistScrollbar' style='max-height:188px; overflow-y:auto;'>`;

		childData.listings.forEach(listing => {	
			domPopup +=			`<div class='flex flex-hor flex-vertical-center flex-separate py-2'>
									<div class='squared div-image ratio69' image='default' style='width:37%;'></div>
									<div class="flex flex-ver" style='width:58%;'>
										<div>Unit di`+ convertTransactionType(listing.jenis_transaksi) +`kan</div>
										<div><b>`+ listing.judul_listing +`</b></div>
										<div>Rp. `+  shortNumber(listing.harga_jual) +`</div>
										<div>`+listing.kamar_tidur+` bed.`+ listing.kamar_mandi +` bath. <i class="fa fa-lock"></i> sq.ft.</div>
									</div>
								</div>`;
		});
		domPopup += 		`</div>
						</div>`;
		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: true, position : "s" })
		.setContent(domPopup)
		.setLatLng(cluster.latlng)
		.openOn(senseMap.map);
		// c.originalEvent.preventDefault();
		// map.fitBounds(map.getBounds());
	}
}


function getChildData(childs){
	let childData = {maxPrice: 0, minPrice : null, listings : []};
	let sumPrice = 0;
	childs.forEach(el => {
		el.listing.indexMarker = el.indexMarker;
		el.listing.idxShowed = el.idxShowed;
		childData.listings.push(el.listing);
		if (childData.minPrice == null || el.listing.harga_jual < childData.minPrice ) {
			childData.minPrice = el.listing.harga_jual;
		}
		if (el.listing.harga_jual > childData.maxPrice ) {
			childData.maxPrice = el.listing.harga_jual;
		}
		sumPrice += parseFloat(el.listing.harga_jual);
	});
	console.log(sumPrice, childs.length);
	childData.avgPrice = sumPrice / childs.length;
	childData.separate = senseMap.checkMarkerSeparate(childs);
	return childData;
}

var requestPage = null;
var listingActive = -1;

var waitZoom = 500;
var zoomPromise;
function updateZoomView(){
	if (senseMap.drawnStatus == false){
		showSeparateMarker(null);
		page = 1;
		senseMap.markersLists[1] = [];
		var idxShowed = 0;
		senseMap.markersLists[0].forEach(e => {
			if(senseMap.map.getBounds().contains(e.getLatLng())){
				senseMap.markersLists[1].push(e);
				e.idxShowed = idxShowed++;
			}
		});
		pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
		pagination.addActiveItemClass(["bg-red txt-white"]);
		pagination.addItemClass(["clickable"]);
		pagination.viewExample(0);
		$("#showedListing").html(senseMap.markersLists[1].length);
		updateShowedListing();
	}
}

senseMap.map.on('draw:created', function(e) {
	let polygon = L.polygon(senseMap.drawnLayers[senseMap.drawnLayers.length-1].getLatLngs());
	showSeparateMarker(null);
	page = 1;
	senseMap.markersLists[1] = [];
	var idxShowed = 0;
	senseMap.markersLists[0].forEach(e => {
		if(polygon.contains(e.getLatLng())) {
			e.idxShowed = idxShowed++;
			senseMap.markersLists[1].push(e);
		}else{
			senseMap.clusterGroups[0].removeLayer(e);
		}
	});
	senseMap.drawnStatus = true;
	senseMap.map.fitBounds(polygon.getBounds());

	// drawData = "&draw=";
	// polygon.getLatLngs()[0].forEach(e =>{
	// 	drawData += e.lat + "," + e.lng + ";"; 
	// });
	// var newUrl = "/listing?"+sendData+drawData+sortData+boundData;
	// window.history.pushState(data, "EraKita | Listing", newUrl);

	pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
	pagination.addActiveItemClass(["bg-red txt-white"]);
	pagination.addItemClass(["clickable"]);
	pagination.viewExample(0);
	$("#totalListing").html(senseMap.markersLists[1].length);
	$("#showedListing").html(senseMap.markersLists[1].length);
	updateShowedListing();
});

senseMap.map.on('zoomend', function(e) {
	updateZoomView();
	clearTimeout(zoomPromise);
	zoomPromise = setTimeout(updateZoomView(), waitZoom);
});

senseMap.map.on('dragend', function(e) {
	updateZoomView();
	clearTimeout(zoomPromise);
	zoomPromise = setTimeout(updateZoomView(), waitZoom);
});

function addEventClickMarker(){
	senseMap.markersLists[0].forEach(element =>{
		$(element).click(function (e){
			if (listingActive != -1){
				$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
			}
			listingActive = $(this)[0].indexMarker;
			let idxShowed = listingActive;
			if ($(this)[0].idxShowed != null){idxShowed=$(this)[0].idxShowed}
			pagination.setCurrentPage(Math.floor((idxShowed)/limit)+1);
			var tempCardActive = $(".listing-item[data-idx='"+listingActive+"']");
			selectListingCard(tempCardActive);
			$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().addClass("activeMarker");
			$('#listProyeks').animate({
				scrollTop: $(tempCardActive).offset().top +  $('#listProyeks').scrollTop() - $('#listProyeks').offset().top
			}, 500);
		})
	});
}

function loadSenseMapMarker(){
	page = 1;
	senseMap.markersLists[0].forEach(element => {
		senseMap.clusterGroups[0].removeLayer(element);
	});
	senseMap.markersLists[0].length = 0; 
	senseMap.markersLists[1].length = 0;  
	console.log("Jumlah0 : " + senseMap.markersLists[0].length);
    showSeparateMarker(null);
	mApp.block("#mapDiv", {});
    $.ajax({
		"url" : API_URL + "/api/get/proyeks/getActive?token=public" +sortData,
        success : function(data){
			data = JSON.parse(data);
			console.log(data)
			// var newUrl = "/listing?"+sortData;
			// window.history.pushState(data, "EraKita | Listing", newUrl);
			$("#totalListing").html(data.data.length);
			$("#showedListing").html(data.data.length);

			let preparedData = prepareDataforMarker(data.data);
			console.log("Jumlah1 : " + senseMap.markersLists[0].length);
			senseMap.addAllMarkers(preparedData, {defaultPopup:false});
			senseMap.markersLists[1] = senseMap.markersLists[0];
			console.log("Jumlah2 : " + senseMap.markersLists[0].length);
			addEventClickMarker();
			pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
			pagination.addActiveItemClass(["bg-red txt-white"]);
			pagination.addItemClass(["clickable"]);
			pagination.viewExample(0);
			pagination.setCurrentPage(1);
			mApp.unblock("#mapDiv", {});
			updateShowedListing();
      }
	});
}

function prepareDataforMarker(data){
	let targetData = ("data" in data) ? data.data : data;
	console.log(targetData)
	var ctrCard = 0;
	targetData.forEach(element => {
		let domIcon =  `<div class='erakita-marker-label' indexMarker='`+ctrCard+`'>`+shortNumber(element.min_jual)+`</div>`
		let temp = JSON.stringify({listing : element, idxShowed : ctrCard, indexMarker : ctrCard++});
		element.additionalData = JSON.parse(temp);
		element.lat = element.lat;
		element.lng = element.long;
		element.icon = senseMap.createIcon({
			html: domIcon,
			className : "bg-red txt-white erakita-marker-container leaflet-div-icon tipe-listing0"}, true)
	});
	console.log(targetData)
	return targetData;
}

function loadListingbySearch(){
	var sendData = {
		atas: senseMap.areaParam[0],
		kanan: senseMap.areaParam[1],
		bawah: senseMap.areaParam[2],
		kiri: senseMap.areaParam[3]
	}
	$.ajax({
		"url" : API_URL + "/api/get/listings/searchArea?token=public",
		"method" : "GET",
		"data" : sendData,
		success : function(data){
			data = JSON.parse(data);
			let preparedData = prepareDataforMarker(data);
			senseMap.addAllMarkers(preparedData, {defaultPopup:false});
			// data.forEach(element => {
			// 	if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(element.lat_listing, element.long_listing), mapPolygonArea) ){
			// 		temp.push(element);
			// 	}    
			// });
			// Total ini data yang sudah menghilangkan data2 yg tidak diperlukan
			// console.log(temp);    
			// data['data'] = temp;                
			// $("#drawsearch>div>div").html("Draw");     
			// clearMarker();
			// showListing(data, 1); 
		}
	});
}

function convertTransactionType(kode_jenis_transaksi){
	switch (kode_jenis_transaksi) {
		case "0":
			return  "jual";
			break;
		case "1":
			return  "sewa";
			break;
		case "2":
			return  "jual/sewa";
			break;
		
		default:
			return "";
			break;
	}
}

$(".leaflet-draw-draw-polygon").click(function (e) { 
	e.preventDefault();
	// console.log("start drawing");
	senseMap.resetAreaParam();
	senseMap.clearDrawnLayer();
	senseMap.drawnStatus = false;
});

$(document).ready(function () {
    loadSenseMapMarker();
    $('#daftar-proyek-filter-priceSlider').ionRangeSlider({
        type: "double",
        grid: true,
        min: 0,
        max: 100000000000,
        from: 0,
        to: 100000000000,
        step : 10000000,
        grid_num: 4,
        grid_snap:true,
        prefix: "Rp. ",
        onStart: function (data) {
            // fired then range slider is ready
            // console.log("onStartPriceSlider : " + data.from + "-" + data.to);
            $(".daftar-proyek-filter-priceInput.min").val(data.from);
            $(".daftar-proyek-filter-priceInput.max").val(data.to);
            $(".daftar-proyek-filter-priceInput").change(function(){
                daftarProyekFilterPriceSliderInstance.update({
                    from: $(".daftar-proyek-filter-priceInput.min").val(),
                    to: $(".daftar-proyek-filter-priceInput.max").val()
                });
            });
        },
        onChange: function (data) {
            // fired on every range slider update
            //console.log("onChange : " + data.from + "-" + data.to);
            $(".daftar-proyek-filter-priceInput.min").val(data.from);
            $(".daftar-proyek-filter-priceInput.max").val(data.to);
        },
        onFinish: function (data) {
            // fired on pointer release
            //console.log("onFinish : " + data.from + "-" + data.to);
            updateOptionBadge();
        },
        onUpdate: function (data) {
            // fired on changing slider with Update method
            //console.log("onUpdate : " + data.from + "-" + data.to);
        }
    });
    daftarProyekFilterPriceSliderInstance = $('#daftar-proyek-filter-priceSlider').data("ionRangeSlider");

});

var sort = "startlisting";
var urutan = "desc";
var sortData = "&sort=startlisting-desc";
function urutkanProyek(e){
    $("#sort-" + sort).find(".fa.fa-check").remove();
    sort = $(e).attr("data-value");
    $("#sort-" + sort).append(" <i class='fa fa-check'></i>");
    urutan = $("#urutan").find(".active").attr("data-value");
    
    sortData = "&sort=" + sort + "-" + urutan;

    loadList();
}
function urutkanProyekAZ(temp){
    urutan = temp;
    sortData = "&sort=" + sort + "-" + urutan;

    loadList();
}


function insertFilterText(ob){
    $("#headSearch").val($(ob).find(".data-show").text());
    $("#headSearch").attr("data-hidden", $(ob).find(".data-hidden").text());
}

function insertDatatoParent(ob){
    let target = $(ob).attr("data-parent");
    $(target).val($(ob).find(".data-show").text());
    $(target).attr("data-hidden", $(ob).find(".data-hidden").text());
    $(ob).parent().parent().fadeOut();
}