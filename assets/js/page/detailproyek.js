var limit = 9;
var page = 1;
var activeListing = -1;
var selectedCenterMap = -1;
var buildingsMarkersArray = [];

// when you are done creating markers then
var clusterOptions = {
     imagePath: '/assets/img/marker/m'
};
var clusterGroupIndividualMarkers;
var pagination = null;
var forceFitBounds = false;
var mapMode = 0;
var senseMap = new SensecodeMap();
/** Using Sensecode-Map.js */
senseMap.initMap("mapDiv");
senseMap.addOnClickMap(function(){
  // this is a function i think
  // console.log("mapCLicked");
});

/**
 * SenseMap Ke - 0 == All Marker
 */
senseMap.addClusterGroup({
  "iconCreateFunction" : function(cluster){
    var childMarkers = cluster.getAllChildMarkers();
    if (senseMap.checkMarkerSeparate(childMarkers)) {
      return L.divIcon({
        html: "<div class='cluster-inner'><div>" + cluster.getChildCount() + "</div></div>",
        className: 'cluster-container',
        iconSize: L.point(40, 40)
      });
    } else {
      let domIcon =  `<div class='erakita-marker-label'>
                <b>`+ cluster.getChildCount() +` Unit</b>
              </div>
            `;
      return L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing-cluster', html: domIcon });
    }
  }
});
/**
 * SenseMap Ke - 1 == Showed Marker
 */
senseMap.addClusterGroup();
var privateMarkerContainer = senseMap.addClusterGroup();

senseMap.addOnHoverCluster(senseMap.clusterGroups[0], clusterOnHover);
senseMap.addOnMouseOutCluster(senseMap.clusterGroups[0], clusterOnMouseOut);
senseMap.addOnMouseClickCluster(senseMap.clusterGroups[0], clusterOnMouseClick);
senseMap.initDrawable();
senseMap.addNewButtons({
  "innerHTML" : "Reset",
  "onClick" : function(){
    senseMap.resetAreaParam();
    senseMap.clearDrawnLayer();
    loadSenseMapMarker();
	senseMap.drawnStatus = false;
  }
});

/** Customized function for listing Page */
clusterGroupIndividualMarkers = senseMap.addClusterGroup(); 
      
var pageHandler = function pageHandling(){
  page = pagination.getCurrentPage();
  console.log(page)
    updateShowedListing();
}


$(document).ready(function(){
    loadSenseMapMarker();
});

function updateShowedListing(){
  var dataShowed = senseMap.markersLists[1].slice((page-1)*limit, (page-1)*limit+limit);
  console.log(dataShowed)
    var row = "";
    var broker = $("#databroker").attr("data-id");
	dataShowed.forEach(el => {
		let e = el.listing;
		
		let gambar = [];
		for (var i=1; i<=e.gambar; i++){
			gambar.push("/assets/img/listing/"+e.id_listing+"/"+i+".jpg");
		}
		gambar = JSON.stringify(gambar);
				
		let jenis = "";
		let bgJenis = "";
		let harga = "";
		let icon = e.tipe_listing == "1" ? "home" : (e.tipe_listing == "2" ? "building-o" : (e.tipe_listing == "3" ? "archive" : (e.tipe_listing == "4" ? "industry" : (e.tipe_listing == "5" ? "road" : "industry"))));

		if (e.jenis_transaksi == "0"){
			jenis = "Jual";
			bgJenis = "jual";
			harga = shortNumber(e.harga_jual);
		}else{
			jenis = "Sewa";
			bgJenis = "sewa";
			harga = shortNumber(e.harga_sewa) + "/thn";
		}

		let favorite = e.favorite == 1 ? "favorite" : "";

		
		row+=	`<div class="col-lg-6 col-sm-12 mb-3 px-2">`;
		row+=	`	<div onclick='selectListingCard(this);' class=" mb-0 listing-item m-portlet m-portlet--rounded" data-idx='`+el.indexMarker+`' data-id='`+e.id_listing+`' data-telp='`+e.telp_broker+`' data-judul='`+e.judul_listing+`' data-lat='`+e.lat_listing+`' data-lng='`+e.long_listing+`' data-tipe='`+e.nama_tipe+`' data-jenis='`+jenis+`'>`;
		row+=	`		<div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='`+gambar+`'>`;
		row+=	`			<div class="listing-slider-badge jenis-`+bgJenis+`">` + jenis + `</div>`;
		row+=	`			<div class="listing-slider-icon jenis-`+bgJenis+`"><i class="fas fa-`+icon+`"></i></div>`;
		row+=	`			<div class="listing-slider-counter" style="z-index:2;">1 of 3</div>`;
		row+=	`		</div>`;
		row+=	`		<div class="m-portlet__body" style="padding:0px;">`;
		row+=	`			<div class="col-lg-10 col-md-10 col-sm-12" style="padding: 10px 5px 10px 10px;">`;
		row+=	`				<div class="listing-price listing-info">` + harga + `</div>`;
		row+=	`				<div class="listing-title listing-info truncate hoverable" onclick="viewDetail(this);"><b>` + e.judul_listing + `</b></div>`;
		row+=	`				<div class="listing-detail">`;
		if (e.kamar_tidur != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>`+e.kamar_tidur+`</div>`;
		}
		if (e.kamar_mandi != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>`+e.kamar_mandi+`</div>`;
		}
		if (e.luas_tanah != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>` + e.luas_tanah + `m<sup>2</sup></div>`;
		}
		if (e.luas_bangunan != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>` + e.luas_bangunan + `m<sup>2</sup></div>`;
		}
		row+=	`				</div>`;
		row+=	`			</div>`;
		row+=	`			<div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding:10px 10px 10px 5px">`;
		row+=	`				<div class="listing-content-icon" style="margin:auto;"><i onclick='favorite(this);' class="fas fa-heart fa-lg ` + favorite + `" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Favorit"></i></div>`;
		row+=	`				<div class="listing-content-icon" style="margin:auto;"><i onclick='shareLink(this);' class="fas fa-share fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Bagikan"></i></div>`;
		row+=	`				<a href="/detaillisting/view/`+e.id_listing+`" style="margin:auto;" class="listing-content-icon txt-black"><i class="fas fa-ellipsis-h fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Tampilkan Lebih Banyak"></i></a>`;
		row+=	`			</div>`;
		row+=	`		</div>`;
		row+=	`	</div>`;
		row+=	`</div>`;

        // row += `<span onclick="selectListingCard(this);" class="flex-1 listing-item hoverable" data-idx="`+el.indexMarker+`" data-id="`+e.id_listing+`" style="padding: 5px 0px;">`;
        // row += `    <div class="flex flex-hor" style="height:100%;">`;
        // row += `        <div style="background-image: url('/assets/img/listing/`+e.id_listing+`/1.jpg'); background-repeat: no-repeat; background-position: center; background-size: cover; min-height: 150px; font-size: 0.8em; color: white; width: 30%;">`;
        // row += `            <div style="background-color: lightseagreen; margin-top: 15px; padding: 1px; width: 64px;"><i class="fa fa-star"> &nbsp;</i>PREMIUM</div>`;
        // row += `            <div style="background-color: darkorange; margin-top: 5px; padding: 1px; width: 86px;"><i class="fa fa-home"> &nbsp;</i>DEVELOPMENT</div>`;
        // row += `        </div>`;
        // row += `        <div class="flex flex-ver" style="width: 65%; padding: 10px;">`;
        // row += `            <a href="/detaillisting/view/`+e.id_listing+`?broker=`+broker+`&rand=1" style="font-weight:bold; font-size: 1.3em; height: 20%;" class="txt-black hoverable">`+e.judul_listing+`</a>`;
        // row += `            <div style="font-weight: bold; font-size: 2em; padding: 15px 0px; color: black; height: 60%;">Rp `+numberWithCommas(e.harga_jual)+`</div>`;
        // row += `            <div class="row" style="height: 20%; color: #888; font-size: 0.8em;">`;
        // row += `                <div class="col-sm-4">Luas <span style="font-weight: bold; font-size: 1.2em; color: black;">`+e.luas_bangunan+` m<sup>2</sup></span></div>`;
        // row += `                <div class="col-sm-4">Kamar Tidur <span style="font-weight: bold; font-size: 1.2em; color: black;">`+e.kamar_tidur+`</span></div>`;
        // row += `                <div class="col-sm-4">Kamar Mandi <span style="font-weight: bold; font-size: 1.2em; color: black;">`+e.kamar_mandi+`</span></div>`;
        // row += `            </div>`;
        // row += `        </div>`;
        // row += `    </div>`;
        // row += `</span>`;
	});
	$("#listUnit").html(row);
	initSlider();
	mApp.initPopovers();
}

function selectListingCard(ob, unit=false){
	$(".listing-item.active").removeClass("active");
	let idx = $(ob).attr("data-idx");
	$(".listing-item[data-idx='"+idx+"']").addClass("active");
	if (!unit){
		showSeparateMarker(idx);
		// changeTabTableDetail(idx);
	}
}

var lastSeparateMarket = undefined;
function showSeparateMarker(markerIndex){
	if (listingActive != -1){
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
	}
	if (lastSeparateMarket !== undefined || markerIndex == null) {
		//publicMarkerContainer.clusterGroup.addLayer(senseMap.markersLists[0][lastSeparateMarket]);
		if (clusterGroupIndividualMarkers.markerList[0] != null){
			privateMarkerContainer.clusterGroup.removeLayer(clusterGroupIndividualMarkers.markerList[0]);
		}	
		clusterGroupIndividualMarkers.markerList = [];
	} 
	if (markerIndex != null) {
		let marker = senseMap.markersLists[0][markerIndex];
		clusterGroupIndividualMarkers.markerList.push(L.marker(marker._latlng, {icon : marker.options.icon}))
		privateMarkerContainer.clusterGroup.addLayer(clusterGroupIndividualMarkers.markerList[0]);
		// publicMarkerContainer.clusterGroup.removeLayer(senseMap.markersLists[0][markerIndex]);
		lastSeparateMarket = markerIndex;
		listingActive = markerIndex;
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().addClass("activeMarker");
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().css("z-index", 9999);
	}
}

function clusterIconCreate(cluster){
	var childMarkers = cluster.getAllChildMarkers();
	var n = 0;
	if (senseMap.checkMarkerSeparate(childMarkers)) {
		return L.divIcon({
			html: "<div class='cluster-inner'><div>" + cluster.getChildCount() + "</div></div>",
			className: 'cluster-container',
			iconSize: L.point(40, 40)
		});
	} else {
		let domIcon =  `<div class='erakita-marker-label'>
							<b>`+ cluster.getChildCount() +` Unit</b>
						</div>
					`;
		return L.divIcon({className: 'erakita-marker-container leaflet-div-icon tipe-listing-cluster', html: domIcon });
	}
}

function clickUnit(e){
	if (listingActive != -1){
		$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
	}
	listingActive = $(e).attr("data-idx");
	let idxShowed = $(e).attr("idx-showed");;
	pagination.setCurrentPage(Math.floor((idxShowed)/limit)+1);
	var tempCardActive = $(".listing-item[data-idx='"+listingActive+"']");
	selectListingCard(tempCardActive, true);
	listingActive = -1;
    $('#listUnit').animate({
        scrollTop: $(tempCardActive).offset().top +  $('#listUnit').scrollTop() - $('#listUnit').offset().top
    }, 500);
}

function clusterOnHover(cluster){
	childData = (getChildData(cluster.layer.getAllChildMarkers()));
	let offsetY = -10;
	if (childData.separate) {
		senseMap.clusterGroups[0].options.zoomToBoundsOnClick = true;
		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: false, position : "s" })
		.setContent("Melihat " + cluster.layer._childCount +' Listing <br> Listing dari harga ' + shortNumber(childData.minPrice) + " - " + shortNumber(childData.maxPrice) + " dengan rata-rata harga " + shortNumber(childData.avgPrice))
		.setLatLng(cluster.latlng)
		.openOn(senseMap.map);
	} else {
		senseMap.clusterGroups[0].options.zoomToBoundsOnClick = false;
	}
}

function clusterOnMouseOut(cluster){
	if (childData.separate) {
		senseMap.map.closePopup();
	}
}
function clusterOnMouseClick(cluster){
	senseMap.map.closePopup();
	if (!childData.separate) {
		offsetY = -18;
		let domPopup = `<div class='flex flex-ver' style='width:320px;'>
							<div class="flex flex-ver" style='border-bottom: 2px solid var(--gray); margin-bottom: 10px;'>
								<div class='label' style='font-size:1.25em;'>`+ c.layer._childCount +` for sale</div>`;
								// <a href='javascript:void(0)' class='my-2' style='font-size:1.25em;'><i class="fa fa-lock"></i> Sign in for more details</a>
		domPopup += `	</div>
							<div class='map-listing-item-container minimalistScrollbar' style='max-height:188px; overflow-y:auto;'>`;

		childData.listings.forEach(listing => {	
			domPopup +=			`<div class='flex flex-hor flex-vertical-center flex-separate py-2'>
									<div class='squared div-image ratio69' image='default' style='width:37%;'></div>
									<div class="flex flex-ver" style='width:58%;'>
										<div>Unit di`+ convertTransactionType(listing.jenis_transaksi) +`kan</div>
										<div><b>`+ listing.judul_listing +`</b></div>
										<div>Rp. `+  shortNumber(listing.harga_jual) +`</div>
										<div>`+listing.kamar_tidur+` bed.`+ listing.kamar_mandi +` bath. <i class="fa fa-lock"></i> sq.ft.</div>
									</div>
								</div>`;
		});
		domPopup += 		`</div>
						</div>`;
		var pop = new L.Rrose({ offset: new L.Point(0, offsetY), closeButton: false, autoPan: true, position : "s" })
		.setContent(domPopup)
		.setLatLng(cluster.latlng)
		.openOn(senseMap.map);
		// c.originalEvent.preventDefault();
		// map.fitBounds(map.getBounds());
	}
}


function getChildData(childs){
	let childData = {maxPrice: 0, minPrice : null, listings : []};
	let sumPrice = 0;
	childs.forEach(el => {
		el.listing.indexMarker = el.indexMarker;
		el.listing.idxShowed = el.idxShowed;
		childData.listings.push(el.listing);
		if (childData.minPrice == null || el.listing.harga_jual < childData.minPrice ) {
			childData.minPrice = el.listing.harga_jual;
		}
		if (el.listing.harga_jual > childData.maxPrice ) {
			childData.maxPrice = el.listing.harga_jual;
		}
		sumPrice += parseFloat(el.listing.harga_jual);
	});
	console.log(sumPrice, childs.length);
	childData.avgPrice = sumPrice / childs.length;
	childData.separate = senseMap.checkMarkerSeparate(childs);
	return childData;
}

var requestPage = null;
var listingActive = -1;

var waitZoom = 500;
var zoomPromise;
function updateZoomView(){
	if (senseMap.drawnStatus == false){
		showSeparateMarker(null);
		page = 1;
		senseMap.markersLists[1] = [];
		var idxShowed = 0;
		senseMap.markersLists[0].forEach(e => {
			if(senseMap.map.getBounds().contains(e.getLatLng())){
				senseMap.markersLists[1].push(e);
				e.idxShowed = idxShowed++;
			}
		});
		pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
		pagination.addActiveItemClass(["bg-red txt-white"]);
		pagination.addItemClass(["clickable"]);
		pagination.viewExample(0);
		$("#showedListing").html(senseMap.markersLists[1].length);
		updateShowedListing();
	}
}

senseMap.map.on('draw:created', function(e) {
	let polygon = L.polygon(senseMap.drawnLayers[senseMap.drawnLayers.length-1].getLatLngs());
	showSeparateMarker(null);
	page = 1;
	senseMap.markersLists[1] = [];
	var idxShowed = 0;
	senseMap.markersLists[0].forEach(e => {
		if(polygon.contains(e.getLatLng())) {
			e.idxShowed = idxShowed++;
			senseMap.markersLists[1].push(e);
		}else{
			senseMap.clusterGroups[0].removeLayer(e);
		}
	});
	senseMap.drawnStatus = true;
	senseMap.map.fitBounds(polygon.getBounds());

	// drawData = "&draw=";
	// polygon.getLatLngs()[0].forEach(e =>{
	// 	drawData += e.lat + "," + e.lng + ";"; 
	// });
	// var newUrl = "/listing?"+sendData+drawData+sortData+boundData;
	// window.history.pushState(data, "EraKita | Listing", newUrl);

	pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
	pagination.addActiveItemClass(["bg-red txt-white"]);
	pagination.addItemClass(["clickable"]);
	pagination.viewExample(0);
	$("#totalListing").html(senseMap.markersLists[1].length);
	$("#showedListing").html(senseMap.markersLists[1].length);
	updateShowedListing();
});

senseMap.map.on('zoomend', function(e) {
	updateZoomView();
	clearTimeout(zoomPromise);
	zoomPromise = setTimeout(updateZoomView(), waitZoom);
});

senseMap.map.on('dragend', function(e) {
	updateZoomView();
	clearTimeout(zoomPromise);
	zoomPromise = setTimeout(updateZoomView(), waitZoom);
});

function addEventClickMarker(){
	senseMap.markersLists[0].forEach(element =>{
		$(element).click(function (e){
			if (listingActive != -1){
				$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().removeClass("activeMarker");
			}
			listingActive = $(this)[0].indexMarker;
			let idxShowed = listingActive;
			if ($(this)[0].idxShowed != null){idxShowed=$(this)[0].idxShowed}
			pagination.setCurrentPage(Math.floor((idxShowed)/limit)+1);
			var tempCardActive = $(".listing-item[data-idx='"+listingActive+"']");
			selectListingCard(tempCardActive);
			$(".erakita-marker-label[indexMarker='"+listingActive+"']").parent().addClass("activeMarker");
            $('#listUnit').animate({
                scrollTop: $(tempCardActive).offset().top +  $('#listUnit').scrollTop() - $('#listUnit').offset().top
            }, 500);
		})
	});
}

function loadSenseMapMarker(){
	page = 1;
	senseMap.markersLists[0].forEach(element => {
		senseMap.clusterGroups[0].removeLayer(element);
	});
	senseMap.markersLists[0].length = 0; 
	senseMap.markersLists[1].length = 0;  
	console.log("Jumlah0 : " + senseMap.markersLists[0].length);
    showSeparateMarker(null);
    var proyek = $("#dataproyek").attr("data-id");
	mApp.block("#mapDiv", {});
    $.ajax({
        "url" :API_URL + "/api/get/listings/getByProyek/"+proyek+"?token=public" + sortData,
        success : function(data){
			data = JSON.parse(data);
			console.log(data)
			// var newUrl = "/listing?"+sortData;
			// window.history.pushState(data, "EraKita | Listing", newUrl);
			$("#totalListing").html(data.data.length);
			$("#showedListing").html(data.data.length);

			let preparedData = prepareDataforMarker(data.data);
			console.log("Jumlah1 : " + senseMap.markersLists[0].length);
			senseMap.addAllMarkers(preparedData, {defaultPopup:false});
			senseMap.markersLists[1] = senseMap.markersLists[0];
			console.log("Jumlah2 : " + senseMap.markersLists[0].length);
			addEventClickMarker();
			pagination = new Paginate(".sensecode-paginate", (senseMap.markersLists[1].length-1)/limit+1, 1, pageHandler);
			pagination.addActiveItemClass(["bg-red txt-white"]);
			pagination.addItemClass(["clickable"]);
			pagination.viewExample(0);
			pagination.setCurrentPage(1);
			mApp.unblock("#mapDiv", {});
			updateShowedListing();
      }
	});
}

function prepareDataforMarker(data){
	let targetData = ("data" in data) ? data.data : data;
	console.log(targetData)
	var ctrCard = 0;
	targetData.forEach(element => {
		let domIcon =  `<div class='erakita-marker-label' indexMarker='`+ctrCard+`'>`+shortNumber(element.harga_jual)+`</div>`
		let temp = JSON.stringify({listing : element, idxShowed : ctrCard, indexMarker : ctrCard++});
		element.additionalData = JSON.parse(temp);
		element.lat = element.lat_listing;
		element.lng = element.long_listing;
		element.icon = senseMap.createIcon({
			html: domIcon,
			className : "bg-red txt-white erakita-marker-container leaflet-div-icon tipe-listing"+ element.jenis_transaksi}, true)
	});
	console.log(targetData)
	return targetData;
}

function loadListingbySearch(){
	var sendData = {
		atas: senseMap.areaParam[0],
		kanan: senseMap.areaParam[1],
		bawah: senseMap.areaParam[2],
		kiri: senseMap.areaParam[3]
	}
	$.ajax({
		"url" : API_URL + "/api/get/listings/searchArea?token=public",
		"method" : "GET",
		"data" : sendData,
		success : function(data){
			data = JSON.parse(data);
			let preparedData = prepareDataforMarker(data);
			senseMap.addAllMarkers(preparedData, {defaultPopup:false});
			// data.forEach(element => {
			// 	if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(element.lat_listing, element.long_listing), mapPolygonArea) ){
			// 		temp.push(element);
			// 	}    
			// });
			// Total ini data yang sudah menghilangkan data2 yg tidak diperlukan
			// console.log(temp);    
			// data['data'] = temp;                
			// $("#drawsearch>div>div").html("Draw");     
			// clearMarker();
			// showListing(data, 1); 
		}
	});
}

function convertTransactionType(kode_jenis_transaksi){
	switch (kode_jenis_transaksi) {
		case "0":
			return  "jual";
			break;
		case "1":
			return  "sewa";
			break;
		case "2":
			return  "jual/sewa";
			break;
		
		default:
			return "";
			break;
	}
}

$(".leaflet-draw-draw-polygon").click(function (e) { 
	e.preventDefault();
	// console.log("start drawing");
	senseMap.resetAreaParam();
	senseMap.clearDrawnLayer();
	senseMap.drawnStatus = false;
});

function loadList(){
    var proyek = $("#dataproyek").attr("data-id");
    var broker = $("#databroker").attr("data-id");
    mApp.block("#listUnit", {});
    $.ajax({
        "url" :API_URL + "/api/get/listings/getByProyek/"+proyek+"?token=public&limit=" + limit + "&offset=" + (page-1)*limit + sortData,
        success : function(data){
            data = JSON.parse(data);
            if (page == 1){
                pagination = new Paginate(".sensecode-paginate", (data.count-1)/limit+1, 1, pageHandler);
                pagination.addActiveItemClass(["bg-red txt-white"]);
                pagination.addItemClass(["clickable"]);
                pagination.viewExample(0);
                $("#showedListing").html(data.count);
                $("#totalListing").html(data.count);
            }
		    var row = "";
            data.data.forEach(e => {
				
                // row += `<span class="flex-1" style="border: dotted #eeeeee 0.5px; padding: 5px 0px;">`;
                // row += `    <div class="flex flex-hor" style="height:100%;">`;
                // row += `        <div style="background-image: url('/assets/img/listing/`+e.id_listing+`/1.jpg'); background-repeat: no-repeat; background-position: center; background-size: cover; min-height: 150px; font-size: 0.8em; color: white; width: 30%;">`;
                // row += `            <div style="background-color: lightseagreen; margin-top: 15px; padding: 1px; width: 64px;"><i class="fa fa-star"> &nbsp;</i>PREMIUM</div>`;
                // row += `            <div style="background-color: darkorange; margin-top: 5px; padding: 1px; width: 86px;"><i class="fa fa-home"> &nbsp;</i>DEVELOPMENT</div>`;
                // row += `        </div>`;
                // row += `        <div class="flex flex-ver" style="width: 50%; padding: 10px;">`;
                // row += `            <a href="/detaillisting/view/`+e.id_listing+`?broker=`+broker+`&rand=1" style="font-weight:bold; font-size: 1.3em; height: 20%;" class="txt-black hoverable">`+e.judul_listing+`</a>`;
                // row += `            <div style="font-weight: bold; font-size: 2em; padding: 15px 0px; color: black; height: 60%;">Rp `+numberWithCommas(e.harga_jual)+`</div>`;
                // row += `            <div class="row" style="height: 20%; color: #888; font-size: 0.8em;">`;
                // row += `                <div class="col-sm-3">Luas <span style="font-weight: bold; font-size: 1.2em; color: black;">`+e.luas_bangunan+` m<sup>2</sup></span></div>`;
                // row += `                <div class="col-sm-3">Kamar Tidur <span style="font-weight: bold; font-size: 1.2em; color: black;">`+e.kamar_tidur+`</span></div>`;
                // row += `                <div class="col-sm-3">Kamar Mandi <span style="font-weight: bold; font-size: 1.2em; color: black;">`+e.kamar_mandi+`</span></div>`;
                // row += `            </div>`;
                // row += `        </div>`;
                // row += `    </div>`;
				// row += `</span>`;

				let gambar = [];
				for (var i=1; i<=e.gambar; i++){
					gambar.push("/assets/img/listing/"+e.id_listing+"/"+i+".jpg");
				}
				gambar = JSON.stringify(gambar);
				
				let jenis = "";
				let bgJenis = "";
				let harga = "";
				let icon = e.tipe_listing == "1" ? "home" : (e.tipe_listing == "2" ? "building-o" : (e.tipe_listing == "3" ? "archive" : (e.tipe_listing == "4" ? "industry" : (e.tipe_listing == "5" ? "road" : "industry"))));

				if (e.jenis_transaksi == "0"){
					jenis = "Jual";
					bgJenis = "jual";
					harga = shortNumber(e.harga_jual);
				}else{
					jenis = "Sewa";
					bgJenis = "sewa";
					harga = shortNumber(e.harga_sewa) + "/thn";
				}

				let favorite = e.favorite == 1 ? "favorite" : "";


				row+=	`<div class="col-lg-6 col-sm-12 mb-3 px-2">`;
				row+=	`	<div onclick='selectListingCard(this);' class=" mb-0 listing-item m-portlet m-portlet--rounded" data-idx='`+el.indexMarker+`' data-id='`+e.id_listing+`' data-telp='`+e.telp_broker+`' data-judul='`+e.judul_listing+`' data-lat='`+e.lat_listing+`' data-lng='`+e.long_listing+`' data-tipe='`+e.nama_tipe+`' data-jenis='`+jenis+`'>`;
				row+=	`		<div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='`+gambar+`'>`;
				row+=	`			<div class="listing-slider-badge jenis-`+bgJenis+`">` + jenis + `</div>`;
				row+=	`			<div class="listing-slider-icon jenis-`+bgJenis+`"><i class="fas fa-`+icon+`"></i></div>`;
				row+=	`			<div class="listing-slider-counter" style="z-index:2;">1 of 3</div>`;
				row+=	`		</div>`;
				row+=	`		<div class="m-portlet__body" style="padding:0px;">`;
				row+=	`			<div class="col-lg-10 col-md-10 col-sm-12" style="padding: 10px 5px 10px 10px;">`;
				row+=	`				<div class="listing-price listing-info">` + harga + `</div>`;
				row+=	`				<div class="listing-title listing-info truncate hoverable" onclick="viewDetail(this);"><b>` + e.judul_listing + `</b></div>`;
				row+=	`				<div class="listing-detail">`;
				if (e.kamar_tidur != null){
					row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>`+e.kamar_tidur+`</div>`;
				}
				if (e.kamar_mandi != null){
					row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>`+e.kamar_mandi+`</div>`;
				}
				if (e.luas_tanah != null){
					row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>` + e.luas_tanah + `m<sup>2</sup></div>`;
				}
				if (e.luas_bangunan != null){
					row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>` + e.luas_bangunan + `m<sup>2</sup></div>`;
				}
				row+=	`				</div>`;
				row+=	`			</div>`;
				row+=	`			<div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding:10px 10px 10px 5px">`;
				row+=	`				<div class="listing-content-icon" style="margin:auto;"><i onclick='favorite(this);' class="fas fa-heart fa-lg ` + favorite + `" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Favorit"></i></div>`;
				row+=	`				<div class="listing-content-icon" style="margin:auto;"><i onclick='shareLink(this);' class="fas fa-share fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Bagikan"></i></div>`;
				row+=	`				<a href="/detaillisting/view/`+e.id_listing+`" style="margin:auto;" class="listing-content-icon txt-black"><i class="fas fa-ellipsis-h fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Tampilkan Lebih Banyak"></i></a>`;
				row+=	`			</div>`;
				row+=	`		</div>`;
				row+=	`	</div>`;
				row+=	`</div>`;
            });
		$("#listUnit").html(row);
		initSlider();
        mApp.unblock("#listUnit", {});
      }
    });
}

var sort = "startlisting";
var urutan = "desc";
var sortData = "&sort=startlisting-desc";
function urutkanUnit(e){
    $("#sort-" + sort).find(".fa.fa-check").remove();
    sort = $(e).attr("data-value");
    $("#sort-" + sort).append(" <i class='fa fa-check'></i>");
    urutan = $("#urutan").find(".active").attr("data-value");
    
    sortData = "&sort=" + sort + "-" + urutan;

    loadSenseMapMarker();
}
function urutkanUnitAZ(temp){
    urutan = temp;
    sortData = "&sort=" + sort + "-" + urutan;

    loadSenseMapMarker();
}


function favorite(element){
	if (userID == null){
		let action = "like";
		let idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		let url = window.location.href;
		Swal.fire({
			type: 'error',
			title: 'Oops...',
			text: 'Harap Login Terlebih Dahulu',
			footer: '<a href="/login?last_url='+url+'&last_action='+action+'#'+idlisting+'">LOGIN DISINI</a>'
		});
	}else{
		var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		var status = !$(element).hasClass('favorite');
		$(element).toggleClass("favorite");
		if (status){
			var url = API_URL + "/api/get/customers/logFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
			if (typeof cookieBroker !== 'undefined'){
				url = API_URL + "/api/get/customers/logFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
			}
			$.post({
				"url" : url
			});
		}
		$.post({
			"url" : API_URL + "/api/get/customers/updateFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&status=" + Number(status),
			success : function(data){
				data = JSON.parse(data);
				if (!data.status){
					$(element).toggleClass("favorite");
				}
			}
		});
	}
}


function shareLink(element) {
	/* Get the text field */
	var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
	var text = BASE_URL + "/detaillisting/view/" + idlisting;

	var body = document.getElementsByTagName('body')[0];
	var tempInput = document.createElement('INPUT');
	body.appendChild(tempInput);
	tempInput.setAttribute('value', text)
	tempInput.select();
	document.execCommand('copy');
	body.removeChild(tempInput);

	/* Alert the copied text */
	Swal.fire({
		type: 'success',
		title: 'Sukses',
		text: 'Berhasil disalin ke Clipboard'
	});
	if (userID != null){
		var url = API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
		if (cookieBroker != null){
			url = API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
		}
		$.post({
			"url" : url
		});
	}
}