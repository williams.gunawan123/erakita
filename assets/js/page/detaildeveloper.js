function updateListing(id){
    $.ajax({
        "url" : API_URL + "/api/get/listings/getByProyek/"+id+"?token=public",
        success : function(data){
          data = JSON.parse(data);
		  var row = "";
          data.forEach(e => {
            row += `<span class="flex-1" style="border: dotted #eeeeee 0.5px;">`;
            row += `    <div class="flex flex-hor" style="height:100%;">`;
            row += `        <div style="background-image: url('/assets/img/listing/`+e.id_listing+`/1.jpg'); background-repeat: no-repeat; background-position: center; background-size: cover; min-height: 150px; font-size: 0.8em; color: white; width: 50%;">`;
            row += `            <div style="background-color: lightseagreen; margin-top: 15px; padding: 1px; width: 64px;"><i class="fa fa-star"> &nbsp;</i>PREMIUM</div>`;
            row += `            <div style="background-color: darkorange; margin-top: 5px; padding: 1px; width: 86px;"><i class="fa fa-home"> &nbsp;</i>DEVELOPMENT</div>`;
            row += `            <!-- <div style="background-color: #888; padding: 3px; width: 35px; opacity: 0.8; border-radius: 5px; margin-top: 10px; margin-left: 10px;"><i class="fa fa-camera"> &nbsp;</i>99</div> -->`;
            row += `        </div>`;
            row += `        <div class="flex flex-ver" style="width: 50%; padding: 10px;">`;
            row += `            <a href="/detaillisting/view/`+e.id_listing+`" class="txt-black hoverable" style="font-weight:bold; font-size: 1.3em; height: 20%;">`+e.judul_listing+`</a>`;
            row += `            <div style="font-weight: bold; font-size: 2em; padding: 15px 0px; color: black; height: 60%;">Rp `+numberWithCommas(e.harga_jual)+`</div>`;
            row += `            <div class="row" style="height: 20%; color: #888; font-size: 0.8em;">`;
            row += `                <div class="col-sm-4">Luas <span style="font-weight: bold; font-size: 1.2em; color: black;">`+e.luas_bangunan+`m2</span></div>`;
            row += `                <div class="col-sm-4">Kamar Tidur <span style="font-weight: bold; font-size: 1.2em; color: black;">`+e.kamar_tidur+`</span></div>`;
            row += `                <div class="col-sm-4">Kamar Mandi <span style="font-weight: bold; font-size: 1.2em; color: black;">`+e.kamar_mandi+`</span></div>`;
            row += `            </div>`;
            row += `        </div>`;
            row += `    </div>`;
            row += `</span>`;
          });
          $("#listUnit").html(row);
        }
    });
}