function scrollTo(idObject, offsetHeader=0){
    console.log(idObject);
    $('html, body').animate({
        scrollTop: ($("#" + idObject).offset().top + parseFloat(offsetHeader))
    }, 500);
}
$("div[scrollTo][scrollTo!='']").click(function (e) { 
    e.preventDefault();
    let targetScroll = $(this).attr("scrollTo");
    let offset = ($(this).attr('scrollOffset') !== undefined) ? $(this).attr('scrollOffset') : 0;
    scrollTo(targetScroll, offset);
});


function ipmt(pv, pmt, rate, per) {
    var tmp = Math.pow(1 + rate, per);
    return 0 - (pv * tmp * rate + pmt * (tmp - 1));
}
function pmt(rate, nper, pv, fv, type) {
    if (!fv) fv = 0;
    if (!type) type = 0;

    if (rate == 0) return -(pv + fv)/nper;
    
    var pvif = Math.pow(1 + rate, nper);
    var pmt = rate / (pvif - 1) * -(pv * pvif + fv);

    if (type == 1) {
        pmt /= (1 + rate);
    };

    return pmt;
}

function ppmt(rate, per, nper, pv, fv, type) {
    if (per < 1 || (per >= nper + 1)) return null;
    var pmta = pmt(rate, nper, pv, fv, type);
    var ipmta = pmt(pv, pmta, rate, per - 1);
    return pmta - ipmta;
}


$(document).ready(function(){
    if (userID != null){
        var idlisting = $("#data-listing").attr('data-id');
        var url = API_URL + "/api/get/customers/logView?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
        if (cookieBroker != null){
            url = API_URL + "/api/get/customers/logView?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
        }
		$.post({
			"url" : url
		});
    }
    var startListing = $("#startListing").val();
    var days = days_passed(new Date(startListing));
    $("#lamaListing").html(days);
    $('#sliderHargaProperti').ionRangeSlider({
        // type: "double",
        grid: true,
        min: $("#sliderHargaProperti").attr('set-min'),
        max: $("#sliderHargaProperti").attr('set-max'),
        from: $("#sliderHargaProperti").attr('set-default'),
        // to: $("#sliderHargaProperti").attr('set-max'),
         step : 10000000,
        // grid_num: 4,
        // grid_snap:true,
        prefix: "Rp. ",
        onStart: function (data) {
            // fired then range slider is ready
            // console.log("onStartPriceSlider : " + data.from + "-" + data.to);
            // $(".priceInput.min").val(data.from);
            // $(".priceInput.max").val(data.to);
            $("#hargaProperti").change(function(){
                hargaPropertiInstance.update({
                    from: $("#hargaProperti").val()
                    //to: $(".priceInput.max").val()
                });
                updateUangMuka();
            });
            $("#hargaProperti").val(data.from);
        },
        onChange: function (data) {
            // fired on every range slider update
            // console.log("onChange : " + data.from + "-" + data.to);
            // $(".priceInput.min").val(data.from);
            // $(".priceInput.max").val(data.to);
            $("#hargaProperti").val(data.from);
        },
        onFinish: function (data) {
            // fired on pointer release
            //console.log("onFinish : " + data.from + "-" + data.to);
            // updateOptionBadge();
            updateUangMuka();
        },
        onUpdate: function (data) {
            // fired on changing slider with Update method
            //console.log("onUpdate : " + data.from + "-" + data.to);
        }
    });
    hargaPropertiInstance = $('#sliderHargaProperti').data("ionRangeSlider");


    $('#sliderUangMuka').ionRangeSlider({
        // type: "double",
        grid: true,
        min: $("#sliderUangMuka").attr('set-min'),
        max: $("#sliderUangMuka").attr('set-max'),
        from: 20,
        // to: $("#sliderHargaProperti").attr('set-max'),
        // step : 10000000,
        // grid_num: 4,
        // grid_snap:true,
        postfix: " %",
        onStart: function (data) {
            // fired then range slider is ready
            // console.log("onStartPriceSlider : " + data.from + "-" + data.to);
            $("#uangMukaPercent").val(data.from);
            // $(".priceInput.max").val(data.to);
            $("#uangMukaPercent").change(function(){
                uangMukaInstance.update({
                    from: $("#uangMukaPercent").val()
                    //to: $(".priceInput.max").val()
                });
                updateUangMuka();
            });
        },
        onChange: function (data) {
            // fired on every range slider update
            //console.log("onChange : " + data.from + "-" + data.to);
            // $(".priceInput.min").val(data.from);
            // $(".priceInput.max").val(data.to);
            $("#uangMukaPercent").val(data.from);
        },
        onFinish: function (data) {
            // fired on pointer release
            //console.log("onFinish : " + data.from + "-" + data.to);
            updateUangMuka();
        },
        onUpdate: function (data) {
            // fired on changing slider with Update method
            //console.log("onUpdate : " + data.from + "-" + data.to);
        }
    });
    uangMukaInstance = $('#sliderUangMuka').data("ionRangeSlider");


    $('#sliderJangkaWaktuAngsuran').ionRangeSlider({
        // type: "double",
        grid: true,
        min: $("#sliderJangkaWaktuAngsuran").attr('set-min'),
        max: $("#sliderJangkaWaktuAngsuran").attr('set-max'),
        from: 5,
        // to: $("#sliderHargaProperti").attr('set-max'),
        // step : 10000000,
        // grid_num: 4,
        // grid_snap:true,
        postfix: " Thn",
        onStart: function (data) {
            // fired then range slider is ready
            // console.log("onStartPriceSlider : " + data.from + "-" + data.to);
            $("#jangkaWaktuAngsuran").val(data.from);
            $('.longLoanLbl').html(data.from);
            $("#jangkaWaktuAngsuran").change(function(){
                jangkaWaktuAngsuranInstance.update({
                    from: $("#jangkaWaktuAngsuran").val()
                });
            });
        },
        onChange: function (data) {
            // fired on every range slider update
            //console.log("onChange : " + data.from + "-" + data.to);
            $("#jangkaWaktuAngsuran").val(data.from);
            $('.longLoanLbl').html(data.from);
        },
        onFinish: function (data) {
            // fired on pointer release
            //console.log("onFinish : " + data.from + "-" + data.to);
            hitungMonthlyPayment();
        },
        onUpdate: function (data) {
            // fired on changing slider with Update method
            //console.log("onUpdate : " + data.from + "-" + data.to);
        }
    });
    jangkaWaktuAngsuranInstance = $('#sliderJangkaWaktuAngsuran').data("ionRangeSlider");

    $('#sliderBungaFixedTahunan').ionRangeSlider({
        // type: "double",
        grid: true,
        min: $("#sliderBungaFixedTahunan").attr('set-min'),
        max: $("#sliderBungaFixedTahunan").attr('set-max'),
        from: 8.75,
        // to: $("#sliderHargaProperti").attr('set-max'),
        step : 0.25,
        // grid_num: 4,
        // grid_snap:true,
        postfix: " %",
        onStart: function (data) {
            // fired then range slider is ready
            // console.log("onStartPriceSlider : " + data.from + "-" + data.to);
            $('#bungaFixedTahunan').val(data.from);
            $('.percentRateLbl').html(data.from);
            $("#bungaFixedTahunan").change(function(){
                bungaFixedTahunanInstance.update({
                    from: $('#bungaFixedTahunan').val()                    
                });
            });
        },
        onChange: function (data) {
            // fired on every range slider update
            //console.log("onChange : " + data.from + "-" + data.to);
            $('#bungaFixedTahunan').val(data.from);
            $('.percentRateLbl').html(data.from);
        },
        onFinish: function (data) {
            // fired on pointer release
            //console.log("onFinish : " + data.from + "-" + data.to);
            hitungMonthlyPayment();        
        },
        onUpdate: function (data) {
            // fired on changing slider with Update method
            //console.log("onUpdate : " + data.from + "-" + data.to);
        }
    });
    bungaFixedTahunanInstance = $('#sliderBungaFixedTahunan').data("ionRangeSlider");
    updateUangMuka();
    hitungMonthlyPayment();
	initSlider();
});

function hitungMonthlyPayment(){
    let totalLoan = parseFloat($("#hargaProperti").val()) - parseFloat($("#uangMuka").val());
    let bungaBulanan = parseFloat($("#bungaFixedTahunan").val())/12/100;
    let jumlahAngsuran = parseFloat($("#jangkaWaktuAngsuran").val())*12;
    //console.log(totalLoan,bungaBulanan,jumlahAngsuran);
    console.log(totalLoan,bungaBulanan,jumlahAngsuran)
    //    480000000 0.7291666666666666 60
    let monthPayment = pmt(bungaBulanan,jumlahAngsuran,totalLoan).toFixed(0);// (totalLoan*bungaBulanan)/(1-Math.pow((1+bungaBulanan),-jumlahAngsuran));
    //console.log(pmt)
    
    $("#pmt").html(numberWithCommas(monthPayment*-1));
    let ppmtSum = 0;
    let ipmtSum = 0;
    for (let index = 0; index < jumlahAngsuran; index++) {
        ppmtSum += ppmt(bungaBulanan,index+1,jumlahAngsuran,totalLoan);   
        ipmtSum += ipmt(bungaBulanan,monthPayment,totalLoan,index);
    }
    ppmtSum*=-1;
    ipmtSum*=-1;
    $(".totalPokok").html(numberWithCommas(totalLoan));
    let totalBunga = monthPayment*-1*jumlahAngsuran - totalLoan;
    console.log(monthPayment, jumlahAngsuran, totalLoan);
    $(".totalBunga").html(numberWithCommas(totalBunga.toFixed(0)));
    $(".totalPokokPercent").css("width", totalLoan/(totalBunga+totalLoan)*100 + "%");
    $(".totalBungaPercent").css("width", totalBunga/(totalBunga+totalLoan)*100 + "%");
}

function updateUangMuka(){
    let uangMuka = parseFloat($("#hargaProperti").val()) * parseFloat($("#uangMukaPercent").val())/100;
    $("#uangMuka").val(uangMuka);
    hitungMonthlyPayment();
}

function onChangeHargaProperti(){
    updateSli
    updateUangMuka();
}











function favorite(element){
	if (userID == null){
        let action = "like";
		var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		let url = window.location.href;
		Swal.fire({
			type: 'error',
			title: 'Oops...',
			text: 'Harap Login Terlebih Dahulu',
			footer: '<a href="/login?last_url='+url+'&last_action='+action+'#'+idlisting+'">LOGIN DISINI</a>'
		});
	}else{
		var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		var status = !$(element).hasClass('favorite');
        $(element).toggleClass("favorite");
        if (status){
            var url = API_URL + "/api/get/customers/logFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
            if (cookieBroker != null){
                url = API_URL + "/api/get/customers/logFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
            }
            $.post({
                "url" : url
            });
        }
		$.post({
			"url" : API_URL + "/api/get/customers/updateFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&status=" + Number(status),
			success : function(data){
				data = JSON.parse(data);
				if (!data.status){
					$(element).toggleClass("favorite");
				}
			}
		});
	}
}

function printMe(){
    window.print();
}

function shareLink(element) {
	/* Get the text field */
    var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
    var text      = BASE_URL + "/detaillisting/view/" + idlisting;

	var body      = document.getElementsByTagName('body')[0];
	var tempInput = document.createElement('INPUT');
	body.appendChild(tempInput);
	tempInput.setAttribute('value', text)
	tempInput.select();
	document.execCommand('copy');
	body.removeChild(tempInput);

	/* Alert the copied text */
	Swal.fire({
		type : 'success',
		title: 'Sukses',
		text : 'Berhasil disalin ke Clipboard'
	});
	if (userID != null){
        var url = API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
        if (cookieBroker != null){
            url = API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
        }
		$.post({
			"url": url
		});
	}
}

function sendWA(element){
    var idListing = $(element).parent().parent().parent().parent().attr('data-id');
    if (userID != null){
        var url = API_URL + "/api/get/customers/logSendWA?token=customer&id_cust=" + userID +"&id_listing=" + idListing;
        if (cookieBroker != null){
            url = API_URL + "/api/get/customers/logSendWA?token=customer&id_cust=" + userID +"&id_listing=" + idListing + "&broker=" + cookieBroker;
        }
		$.post({
			"url": url
		});
	}
    var judul = $(element).parent().parent().parent().parent().attr('data-judul');
    var telp  = $(element).text();
    var pesan = "Halo,%20Saya%20mau%20menanyakan%20mengenai%20listingan%20" + judul;
    var win   = window.open("https://api.whatsapp.com/send?phone="+telp+"&text="+pesan, '_blank');
    win.focus();
}

function getJadwalBroker(){
    $.post({
        "url" : API_URL + "/api/get/appointments/getBrokerSchedule?token=public&broker=" + userID,
        success : function(data){
            data = JSON.parse(data);
            jadwalBroker = data.data;
        }
    });
}

function updateJamAppointment(hari, e){
    var broker = $(e).parent().parent().parent().parent().parent().attr("data-broker");
    $.post({
        "url" : API_URL + "/api/get/appointments/getBrokerSchedule?token=public&broker=" + broker,
        success : function(data){
            data = JSON.parse(data);
            var jadwalBroker = data.data;
            if (jadwalBroker[listDay2[hari]] == 0){
                $(".timepick").html("Tidak ada jadwal yang tersedia.");
            }else{
                var jamsekarang = new Date("2000-08-08 " + jadwalBroker['start_time']);
                var jarak = Number(jadwalBroker['padding_start']) + Number(jadwalBroker['padding_end']) + Number(jadwalBroker['duration']);
                var jamakhir = new Date("2000-08-08 " + jadwalBroker['end_time']);
                var dom =`<div class="flex-wrap flex-hor flex lastnorm px-5" style="padding:0px 40px; margin-bottom:20px; padding-right: 30px !important;">`;
                var active = " active";
                while ((jamsekarang.getHours()*60)+(jamsekarang.getMinutes())+jarak < (jamakhir.getHours()*60)+(jamakhir.getMinutes())){
                    var tempJam = jamsekarang.getHours();
                    var tempMin = jamsekarang.getMinutes();
                    if (tempJam < 10){
                        tempJam = "0" + tempJam;
                    }
                    if (tempMin < 10){
                        tempMin = "0" + tempMin;
                    }
                    dom += `<div style='width:calc(calc(100% / 4) - 4 * 5px); margin-right:5px;' class="rounded-s mini-btn mb-2 activeable-solo bg-red`+active+`" active-group="time-booking">`+tempJam+`:`+ tempMin + `</div>`;
                    active = "";
                    var menitbaru = (jamsekarang.getHours()*60)+(jamsekarang.getMinutes())+jarak;
                    jamsekarang.setHours(menitbaru/60);
                    jamsekarang.setMinutes(menitbaru%60);
                }
                dom += `</div>`;
                $(".timepick").html(dom);
                $(".activeable-solo").click(function(){
                    $(".activeable-solo[active-group='"+ $(this).attr("active-group") +"']").removeClass("active");
                    $(this).toggleClass("active");    
                });
            }
        }
    });
}

function jadwalkan(element) {
    if(!$(element).parent().children().first().next().find(".active").hasClass("active")) {
        Swal.fire({
            type : 'error',
            title: 'Error',
            text : 'Pastikan Sudah Memilih Jadwal Hari!'
        });
    } else {
        var idListing       = $(element).parent().parent().parent().attr('data-id');
        var idBroker        = $(element).parent().parent().parent().attr('data-broker');
        var dateAppointment = new Date().getFullYear() + "-" + ('0'.repeat(2) + (jQuery.inArray( $(element).parent().children().first().next().find(".active").children().first().next().next().text(), listMonth ) + 1)).slice(-2) + "-" + $(element).parent().children().first().next().find(".active").children().first().next().text();
        var timeAppointment = $(element).parent().children().first().next().next().find(".active").text();
        var selectedDate    = new Date(dateAppointment);
        var nameDate        = listDay[selectedDate.getDay()] + ", " + selectedDate.getDate() + " " + listMonth[selectedDate.getMonth()] + " " + selectedDate.getFullYear();

        Swal.fire({
            title            : 'Cek Ulang Apakah Jadwal Sudah Sesuai?',
            text             : "Dijadwalkan Pada " + nameDate + " Jam " + timeAppointment,
            type             : 'warning',
            showCancelButton : true,
            confirmButtonText: 'Ya, Sudah Yakin!',
            cancelButtonText : 'Batalkan!',
            reverseButtons   : true
        }).then(function(result){
            if (result.value) {
                if (userID != null){
                    var postData = JSON.stringify({"listing" : idListing, "customer" : userID,"broker" : idBroker,"time_appointment" : dateAppointment + " " + timeAppointment})
                    $.post(API_URL + "/api/post/appointments/add?token=customer", postData, function (data, status) {
                        Swal.fire({
                            type : 'success',
                            title: 'Sukses',
                            text : 'Berhasil Menjadwalkan!'
                        });
                    }, 'json');
                } else {
                    Swal.fire({
                        type : 'error',
                        title: 'Gagal',
                        text : 'Pastikan Anda Sudah Login!'
                    });
                }
            }
        });
    }
}

function xout(element){
	if (userID == null){
        let action = "xout";
		var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		let url = window.location.href;
		Swal.fire({
			type: 'error',
			title: 'Oops...',
			text: 'Harap Login Terlebih Dahulu',
			footer: '<a href="/login?last_url='+url+'&last_action='+action+'#'+idlisting+'">LOGIN DISINI</a>'
		});
	}else{
		var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		var status = !$(element).hasClass('xout');
		$(element).toggleClass("xout");
		$.post({
			"url" : API_URL + "/api/get/customers/updateXout?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&status=" + Number(status),
			success : function(data){
				data = JSON.parse(data);
				if (!data.status){
					$(element).toggleClass("xout");
				}
			}
		});
	}
}

var listMonth = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
var listDay = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
var listDay2 = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
var today = new Date();
let x = today;
var dom = "";
for (let index = 0; index < 14; index++) {
    x.setDate(x.getDate() + 1);
    let day = today.getDay();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = today.getMonth(); //January is 0!
    let yyyy = today.getFullYear();

    if (index==0) {
        dom += `<div class="date-item current flex flex-ver txt-center padding-m" onclick="updateJamAppointment('`+day+`', this);">`;    
    } else {
        dom += `<div class="date-item flex flex-ver txt-center padding-m" onclick="updateJamAppointment('`+day+`', this);">`;
    }
    dom += `<div>`+listDay[day]+`</div>`;
    dom += `<div>`+dd+`</div>`;
    dom += `<div>`+listMonth[mm]+`</div>`;
    dom += `</div>`;
}
$(".date-item-container").html(dom);


$(".date-swipe-right").click(function (e) { 
    e.preventDefault();
    var leftScroll = $(".date-item-container").scrollLeft()+100;
    var maxLeftScroll = $('.date-item-container').get(0).scrollWidth - $('.date-item-container').get(0).clientWidth;
    $(".date-item-container").stop().animate({scrollLeft:leftScroll}, 100, 'swing', function() { 
        if (leftScroll >= maxLeftScroll) {
            console.log("Mentok");
        }
     });
});
$(".date-swipe-left").click(function (e) { 
    e.preventDefault();
    var leftScroll = $(".date-item-container").scrollLeft()-100;
    var maxLeftScroll = $('.date-item-container').get(0).scrollWidth - $('.date-item-container').get(0).clientWidth;
    $(".date-item-container").stop().animate({scrollLeft:leftScroll}, 100, 'swing', function() { 
        if (leftScroll >= maxLeftScroll) {
            console.log("Mentok");
        }
     });
});

$('#map-slider').ionRangeSlider({
    type: "single",
    grid: true,
    min: 500,
    max: 3000,
    from: 1000,
    step : 500,
    grid_num: 6,
    prettify_separator: ",",
    hide_min_max: true,
    extra_classes: "ion-slider-map",
    grid_snap:true,
    onStart: function (data) {
        // fired then range slider is ready
        console.log("onStartPriceSlider : " + data.from);
        // $(".priceInput.min").val(data.from);
        // $(".priceInput.max").val(data.to);
        // $(".priceInput").change(function(){
        //     priceSliderInstance.update({
        //         from: $(".priceInput.min").val(),
        //         to: $(".priceInput.max").val()
        //     });
        // });
    },
    onChange: function (data) {
        // fired on every range slider update
        console.log("onChange : " + data.from);
        // $(".priceInput.min").val(data.from);
        // $(".priceInput.max").val(data.to);
    },
    onFinish: function (data) {
        // fired on pointer release
        radius = data.from;
        updateMarker(1, -2);
        //console.log("onFinish : " + data.from + "-" + data.to);
        // updateOptionBadge();
    },
    onUpdate: function (data) {
        // fired on changing slider with Update method
        //console.log("onUpdate : " + data.from + "-" + data.to);
    }
});
priceSliderInstance = $('#priceSlider').data("ionRangeSlider");


$(".date-item").click(function(){
    $(".date-item.active").removeClass("active");
    $(this).addClass("active");
    $(".timepick").slideDown(250);
    
});

$(".mapCategoryFilter").click(function(){
    // Rob ajax disini untuk card near place 


    //
    /* Ini Satu Category (Ex : Sekolah)
        <div class="mapNearPlace">		
            <h6 class="mapNearCategory">Sekolah <span>x</span></h6>
            <div class="flex-flex-ver mapNearItemContainer minimalistScrollbar" style="max-height:200px; overflow-y:auto;">
                <?php for ($i=0; $i < 10; $i++) { ?>
                
                
                <div class="flex flex-hor flex-vertical-center mapItem">
                    <div class="mapItemName">Sekolah Menengah Atas Negeri 9 Surabaya</div>
                    <div class="flex flex-ver">
                        <div class="map-time">3 Mins</div>
                        <div class="map-far">230 m</div>
                    </div>
                </div>
            
            <?php } ?>
            </div>
        </div>
    */
});











/*
var myModal = new notif("Create","Body","");
myModal.appendButtonTrigger(`<button type="button" class="btn btn-danger btnReject" @modal>Reject</button>`,"#actions");
myModal.appendButtonTrigger(`<button type="button" class="btn btn-success btnApprove" @modal>Approve</button>`,"#actions");
myModal.appendModalTo("body");

$(".btnReject").click(function () { 
  var body = "";
  body += "<input type='hidden' id='modal-id' name='id'>";
  body += "Are You Sure to Reject Current Listing ?";
  notif.changeStructure("Reject Listing", body, `<button type="button" class="btn btn-danger" data-dismiss="modal" onClick="declineListing();">REJECT</button>`)  ;
  $("#modal-id").attr("value",$(this).parent().attr("data-id"));  
});


$(".btnApprove").click(function () { 
  var body = "";
  body += "<input type='hidden' id='modal-id' name='id'>";
  body += "Are You Sure to Reject Current Listing ?";
  notif.changeStructure("Reject Listing", body, `<button type="button" class="btn btn-success" data-dismiss="modal" onClick="acceptListing();">APPROVE</button>`)  ;
  $("#modal-id").attr("value",$(this).parent().attr("data-id"));  
});

*/