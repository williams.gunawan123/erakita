var idDetail = 0;
var flag = 0;
var ctrListing = -1;
var selectedCenterMap = -1;
var jumlahListing=100;
var limit = 10;
var page = 1;

var spiderConfig = {
	keepSpiderfied: true,
	event: 'mouseover',
	circleSpiralSwitchover:0
};

var pagination = null;

$(document).ready(function () {
	var url = window.location.href;
    var arrUrl = url.split('/');
	var arrUrl2 = arrUrl[3].split('?');
	if (arrUrl2.length > 1){
		var param = arrUrl2[1];
		var sendSort = param.split("&sort=");
		sendData = sendSort[0];
		sortData = "&sort=" + sendSort[1];
	}
	loadList();	
});

var pageHandler = function pageHandling(){
	page = pagination.getCurrentPage();
	loadList();
}

function showListing(data){
	var row = "";
	var ctr = 1;
	var markers = [];
	var infowindow = new google.maps.InfoWindow();
	var infowindowstate = false;
	var markerSpiderfier = new OverlappingMarkerSpiderfier(map, spiderConfig);
	   
	
	data.data.forEach(e => {
		ctrListing++;
		var gambar = [];
		for (var i=1; i<=e.gambar; i++){
			gambar.push("/assets/img/listing/"+e.id_listing+"/"+i+".jpg");
		}
		gambar = JSON.stringify(gambar);

		if (ctr % 2 == 1){
			row += `<div class="row">`;
		}
		if (e.jenis_transaksi == "0"){
			var jenis = "Jual";
			var bgJenis = "jual";
			var harga = shortNumber(e.harga_jual);
		}else if (e.jenis_transaksi == "1"){
			var jenis = "Sewa";
			var bgJenis = "sewa";
			var harga = shortNumber(e.harga_sewa) + "/thn";
		}else{
			var jenis = "Jual Sewa";
			var bgJenis = "jual_sewa";
			var harga = shortNumber(e.harga_jual) + " - " + shortNumber(e.harga_sewa) + "/thn";;
		}
		var favorite = "";
		if (e.favorite==1){
			favorite = "favorite";
		}
		var pinIcon = new google.maps.MarkerImage(
			// "/assets/img/marker/" + e.nama_tipe + " " + jenis + ".png",
			"/assets/img/marker/label.png",
			null, /* size is determined at runtime */
			null, /* origin is 0,0 */
			null, /* anchor is bottom center of the scaled image */
			new google.maps.Size(40, 30));  /* Size given in CSS Length units */
		
		var latLng = new google.maps.LatLng(e.lat_listing, e.long_listing);
		var marker = new google.maps.Marker({
			position: latLng,
			icon: pinIcon
		});
		marker.setLabel(harga);
		markerSpiderfier.addMarker(marker);  // Adds the Marker to OverlappingMarkerSpiderfier
		
		overlay = new CustomMarker(
			latLng, 
			map,harga,jenis,
			{
				marker_id: '123'
			}
		);
		
		google.maps.event.addListener(marker, 'spider_click', function() {
			if (!infowindowstate){
				var html = "<b>"+e.judul_listing+"</b><hr>";
				html += jenis + " - " + harga;
				infowindow.setContent(html);
				infowindow.open(map, this);
				infowindowstate = true;
			}else{
				infowindow.close();
				infowindowstate = false;
			}
		});
		markers.push(marker);
		

		row += `	<div class="col-lg-6 col-sm-12 ">`;
		row += `		<div onclick='selectListingCard(this);' class="listing-item m-portlet m-portlet--rounded" id='listing`+ctrListing+`' data-ctr='`+ctrListing+`' data-id='`+e.id_listing+`' data-telp='`+e.telp_broker+`' data-judul='`+e.judul_listing+`' data-lat='`+e.lat_listing+`' data-lng='`+e.long_listing+`' data-tipe='`+e.nama_tipe+`' data-jenis='`+jenis+`'>`;
		row += `			<div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='`+gambar+`'>`;
		row += `				<div class="listing-slider-badge jenis-`+bgJenis+`">` + jenis + `</div>`;
		row += `				<div class="listing-slider-icon jenis-`+bgJenis+`"><i class="fas fa-home"></i></div>`;
		row += `				<div class="listing-slider-counter" style="z-index:2;">1 of 3</div>`;
		row += `			</div>`;
		row += `			<div class="m-portlet__body" style="padding:0px;">`;
		row += `				<div class="col-lg-10 col-md-10 col-sm-12" style="padding: 10px 5px 10px 10px;">`;
		row += `					<div class="listing-price listing-info">` + harga + `</div>`;
		row += `					<div class="listing-title listing-info truncate hoverable" onclick="viewDetail(this);"><b>` + e.judul_listing + `</b></div>`;
		row += `					<div class="listing-jalan listing-info truncate hoverable" onclick="updateCenterMap(this);">` + e.alamat_listing + `</div>`;
		row += `					<div class="listing-kota listing-info truncate">` + e.nama_kecamatan + `, ` + e.nama_kota + `</div>`;
		row += `					<div class="listing-detail">`;
		if (e.kamar_tidur != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>`+e.kamar_tidur+`</div>`;
		}
		if (e.kamar_mandi != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>`+e.kamar_mandi+`</div>`;
		}
		if (e.luas_tanah != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>` + e.luas_tanah + `m<sup>2</sup></div>`;
		}
		if (e.luas_bangunan != null){
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>` + e.luas_bangunan + `m<sup>2</sup></div>`;
		}
		row += `					</div>`;
		row += `				</div>`;
		row += `				<div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding:10px 10px 10px 5px">`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='sendWA(this);' class="fab fa-whatsapp fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Pesan WhatsApp"></i></div>`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='favorite(this);' class="fas fa-heart fa-lg ` + favorite + `" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Favorit"></i></div>`;
		row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='shareLink(this);' class="fas fa-share fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Bagikan"></i></div>`;
		row += `					<a href="/detaillisting/view/`+e.id_listing+`" style="margin:auto;" class="listing-content-icon txt-black"><i class="fas fa-ellipsis-h fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Tampilkan Lebih Banyak"></i></a>`;
		row += `				</div>`;
		row += `			</div>`;
		row += `		</div>`;
		row += `	</div>`;
		if (ctr % 2 == 0){
			row += `</div>`;
		}
		ctr++;
	});

	markerSpiderfier.addListener('click', function(marker, e) {
		infowindow.setContent(marker.title);
		infowindow.open(map, marker);
	});
	markerSpiderfier.addListener('spiderfy', function(markers) {
		infowindow.close();
	});

	if (flag != 0){
		initMarker(markers);
	}else{
		var centerMap = [data.data[0].lat_listing, data.data[0].long_listing];
		initMap(markers, centerMap);
	}
	$("#showedListing").html(ctrListing+1);
	$("#totalListing").html(ctrListing+1);
	flag += 10;
	if (ctr % 2 == 0){
		row += `</div>`;
	}
	$("#tabs-foto").html(row);
	initSlider();
	mApp.initPopovers();
}

function loadList(requestPage = null){
	if (requestPage != null){
		page = requestPage;
	}
	mApp.block("#tabs-foto", {});
    $.ajax({
		"url" : API_URL + "/api/get/listings/search?token=public&user_cust=" + userID + "&limit=10&offset=" + (page-1)*limit + "&save=" + saveSearch + sendData + sortData,
        // "url" : API_URL + "/api/get/listings/getActive?token=public&user_cust=" + userID,
        success : function(data){
		  data = JSON.parse(data);
		  if (page == 1){
			pagination = new Paginate(".sensecode-paginate", (data.count-1)/limit+1, 1, pageHandler);
			pagination.addActiveItemClass(["bg-red txt-white"]);
			pagination.addItemClass(["clickable"]);
			pagination.viewExample(0);

			var newUrl = "/listing?"+sendData+sortData;
			window.history.pushState(data, "EraKita | Listing", newUrl);
		  }
		  showListing(data);
		  
		  mApp.unblock("#tabs-foto", {});
        }
	});
}

function updateCenterMap(element){
	var lat = $(element).parent().parent().parent().attr('data-lat');
	var lng = $(element).parent().parent().parent().attr('data-lng');
	var ctrMarker = $(element).parent().parent().parent().attr('data-ctr');
	var center = new google.maps.LatLng(lat, lng);
	map.panTo(center);
	var pinIcon = new google.maps.MarkerImage(
		"/assets/img/marker/default.png",
		null, /* size is determined at runtime */
		null, /* origin is 0,0 */
		null, /* anchor is bottom center of the scaled image */
		new google.maps.Size(30, 50));  /* Size given in CSS Length units */
	allMarkers[ctrMarker].setIcon(pinIcon);
	if (selectedCenterMap != -1){
		var tipe = $("#listing"+selectedCenterMap).attr('data-tipe');
		var jenis =  $("#listing"+selectedCenterMap).attr('data-jenis');
		var pinIcon = new google.maps.MarkerImage(
			"/assets/img/marker/" + tipe + " " + jenis + ".png",
			null, /* size is determined at runtime */
			null, /* origin is 0,0 */
			null, /* anchor is bottom center of the scaled image */
			new google.maps.Size(40, 50));  /* Size given in CSS Length units */
		allMarkers[selectedCenterMap].setIcon(pinIcon);
	}
	selectedCenterMap = ctrMarker;
}

function viewDetail(element){
	var idlisting = $(element).parent().parent().parent().attr('data-id');
	if (idlisting == idDetail){
		$("#my-chevron").click();    
	}else{
		idDetail = idlisting;
		$("#detailListing>div").fadeOut(100, function(){
            $.post({
				"url" : API_URL + "/api/get/listings/getById/"+idlisting+"?token=public",
				success : function(data){
					data = JSON.parse(data);
					if(data.jenis_transaksi == 0){
						var jenis = "Jual";
						var harga = "Rp " + numberWithCommas(data.harga_jual);
					}else if(data.jenis_transaksi == 1){
						var jenis = "Sewa";
						var harga = "Rp " + numberWithCommas(data.harga_sewa) + "/thn";
					}else{
						var jenis = "Jual - Sewa";
						var harga = "Rp " + numberWithCommas(data.harga_jual) + " - Rp " + numberWithCommas(data.harga_sewa) + "/thn";
					}
					var gambar= "";
					var thumb = "";
					var dom = `<div class="single-list-slider owl-carousel" id="sl-slider">`;
					for (var i=1; i<=data.gambar; i++){
						gambar += `<div class="sl-item set-bg" data-setbg="/assets/img/listing/`+data.id_listing+`/`+i+`.jpg">`;
						gambar += `	<div class="sale-notic">FOR SALE</div>`;
						gambar += `</div>`;
						thumb += `<div class="sl-thumb set-bg" data-setbg="/assets/img/listing/`+data.id_listing+`/`+i+`.jpg"></div>`;
					}
					dom += gambar + `</div>`;
					dom += `<div class="owl-carousel sl-thumb-slider" id="sl-slider-thumb">`;
					dom += thumb + `</div>`;

					$("#my-slider").html(dom);
					initSlSlider();
					$("#detailJudul").html(data.judul_listing);
					$("#detailAlamat").html('<i class="fa fa-map-marker"></i> ' + data.alamat_listing);
					$("#detailHarga").html(harga);
					$("#detailLuas").html('<i class="fa fa-th-large"></i> ' + data.luas_tanah + " m<sup>2</sup>")
					$("#detailKTidur").html('<i class="fa fa-bed"></i> ' + data.kamar_tidur + " Kamar Tidur");
					$("#detailBroker").html('<i class="fa fa-user"></i> ' + data.nama_broker);
					$("#detailGarasi").html('<i class="fa fa-car"></i> ' + data.jml_garasi + " Garasi");
					$("#detailLantai").html('<i class="fa fa-building-o"></i> ' +data.jml_lantai +' Tingkat');
					$("#detailKMandi").html('<i class="fa fa-bath"></i> ' + data.kamar_mandi + " Kamar Mandi");
					$("#detailUmur").html('<i class="fa fa-trophy"></i> Tahun ' + data.tahun_bangunan);
					$(".description").html('<p>'+data.deskripsi_listing+'</p>');
					if (data.url_video != null){
						var video = '<iframe width="100%" height="500px" src="'+data.url_video + '"></iframe>';
					}else{
						var video = "No Video Preview";
					}
					$(".perview-video").html(video);
				}
			}); 
            $("#detailListing>div").fadeIn(1000);
        });
		if ($("#my-chevron").attr("data-show")=="hide") {
			$("#my-chevron").click();
		} 
	}
}

function favorite(element){
	if (userID == null){
		Swal.fire({
			type: 'error',
			title: 'Oops...',
			text: 'Harap Login Terlebih Dahulu',
			footer: '<a href="/login">LOGIN DISINI</a>'
		});
	}else{
		var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		var status = !$(element).hasClass('favorite');
		$(element).toggleClass("favorite");
		if (status){
			var url = API_URL + "/api/get/customers/logFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
			if ($cookieBroker != null){
				url = API_URL + "/api/get/customers/logFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
			}
			$.post({
				"url" : url
			});
		}
		$.post({
			"url" : API_URL + "/api/get/customers/updateFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&status=" + Number(status),
			success : function(data){
				data = JSON.parse(data);
				if (!data.status){
					$(element).toggleClass("favorite");
				}
			}
		});
	}
}

function sendWA(element){
	var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
	var judul = $(element).parent().parent().parent().parent().attr('data-judul');
	var telp = $(element).parent().parent().parent().parent().attr('data-telp');
	var pesan = "Halo,%20Saya%20mau%20menanyakan%20mengenai%20listingan%20" + judul;
	var win = window.open("https://api.whatsapp.com/send?phone="+telp+"&text="+pesan, '_blank');
	if (userID != null){
		var url = API_URL + "/api/get/customers/logSendWA?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
		if (cookieBroker != null){
			url = API_URL + "/api/get/customers/logSendWA?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
		}
		$.post({
			"url" : url
		});
	}
	win.focus();
}

function shareLink(element) {
	/* Get the text field */
	var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
	var text = BASE_URL + "/detaillisting/view/" + idlisting;

	var body = document.getElementsByTagName('body')[0];
	var tempInput = document.createElement('INPUT');
	body.appendChild(tempInput);
	tempInput.setAttribute('value', text)
	tempInput.select();
	document.execCommand('copy');
	body.removeChild(tempInput);

	/* Alert the copied text */
	Swal.fire({
		type: 'success',
		title: 'Sukses',
		text: 'Berhasil disalin ke Clipboard'
	});
	if (userID != null){
		var url = API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting;
		if (cookieBroker != null){
			url = API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&broker=" + cookieBroker;
		}
		$.post({
			"url" : url
		});
	}
}

$(".chevron").click(function (e) {
	if ($(this).attr("data-show") == "hide") {
		$(this).attr("data-show", "show");
		$(this).removeClass("chevron-flip");
		$("#detailListing").css("transform", "translateX(0)");
	} else {
		$(this).attr("data-show", "hide");
		$(this).addClass("chevron-flip");
		$("#detailListing").css("transform", "translateX(-100%)");
	}
});

function selectListingCard(ob){
	$(".listing-item.active").removeClass("active");
	$(ob).addClass("active");
}