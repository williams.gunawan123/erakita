$( document ).ready(function() {
    // Do your Ajax Here

	function next(to){
		if (listImage.length>1) {
			index+= to;
			index = index % listImage.length;
			if (index<0) {
				index=listImage.length-1;
			}
			$("#carousel-preview").css("background-image","url("+listImage[index]+")");
		}
	}

	$(".card-img>.next").click(function(){
		index = parseInt( $(this).attr("data-index"));	
		index += 1;
		index = index % 3;
		var img = $(this).attr("data-img-"+index);
		$(this).css("background-image","url("+img+")");
	});

	
	$(".card-img>.previous").click(function(){
		index = parseInt( $(this).attr("data-index"));	
		index -= 1;
		if (index<0) {
			index = 2;
		}
		var img = $(this).attr("data-img-"+index);
		$(this).css("background-image","url("+img+")");
	});

    function showHotListing(){
			$.ajax({
				"url" : API_URL + "/api/get/listings/getActive?token=public&limit=6&offset=0",
				success : function(data){
					data = JSON.parse(data);
					var row = "";
					$.each(data.data, function (indexInArray, e) { 
						var jualsewa = "";
						e.harga_listing = 0;
						if (e.jenis_transaksi == 0) {
							e.harga_listing = numberWithCommas(e.harga_jual);	
							jualsewa = "Jual";
						} else if (e.jenis_transaksi == 1) {
							e.harga_listing = numberWithCommas(e.harga_sewa);	
							jualsewa = "Sewa";
						} else if (e.jenis_transaksi == 2) {
							e.harga_listing = numberWithCommas(e.harga_jual) + " - " + numberWithCommas(e.harga_sewa) + "/th";	
							jualsewa = "Jual/Sewa";
						}

						row += "<div class='col-lg-4 col-md-6'>";
						row += "<div class='feature-item'>";
						row += "<div data-img-0='1.jpg' data-img-1='2.jpg' data-img-2='3.jpg' data-index='1' class='feature-pic set-bg' style=\"background-image: url('/assets/img/listing/"+ e.id_listing + "/1.jpg')\">";
						row += "<div class='sale-notic'>"+jualsewa+"</div>";						
						row += "</div>";
						row += "<div class='feature-text'>";
						row += "<div class='text-center feature-title'>";
						row += "<a href='/detaillisting/view/"+e.id_listing+"'><h5 class='truncate hoverable' style='padding:0px 10px;'>"+ e.judul_listing+"</h5></a>";
						row += "<p class='truncate' style='width:75%; margin:auto;'><i class='fa fa-map-marker'></i> " + e.alamat_listing + "</p>";
						row += "</div>";
						row += "<div class='room-info-warp flex flex-hor' style='flex-wrap:wrap;'>";
						
						if (e.luas_tanah != null){
							row += "<p class='flex-2'><i class='fa fa-th-large'></i> " + e.luas_tanah + " m<sup>2</sup></p>";
						}else{
							row += "<p class='flex-2'><i class='fa fa-th-large'></i> N/A </p>";
						}
						if (e.kamar_tidur != null){
							row += "<p class='flex-2'><i class='fa fa-bed'></i> "+ e.kamar_tidur+" Kamar</p>";
						}else{
							row += "<p class='flex-2'><i class='fa fa-bed'></i> N/A </p>";
						}
						if (e.luas_bangunan != null){
							row += "<p class='flex-2'><i class='fa fa-building'></i> " + e.luas_bangunan + " m<sup>2</sup></p>";
						}else{
							row += "<p class='flex-2'><i class='fa fa-building'></i> N/A </p>";
						}
						if (e.kamar_mandi != null){
							row += "<p class='flex-2'><i class='fa fa-bath'></i> "+e.kamar_mandi +" Kamar Mandi</p>";
						}else{
							row += "<p class='flex-2'><i class='fa fa-bath'></i> N/A </p>";
						}
						
						row += "</div>";
						row += "<div class='room-info' style='padding-left: 10px;padding-right: 10px;'>";
						row += "<div class='rf-left'>";
						row += "<p><i class='fa fa-user'></i> <a href='/broker/detail/"+e.id_broker+"'>" + e.nama_broker + "</a></p>";
						row += "</div>";
						row += "<div class='rf-right'>";

						var days = days_passed(new Date(e.start_listing));

						row += "<p><i class='fa fa-clock-o'></i> "+days+"</p>";
						row += "</div>";
						row += "</div>";
						row += "</div>";						
						
						row += "<a href='/detaillisting/view/"+e.id_listing+"' class='room-price'>Rp " + e.harga_listing + "</a>";
						row += "</div>";
						row += "</div>";
						row += "</div>";
						//console.log(valueOfElement);
					});
					// data.ListingData.forEach(e => {
					// 	row += "<div class='col-lg-4 col-md-6'>";
					// 	row += "<div class='feature-item'>";
					// 	row += "<div data-img-0='" + e.id_listing + "-1.jpg' data-img-1='" + e.id_listing + "-1.jpg' data-img-2='" + e.id_listing + "-1.jpg' data-index='1' class='feature-pic set-bg' style=\"background-image: url('/assets/upload/listing/"+ e.id_listing + "-1.jpg')\">";
					// 	row += "<div class='sale-notic'>FOR SALE</div>";						
					// 	row += "</div>";
					// 	row += "<div class='feature-text'>";
					// 	row += "<div class='text-center feature-title'>";
					// 	row += "<h5>Jual Rumah ...</h5>";
					// 	row += "<p><i class='fa fa-map-marker'></i> " + e.jalan_listing + "</p>";
					// 	row += "</div>";
					// 	row += "<div class='room-info-warp'>";
					// 	row += "<div class='rf-left'>";
					// 	row += "<p><i class='fa fa-th-large'></i> " + (e.ltanah_listing*e.ptanah_listing) + " m2</p>";
					// 	row += "<p><i class='fa fa-bed'></i> 10 Bedrooms</p>";
					// 	row += "</div>";
					// 	row += "<div class='rf-right'>";
					// 	row += "<p><i class='fa fa-car'></i> 2 Garages</p>";
					// 	row += "<p><i class='fa fa-bath'></i> 6 Bathrooms</p>";
					// 		row += "</div>";
					// 	row += "</div>";
					// 	row += "<div class='room-info'>";
					// 	row += "<div class='rf-left'>";
					// 	row += "<p><i class='fa fa-user'></i> " + e.nama_user + "</p>";
					// 	row += "</div>";
					// 	row += "<div class='rf-right'>";
					// 	row += "<p><i class='fa fa-clock-o'></i> 1 days ago</p>";
					// 	row += "</div>";
					// 	row += "</div>";
					// 	row += "</div>";
					// 	e.harga_listing = numberWithCommas(e.harga_listing);
					// 	row += "<a href='#' class='room-price'>Rp " + e.harga_listing + "</a>";
					// 	row += "</div>";
					// 	row += "</div>";
					// 	row += "</div>";


					// 	// if (e.status_listing == 0){
					// 	// 	row += "<td>Pending</td>";
					// 	// }else{
					// 	// 	row += "<td>Active</td>";
					// 	// }
					// 	// row += "<td><a href='detaillisting/view/" + e.id_listing + "'><button class='btn btn-primary'><i class='fa fa-search'></i></button></a></td>";
					// 	// row += "</tr>";
					// });
					$("#hotListing").html(row);        
				}
			});
		}
		showHotListing();

		function showRecentSold(){
			$.ajax({
				"url" : API_URL + "/api/get/listings/getActive?token=public&limit=4&offset=0",
				success : function(data){
					data = JSON.parse(data);
					var row = "";
					// belum jadi 
					data.data.forEach(e => {
						var jualsewa = "";
						e.harga_listing = 0;
						if (e.jenis_transaksi == 0) {
							e.harga_listing = shortNumber(e.harga_jual);	
							jualsewa = "Jual";
						} else if (e.jenis_transaksi == 1) {
							e.harga_listing = shortNumber(e.harga_sewa);	
							jualsewa = "Sewa";
						} else if (e.jenis_transaksi == 2) {
							e.harga_listing = shortNumber(e.harga_jual) + " - " + shortNumber(e.harga_sewa) + "/th";	
							jualsewa = "Jual/Sewa";
						}
						
						//console.log(e);
						row += `<div class="col-md-6">`;
						row += `	<div class="propertie-item set-bg" style=\"background-image: url('/assets/img/listing/` + e.id_listing + `/1.jpg')\">`;
						row += `		<div class="sale-notic">`+jualsewa+`</div>`;
						row += `		<div class="propertie-info text-white">`;
						row += `			<div class="info-warp" style="width:75%;">`;
						row += `				<a href='/detaillisting/view/`+e.id_listing+`'><h5 class="truncate hoverable" style="width:100%;">`+e.judul_listing+`</h5>`;
						row += `				<p class="truncate" style="width:100%;"><i class="fa fa-map-marker"></i> ` + e.alamat_listing + `</p>`;
						row += `			</div>`;
						row += `			<div class="price">Rp ` + e.harga_listing + `</div>`;
						row += `		</div>`;
						row += `	</div>`;
						row += `</div>`;
					});
					$("#recentSold").html(row);        
				}
			});
		}
		showRecentSold();
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}