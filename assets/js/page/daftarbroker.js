var page = 1;
var limit = 9;
var sendData = "";
$(document).ready(function () {
    loadList();
    loadDaftarCabang();
});
function loadDaftarCabang(){
    //http://api.erakita.local/api/get/Cabangs/getActive?token=customer
    $.get(API_URL + "/api/get/Cabangs/getActive?token=customer",  function (data) {
        //console.log(data.data);
        var cabangList = "";
        //cabangList += `<option value="">Semua</option>`;
        $.each(data , function(i, e) { 
            cabangList += `<option value="`+e.nama_cabang+`">`;
            cabangList += e.nama_cabang;
            cabangList += `</option>`;
            $('#srCabangBroker').html(cabangList);
        });
        $('#srCabangBroker').selectpicker('refresh');
    },'json');
}
var pageHandler = function pageHandling(){
	page = pagination.getCurrentPage();
	loadList();
}
function loadList(){
    mApp.block('#daftarBroker', {});
    $.ajax({
        "url" : API_URL + "/api/get/brokers/getActive?token=public&limit="+limit+"&offset=" + (page-1)*limit + sendData + sortData,
        success : function(data){
          data = JSON.parse(data);
          if (page == 1){
            pagination = new Paginate(".sensecode-paginate", (data.jumlahTerhit-1)/limit+1, 1, pageHandler);
            pagination.addActiveItemClass(["bg-red txt-white"]);
            pagination.addItemClass(["clickable"]);
            pagination.viewExample(0);
            $("#showedBroker").html(data.jumlahTerhit);
            $("#totalBroker").html(data.jumlahBroker);
          }
		  var row = "";
          data.data.forEach(e => {
            if (e.foto_broker != ""){
                var foto = e.foto_broker;
            }else{
                var foto = "default.png";
            }
			    row+= `<div class="col-12 col-sm-12 col-md-6 p-2 col-lg-4">`;
				row+= `	<div class="col-12 card">`;
				row+= `		<div class="row">`;
				row+= `			<div class="col-9 align-items-center txt-center d-flex flex-column align-self-center">`;
				row+= `				<div class="hoverable txt-black" style="font-size:1.5em"><a href="/broker/detail/`+e.id_broker+`">` + e.nama_broker + `</a></div>`;
				row+= `				<div class="hoverable txt-black label"><a href="/cabang/detail/`+e.id_cabang+`">` + e.nama_cabang + `</a></div>`;
				row+= `				<div class="ratio11 square align-self-center circle col-6 p-0 div-image my-1" style="background-image:url('/assets/img/profile-broker/` + foto + `');"></div>`;
				row+= `			</div>`;
				row+= `			<div class="col-3 align-self-center">`;
				row+= `				<button type="button" class="rounded-top btn col-12 btn-info"><span style="color: #FFFFFF; font-size: 1.3em;" class="fa fa-phone"></span></button>`;
				row+= `				<button onclick="sendWA('` + e.nama_broker + `','` + e.telp_broker + `');" type="button" class="rounded-bottom btn col-12 btn-success"><span style="color: #FFFFFF; font-size: 1.3em;" class="fa fa-whatsapp"></span></button>`;
				row+= `			</div>`;
				row+= `		</div>`;
				row+= `		<div class="col-12 px-0 flex-hor flex-wrap pb-4 pt-1 d-flex">`;
				row+= `			<div class="col-12 col-sm-12 col-md-6 col-lg-6 p-1">`;
				row+= `				<div class="col-12 bg-success txt-white rounded py-2">`;
				row+= `					<i class="fa fa-home fa-lg"></i>`;
				row+= `					<div class="txt-bold">Mensharekan</div>`;
				row+= `					<div style="font-size:0.9em;">`+e.mensharekan+` Properti</div>`;
				row+= `				</div>`;
				row+= `			</div>`;
				row+= `			<div class="col-12 col-sm-12 col-md-6 col-lg-6 p-1">`;
				row+= `				<div class="col-12 bg-success txt-white rounded py-2">`;
				row+= `					<i class="fa fa-home fa-lg"></i>`;
				row+= `					<div class="txt-bold" >Mensharekan</div>`;
				row+= `					<div style="font-size:0.9em;">`+e.mensharekan+` Properti</div>`;
				row+= `				</div>`;
				row+= `			</div>`;
				row+= `			<div class="col-12 col-sm-12 col-md-6 col-lg-6 p-1">`;
				row+= `				<div class="col-12 bg-success txt-white rounded py-2">`;
				row+= `					<i class="fa fa-home fa-lg"></i>`;
				row+= `					<div class="txt-bold" >Mensharekan</div>`;
				row+= `					<div style="font-size:0.9em;">`+e.mensharekan+` Properti</div>`;
				row+= `				</div>`;
				row+= `			</div>`;
				row+= `			<div class="col-12 col-sm-12 col-md-6 col-lg-6 p-1">`;
				row+= `				<div class="col-12 bg-success txt-white rounded py-2">`;
				row+= `					<i class="fa fa-home fa-lg"></i>`;
				row+= `					<div class="txt-bold" >Mensharekan</div>`;
				row+= `					<div style="font-size:0.9em;">`+e.mensharekan+` Properti</div>`;
				row+= `				</div>`;
				row+= `			</div>`;
				row+= `		</div>`;
				row+= `	</div>`;
				row+= `</div>`;
		  });
          $("#daftarBroker").html(row);
          mApp.unblock('#daftarBroker', {});
        }
    });
}

function sendWA(namabroker, telp){
    var pesan = "Halo " + namabroker + ",%20Saya%20mau%20menanyakan%20mengenai%20listing%20";
    var win = window.open("https://api.whatsapp.com/send?phone="+telp+"&text="+pesan, '_blank');
    win.focus();
}

function reset(){
    $("#srNamaBroker").val("");
    $("#srCabangBroker").val("");
    page = 1;
    sendData = "";
    loadList();
}

function searchBroker(){
    page = 1;
    sendData = "";
    if ($("#srNamaBroker").val() != ""){
        sendData += "&nama="+ $("#srNamaBroker").val();
    }
    if ($("#srCabangBroker").val() != "" && $("#srCabangBroker").val().length > 0){
        sendData += "&cabang="+JSON.stringify($("#srCabangBroker").val());
    }
    loadList();
}

var sort = "aktivitas";
var urutan = "desc";
var sortData = "&sort=aktivitas-desc";
function urutkanBroker(e){
    $("#sort-" + sort).find(".fa.fa-check").remove();
    sort = $(e).attr("data-value");
    $("#sort-" + sort).append(" <i class='fa fa-check'></i>");
    urutan = $("#urutan").find(".active").attr("data-value");
    
    sortData = "&sort=" + sort + "-" + urutan;

    loadList();
}
function urutkanBrokerAZ(temp){
    urutan = temp;
    sortData = "&sort=" + sort + "-" + urutan;

    loadList();
}