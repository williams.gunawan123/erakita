$(document).ready(function () {
    loadJadwal();
});

var listMonth = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
var listDay = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];

function loadJadwal(){
    var dom = "";
    if (userID != null){
        $.get(API_URL + "/api/post/appointments/getByCustomer?token=customer&customer=" + userID,  function (data) {
            data.data.forEach(e => {
                var dateAppointment = new Date(e.time_appointment);
                var h = dateAppointment.getHours();
                var m = dateAppointment.getMinutes();
                if (h < 10) h = '0' + h;
                if (m < 10) m = '0' + m;
                var timeAppointment = h + ":" + m;

                $.get(API_URL + "/api/get/listings/getById/"+e.id_listing+"?token=public",  function (listing) {
                    dom += `<tr>`;
                    dom += `<td>
                                <a href="/detaillisting/view/`+e.id_listing+`">`+listing.judul_listing+`</a>
                                <div class="label">`+listing.alamat_listing+`</div>
                                <div class="label">Gudang sewa, >10,000 m<sup>2</sup></div>
                            </td>`;
                    dom += `<td>`+listDay[dateAppointment.getDay()]+`</td>`;
                    dom += `<td>`+timeAppointment+` / `+dateAppointment.getDate()+` `+listMonth[dateAppointment.getMonth()]+` `+dateAppointment.getFullYear()+`</td>`;
                    dom += `<td>
                                <div class="flex flex-hor" style="position:relative;">
                                    <a href="#" class="user-photo-mini" style="margin:5px 5px; background-image:url('/assets/img/profile-customer/`+listing.foto_broker+`');"></a>
                                    <div class="flex flex-ver" style='justify-content: center;'>
                                        <div><b>`+listing.nama_broker+`</b></div>
                                        <div>+`+listing.telp_broker+`</div>
                                        <div>`+listing.email_broker+`</div>
                                    </div>
                                    <div class="flex flex-ver" style="" data-judul="`+listing.judul_listing+`" data-id="`+e.id_listing+`">
                                        <div class="listing-content-icon" style="margin:auto;"><i onclick='sendWA(this);' style="margin:10px; font-size: 1.3em;" class="txt-white bg-red rounded-s padding-s fab fa-whatsapp" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Pesan WhatsApp"></i></div>
                                        <div class="listing-content-icon" style="margin:auto;"><i onclick='sendWA(this);' style="margin:10px; font-size: 1.3em;" class="txt-white bg-red rounded-s padding-s fa fa-phone" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Call Dial"></i></div>                                        
                                    </div>
                                </div>
                            </td>`;
                    if(e.status == "0") {
                        dom += `<td><div class="txt-bold" style="">Pending</div></td>`;
                    } else {
                        dom += `<td><div class="txt-bold" style="">Accepted</div></td>`;
                    }
                    dom += `<td><i class="hoverable txt-md fa fa-times"></i></td>`;
                    dom += `</tr>`;
                    $(".my-table").children().first().next().html(dom);
                }, 'json');
            });
        }, 'json');
    }
}

function sendWA(element){
    var idListing = $(element).parent().parent().attr('data-id');
    if (userID != null){
		$.post({
			"url": API_URL + "/api/get/customers/logSendWA?token=customer&id_cust=" + userID +"&id_listing=" + idListing
		});
	}
    var judul = $(element).parent().parent().attr('data-judul');
    var telp = $(element).parent().parent().parent().children().first().next().children().first().next().text();
    var pesan = "Halo,%20Saya%20mau%20menanyakan%20mengenai%20listingan%20" + encodeURIComponent(judul);
    var win   = window.open("https://api.whatsapp.com/send?phone="+telp+"&text="+pesan, '_blank');
    win.focus();
}