$(document).ready(function () {
	loadList();
});

function loadList(){
    $.ajax({
        "url" : API_URL + "/api/get/customers/getFavorite?token=customer&id_cust=" + userID,
        success : function(data){
          data = JSON.parse(data);
		  var row = "";
		  var ctr = 1;
          data.forEach(e => {
			var gambar = [];
			for (var i=1; i<=e.gambar; i++){
				gambar.push("/assets/img/listing/"+e.id_listing+"/"+i+".jpg");
			}
			gambar = JSON.stringify(gambar);

			if (ctr % 2 == 1){
				row += `<div class="row">`;
			}
			if (e.jenis_transaksi == "0"){
				var jenis = "JUAL";
				var harga = shortNumber(e.harga_jual);
			}else if (e.jenis_transaksi == "1"){
				var jenis = "SEWA";
				var harga = shortNumber(e.harga_sewa) + "/thn";
			}else{
				var jenis = "JUAL SEWA";
				var harga = shortNumber(e.harga_jual) + " - " + shortNumber(e.harga_sewa) + "/thn";;
			}
			var favorite = "";
			if (e.favorite==1){
				favorite = "favorite";
			}
			var xout = "";
			if (e.xout==1){
				xout = "xout";
			}
			row += `	<div class="col-lg-6 col-sm-12 ">`;
			row += `		<div class="m-portlet m-portlet--rounded" data-id='`+e.id_listing+`' data-telp='`+e.telp_broker+`' data-judul='`+e.judul_listing+`'>`;
			row += `			<div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='`+gambar+`'>`;
			row += `				<div class="listing-slider-badge">` + jenis + `</div>`;
			row += `				<div class="listing-slider-icon"><i class="fas fa-home"></i></div>`;
			row += `				<div class="listing-slider-counter">1 of 3</div>`;
			row += `			</div>`;
			row += `			<div class="m-portlet__body">`;
			row += `				<div class="col-lg-10 col-md-10 col-sm-12" style="padding-left: 0;padding-right:0" onclick="viewDetail(this);">`;
			row += `					<div class="listing-price listing-info">` + harga + `</div>`;
			row += `					<div class="listing-title listing-info truncate"><b>` + e.judul_listing + `</b></div>`;
			row += `					<div class="listing-jalan listing-info truncate">` + e.alamat_listing + `</div>`;
			row += `					<div class="listing-kota listing-info truncate">` + e.nama_kecamatan + `, ` + e.nama_kota + `</div>`;
			row += `					<div class="listing-detail">`;
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>`+e.kamar_tidur+`</div>`;
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>`+e.kamar_mandi+`</div>`;
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>` + e.luas_tanah + `m<sup>2</sup></div>`;
			row += `						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>` + e.luas_bangunan + `m<sup>2</sup></div>`;
			row += `					</div>`;
			row += `				</div>`;
			row += `				<div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding-right:0">`;
			row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='sendWA(this);' class="fab fa-whatsapp fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Pesan WhatsApp"></i></div>`;
			row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='favorite(this);' class="fas fa-heart fa-lg ` + favorite + `" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Favorit"></i></div>`;
			row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='xout(this);' class="fas fa-times fa-lg ` + xout + `" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Blokir"></i></div>`;
			row += `					<div class="listing-content-icon" style="margin:auto;"><i onclick='shareLink(this);' class="fas fa-share fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Bagikan"></i></div>`;
			row += `					<a href="/detaillisting/view/`+e.id_listing+`" style="margin:auto;" class="listing-content-icon txt-black"><i class="fas fa-ellipsis-h fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Tampilkan Lebih Banyak"></i></a>`;
			row += `				</div>`;
			row += `			</div>`;
			row += `		</div>`;
			row += `	</div>`;
			if (ctr % 2 == 0){
				row += `</div>`;
			}
			ctr++;
		  });
		  $("#showedListing").html(ctr-1);
		  $("#totalListing").html(ctr-1);
		  if (ctr % 2 == 0){
			row += `</div>`;
		  }
		  $("#tabs-foto").html(row);
		  initSlider();
		  mApp.initPopovers();
        }
    });
}

function viewDetail(element){
	var chevron = $(".chevron");
	if (chevron.attr("data-show") == "hide") {
		chevron.attr("data-show", "show");
		chevron.removeClass("chevron-flip");
		var idlisting = $(element).parent().parent().attr('data-id');
		$.post({
			"url" : API_URL + "/api/get/listings/getById/"+idlisting+"?token=public",
			success : function(data){
				data = JSON.parse(data);
				console.log(data);
				//edit data detail disini
			}
		});
		$("#detailListing").css("transform", "translateX(0)");
	} else {
		chevron.attr("data-show", "hide");
		chevron.addClass("chevron-flip");
		$("#detailListing").css("transform", "translateX(-100%)");
	}
}

function favorite(element){
	if (userID == null){
		Swal.fire({
			type: 'error',
			title: 'Oops...',
			text: 'Harap Login Terlebih Dahulu',
			footer: '<a href="/login">LOGIN DISINI</a>'
		});
	}else{
		var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		var status = !$(element).hasClass('favorite');
		$(element).toggleClass("favorite");
		$.post({
			"url" : API_URL + "/api/get/customers/updateFavorite?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&status=" + Number(status),
			success : function(data){
				data = JSON.parse(data);
				if (!data.status){
					$(element).toggleClass("favorite");
				}
			}
		});
	}
}

function xout(element){
	if (userID == null){
		Swal.fire({
			type: 'error',
			title: 'Oops...',
			text: 'Harap Login Terlebih Dahulu',
			footer: '<a href="/login">LOGIN DISINI</a>'
		});
	}else{
		var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
		var status = !$(element).hasClass('xout');
		$(element).toggleClass("xout");
		$.post({
			"url" : API_URL + "/api/get/customers/updateXout?token=customer&id_cust=" + userID +"&id_listing=" + idlisting + "&status=" + Number(status),
			success : function(data){
				data = JSON.parse(data);
				if (!data.status){
					$(element).toggleClass("xout");
				}
			}
		});
	}
}

function sendWA(element){
		var judul = $(element).parent().parent().parent().parent().attr('data-judul');
		var telp = $(element).parent().parent().parent().parent().attr('data-telp');
		var pesan = "Halo,%20Saya%20mau%20menanyakan%20mengenai%20listingan%20" + judul;
		var win = window.open("https://api.whatsapp.com/send?phone="+telp+"&text="+pesan, '_blank');
		win.focus();
}

function shareLink(element) {
	/* Get the text field */
	var idlisting = $(element).parent().parent().parent().parent().attr('data-id');
	var text = BASE_URL + "/detaillisting/view/" + idlisting;

	var body = document.getElementsByTagName('body')[0];
	var tempInput = document.createElement('INPUT');
	body.appendChild(tempInput);
	tempInput.setAttribute('value', text)
	tempInput.select();
	document.execCommand('copy');
	body.removeChild(tempInput);

	/* Alert the copied text */
	Swal.fire({
		type: 'success',
		title: 'Sukses',
		text: 'Berhasil disalin ke Clipboard'
	});
	if (userID != null){
		$.post({
			"url" : API_URL + "/api/get/customers/logShare?token=customer&id_cust=" + userID +"&id_listing=" + idlisting
		});
	}
}

$(".chevron").click(function (e) {
	if ($(this).attr("data-show") == "hide") {
		$(this).attr("data-show", "show");
		$(this).removeClass("chevron-flip");
		$("#detailListing").css("transform", "translateX(0)");
	} else {
		$(this).attr("data-show", "hide");
		$(this).addClass("chevron-flip");
		$("#detailListing").css("transform", "translateX(-100%)");
	}
});

var LatLongData = [];

// getLongLat();
function getLongLat() {
	var sendData = {
		harga_min: 2000,
		harga_max: 654123000,
		listrik: '1',
		k_tidur: '1',
		//k_mandi: 
		garasi: '1',
		dapur: '1',
		gudang: '1',
		pam: '1'
	};
	$.post({
		"url": "/api/get/web/listing/getActiveLongLat?token=Admin",
		data: {
			data: JSON.stringify(sendData)
		},
		success: function (data) {
			data = JSON.parse(data);
			//console.log(data);
			data.forEach(element => {
				//console.log("lat = " + element.lat_listing + " , long = " + element.long_listing);
				LatLongData.push({"Lat" : element.lat_listing, "Long" : element.long_listing});
				
			});
			console.log(LatLongData);
			//console.log(LatLongData[1].Lat);
		}
	});
}