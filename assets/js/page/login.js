function keyPressLogin(e){
    // Ux on Submit loading
    e.preventDefault();
    login();
}
function login(){
    $("#errLogin").html("");
    var sendData = {"telp":$("#inputPhone").val(), "password":$("#inputPassword").val(), "rememberMe" : $("#customCheck1").val()};
    mApp.block('.user-popup', {}); // Add UX for loading ajax

    $.post({
        "url" : "/auth/login",
        "data" : sendData,
        success : function(data){
            data = JSON.parse(data);
            if (data.key != 1 || data.userData.id_customer == null){
                $("#errLogin").html("Username/Password tidak sesuai");
            }else{
                var row = "";
                row += `<div id="divProfile" style="position:relative; display: inline-block;margin-left:10px; text-align:left !important;">`;
                row += `    <div class="ajax txt-bold bg-red txt-white rounded-m padding-m user-section clickable" id="user-profile-container" data-fadein="profile-content">`;
                row += `        <span>`+data.userData.nama_customer+` &nbsp;&nbsp;<i class="fa fa-caret-down"></i> &nbsp; &nbsp; <i class="fa fa-user-circle"></i></span>`;
                row += `    </div>`;
                row += `    <div id="profile-content" class="bg-white txt-white profile-content flex flex-ver" style="padding-top:5px;padding-bottom:5px; width:100%; border-radius:0px 0px 10px 10px; z-index:2;display:none; border-left: none;">`;
                row += `        <a href="#" class="user-photo-mini" style="border-left:none;background-image:url('/assets/img/profile-customer/`+data.userData.photo_customer+`);"></a>`;
                row += `        <a class="txt-black hoverable txt-bold padding-m animated" href="#" style="border-top: 1px solid #00000030;border-left: none;">My Profile</a>`;
                row += `        <a class="txt-black hoverable txt-bold padding-m animated" href="/listingsaya" style="border-left: none;">Listing Saya</a>`;
                row += `        <a class="txt-black hoverable txt-bold padding-m animated" href="/listingsaya" style="border-left: none;">Favorite & X-Out</a>`;
                row += `        <a class="txt-black hoverable txt-bold padding-m animated" href="/wishlist" style="border-left: none;">Wishlist Saya</a>`;
                row += `        <a class="txt-black hoverable txt-bold padding-m animated" href="/listingsaya" style="border-left: none;">Jadwal Saya</a>`;
                row += `        <a class="txt-black hoverable txt-bold padding-m animated" href="/listingsaya" style="border-left: none;">Pengaturan Saya</a>`;
                row += `        <a class="txt-black hoverable txt-bold padding-m animated" style="border-left: none;" onclick="logout();">Logout</a>`;
                row += `    </div>`;
                row += `</div>`;
                $("#divLogin").parent().append(row);
                $("#divLogin").css("display", "none");
                $(".user-popup").css("display", "none");
                ajaxInit();
            }
            mApp.unblock('.user-popup');
            window.location.href = "";
        }
    });    
}

function logout(){
    $.post({
        "url" : "/auth/logout",
        success : function(){
            $("#divLogin").css("display", "block");
            $("#divProfile").remove();
            window.location.href = "";
        }
    });
}

function register(){
    var sendData = { nama: $("#regisName").val(),
        email: $("#regisEmail").val(),
        telp: $("#regisTelephone").val(),
        password: $("#regisPassword").val(),
        cpassword: $("#regisConfirmPassword").val()
    };
    $("#errRegister").html("");

    if (!$("#customCheck2").prop("checked")){
        $("#errRegister").html("Please Accept Terms and Agreement");
    }else if ($("#regisPassword").val() != $("#regisConfirmPassword").val()){
        $("#errRegister").html("Password dan Confirm Password Tidak Sesuai");
    }else{
        $.post({
            // "url" : API_URL + "/api/post/customers/register?token=public",
            "url" : "/auth/register",
            data: sendData,
            success : function(data){
                data = JSON.parse(data);
                if (data['key'] == 1){
                    window.location = "/register/success";
                }else if (data['key'] == 0){
                    $("#errRegister").html("Telephone sudah pernah digunakan");
                }
            }
        });
    }
}