

function CustomMarker(latlng, map, harga, tipe, args) {
	this.latlng = latlng;	
	this.args = args;
	this.harga = harga;
	this.tipe = tipe;
	this.visible = true;
	this.setMap(map);
}

CustomMarker.prototype = new google.maps.OverlayView();

CustomMarker.prototype.draw = function() {
	
	var self = this;
	// console.log(this);
	var div = this.div;
	
	if (!div) {
	
		div = this.div = document.createElement('div');
		
		div.className = 'marker listing-marker ' + this.tipe.toLowerCase();
		//console.log(this.visible);
		// console.log(this);
		// if (!this.visible) {
		// 	div.style.display = "none";
		// }
		div.style.position = 'absolute';
		div.style.cursor = 'pointer';
		div.style.overflow = "visible";
		// div.style.height = '20px';

		div.style.background = "";

		div.innerHTML = "<div style='transform: translate(-40%,-100%);font-size:1.25em; color: white; text-shadow:0px 0px 2px black;  width:100%; padding:4px 10px; background-color: var(--bg-"+ this.tipe.toLowerCase() +");'>"+this.harga+"</div>";

		if (typeof(self.args.marker_id) !== 'undefined') {
			div.dataset.marker_id = self.args.marker_id;
		}
		
		google.maps.event.addDomListener(div, "click", function(event) {
			console.log('You clicked on a custom marker!');
			google.maps.event.trigger(self, "click");
		});
		
		var panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	}
	
	var point = this.getProjection().fromLatLngToDivPixel(this.latlng);
	
	if (point) {
		div.style.left = (point.x - 10) + 'px';
		div.style.top = (point.y - 20) + 'px';
	}
};

CustomMarker.prototype.remove = function() {
	if (this.div) {
		this.div.parentNode.removeChild(this.div);
		this.div = null;
	}	
};

CustomMarker.prototype.getPosition = function() {
	return this.latlng;	
};


CustomMarker.prototype.getPoint = function() 
{
    var bounds = this.getBounds();
    var projection = this.getProjection();
    var sw = projection.fromLatLngToDivPixel(bounds.getSouthWest());  
        var ne = projection.fromLatLngToDivPixel(bounds.getNorthEast()); 

    return new google.maps.Point(sw.x, ne.y);
};

CustomMarker.prototype.getBounds = function() 
{
    return new google.maps.LatLngBounds(this.position, this.position); 
};

CustomMarker.prototype.getSuperContainer = function()
{
    var panes = this.getPanes();
    return jQuery(panes ? panes.overlayImage : "");
};


CustomMarker.prototype.getContainer = function()
{
    // return inner container
};

CustomMarker.prototype._generatePopupContent = function()
{
    // return markup for the popupwindow
};

CustomMarker.prototype._addListeners = function()
{
    // add handlers for the pushpin
};

CustomMarker.prototype.onAdd = function()
{
	// customize content here
	//console.log("onAdd");
	this.div.style.display = "block";
};

CustomMarker.prototype.onRemove = function()
{
	// remove pin container here
	// console.log("onRemove");
	this.div.style.display = "none";
};

CustomMarker.prototype.setVisible = function(visible)
{
	// set display block or hidden
	this.visible = visible;
};


	
CustomMarker.prototype.getVisible = function()
{
	return this.visible;
};
CustomMarker.prototype.getDraggable = function() { return false; };