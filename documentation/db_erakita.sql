-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2019 at 11:57 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_erakita`
--
CREATE DATABASE IF NOT EXISTS `db_erakita` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_erakita`;

-- --------------------------------------------------------

--
-- Table structure for table `cabang`
--

DROP TABLE IF EXISTS `cabang`;
CREATE TABLE `cabang` (
  `id_cabang` int(11) NOT NULL,
  `nama_cabang` varchar(50) NOT NULL,
  `alamat_cabang` mediumtext NOT NULL,
  `status_cabang` varchar(1) NOT NULL COMMENT '0 = tidak avail || 1 = avail'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cabang_fax_telp`
--

DROP TABLE IF EXISTS `cabang_fax_telp`;
CREATE TABLE `cabang_fax_telp` (
  `id_cabang` int(11) NOT NULL,
  `telp_cabang` varchar(20) NOT NULL,
  `status` varchar(1) NOT NULL COMMENT '0 = telp || 1 = fax'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL,
  `nama_customer` varchar(50) NOT NULL,
  `email_customer` varchar(30) DEFAULT NULL,
  `alamat_customer` mediumtext,
  `telp_customer` varchar(20) NOT NULL,
  `password_customer` varchar(60) NOT NULL,
  `status_customer` varchar(1) NOT NULL COMMENT '0 = tidak aktif || 1 = aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `history_pindah_listing`
--

DROP TABLE IF EXISTS `history_pindah_listing`;
CREATE TABLE `history_pindah_listing` (
  `tanggal_pindah` date NOT NULL,
  `id_listing_lama` int(11) NOT NULL,
  `id_listing_baru` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `listing_aktif`
--

DROP TABLE IF EXISTS `listing_aktif`;
CREATE TABLE `listing_aktif` (
  `id_listing` int(11) NOT NULL,
  `jalan_listing` varchar(50) NOT NULL,
  `blok_listing` varchar(10) NOT NULL,
  `no_listing` varchar(3) NOT NULL,
  `keluarahan_listing` varchar(30) NOT NULL,
  `kecamatan_listing` varchar(30) NOT NULL,
  `kota_listing` varchar(30) NOT NULL,
  `kodepos_listing` varchar(7) NOT NULL,
  `ptanah_listing` int(5) NOT NULL,
  `ltanah_listing` int(5) NOT NULL,
  `lbangunan_listing` int(5) NOT NULL,
  `hadap_listing` varchar(1) NOT NULL,
  `ubangunan_listing` int(5) NOT NULL,
  `hak_listing` varchar(1) NOT NULL,
  `imb_listing` varchar(1) NOT NULL COMMENT '0 = tidak ada || 1 = ada',
  `tipe_listing` varchar(1) NOT NULL,
  `line_listing` varchar(1) NOT NULL,
  `listrik_listing` varchar(5) NOT NULL,
  `pam_listing` varchar(1) NOT NULL COMMENT '0 = tidak ada || 1 = ada',
  `sumur_listing` varchar(1) NOT NULL COMMENT '0 = tidak ada || 1 = ada',
  `harga_listing` int(20) NOT NULL,
  `lat_listing` float NOT NULL,
  `long_listing` float NOT NULL,
  `status_listing` varchar(1) NOT NULL COMMENT '0 = pending || 1 = jual || 2 = sewa || 3 = pending',
  `minim_sewa_listing` varchar(3) DEFAULT NULL,
  `start_listing` date NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `listing_data_rumah`
--

DROP TABLE IF EXISTS `listing_data_rumah`;
CREATE TABLE `listing_data_rumah` (
  `id_listing` int(11) NOT NULL,
  `ac_central` varchar(2) NOT NULL,
  `ac_split` varchar(2) NOT NULL,
  `ac_window` varchar(2) NOT NULL,
  `r_tamu` varchar(2) NOT NULL,
  `r_keluarga` varchar(2) NOT NULL,
  `r_makan` varchar(2) NOT NULL,
  `r_belajar` varchar(2) NOT NULL,
  `k_tidur` varchar(2) NOT NULL,
  `k_utama` varchar(2) NOT NULL,
  `k_biasa` varchar(2) NOT NULL,
  `k_pembantu` varchar(2) NOT NULL,
  `dapur` varchar(2) NOT NULL,
  `gudang` varchar(2) NOT NULL,
  `garasi` varchar(2) NOT NULL,
  `carport` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `listing_lantai_rumah`
--

DROP TABLE IF EXISTS `listing_lantai_rumah`;
CREATE TABLE `listing_lantai_rumah` (
  `id_listing` int(11) NOT NULL,
  `lt_teras` int(11) NOT NULL,
  `lt_tamu` int(11) NOT NULL,
  `lt_keluarga` int(11) NOT NULL,
  `lt_tidur` int(11) NOT NULL,
  `lt_dapur` int(11) NOT NULL,
  `lt_mandi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `listing_non_aktif`
--

DROP TABLE IF EXISTS `listing_non_aktif`;
CREATE TABLE `listing_non_aktif` (
  `id_listing` int(11) NOT NULL,
  `jalan_listing` varchar(50) NOT NULL,
  `blok_listing` varchar(10) NOT NULL,
  `no_listing` varchar(3) NOT NULL,
  `keluarahan_listing` varchar(30) NOT NULL,
  `kecamatan_listing` varchar(30) NOT NULL,
  `kota_listing` varchar(30) NOT NULL,
  `kodepos_listing` varchar(7) NOT NULL,
  `ptanah_listing` int(5) NOT NULL,
  `ltanah_listing` int(5) NOT NULL,
  `lbangunan_listing` int(5) NOT NULL,
  `hadap_listing` varchar(1) NOT NULL,
  `ubangunan_listing` int(5) NOT NULL,
  `hak_listing` varchar(1) NOT NULL,
  `imb_listing` varchar(1) NOT NULL COMMENT '0 = tidak ada || 1 = ada',
  `tipe_listing` varchar(1) NOT NULL,
  `line_listing` varchar(1) NOT NULL,
  `listrik_listing` varchar(5) NOT NULL,
  `pam_listing` varchar(1) NOT NULL COMMENT '0 = tidak ada || 1 = ada',
  `sumur_listing` varchar(1) NOT NULL COMMENT '0 = tidak ada || 1 = ada',
  `harga_listing` int(20) NOT NULL,
  `lat_listing` float NOT NULL,
  `long_listing` float NOT NULL,
  `status_listing` varchar(1) NOT NULL COMMENT '0 = pending || 1 = jual || 2 = sewa',
  `minim_sewa_listing` varchar(3) DEFAULT NULL,
  `start_listing` date NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `listing_photo`
--

DROP TABLE IF EXISTS `listing_photo`;
CREATE TABLE `listing_photo` (
  `id_listing` int(11) NOT NULL,
  `foto_listing` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tipe_hak`
--

DROP TABLE IF EXISTS `tipe_hak`;
CREATE TABLE `tipe_hak` (
  `id_hak` int(11) NOT NULL,
  `nama_hak` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tipe_lantai`
--

DROP TABLE IF EXISTS `tipe_lantai`;
CREATE TABLE `tipe_lantai` (
  `id_lantai` int(11) NOT NULL,
  `nama_lantai` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tipe_listing`
--

DROP TABLE IF EXISTS `tipe_listing`;
CREATE TABLE `tipe_listing` (
  `id_tipe` int(11) NOT NULL,
  `nama_tipe` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tipe_user`
--

DROP TABLE IF EXISTS `tipe_user`;
CREATE TABLE `tipe_user` (
  `id_tipe_user` varchar(2) NOT NULL,
  `nama_tipe_user` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe_user`
--

INSERT INTO `tipe_user` (`id_tipe_user`, `nama_tipe_user`) VALUES
('AD', 'Admin'),
('BP', 'Broker Pro'),
('DE', 'Developer'),
('OP', 'Operator'),
('SA', 'Super Admin');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `email_user` varchar(30) NOT NULL,
  `foto_user` text NOT NULL,
  `password_user` varchar(60) NOT NULL,
  `role_user` varchar(1) NOT NULL,
  `status_user` varchar(1) NOT NULL COMMENT '0 = tidak aktif || 1 = aktif',
  `id_cabang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_telp`
--

DROP TABLE IF EXISTS `user_telp`;
CREATE TABLE `user_telp` (
  `id_user` int(11) NOT NULL,
  `telp_user` varchar(20) NOT NULL,
  `type_telp` varchar(1) NOT NULL COMMENT '0 = telp || 1 = wa'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cabang`
--
ALTER TABLE `cabang`
  ADD PRIMARY KEY (`id_cabang`);

--
-- Indexes for table `cabang_fax_telp`
--
ALTER TABLE `cabang_fax_telp`
  ADD PRIMARY KEY (`id_cabang`,`telp_cabang`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `history_pindah_listing`
--
ALTER TABLE `history_pindah_listing`
  ADD PRIMARY KEY (`tanggal_pindah`,`id_listing_lama`);

--
-- Indexes for table `listing_aktif`
--
ALTER TABLE `listing_aktif`
  ADD PRIMARY KEY (`id_listing`);

--
-- Indexes for table `listing_data_rumah`
--
ALTER TABLE `listing_data_rumah`
  ADD PRIMARY KEY (`id_listing`);

--
-- Indexes for table `listing_lantai_rumah`
--
ALTER TABLE `listing_lantai_rumah`
  ADD PRIMARY KEY (`id_listing`);

--
-- Indexes for table `listing_non_aktif`
--
ALTER TABLE `listing_non_aktif`
  ADD PRIMARY KEY (`id_listing`);

--
-- Indexes for table `listing_photo`
--
ALTER TABLE `listing_photo`
  ADD PRIMARY KEY (`id_listing`,`foto_listing`) USING BTREE;

--
-- Indexes for table `tipe_hak`
--
ALTER TABLE `tipe_hak`
  ADD PRIMARY KEY (`id_hak`);

--
-- Indexes for table `tipe_lantai`
--
ALTER TABLE `tipe_lantai`
  ADD PRIMARY KEY (`id_lantai`);

--
-- Indexes for table `tipe_listing`
--
ALTER TABLE `tipe_listing`
  ADD PRIMARY KEY (`id_tipe`);

--
-- Indexes for table `tipe_user`
--
ALTER TABLE `tipe_user`
  ADD PRIMARY KEY (`id_tipe_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user_telp`
--
ALTER TABLE `user_telp`
  ADD PRIMARY KEY (`id_user`,`telp_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cabang`
--
ALTER TABLE `cabang`
  MODIFY `id_cabang` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `listing_aktif`
--
ALTER TABLE `listing_aktif`
  MODIFY `id_listing` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tipe_hak`
--
ALTER TABLE `tipe_hak`
  MODIFY `id_hak` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tipe_lantai`
--
ALTER TABLE `tipe_lantai`
  MODIFY `id_lantai` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tipe_listing`
--
ALTER TABLE `tipe_listing`
  MODIFY `id_tipe` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
