<!-- Page Preloder -->
<div id="preloder">
	<div class="loader"></div>
</div>


<!-- Header section -->
<header class="header-section" style="background-color:transparent; box-shadow:none;">
	<div class="header-top" style="background-color: transparent;">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 header-top-left">
					<!--div class="top-info">
						<i class="fa fa-phone"></i>
						(+88) 666 121 4321
					</div-->
					<!--div class="top-info">
						<i class="fa fa-envelope"></i>
						info.leramiz@colorlib.com
					</div-->
				</div>
				<div class="col-lg-6 text-lg-right header-top-right">
					<div class="top-social">
						<a href=""><i class="fa fa-facebook"></i></a>
						<a href=""><i class="fa fa-twitter"></i></a>
						<a href=""><i class="fa fa-instagram"></i></a>
						<!--a href=""><i class="fa fa-pinterest"></i></a-->
						<!--a href=""><i class="fa fa-linkedin"></i></a-->
					</div>
					<!-- <div class="user-panel">
						<a href="/register"><i class="fa fa-user-circle-o"></i> Register</a>
						<a href="/login"><i class="fa fa-sign-in"></i> Login</a>
					</div> -->
					<?php 
					if (isset($_SESSION["loggedUser"]) && !empty($_SESSION["loggedUser"])) {?>
						<div class="" id="divProfile" style="position:relative; display: inline-block;margin-left:10px; text-align:left !important;">
							<div class="txt-bold bg-red txt-white rounded-m padding-m user-section clickable" id="user-profile-container" data-fadein="profile-content">
								<span><?= $_SESSION["loggedName"] ?> &nbsp;&nbsp;<i class="fa fa-caret-down"></i> &nbsp; &nbsp; <i class="fa fa-user-circle"></i></span>
							</div>
							<div id="profile-content" class="bg-white txt-white profile-content flex flex-ver" style="padding-top:5px;padding-bottom:5px; width:100%; border-radius:0px 0px 10px 10px; z-index:2;display:none; border-left: none;">
								<a href="#" class="user-photo-mini" style="border-left:none;background-image:url('<?= (isset($_SESSION["loggedPhoto"]) && !empty($_SESSION["loggedPhoto"])) ? "/assets/img/profile-customer/".$_SESSION["loggedPhoto"] : "/assets/img/profile-customer/default.png" ; ?>');"></a>
								<a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["profile"])) ? "active" : "" ; ?>" href="#" style="border-top: 1px solid #00000030;border-left: none;">My Profile</a>
								<a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["listingsaya"])) ? "active" : "" ; ?>" href="/listingsaya" style="border-left: none;">Listing Saya</a>
								<a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["favoritexout"])) ? "active" : "" ; ?>" href="/listingsaya" style="border-left: none;">Favorite & X-Out</a>
								<a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["wishlist"])) ? "active" : "" ; ?>" href="/listingsaya" style="border-left: none;">Wishlist Saya</a>
								<a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["jadwal"])) ? "active" : "" ; ?>" href="/listingsaya" style="border-left: none;">Jadwal Saya</a>
								<a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["setting"])) ? "active" : "" ; ?>" href="/listingsaya" style="border-left: none;">Pengaturan Saya</a>
								<a class="txt-black hoverable txt-bold padding-m animated" style="border-left: none;" onclick="logout();">Logout</a>
							</div>
						</div>
						<?php    
						$displayLogin = "display:none;";
					}
					else {$displayLogin="";}?>
						<div class="header-popup" id="divLogin" style="float:right;<?=$displayLogin?>">
							<!-- <a href="/register"><i class="fa fa-user-circle-o"></i> Register</a>
							<a href="/login"><i class="fa fa-sign-in"></i> Login</a> -->
							<div style="position:relative;">
								<a href="javascript:void(0)" onclick="nyalainPopup();" class="txt-bold bg-red txt-white rounded-m padding-m user-section clickable" style="padding-top:5px;padding-bottom:5px;">
									<span><i class="fa fa-sign-in"></i> Login</span>
								</a>
								<?php
									$this->load->view("page-part/loginregister-popup");
								?>

							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="site-navbar">
					<a href="/" class="site-logo"><img src="/assets/img/erakita-logo-putih.png" alt="" width="200px" style="margin-left:50px"></a>
					<div class="nav-switch">
						<i class="fa fa-bars"></i>
					</div>
					<ul class="main-menu">
						<!-- Wills Part -->
						<li><a href="/">Home</a></li>
						<li><a href="/listing">Listing</a></li>
						<li><a href="/broker">Broker</a></li>
						<li><a href="/cabang">Cabang</a></li>
						<!-- <li><a href="/developer">Developer</a></li> -->
						<li><a href="/proyek">Primary Market</a></li>				
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>
<script src="/assets/js/page/login.js"></script>
<!-- Header section end -->