<div class="user-popup" style="display:none;">
    <div class="login-content">
        <div class="card card-signin">
            <div class="card-body" style="text-align:left;">
            <h5 class="card-title text-center">Sign In</h5>
            <form class="form-signin" onsubmit="keyPressLogin(event)">
                <div class="form-label-group mb-2">
                    <label for="inputPhone">Phone Number</label>
                    <input type="text" id="inputPhone" class="form-control" style="" placeholder="Phone Number" required autofocus>
                </div>

                <div class="form-label-group">
                    <label for="inputPassword">Password</label>
                    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                    <small id="errLogin" class="text-danger"></small> 
                </div>

                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input" name="rememberMe" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">Remember password</label>
                </div>
                <div class="mb-2">
                    <a href="/forgot">Forgot Password?</a>
                    <a href="javascript:void(0)" onclick="toRegister();" style="float:right">Register here</a>
                </div>
                <button type="button" id="btnLogin" class="btn btn-primary btn-block text-uppercase" onclick="login();">Sign in</button>
                <hr class="my-2">
                <button class="btn btn-google btn-block text-uppercase" type="submit"><i class="fa fa-google mr-2"></i> Sign in with Google</button>
                <!-- <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i class="fa fa-facebook-f mr-2"></i> Sign in with Facebook</button> -->
            </form>
            </div>
        </div>
    </div>

    <div class="register-content" style="display:none;">
        <div class="card card-signin">
            <div class="card-body">
            <h5 class="card-title text-center">Sign Up</h5>
            <form class="form-signin">

                <div class="mb-2 form-label-group">
                    <label for="regisName">Full Name</label>
                    <input type="text" id="regisName" class="form-control" style="" placeholder="Full Name" required autofocus>
                </div>

                <div class="mb-2 form-label-group">
                    <label for="regisEmail">Email address</label>
                    <input type="email" id="regisEmail" class="form-control" style="" placeholder="Email address" required autofocus>
                </div>
<!-- 
                <div class="mb-2 form-label-group">
                    <label for="inputAddress">Address</label>
                    <input type="text" id="inputAddress" class="form-control" placeholder="Address" required>
                </div> -->

                
                <div class="mb-2 form-label-group">
                    <label for="regisTelephone">Telephone</label>
                    <input type="text" id="regisTelephone" class="form-control" placeholder="Telephone" required>
                </div>

                <div class="mb-2 form-label-group">
                    <label for="regisPassword">Password</label>
                    <input type="password" id="regisPassword" class="form-control" placeholder="Password" required>
                </div>

                
                <div class="mb-2 form-label-group">
                    <label for="regisConfirmPassword">Confirm Password</label>
                    <input type="password" id="regisConfirmPassword" class="form-control" placeholder="Confirm Password" required>
                </div>
                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                    <label class="custom-control-label" for="customCheck2">Accept <a href="javascript:void(0)">terms and agreement</a></label>
                </div>
                <div class="mb-2">
                    <a href="javascript:void(0)" onclick="toLogin();">Already have account? Login here</a>
                    <!-- <a href="javascript:void(0)" onclick="toRegister();" style="float:right">Register here</a> -->
                    <small id="errRegister" class="text-danger"></small> 
                </div>
                
                
                <button class="btn btn-primary btn-block text-uppercase" type="button" onclick="register();">Sign up</button>
                <hr class="my-2">
                <button class="btn btn-google btn-block text-uppercase" type="submit"><i class="fa fa-google mr-2"></i> Sign up with Google</button>  
            </form>
            </div>
        </div>
    </div>
</div>
<?php if (isset($_COOKIE['Era-CustLogin'])){ ?>
    <script>$("#btnLogin").click();</script>
<?php } ?>