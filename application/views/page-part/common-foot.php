<?php if (isset($footer) && $footer==false) {
	
}
else{
	
?>
<!-- Clients section -->

<div class="clients-section" style="transform: scale(0.75);">
	<div class="container">
		<div class="clients-slider owl-carousel" style="background-color: white;border: 10px solid #d23F2f;padding: 0px 40px;">
			<a href="#">
				<img src="/assets/img/partner/BCA.png" alt="">
			</a>
			<a href="#">
				<img src='assets/img/partner/HSBC.png' alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/CIMB Niaga.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/Mandiri.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/MNC Bank.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/Panin Bank.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/BTN.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/BRI.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/BNI.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/Bank Mega.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/OCBC NISP.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/UOB.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/Standard Chartered.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/Permata Bank.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/KEB Hana Bank.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/DBS.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/China Constuction Bank.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/commonwealth bank.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/CTBC Bank.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/Bii MayBank.png" alt="" style="transform: scale(0.5);">
			</a>
			<a href="#">
				<img src="/assets/img/partner/Bank Muamalat.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/Bank BJB.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/Bank Artha Graha.png" alt="">
			</a>
			<a href="#">
				<img src="/assets/img/partner/ANZ.png" alt="">
			</a>
		</div>
	</div>
</div>
<!-- Clients section end -->
<!-- 468 px before, Target 468 * 40% = 187px -->
<!-- Footer section -->
<footer class="footer-section set-bg pb-0" data-setbg="/assets/img/footer-bg.jpg" style="padding-top:50px;">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 footer-widget">
					<img src="/assets/img/erakita-logo-putih.png" alt="" width="125px" style="margin-bottom:2px;">
					<div class="social">
						<a href="https://www.facebook.com/pages/Era-Kita/1009166539160335" target="_blank"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-instagram"></i></a>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 footer-widget">
					<div class="contact-widget">
						<h5 class="fw-title mb-2" style="font-size:0.9em;">CONTACT US</h5>
						<p class="mb-1" style="font-size:0.9em;"><i class="fa fa-map-marker"></i>Jl. Raya Lontar No.299, Lontar, Kec. Sambikerep, Kota SBY, Jawa Timur 60216</p>
						<p class="mb-1" style="font-size:0.9em;"><i class="fa fa-phone"></i>(+6231) 7455999</p>
						<p class="mb-1" style="font-size:0.9em;"><i class="fa fa-envelope"></i>erakitagroup@gmail.com</p>
						<!-- <p class="mb-1" style="font-size:0.8em;"><i class="fa fa-clock-o"></i>Mon - Fri, 08.30 AM - 04 PM</p> -->
					</div>
				</div>
				
			</div>
			<div class="footer-bottom" style="padding-top:0px; margin-top:5px;">
				<div class="footer-nav">
					<ul>
						<li><a href="/home">Home</a></li>
						<li><a href="/listing">Listing</a></li>
						<li><a href="/broker">Broker</a></li>
						<li><a href="/cabang">Cabang</a></li>
						<li><a href="/developer">Developer</a></li>
						<li><a href="/proyek">Proyek</a></li>
					</ul>
				</div>
				<div class="copyright">
					<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Era Kita
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer section end -->
<?php } ?>                  
	<!-- Begin Echo JS Part-->


	<!--begin:: Global Mandatory Vendors -->
	<script src="/assets/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
	<script src="/assets/vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
	<script src="/assets/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
	<script src="/assets/vendors/moment/min/moment.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
	<script src="/assets/vendors/wnumb/wNumb.js" type="text/javascript"></script>

	<!--end:: Global Mandatory Vendors -->

	<!--begin:: Global Optional Vendors -->
	<script src="/assets/vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
	<script src="/assets/vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
	<script src="/assets/vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
	<script src="/assets/vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
	<script src="/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js" type="text/javascript"></script>
	<script src="/assets/vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js" type="text/javascript"></script>
	<script src="/assets/vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
	<script src="/assets/vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js" type="text/javascript"></script>
	<script src="/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
	<script src="/assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
	<script src="/assets/vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
	<script src="/assets/vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
	<script src="/assets/vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
	<script src="/assets/vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
	<script src="/assets/vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
	<script src="/assets/vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
	<script src="/assets/vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
	<script src="/assets/vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
	<script src="/assets/vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
	<script src="/assets/vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
	<script src="/assets/vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
	<script src="/assets/vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
	<script src="/assets/vendors/autosize/dist/autosize.js" type="text/javascript"></script>
	<script src="/assets/vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
	<script src="/assets/vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
	<script src="/assets/vendors/summernote/dist/summernote.js" type="text/javascript"></script>
	<script src="/assets/vendors/markdown/lib/markdown.js" type="text/javascript"></script>
	<script src="/assets/vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
	<script src="/assets/vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
	<script src="/assets/vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
	<script src="/assets/vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
	<script src="/assets/vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
	<script src="/assets/vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
	<script src="/assets/vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/jstree/dist/jstree.js" type="text/javascript"></script>
	<script src="/assets/vendors/raphael/raphael.js" type="text/javascript"></script>
	<script src="/assets/vendors/morris.js/morris.js" type="text/javascript"></script>
	<script src="/assets/vendors/chartist/dist/chartist.js" type="text/javascript"></script>
	<script src="/assets/vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
	<script src="/assets/vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
	<script src="/assets/vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
	<script src="/assets/vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
	<script src="/assets/vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
	<script src="/assets/vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

	<!--end:: Global Optional Vendors -->

	<!--begin::Global Theme Bundle -->
	<script src="/assets/demo/base/scripts.bundle.js" type="text/javascript"></script>
	<script src="/assets/demo/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<?php 
		echo $assets->stringJs();
		echo $assets->stringCustom("js");
	?>
	
	<script src="/assets/vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
	
	<!-- End Echo JS Part-->
