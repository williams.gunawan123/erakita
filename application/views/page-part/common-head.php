<!DOCTYPE html>
<html lang="en">
<head>
	<script src="/assets/js/pace.min.js"></script>
	<title><?= $webTitle ?> | <?= $pageTitle ?></title>
    <meta charset="UTF-8">
	<meta name="description" content="<?= $webTitle ?> <?= $pageTitle ?>">
	<meta name="keywords" content="<?= $webTitle ?>, unica, creative, html, <?= $pageTitle ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="/assets/img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">

	<!--begin::Web font -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	
	<script>
		WebFont.load({
					google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
					active: function() {
							sessionStorage.fonts = true;
					}
				});
			</script>

	<!--end::Web font -->

	<!--begin:: Global Mandatory Vendors -->
	<link href="/assets/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

	<!--end:: Global Mandatory Vendors -->

	<!--begin:: Global Optional Vendors -->
	<link href="/assets/vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


	<!--end:: Global Optional Vendors -->

	<!--begin::Global Theme Styles -->
	<link href="/assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<!-- Begin Echo CSS Part-->
	<?php 
		echo $assets->stringCss();
		echo $assets->stringCustom("css");
	?>
	<!-- End Echo CSS Part-->

	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<?php
    if (isset($_SESSION['loggedUser'])){
        echo "<script> const userID = " .$_SESSION['loggedUser'].";</script>";
        if (!isset($_SESSION['activeOneSignal'])){
            echo "<script src='https://cdn.onesignal.com/sdks/OneSignalSDK.js' async></script>";
            echo "<script>var OneSignal = OneSignal || [];</script>";
            echo "<script>
                    OneSignal.push(['init', {
                    appId: '84d55ee8-4a26-45fe-9813-7f41f299d550',
                    // Your other init settings
                    }]);
                    OneSignal.push(['sendTags', {id: '".$_SESSION['loggedUser']."', tipe: 'customer'}]);
                </script>";
            $_SESSION['activeOneSignal'] = 1;
        }
    }else{
        echo "<script> const userID = null;</script>";
    }
?>
