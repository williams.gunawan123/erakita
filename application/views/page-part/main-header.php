<header class="flex flex-ver">
    <div class="flex flex-hor flex-separate"> 
        <div class="flex flex-hor flex-vertical-center">
            <a href="/" class="erakita-logo margin-col-xlg" style="width: 50px;"><div></div></a>
            <div class="flex flex-hor search-header flex-vertical-stretch">
                <div style="position:relative; width: 20vw;">
                    <input id="headSearch" onkeyup="changeDropSearch();" onfocusout="$(this).parent().find('.dropdown-container').fadeOut();" onfocus="$(this).parent().find('.dropdown-container').fadeIn();" data-hidden='' type="text" style="height:100%; width:100%; border: 1px solid rgba(0, 0, 0, 0.062); padding: 0px 10px" placeholder="Kota, Daerah, Alamat, Provinsi ... " class="flex" autocomplete="off">
                    <div class="minimalistScrollbar dropdown-container bg-white shadow" style="display:none; position:absolute; left:0px; width:100%; max-height:60vh; overflow-x:auto;">
                        <!-- <div class="dropdown-separator"><i class="fa fa-clock mr-2"></i> Pencarian Tersimpan</div>
                        <div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" onclick="insertMe(this);">
                            <div>
                                <div class="data-show">Kenjeran</div>
                                <div class="data-hidden label">Surabaya, Jawa Timur</div>
                            </div>
                            <div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
                        </div>
                        <div class="dropdown-separator"><i class="fas mr-2 fa-map-marker-alt"></i> Wilayah</div>
                        <div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" onclick="insertMe(this);">
                            <div>
                                <div class="data-show">Kenjeran</div>
                                <div class="data-hidden label">Surabaya, Jawa Timur</div>
                            </div>
                            <div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
                        </div>
                        <div class="dropdown-separator"><i class="fas mr-2 fa-user-tie"></i> Agen</div>
                        <div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" onclick="insertMe(this);">
                            <div>
                                <div class="data-show">Kenjeran</div>
                                <div class="data-hidden label">Surabaya, Jawa Timur</div>
                            </div>
                            <div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
                        </div>
                        <div class="dropdown-separator"><i class="fas mr-2 fa-building"></i> Proyek</div>
                        <div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" onclick="insertMe(this);">
                            <div>
                                <div class="data-show">Kenjeran</div>
                                <div class="data-hidden label">Surabaya, Jawa Timur</div>
                            </div>
                            <div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
                        </div> -->
                    </div>
                </div>
                <!-- <div style="min-width:200px;">
                    <select class="form-control m-select2" style="height:100%" id="header-select2" name="searchBox">
                        <optgroup label="Alaskan/Hawaiian Time Zone">
                            <option></option>
                            
                        </optgroup>
                    </select>
                    
                    <input type="text" />
                </div> -->
                <div class="flex flex-ver flex-vertical-center show-advance" id="opsi-lain">
                    <div>
                        <span class="selectedOptionBadge m-badge m-badge--warning bg-red txt-white" style="display:none">0</span> Filter <i class="fa fa-chevron-down"></i>
                    </div>
                </div>
                <div class="flex flex-btn bg-red header-search-btn flex-vertical-center flex-centered clickable" onclick="search(0)">
                    <i class="fa fa-search txt-white"></i>
                </div>
            </div>
        </div>
        <div class="flex flex-hor flex-vertical-center" style="position:relative;">
            <!-- <a href=""           class="txt-black  txt-bold hoverable padding-m animated">031-XXXXXXX</a> -->
            <a href="/"          style="padding-right:10px; padding-left:10px;" class="txt-black  txt-bold hoverable padding-s animated <?= (isset($pageData["_currentPage"]["home"])) ? "active" : "" ; ?>">Home</a>
            <a href="/listing"   style="padding-right:10px; padding-left:10px;" class="txt-black  txt-bold hoverable padding-s animated <?= (isset($pageData["_currentPage"]["listing"])) ? "active" : "" ; ?>">Listing</a>
            <a href="/broker"    style="padding-right:10px; padding-left:10px;" class="txt-black  txt-bold hoverable padding-s animated <?= (isset($pageData["_currentPage"]["broker"])) ? "active" : "" ; ?>">Broker</a>
            <a href="/cabang"    style="padding-right:10px; padding-left:10px;" class="txt-black  txt-bold hoverable padding-s animated <?= (isset($pageData["_currentPage"]["cabang"])) ? "active" : "" ; ?>">Cabang</a>
            <!-- <a href="/developer" style="padding-right:10px; padding-left:10px;" class="txt-black  txt-bold hoverable padding-s animated <?= (isset($pageData["_currentPage"]["developer"])) ? "active" : "" ; ?>">Developer</a> -->
            <a href="/proyek"    style="padding-right:10px; padding-left:10px;" class="txt-black  txt-bold hoverable padding-s animated <?= (isset($pageData["_currentPage"]["proyek"])) ? "active" : "" ; ?> margin-col-lg">Primary Market</a>

            <?php 
            if (isset($_SESSION["loggedUser"]) && !empty($_SESSION["loggedUser"])) {?>
                 <div class="listing-part-header-order" style="position:relative; margin-right:15px">
                    <div onclick="$(this).parent().find('.sorter-tools').fadeToggle();" style="cursor:pointer;" class="hoverable">
                        <i class="fa fa-bell"> </i>
                    </div>
                    <div class="card flex flex-ver shadow sorter-tools" style="display:none; position:absolute; background-color:white; z-index:10;width:250px; transform:translateX(-50%);    padding-top: 15px;clip-path: polygon(0px 15px, 40% 15px, 50% 0px, 60% 15px, 100% 15px, 100% 100%, 0px 100%);">
                        <div class="flex flex-hor flex-equal" style="" id="divGroupNotif">
                            <div active-group="sortir-alphabet" onclick="loadNotifWishlist();" class="txt-center activeable-solo hoverable txt-bold animated active" data-value="asc">Wishlist</div>
                            <div active-group="sortir-alphabet" onclick="loadNotifListing();" class="txt-center activeable-solo hoverable txt-bold animated" data-value="desc">Listing</div>
                        </div>
                        <div class="flex-ver pl-3" id="divNotif">
                            <!-- <div class="hoverable" onclick="sort('');">Recommended</div> -->
                            <div class="hoverable">Notif 1 <i class="fa fa-check"></i></div>
                        </div>
                    </div>
                </div>
                <div style="position:relative;" id="divProfile">
                    <div class="txt-bold bg-red txt-white rounded-m padding-m user-section clickable" id="user-profile-container" data-fadein="profile-content">
                        <span><?= $_SESSION["loggedName"] ?> &nbsp;&nbsp;<i class="fa fa-caret-down"></i> &nbsp; &nbsp; <i class="fa fa-user-circle"></i></span>                      
                    </div>
                    <div id="profile-content" class="bg-white txt-white profile-content flex flex-ver" style="padding-top:5px;padding-bottom:5px; width:100%; border-radius:0px 0px 10px 10px; display:none;">
                        <a href="#" class="user-photo-mini" style="background-image:url('<?= (isset($_SESSION["loggedPhoto"])) ? "/assets/img/profile-customer/".$_SESSION["loggedPhoto"] : "/assets/img/profile-customer/default.png" ; ?>');"></a>
                        <a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["profile"])) ? "active" : "" ; ?>" href="/profile" style="border-top: 1px solid #00000030;">My Profile</a>
                        <a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["listingsaya"])) ? "active" : "" ; ?>" href="/listingsaya">Listing Saya</a>
                        <a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["favoritexout"])) ? "active" : "" ; ?>" href="/favoritxout">Favorite & X-Out</a>
                        <a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["wishlist"])) ? "active" : "" ; ?>" href="/wishlist">Wishlist Saya</a>
                        <a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["jadwal"])) ? "active" : "" ; ?>" href="/jadwal">Jadwal Saya</a>
                        <a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["createlisting"])) ? "active" : "" ; ?>" href="/createlisting">Tambah Listing</a>
                        <a class="txt-black hoverable txt-bold padding-m animated <?= (isset($pageData["_currentPage"]["setting"])) ? "active" : "" ; ?>" href="/setting">Pengaturan Saya</a>
                        <a class="txt-black hoverable txt-bold padding-m animated" onclick="logout();">Logout</a>
                    </div>
                </div>
                <?php    
						$displayLogin = "display:none;";
					}
					else {$displayLogin="";}?>
                <div class="header-popup" id="divLogin" style="float:right;<?=$displayLogin?>">
                    <a  href="javascript:void(0)" onclick="nyalainPopup();"  class="txt-bold bg-red txt-white rounded-m padding-m user-section clickable" style="padding-top:5px;padding-bottom:5px;">
                        <span>Login</span>
                    </a>
                    <?php
                        $this->load->view("page-part/loginregister-popup");
                    ?>

                </div>
        </div>
    </div>
    <div class="search-advance-container flex flex-ver transition">
        <form action="/listing/search" id="formSearch">
            <div class="adv-search-inner-container">
                <div class="adv-search-top">
                    <div class="flex flex-hor flex-equal flex-centered adv-search-item">
                        <div class="flex flex-ver" id="adv-search-item-jns-transaksi">
                            <div class="txt-center">
                                <h6>Jenis Transaksi</h6>
                                <div class="flex option-search-container flex-hor flex-vertical-center flex-centered flex-equal" id="jenis-search">
                                    <div class="option-search inlabel-btn flex flex-ver flex-centered txt-center flex-6 padding-m" data-value="0">
                                        <div class=""><i class="fa fa-heart"></i></div>
                                        <div class="mini-txt">Jual</div>
                                    </div>    
                                    <div class="option-search inlabel-btn flex flex-ver flex-centered txt-center flex-6 padding-m" data-value="1">
                                        <div class=""><i class="fa fa-heart"></i></div>
                                        <div class="mini-txt">Sewa</div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="flex flex-ver" id="adv-search-item-tipe-property">
                            <div class="txt-center">
                                <h6>Tipe Property</h6>
                                <div class="flex flex-hor option-search-container flex-vertical-stretch flex-centered" id="tipe-search">
                                    <div class="option-search inlabel-btn flex flex-ver flex-centered txt-center flex-6 padding-m"  data-value="1">
                                        <div class=""><i class="fa fa-heart"></i></div>
                                        <div class="mini-txt">Rumah</div>
                                    </div>
                                    <div class="option-search inlabel-btn flex flex-ver flex-centered txt-center flex-6 padding-m" data-value="2">
                                        <div class=""><i class="fa fa-times"></i></div>
                                        <div class="mini-txt">Apartemen</div>
                                    </div>
                                    
                                    <div class="option-search inlabel-btn flex flex-ver flex-centered txt-center flex-6 padding-m" data-value="3">
                                        <div class=""><i class="fa fa-times"></i></div>
                                        <div class="mini-txt">Gudang</div>
                                    </div>
                                    
                                    <div class="option-search inlabel-btn flex flex-ver flex-centered txt-center flex-6 padding-m" data-value="4">
                                        <div class=""><i class="fa fa-times"></i></div>
                                        <div class="mini-txt">Ruko</div>
                                    </div>
                                    
                                    <div class="option-search inlabel-btn flex flex-ver flex-centered txt-center flex-6 padding-m" data-value="5">
                                        <div class=""><i class="fa fa-times"></i></div>
                                        <div class="mini-txt">Tanah</div>
                                    </div>
                                    
                                    <div class="option-search inlabel-btn flex flex-ver flex-centered txt-center flex-6 padding-m" data-value="6">
                                        <div class=""><i class="fa fa-times"></i></div>
                                        <div class="mini-txt">Pabrik</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                    
                    <div class="flex flex-ver adv-search-item">
                        <div class="flex flex-hor flex-equal">
                            <div class="flex flex-ver margin-col-xlg">
                                <h6>Harga Min</h6>
                                <input class="priceInput min money" step:1000000 type="Number" onchange="cekPrice(this);" id="minPrice">
                                <small class="errPrice" class="text-danger" style="color:red;"></small> 
                            </div>
                            <div class="flex flex-ver">
                                <h6>Harga Max</h6>
                                <input class="priceInput max money" step:1000000 type="Number" onchange="cekPrice(this);" id="maxPrice">
                                <small class="errPrice" class="text-danger" style="color:red;"></small> 
                            </div>
                        </div>        
                        <div class="m-ion-range-slider">
                            <input type="hidden" id="priceSlider" />
                        </div>
                    </div>

                    <div class="adv-search-item">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-12 mb-2">
                                <div class="flex flex-ver">
                                    <h6>Kamar Tidur</h6>
                                    <div class="flex flex-hor flex-equal selectrange">
                                        <select onchange="cekKT(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="minKT">
                                            <option value='-1'>No Min</option>    
                                            <option value='1'>1</option>
                                            <option value='2'>2</option>
                                            <option value='3'>3</option>
                                            <option value='4'>4</option>
                                            <option value='5'>5</option>
                                            <option value='6'>6</option>
                                            <option value='7'>7</option>
                                            <option value='8'>8</option>
                                            <option value='9'>9</option>
                                            <option value='10'>10+</option>
                                        </select>
                                        <select onchange="cekKT(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="maxKT">
                                            <option value='-1'>No Max</option>    
                                            <option value='1'>1</option>
                                            <option value='2'>2</option>
                                            <option value='3'>3</option>
                                            <option value='4'>4</option>
                                            <option value='5'>5</option>
                                            <option value='6'>6</option>
                                            <option value='7'>7</option>
                                            <option value='8'>8</option>
                                            <option value='9'>9</option>
                                            <option value='10'>10+</option>
                                        </select>
                                    </div>
                                    <small class="errKT" class="text-danger" style="color:red;"></small> 
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-12 mb-2">
                                <div class="flex flex-ver ">
                                    <h6>Kamar Mandi</h6>
                                    <div class="flex flex-hor flex-equal selectrange">
                                        <select onchange="cekKM(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="minKM">
                                            <option value='-1'>No Min</option>    
                                            <option value='1'>1</option>
                                            <option value='2'>2</option>
                                            <option value='3'>3</option>
                                            <option value='4'>4</option>
                                            <option value='5'>5</option>
                                            <option value='6'>6</option>
                                            <option value='7'>7</option>
                                            <option value='8'>8</option>
                                            <option value='9'>9</option>
                                            <option value='10'>10+</option>
                                        </select>
                                        <select onchange="cekKM(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="maxKM">
                                            <option value='-1'>No Max</option>    
                                            <option value='1'>1</option>
                                            <option value='2'>2</option>
                                            <option value='3'>3</option>
                                            <option value='4'>4</option>
                                            <option value='5'>5</option>
                                            <option value='6'>6</option>
                                            <option value='7'>7</option>
                                            <option value='8'>8</option>
                                            <option value='9'>9</option>
                                            <option value='10'>10+</option>
                                        </select>
                                    </div>        
                                    <small class="errKM" class="text-danger" style="color:red;"></small>         
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-12 mb-2">
                                <div class="flex flex-ver ">
                                    <h6>Luas Tanah</h6>
                                    <div class="flex flex-hor flex-equal selectrange">
                                        <select onchange="cekLT(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="minLT">
                                            <option value='-1'>No Min</option>    
                                            <option value='25'>25</option>
                                            <option value='50'>50</option>
                                            <option value='75'>75</option>
                                            <option value='500'>500</option>
                                            <option value='750'>750</option>
                                            <option value='1000'>1000</option>
                                            <option value='1250'>1250</option>
                                            <option value='1500'>1500</option>
                                            <option value='2000'>2000</option>
                                            <option value='4000'>4000</option>
                                            <option value='6000'>6000</option>
                                            <option value='8000'>8000</option>
                                            <option value='10000'>10000</option>
                                            <option value='20000'>20000</option>
                                            <option value='30000'>30000</option>
                                            <option value='40000'>40000</option>
                                            <option value='50000'>50000</option>
                                        </select>
                                        <select onchange="cekLT(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="maxLT">
                                            <option value='-1'>No Max</option>    
                                            <option value='25'>25</option>
                                            <option value='50'>50</option>
                                            <option value='75'>75</option>
                                            <option value='500'>500</option>
                                            <option value='750'>750</option>
                                            <option value='1000'>1000</option>
                                            <option value='1250'>1250</option>
                                            <option value='1500'>1500</option>
                                            <option value='2000'>2000</option>
                                            <option value='4000'>4000</option>
                                            <option value='6000'>6000</option>
                                            <option value='8000'>8000</option>
                                            <option value='10000'>10000</option>
                                            <option value='20000'>20000</option>
                                            <option value='30000'>30000</option>
                                            <option value='40000'>40000</option>
                                            <option value='50000'>50000</option>
                                        </select>
                                    </div>
                                    <small class="errLT" class="text-danger" style="color:red;"></small> 
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-12 mb-2">
                                <div class="flex flex-ver ">
                                    <h6>Luas Bangunan</h6>
                                    <div class="flex flex-hor flex-equal selectrange">
                                        <select onchange="cekLB(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="minLB">
                                            <option value='-1'>No Min</option>    
                                            <option value='25'>25</option>
                                            <option value='50'>50</option>
                                            <option value='75'>75</option>
                                            <option value='500'>500</option>
                                            <option value='750'>750</option>
                                            <option value='1000'>1000</option>
                                            <option value='1250'>1250</option>
                                            <option value='1500'>1500</option>
                                            <option value='2000'>2000</option>
                                            <option value='4000'>4000</option>
                                            <option value='6000'>6000</option>
                                            <option value='8000'>8000</option>
                                            <option value='10000'>10000</option>
                                            <option value='20000'>20000</option>
                                            <option value='30000'>30000</option>
                                            <option value='40000'>40000</option>
                                            <option value='50000'>50000</option>
                                        </select>
                                        <select onchange="cekLB(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="maxLB">
                                            <option value='-1'>No Max</option>    
                                            <option value='25'>25</option>
                                            <option value='50'>50</option>
                                            <option value='75'>75</option>
                                            <option value='500'>500</option>
                                            <option value='750'>750</option>
                                            <option value='1000'>1000</option>
                                            <option value='1250'>1250</option>
                                            <option value='1500'>1500</option>
                                            <option value='2000'>2000</option>
                                            <option value='4000'>4000</option>
                                            <option value='6000'>6000</option>
                                            <option value='8000'>8000</option>
                                            <option value='10000'>10000</option>
                                            <option value='20000'>20000</option>
                                            <option value='30000'>30000</option>
                                            <option value='40000'>40000</option>
                                            <option value='50000'>50000</option>
                                        </select>
                                    </div>    
                                    <small class="errLB" class="text-danger" style="color:red;"></small>             
                                </div>
                            </div>
                        </div>
                        <!-- 
                        <table width="auto">
                            <tr>
                                <td style="width:50%; padding-right:17.5px;">
                                    <div class="flex flex-ver">
                                        <h6>Kamar Tidur</h6>
                                        <div class="flex flex-hor flex-equal selectrange">
                                            <select onchange="cekKT(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="minKT">
                                                <option value='-1'>No Min</option>    
                                                <option value='1'>1</option>
                                                <option value='2'>2</option>
                                                <option value='3'>3</option>
                                                <option value='4'>4</option>
                                                <option value='5'>5</option>
                                                <option value='6'>6</option>
                                                <option value='7'>7</option>
                                                <option value='8'>8</option>
                                                <option value='9'>9</option>
                                                <option value='10'>10+</option>
                                            </select>
                                            <select onchange="cekKT(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="maxKT">
                                                <option value='-1'>No Max</option>    
                                                <option value='1'>1</option>
                                                <option value='2'>2</option>
                                                <option value='3'>3</option>
                                                <option value='4'>4</option>
                                                <option value='5'>5</option>
                                                <option value='6'>6</option>
                                                <option value='7'>7</option>
                                                <option value='8'>8</option>
                                                <option value='9'>9</option>
                                                <option value='10'>10+</option>
                                            </select>
                                        </div>
                                        <small class="errKT" class="text-danger" style="color:red;"></small> 
                                    </div>
                                </td>
                                <td style="width:50%; padding-left:17.5px;">
                                    <div class="flex flex-ver ">
                                        <h6>Kamar Mandi</h6>
                                        <div class="flex flex-hor flex-equal selectrange">
                                            <select onchange="cekKM(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="minKM">
                                                <option value='-1'>No Min</option>    
                                                <option value='1'>1</option>
                                                <option value='2'>2</option>
                                                <option value='3'>3</option>
                                                <option value='4'>4</option>
                                                <option value='5'>5</option>
                                                <option value='6'>6</option>
                                                <option value='7'>7</option>
                                                <option value='8'>8</option>
                                                <option value='9'>9</option>
                                                <option value='10'>10+</option>
                                            </select>
                                            <select onchange="cekKM(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="maxKM">
                                                <option value='-1'>No Max</option>    
                                                <option value='1'>1</option>
                                                <option value='2'>2</option>
                                                <option value='3'>3</option>
                                                <option value='4'>4</option>
                                                <option value='5'>5</option>
                                                <option value='6'>6</option>
                                                <option value='7'>7</option>
                                                <option value='8'>8</option>
                                                <option value='9'>9</option>
                                                <option value='10'>10+</option>
                                            </select>
                                        </div>        
                                        <small class="errKM" class="text-danger" style="color:red;"></small>         
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:50%; padding-right:17.5px;">
                                    <div class="flex flex-ver ">
                                        <h6>Luas Tanah</h6>
                                        <div class="flex flex-hor flex-equal selectrange">
                                            <select onchange="cekLT(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="minLT">
                                                <option value='-1'>No Min</option>    
                                                <option value='25'>25</option>
                                                <option value='50'>50</option>
                                                <option value='75'>75</option>
                                                <option value='500'>500</option>
                                                <option value='750'>750</option>
                                                <option value='1000'>1000</option>
                                                <option value='1250'>1250</option>
                                                <option value='1500'>1500</option>
                                                <option value='2000'>2000</option>
                                                <option value='4000'>4000</option>
                                                <option value='6000'>6000</option>
                                                <option value='8000'>8000</option>
                                                <option value='10000'>10000</option>
                                                <option value='20000'>20000</option>
                                                <option value='30000'>30000</option>
                                                <option value='40000'>40000</option>
                                                <option value='50000'>50000</option>
                                            </select>
                                            <select onchange="cekLT(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="maxLT">
                                                <option value='-1'>No Max</option>    
                                                <option value='25'>25</option>
                                                <option value='50'>50</option>
                                                <option value='75'>75</option>
                                                <option value='500'>500</option>
                                                <option value='750'>750</option>
                                                <option value='1000'>1000</option>
                                                <option value='1250'>1250</option>
                                                <option value='1500'>1500</option>
                                                <option value='2000'>2000</option>
                                                <option value='4000'>4000</option>
                                                <option value='6000'>6000</option>
                                                <option value='8000'>8000</option>
                                                <option value='10000'>10000</option>
                                                <option value='20000'>20000</option>
                                                <option value='30000'>30000</option>
                                                <option value='40000'>40000</option>
                                                <option value='50000'>50000</option>
                                            </select>
                                        </div>
                                        <small class="errLT" class="text-danger" style="color:red;"></small> 
                                    </div>
                                </td>
                                <td style="width:50%; padding-left:17.5px;">
                                    <div class="flex flex-ver ">
                                        <h6>Luas Bangunan</h6>
                                        <div class="flex flex-hor flex-equal selectrange">
                                            <select onchange="cekLB(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="minLB">
                                                <option value='-1'>No Min</option>    
                                                <option value='25'>25</option>
                                                <option value='50'>50</option>
                                                <option value='75'>75</option>
                                                <option value='500'>500</option>
                                                <option value='750'>750</option>
                                                <option value='1000'>1000</option>
                                                <option value='1250'>1250</option>
                                                <option value='1500'>1500</option>
                                                <option value='2000'>2000</option>
                                                <option value='4000'>4000</option>
                                                <option value='6000'>6000</option>
                                                <option value='8000'>8000</option>
                                                <option value='10000'>10000</option>
                                                <option value='20000'>20000</option>
                                                <option value='30000'>30000</option>
                                                <option value='40000'>40000</option>
                                                <option value='50000'>50000</option>
                                            </select>
                                            <select onchange="cekLB(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="maxLB">
                                                <option value='-1'>No Max</option>    
                                                <option value='25'>25</option>
                                                <option value='50'>50</option>
                                                <option value='75'>75</option>
                                                <option value='500'>500</option>
                                                <option value='750'>750</option>
                                                <option value='1000'>1000</option>
                                                <option value='1250'>1250</option>
                                                <option value='1500'>1500</option>
                                                <option value='2000'>2000</option>
                                                <option value='4000'>4000</option>
                                                <option value='6000'>6000</option>
                                                <option value='8000'>8000</option>
                                                <option value='10000'>10000</option>
                                                <option value='20000'>20000</option>
                                                <option value='30000'>30000</option>
                                                <option value='40000'>40000</option>
                                                <option value='50000'>50000</option>
                                            </select>
                                        </div>    
                                        <small class="errLB" class="text-danger" style="color:red;"></small>             
                                    </div>
                                </td>
                            </tr>
                        </table>
                         -->
                    </div>            
                    
                    <div class="flex flex-ver adv-search-item">
                        <div class="txt-center ">
                            <h6>Sertifikat</h6>
                            <div class="flex flex-hor option-search-container flex-centered flex-equal" id="adv-search-item-sertifikat">
                                <div class="inlabel-btn option-search flex flex-ver flex-centered txt-center flex-6 padding-m"  data-value="Hak Milik">
                                    <div class="mini-txt">SHM</div>
                                    <input type="hidden" name="hiddenSHM" class="hidden-param"/>
                                </div>
                                <div class="inlabel-btn option-search flex flex-ver flex-centered txt-center flex-6 padding-m"  data-value="Hak Guna Bangunan">
                                    <div class="mini-txt">HGB</div>
                                    <input type="hidden" name="hiddenHGB" class="hidden-param"/>
                                </div>
                                <div class="inlabel-btn option-search flex flex-ver flex-centered txt-center flex-6 padding-m"  data-value="Strata">
                                    <div class="mini-txt">Strata</div>
                                    <input type="hidden" name="hiddenStrata" class="hidden-param"/>
                                </div>
                                <div class="inlabel-btn option-search flex flex-ver flex-centered txt-center flex-6 padding-m"  data-value="Girik">
                                    <div class="mini-txt">Girik</div>
                                    <input type="hidden" name="hiddenGirik" class="hidden-param"/>
                                </div>
                                <div class="inlabel-btn option-search flex flex-ver flex-centered txt-center flex-6 padding-m"  data-value="Lainnya">
                                    <div class="mini-txt">Lainnya</div>
                                    <input type="hidden" name="hiddenLainnya" class="hidden-param"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="flex flex-ver adv-search-item" style="margin-bottom: 0px !important;">
                        <h6 class="txt-center">Status Property</h6>
                        <div class="flex flex-hor flex-centered">
                            <div class="flex flex-ver flex-vertical-center flex-6">
                                <h6>Aktif</h6>
                                <span class="m-switch m-switch--icon m-switch--accent">
                                    <label>
                                        <input type="checkbox" id="status-active" checked>
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="flex flex-ver flex-vertical-center flex-6">
                                <h6>Terjual</h6>
                                <div>
                                    <span class="m-switch m-switch--icon m-switch--accent" style="margin:auto 0px;">
                                        <label style="margin:auto 0px;">
                                            <input type="checkbox" id="statusTerjual" name="statusTerjual">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            
                        </div>      
                    </div>

                    <div class="flex flex-hor mt-1 flex-centered status-active flex-equal adv-search-item">
                        <div class="flex flex-ver flex-vertical-center margin-col-xlg">
                            <h6>Waktu Aktif</h6>
                            <select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="listingWaktuAktif" name="listingWaktuAktif">
                                <option value='-1'>No Max</option>    
                                <option value='7'>Kurang dari 7 hari</option>
                                <option value='14'>Kurang dari 14 hari</option>
                                <option value='30'>Kurang dari 30 hari</option>
                                <option value='90'>Kurang dari 90 hari</option>
                            </select>
                        </div>
                        <div class="flex flex-ver flex-vertical-center">
                            <h6>Waktu Terjual</h6>
                            <select disabled class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="listingWaktuTerjual" name="listingWaktuTerjual">
                                <option value='-1'>No Max</option>    
                                <option value='30'>1 bulan terakhir</option>
                                <option value='90'>3 bulan terakhir</option>
                                <option value='180'>6 bulan terakhir</option>
                                <option value='365'>1 tahun terakhir</option>
                            </select>
                        </div>
                    </div>
                </div>
            
                <div class="adv-search-bottom flex flex-hor">
                    <div class="flex flex-hor flex-separate adv-search-item flex-vertical-center">
                        <!-- <div class="btn btn-reset bg-red txt-white">Reset</div> -->
                        
                        <a href="javascript:void(0)" style="font-weight:bold; cursor: pointer; font-size:1.25em;" onclick="resetFilter();">Reset</a>
                        <div class="flex flex-hor-reverse flex-centered flex-vertical-center">
                            <div class="btn txt-white bg-red" onclick="advanceSearch(0);">Search</div>
                            <div class="btn txt-white bg-red mr-3" onclick="advanceSearch(1);">Save & Search</div>
                            <div class="btn btn-info mr-3" onclick="closeFilter();">Cancel</div>
                            <div class="label mr-3 " id="matchProperty" style="font-size:1em;"> XX Property </div>
                        </div>
                    </div>
                </div>      
            </div>
        </form>
</header>
<script src="/assets/js/page/login.js"></script>
<script>
function advanceSearch(save){
    $("#opsi-lain").click();
    if (save && userID == null){
        Swal.fire({
			type: 'error',
			title: 'Oops...',
			text: 'Harap Login Terlebih Dahulu',
			footer: '<a href="/login">LOGIN DISINI</a>'
		});
    }else{
        search(save);
    }
}
var sendData = "";
var boundData = "";
var forceFitBounds = false;
var saveSearch = 0;

function hitungMatch(){
    var paramSearch = "";
    var searchValue = $("#headSearch").val();
    var searchTipe = $("#headSearch").attr("data-hidden");

    if (searchValue != "" && searchTipe != ""){
        paramSearch += "&searchValue=" + searchTipe + "-" + searchValue;
    }

    var jenis = $("#jenis-search").find(".active");
    if (jenis.length > 0){
        if (jenis.length > 1){
            jenis = "2";
        }else{
            jenis = jenis.attr("data-value");
        }
        paramSearch += "&jenis=" + jenis;
    }

    var sertifikat = $("#adv-search-item-sertifikat").find(".active");
    if (sertifikat.length > 0){
        var arrSertifikat = "";
        sertifikat.each(function () {
            arrSertifikat += $(this).attr("data-value") + "-";
        });
        paramSearch += "&sertifikat=" + arrSertifikat;
    }
    // paramSearch += "&statusTerjual=" + $("#statusTerjual").val();
    // paramSearch += "&statusListing=" + $("#statusListing").val();
    // if ($("#waktuListing").val() != -1){
    //     var jumlahHari = $("#waktuListing").val();
    //     var tglStart = new Date();
    //     tglStart.setDate(tglStart.getDate()-jumlahHari);
    //     paramSearch += "&waktuListing=" + tglStart.getFullYear() + "-" + (tglStart.getMonth()+1) + "-" + tglStart.getDate();
    // }

    var tipe = $("#tipe-search").find(".active");
    if (tipe.length > 0){
        var arrTipe = "";
        tipe.each(function () {
            arrTipe += $(this).attr("data-value") + "-";
        });
        paramSearch += "&tipe=" + arrTipe;
    }
    if ($("#minPrice").val() > 0){
        paramSearch += "&minPrice=" + $("#minPrice").val();
    }
    if ($("#maxPrice").val() < 100000000000){
        paramSearch += "&maxPrice=" + $("#maxPrice").val();
    }
    if ($("#minKT").val() > 0){
        paramSearch += "&minKT=" + $("#minKT").val();
    }
    if ($("#maxKT").val() > 0){
        paramSearch += "&maxKT=" + $("#maxKT").val();
    }

    if ($("#minKM").val() > 0){
        paramSearch += "&minKM=" + $("#minKM").val();
    }
    if ($("#maxKM").val() > 0){
        paramSearch += "&maxKM=" + $("#maxKM").val();
    }

    if ($("#minLT").val() > 0){
        paramSearch += "&minLT=" + $("#minLT").val();
    }
    if ($("#maxLT").val() > 0){
        paramSearch += "&maxLT=" + $("#maxLT").val();
    }

    if ($("#minLB").val() > 0){
        paramSearch += "&minLB=" + $("#minLB").val();
    }
    if ($("#maxLB").val() > 0){
        paramSearch += "&maxLB=" + $("#maxLB").val();
    }
    if ($("#status-active").prop('checked')){
        paramSearch += "&statusListing=1";
        var jumlahHari = $("#listingWaktuAktif").val();
        if (jumlahHari != -1){
            var tglStart = new Date();
            tglStart.setDate(tglStart.getDate()-jumlahHari);
            paramSearch += "&waktuListing=" + tglStart.getFullYear() + "-" + (tglStart.getMonth()+1) + "-" + tglStart.getDate();   
        }
    }else if ($("#statusTerjual").prop('checked')){
        paramSearch += "&statusListing=-1";
        var jumlahHari = $("#listingWaktuTerjual").val();
        if (jumlahHari != -1){
            var tglStart = new Date();
            tglStart.setDate(tglStart.getDate()-jumlahHari);
            paramSearch += "&terjual=" + tglStart.getFullYear() + "-" + (tglStart.getMonth()+1) + "-" + tglStart.getDate();   
        }else{
            paramSearch += "&terjual=-1";
        }
    }

    $.ajax({
		"url" : API_URL + "/api/get/listings/search?token=public&limit=10&offset=0&save=0&" + paramSearch,
        success : function(data){
     	  data = JSON.parse(data);
          $("#matchProperty").html(data.count + " Property");
        }
	});
}

function search(save) {
    saveSearch = save;
    sendData = "";
    var searchValue = $("#headSearch").val();
    var searchTipe = $("#headSearch").attr("data-hidden");

    if (searchValue != "" && searchTipe != ""){
        sendData += "&searchValue=" + searchTipe + "-" + searchValue;
    }

    var jenis = $("#jenis-search").find(".active");
    if (jenis.length > 0){
        if (jenis.length > 1){
            jenis = "2";
        }else{
            jenis = jenis.attr("data-value");
        }
        sendData += "&jenis=" + jenis;
    }

    var sertifikat = $("#adv-search-item-sertifikat").find(".active");
    if (sertifikat.length > 0){
        var arrSertifikat = "";
        sertifikat.each(function () {
            arrSertifikat += $(this).attr("data-value") + "-";
        });
        sendData += "&sertifikat=" + arrSertifikat;
    }
    // sendData += "&statusTerjual=" + $("#statusTerjual").val();
    // sendData += "&statusListing=" + $("#statusListing").val();
    // if ($("#waktuListing").val() != -1){
    //     var jumlahHari = $("#waktuListing").val();
    //     var tglStart = new Date();
    //     tglStart.setDate(tglStart.getDate()-jumlahHari);
    //     sendData += "&waktuListing=" + tglStart.getFullYear() + "-" + (tglStart.getMonth()+1) + "-" + tglStart.getDate();
    // }

    var tipe = $("#tipe-search").find(".active");
    if (tipe.length > 0){
        var arrTipe = "";
        tipe.each(function () {
            arrTipe += $(this).attr("data-value") + "-";
        });
        sendData += "&tipe=" + arrTipe;
    }
    if ($("#minPrice").val() > 0){
        sendData += "&minPrice=" + $("#minPrice").val();
    }
    if ($("#maxPrice").val() < 100000000000){
        sendData += "&maxPrice=" + $("#maxPrice").val();
    }
    if ($("#minKT").val() > 0){
        sendData += "&minKT=" + $("#minKT").val();
    }
    if ($("#maxKT").val() > 0){
        sendData += "&maxKT=" + $("#maxKT").val();
    }

    if ($("#minKM").val() > 0){
        sendData += "&minKM=" + $("#minKM").val();
    }
    if ($("#maxKM").val() > 0){
        sendData += "&maxKM=" + $("#maxKM").val();
    }

    if ($("#minLT").val() > 0){
        sendData += "&minLT=" + $("#minLT").val();
    }
    if ($("#maxLT").val() > 0){
        sendData += "&maxLT=" + $("#maxLT").val();
    }

    if ($("#minLB").val() > 0){
        sendData += "&minLB=" + $("#minLB").val();
    }
    if ($("#maxLB").val() > 0){
        sendData += "&maxLB=" + $("#maxLB").val();
    }
    if ($("#status-active").prop('checked')){
        sendData += "&statusListing=1";
        var jumlahHari = $("#listingWaktuAktif").val();
        if (jumlahHari != -1){
            var tglStart = new Date();
            tglStart.setDate(tglStart.getDate()-jumlahHari);
            sendData += "&waktuListing=" + tglStart.getFullYear() + "-" + (tglStart.getMonth()+1) + "-" + tglStart.getDate();   
        }
    }else if ($("#statusTerjual").prop('checked')){
        sendData += "&statusListing=-1";
        var jumlahHari = $("#listingWaktuTerjual").val();
        if (jumlahHari != -1){
            var tglStart = new Date();
            tglStart.setDate(tglStart.getDate()-jumlahHari);
            sendData += "&terjual=" + tglStart.getFullYear() + "-" + (tglStart.getMonth()+1) + "-" + tglStart.getDate();   
        }else{
            sendData += "&terjual=-1";
        }
    }
    forceFitBounds = true;
    boundData = "";
    drawData = "";
    $("header").removeClass("active");
    refreshData();
}

function refreshData(){
    var url = window.location.href;
    var arrUrl = url.split('/');
    var arrUrl2 = arrUrl[3].split('?');
    if (arrUrl2[0] == 'listing'){
        loadSenseMapMarker();
    }else{
        window.location = BASE_URL + "/listing?"+sendData+sortData;
    }
}


function cekPrice(e){
    var minPrice = parseInt($("#minPrice").val());
    var maxPrice = parseInt($("#maxPrice").val());
    if (minPrice > maxPrice){
        $(".errPrice").html("*Nilai maksimum harus lebih kecil");
        if ($(e).attr('id') == 'minPrice'){
            $(e).val(0);
        }else{
            $(e).val(100000000000);
        }
    }else{
        $(".errPrice").html("");
    }
}

function cekKT(e){
    var minKT = parseInt($("#minKT").val());
    var maxKT = parseInt($("#maxKT").val());
    if (minKT != -1 && maxKT != -1 && minKT>maxKT){
        $(".errKT").html("*Nilai maksimum harus lebih kecil");
        $(e).val(-1);
    }else{
        $(".errKT").html("");
    }
}

function cekKM(e){
    var minKM = parseInt($("#minKM").val());
    var maxKM = parseInt($("#maxKM").val());
    if (minKM != -1 && maxKM != -1 && minKM>maxKM){
        $(".errKM").html("*Nilai maksimum harus lebih kecil");
        $(e).val(-1);
    }else{
        $(".errKM").html("");
    }
}

function cekLT(e){
    var minLT = parseInt($("#minLT").val());
    var maxLT = parseInt($("#maxLT").val());
    if (minLT != -1 && maxLT != -1 && minLT>maxLT){
        $(".errLT").html("*Nilai maksimum harus lebih kecil");
        $(e).val(-1);
    }else{
        $(".errLT").html("");
    }
}

function cekLB(e){
    var minLB = parseInt($("#minLB").val());
    var maxLB = parseInt($("#maxLB").val());
    if (minLB != -1 && maxLB != -1 && minLB>maxLB){
        $(".errLB").html("*Nilai maksimum harus lebih kecil");
        $(e).val(-1);
    }else{
        $(".errLB").html("");
    }
}

function closeFilter(){
    $("header").toggleClass("active");
}
function resetFilter(){
    $("#jenis-search>.active").removeClass("active");
    $("#tipe-search>.active").removeClass("active");
    $("#adv-search-item-sertifikat>.active").removeClass("active");
    $("#minPrice").val(0);
    $("#maxPrice").val(100000000000);
    $("#minKT").val(-1);
    $("#maxKT").val(-1);
    $("#minKM").val(-1);
    $("#maxKM").val(-1);
    $("#minLT").val(-1);
    $("#maxLT").val(-1);
    $("#minLB").val(-1);
    $("#maxLB").val(-1);
    $("#statusTerjual").prop("checked", false);
    $("#status-active").prop("checked", true);
    $("#statusListing").val(-1);
    $("#waktuListing").val(-1);
    updateOptionBadge();
    priceSliderInstance.update({
        from: $(".priceInput.min").val(),
        to: $(".priceInput.max").val()
    });
}

function loadNotifWishlist(){
    mApp.block("#divNotif", {});
    $.ajax({
		"url" : API_URL + "/api/get/notifications/getCustomerWishlist?token=customer&customer=" + userID,
        success : function(data){
            data = JSON.parse(data);
            var row = "";
            data.data.forEach(e => {
                var bold = ""
                if (e.status == 0){
                    bold = "font-weight: bold;";
                }
                row += `<div class="hoverable" style="`+bold+`">`+ e.label + ` - ` + e.judul_listing +`</i></div>`;
            });
            $("#divNotif").html(row);
		    mApp.unblock("#divNotif", {});
        }
	});
}

function loadNotifListing(){
    mApp.block("#divNotif", {});
    $.ajax({
		"url" : API_URL + "/api/get/notifications/getStatusListing?token=customer&customer=" + userID,
        success : function(data){
            data = JSON.parse(data);
            var row = "";
            data.data.forEach(e => {
                var bold = "";
                if (e.read_customer == 0){
                    bold = "font-weight: bold;";
                }
                if (e.status_listing == 0){
                    row += `<div class="hoverable" style="`+bold+`">Listing "`+ e.judul_listing + `" ditambahkan oleh ` + e.nama_broker + ` sebagai listing Anda</i></div>`;
                }else{
                    row += `<div class="hoverable" style="`+bold+`">Listing "`+ e.judul_listing + `" sudah aktif</i></div>`;
                }
                
            });
            $("#divNotif").html(row);
		    mApp.unblock("#divNotif", {});
        }
	});
}
</script>