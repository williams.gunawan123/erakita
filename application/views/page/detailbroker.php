<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body style="overflow-y:auto;">

	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->

	<!-- Breadcrumb -->
	<div class="site-breadcrumb" style="margin-top: 50px;">
		<div class="container">
			<a href="/"><i class="fa fa-home"></i>Home</a>
            <a href="/broker"><i class="fa fa-angle-right"></i>Broker</a>
			<span><i class="fa fa-angle-right"></i><?=$dataBroker->nama_broker?></span>
		</div>
	</div>

    
	<section class="my-container flex flex-ver">
        <div class="flex flex-ver">
            <div style="background-color:gainsboro; height:250px; align-items:stretch; padding-top:20px;" >
                <div class="flex flex-hor" id="databroker" data-id="<?=$dataBroker->id_broker?>" style="padding: 0px 5%; margin: auto;">
                    <?php
                        if ($dataBroker->foto_broker != ""){
                            $foto = $dataBroker->foto_broker;
                        }else{
                            $foto = "default.png";
                        }
                    ?>
                    <div class="square" style="border-radius:50%; border: 2px solid white; background-image:url('/assets/img/profile-broker/<?=$foto?>');height:10vw; width:10vw; background-size:cover; background-repeat:no-repeat; background-position: center;">
                        
                    </div>
                    <div class="flex flex-ver flex-equal" style="flex-grow:1;">
                        <div class="flex flex-hor flex-separate padding-s flex-vertical-center" style=" font-size:1.5em;">
                            <div class="flex flex-hor">
                                <div class="flex flex-ver txt-bold  margin-col-md" style="margin:auto;">
                                    <span><?=$dataBroker->nama_broker?></span>
                                    <a class="txt-black hoverable" href="/cabang/detail/<?=$dataBroker->id_cabang?>"><span> <?=$dataBroker->nama_cabang?></span></a>
                                </div>
                                <div class="flex txt-white" style="background-image:url('/assets/img/erakita-logo-square.png'); background-size:contain; background-repeat:no-repeat; height:50px; width:50px; margin-left:30px;"></div>
                            </div>
                            <div class="flex flex-hor flex-equal">
                                <div class="clickable padding-m txt-white" style="border-radius: 5px; margin: 2px; background-color: #c41c00;"><span class="fa fa-envelope" style="color: #FFFFFF; font-size: 1.3em;"><span></span></div>
                                <div class="clickable padding-m txt-white" style="border-radius: 5px; margin: 2px; background-color: #2e7d32;"><span class="fa fa-phone" style="color: #FFFFFF; font-size: 1.3em;"><span></span></div>
                                <div class="clickable padding-m txt-white" style="border-radius: 5px; margin: 2px; background-color: #128C7E;"><span class="fa fa-whatsapp" style="color: #FFFFFF; font-size: 1.3em;"><span></span></div>
                            </div>
                        </div>
                        <table style="margin-left: 7px;">
                            <tr>
                                <td><span style="color: #c41c00; font-size: 1.3em;" class="fa fa-envelope"></span></td>
                                <td>:</td>
                                <td><span style="color: #666F;" class="margin-col-md"><?=$dataBroker->email_broker?></span></td>
                            </tr>
                            <tr>
                                <td><span style="color: #2e7d32; font-size: 1.3em;" class="fa fa-phone"></span></td>
                                <td>:</td>
                                <td><span style="color: #666F;" class=" margin-col-md"><?=$dataBroker->telp_broker?></span></td>
                            </tr>
                            <tr>
                                <td><span style="color: #128C7E; font-size: 1.3em;" class="fa fa-whatsapp"></span></td>
                                <td>:</td>
                                <td><span style="color: #666F;" class="margin-col-md"><?=$dataBroker->telp_broker?></span></td>
                            </tr>
                        </table>
                    </div>
                    
                </div>

            </div>
            <div style=" height:100px; margin-top:-1rem; margin-bottom:16rem; flex-wrap:wrap;" class="my-container flex flex-hor">
                <div class="flex-2 padding-s" style="">
                    <div style="background-color:white; box-shadow:0px 0px 20px 1px #0000001c; height:100%; padding: 10px 0px;">
                        <div class="flex flex-ver my-container">
                            <div class="flex flex-hor txt-lg"> <i class="fa fa-home padding-s"></i><i class="fa fa-search padding-s"></i></div>
                            <div class="txt-bold" >Menlistingkan</div>
                            <div style="font-size:0.9em;"><?=$dataBroker->menlistingkan?> Properti</div>
                            <div style="font-size:0.9em;">(<?=$dataBroker->menlistingkan?> Baru Saja)</div>
                            <div style="font-size:0.9em;">HMin - HMax</div>
                        </div>
                    </div>
                </div>
                
                <div class="flex-2 padding-s" style="">
                    <div style="background-color:white; box-shadow:0px 0px 20px 1px #0000001c; height:100%; padding: 10px 0px;">
                        <div class="flex flex-ver my-container">
                            <div class="flex flex-hor txt-lg"> <i class="fa fa-home  padding-s"></i><i class="fa fa-search  padding-s"></i></div>
                            <div class="txt-bold" >Mensharekan</div>
                            <div style="font-size:0.9em;"><?=$dataBroker->mensharekan?> Properti</div>
                            <div style="font-size:0.9em;">(<?=$dataBroker->mensharekan?> Baru Saja)</div>
                            <div style="font-size:0.9em;">HMin - HMax</div>
                        </div>
                    </div>
                </div>
                
                <div class="flex-2 padding-s" style="">
                    <div style="background-color:white; box-shadow:0px 0px 20px 1px #0000001c; height:100%; padding: 10px 0px;">
                        <div class="flex flex-ver my-container">
                            <div class="flex flex-hor txt-lg"> <i class="fa fa-home  padding-s"></i><i class="fa fa-search  padding-s"></i></div>
                            <div class="txt-bold" >Mentransaksikan</div>
                            <div style="font-size:0.9em;">24 Properti</div>
                            <div style="font-size:0.9em;">(3 Baru Saja)</div>
                            <div style="font-size:0.9em;">HMin - HMax</div>
                        </div>
                    </div>
                </div>
                
                <div class="flex-2 padding-s" style="">
                    <div style="background-color:white; box-shadow:0px 0px 20px 1px #0000001c; height:100%; padding: 10px 0px;">
                        <div class="flex flex-ver my-container">
                            <div class="flex flex-hor txt-lg" style=""> <i class="fa fa-home  padding-s"></i><i class="fa fa-search  padding-s"></i></div>
                            <div class="txt-bold" >CoBroke Mentransaksikan</div>
                            <div style="font-size:0.9em;">24 Properti</div>
                            <div style="font-size:0.9em;">(3 Baru Saja)</div>
                            <div style="font-size:0.9em;">HMin - HMax</div>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="listing-part col-lg-12 col-sm-12 col-md-12">
                <div class="listing-part-header">
                    <div class="listing-part-header-left">
                        <div class="listing-part-header-title">
                            Menampilkan <span class="listing-part-header-title-text" id="showedListing">0</span> dari <span class="listing-part-header-title-text" id="totalListing">0</span> properti
                        </div>
                        
                        <div class="listing-part-header-order" style="position:relative">
                            <div onclick="$(this).parent().find('.sorter-tools').fadeToggle();" style="cursor:pointer;" class="hoverable">
                                <i class="fa fa-filter"></i>
                                Urutkan
                            </div>
                            <div class="card flex flex-ver shadow sorter-tools" style="display:none; position:absolute; background-color:white; z-index:10;width:250%; transform:translateX(-50%);    padding-top: 15px;clip-path: polygon(0px 15px, 40% 15px, 50% 0px, 60% 15px, 100% 15px, 100% 100%, 0px 100%);">
                                <div class="flex flex-hor flex-equal" style="" id="urutan">
                                    <div active-group="sortir-alphabet" onclick="urutkanListingBrokerAZ('asc');" class="txt-center activeable-solo hoverable txt-bold animated active" data-value="asc">A-Z</div>
                                    <div active-group="sortir-alphabet" onclick="urutkanListingBrokerAZ('desc');" class="txt-center activeable-solo hoverable txt-bold animated" data-value="desc">Z-A</div>
                                </div>
                                <div class="flex-ver pl-3">
                                    <!-- <div class="hoverable" onclick="sort('');">Recommended</div> -->
                                    <div class="hoverable" data-value="startlisting" id="sort-startlisting" onclick="urutkanListingBroker(this);">Terbaru <i class="fa fa-check"></i></div>
                                    <div class="hoverable" data-value="harga" id="sort-harga" onclick="urutkanListingBroker(this);">Harga</div>
                                    <div class="hoverable" data-value="kamartidur" id="sort-kamartidur" onclick="urutkanListingBroker(this);">Kamar Tidur</div>
                                    <div class="hoverable" data-value="kamarmandi" id="sort-kamarmandi" onclick="urutkanListingBroker(this);">Kamar Mandi</div>
                                    <div class="hoverable" data-value="luastanah" id="sort-luastanah" onclick="urutkanListingBroker(this);">Luas Tanah</div>
                                    <div class="hoverable" data-value="luasbangunan" id="sort-luasbangunan" onclick="urutkanListingBroker(this);">Luas Bangunan</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="listing-part-header-right">
                        <div class="listing-part-header-tabs">
                            <ul class="nav nav-pills" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tabs-foto">Foto</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-tabel">Tabel</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Start Map -->
                <div class="my-container" style="width:100%;">
                    <!--Map Section -->
                    <div class="col-md-12 single-list-page hide-on-small-only" id="mapDiv" style="margin-bottom:10px;height:50vh;"></div>
                    <!--End of map Section -->
                </div>
                <div class="tab-content listing-part-content" style="background-color:white;">
                    <div class="tab-pane active flex flex-hor" style="display:flex; flex-wrap: wrap;" id="tabs-foto" role="tabpanel">
                        <!-- Data Listing -->
                        
                    </div>
                    <div class="sensecode-paginate" style="margin-top:20px;" id="listing-pagination"></div>
                    <div class="tab-pane" id="tabs-tabel">
                        <div class="row flex flex-hor"  style="align-content: stretch; align-items: stretch; margin-bottom: 10px;">
                            <div class="listing-table-detail-listing col-lg-6 col-sm-12" style="height:100%;">
                                <div class="m-portlet m-portlet--rounded" style="margin:0px;">
                                    <?php $gambar = json_encode(["/assets/img/single-list-slider/1.jpg","/assets/img/single-list-slider/2.jpg","/assets/img/single-list-slider/3.jpg"], JSON_OBJECT_AS_ARRAY) ?>
                                    <div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='<?= $gambar ?>'>
                                        <div class="listing-slider-badge">Open House</div>
                                        <div class="listing-slider-icon"><i class="fas fa-home"></i></div>
                                        <div class="listing-slider-counter">1 of 3</div>
                                    </div>
                                    <div class="m-portlet__body flex flex-ver" style="padding:10px;">
                                        <div class="col-sm-12" style="padding-left: 0;padding-right:0">
                                            <div class="listing-price listing-info">500 Jt - 900 Jt</div>
                                            <div class="listing-jalan listing-info">Jalan Kesuma Bangsa XXXi belebeb</div>
                                            <div class="listing-kota listing-info">Daerah, Kota</div>
                                        </div>
                                        <div class="col-sm-12" style="padding-left: 0;padding-right:0">
                                            <div class="listing-detail flex flex-hor flex-vertical-center flex-equal">
                                                <div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>2</div>
                                                <div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>3</div>
                                                <div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>200m<sup>2</sup></div>
                                                <div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>500m<sup>2</sup></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="listing-table-detail-agent col-lg-6 col-sm-12" style="padding-left:0px;">
                                <div class="flex flex-ver m-portlet__body" style="align-content:stretch;height: 100%;">
                                    <div class="flex flex-hor flex-separate flex-vertical-center">
                                        <div class="flex flex-hor">
                                        <a href="#" class="user-photo-mini" style="margin:10px;background-image:url('<?= (isset($_SESSION["loggedPhoto"])) ? "/assets/img/profile-customer/".$_SESSION["loggedPhoto"] : "/assets/img/profile-customer/default.png" ; ?>');"></a>
                                        </div>
                                        <div class="flex flex-ver" style=" margin:auto">
                                            <!-- <div class="listing-title listing-info">Dilist oleh</div> -->
                                            <div class="listing-title listing-info hoverable txt-bold" style="font-size:1.5em;">Delvin Limanto</div>
                                            <div class="listing-title listing-info label" style="font-size:0.9em;">ERA Kita Cabang Pakuwon</div>
                                        </div>
                                        
                                    </div>
                                    <div class="flex flex-hor" style="height:100%">
                                        <div class="flex flex-ver flex-separate col-lg-10 col-md-10 col-sm-12" style="padding:0px 0px; align-content:stretch;height: 100%; font-size:1.2em;">
                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-sm-5">Panjang</div>
                                                <div class="col-lg-7 col-md-7 col-sm-7 text-right">20 m</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-sm-5">Lebar</div>
                                                <div class="col-lg-7 col-md-7 col-sm-7 text-right">7 m</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-sm-5">Sertifikat</div>
                                                <div class="col-lg-7 col-md-7 col-sm-7 text-right">HGB</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-sm-5">Hadap</div>
                                                <div class="col-lg-7 col-md-7 col-sm-7 text-right">Timur</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-sm-5">Pengembang</div>
                                                <div class="col-lg-7 col-md-7 col-sm-7 text-right">Pakuwon</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-sm-5">Proyek</div>
                                                <div class="col-lg-7 col-md-7 col-sm-7 text-right">Grand Island</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding-left: 0;padding-right:0">
                                            <div class="listing-content-icon"><i style="font-size: 1.3em;" class="fab fa-whatsapp"></i></div>
                                            <div class="listing-content-icon"><i style="font-size: 1.3em;" class="fas fa-heart"></i></div>
                                            <div class="listing-content-icon"><i style="font-size: 1.3em;" class="fas fa-share"></i></div>
                                            <div class="listing-content-icon"><i style="font-size: 1.3em;" class="fas fa-ellipsis-h"></i></div>
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="listing-table-detail">
                            <div class="m-portlet m-portlet--mobile">
                                <div class="m-portlet__body">

                                    <!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                                        <thead>
                                            <tr>
                                                <th>Alamat</th>
                                                <th>Daerah</th>
                                                <th>Kota</th>
                                                <th>Harga</th>
                                                <th>Kamar Tidur</th>
                                                <th>Kamar Mandi</th>
                                                <th>Luas Bangunan</th>
                                                <th>Luas Tanah</th>
                                                <th>Rp/m2</th>
                                                <th>Hadap</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i=0; $i<20; $i++){ ?>
                                                <tr>
                                                    <td>Jalan Ngagel</td>
                                                    <td>Gubeng</td>
                                                    <td>Surabaya</td>
                                                    <td>1.5 M</td>
                                                    <td>3</td>
                                                    <td>2</td>
                                                    <td>270 m2</td>
                                                    <td>300 m2</td>
                                                    <td>10 jt</td>
                                                    <td>Timur</td>
                                                    <td></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Alamat</th>
                                                <th>Daerah</th>
                                                <th>Kota</th>
                                                <th>Harga</th>
                                                <th>Kamar Tidur</th>
                                                <th>Kamar Mandi</th>
                                                <th>Luas Bangunan</th>
                                                <th>Luas Tanah</th>
                                                <th>Rp/m2</th>
                                                <th>Hadap</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>				
                    </div>
                    <div class="tab-pane" id="tabs-statistik">Coming Soon...</div>
                </div>
            </div>
        </div>
        <!--Start Map-->
        
	</section>
	<!-- Page -->
	<section class="page-section">
		<div class="container">
			
			<div class="row">
                
				<!-- sidebar -->
				
			</div>
		</div>
	</section>
	<!-- Page end -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
    <?php 
        $set['footer']=false;
        $this->load->view('page-part/common-foot', $set);
    ?>
    <script src="https://cdn.rawgit.com/hayeswise/Leaflet.PointInPolygon/v1.0.0/wise-leaflet-pip.js"></script>
</body>
</html>