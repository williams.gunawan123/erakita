<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/header');?>
	<!-- Header section end -->

	<!-- Hero section -->
	<section class="set-bg jumbotron" data-setbg="/assets/img/bg-home2.jpg" style="background-size: cover;background-position: center -14vh;">
		<div class="container hero-text text-white">
			<h2>WE DELIVER TRUST, EXPERIENCE AND VALUE IN REAL ESTATE</h2>
			<p>Search real estate property records, houses, condos, land and more on leramiz.com®.<br>Find property info from the most comprehensive source data.</p>
		</div>
	</section>
	<!-- Hero section end -->

	<!-- Filter form section -->
	<div class="filter-search" style="display:flex; width:100%; align-content:center">
		<div class="container" style="display:flex;">
			<form class="filter-form" style="width: auto;display: flex; margin:auto;">
				<input type="text" placeholder="Cari Listing ..." style="width:calc(43vw - 100px);">
				<!-- <select>
					<option value="City">City</option>
				</select>
				<select>
					<option value="City">State</option>
				</select> -->
				<button class="site-btn fs-submit" style="width:15vw;">SEARCH</button>
			</form>
		</div>
	</div>
	<!-- Filter form section end -->

	<!-- feature section -->
	<section class="feature-section spad">
		<div class="container">
			<div class="section-title text-center">
				<h3>Hot Listings</h3>
				<p>Browse houses and flats for sale and to rent in your area</p>
			</div>
			<div class="row" id="hotListing">
			</div>
		</div>
	</section>
	<!-- feature section end -->

	<!-- Properties section -->
	<section class="properties-section spad">
		<div class="container">
			<div class="section-title text-center">
				<h3>New Listings</h3>
				<p>Discover how much the latest properties have been sold for</p>
			</div>
			<div class="row" id="recentSold">
			</div>
		</div>
	</section>
	<!-- Properties section end -->


	<!-- Services section -->
	<!-- <section class="services-section spad set-bg" data-setbg="<?= base_url('assets/img/service-bg.jpg')?>">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<img src="<?= base_url('assets/img/service.jpg')?>" alt="">
				</div>
				<div class="col-lg-5 offset-lg-1 pl-lg-0">
					<div class="section-title text-white">
						<h3>OUR SERVICES</h3>
						<p>We provide the perfect service for </p>
					</div>
					<div class="services">
						<div class="service-item">
							<i class="fa fa-comments"></i>
							<div class="service-text">
								<h5>Consultant Service</h5>
								<p>In Aenean purus, pretium sito amet sapien denim consectet sed urna placerat sodales magna leo.</p>
							</div>
						</div>
						<div class="service-item">
							<i class="fa fa-home"></i>
							<div class="service-text">
								<h5>Properties Management</h5>
								<p>In Aenean purus, pretium sito amet sapien denim consectet sed urna placerat sodales magna leo.</p>
							</div>
						</div>
						<div class="service-item">
							<i class="fa fa-briefcase"></i>
							<div class="service-text">
								<h5>Renting and Selling</h5>
								<p>In Aenean purus, pretium sito amet sapien denim consectet sed urna placerat sodales magna leo.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<!-- Services section end -->

	<!-- feature category section -->
	<section class="feature-category-section spad">
		<div class="container">
			<div class="section-title text-center">
				<h3>LOOKING PROPERTY</h3>
				<p>What kind of property are you looking for? We will help you</p>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-6 f-cata">
					<img src="<?= base_url('assets/img/feature-cate/1.jpg')?>" alt="">
					<h5>Apartment for rent</h5>
				</div>
				<div class="col-lg-3 col-md-6 f-cata">
					<img src="<?= base_url('assets/img/feature-cate/2.jpg')?>" alt="">
					<h5>Family Home</h5>
				</div>
				<div class="col-lg-3 col-md-6 f-cata">
					<img src="<?= base_url('assets/img/feature-cate/3.jpg')?>" alt="">
					<h5>Resort Villas</h5>
				</div>
				<div class="col-lg-3 col-md-6 f-cata">
					<img src="<?= base_url('assets/img/feature-cate/4.jpg')?>" alt="">
					<h5>Office Building</h5>
				</div>
			</div>
		</div>
	</section>
	<!-- feature category section end-->


	<!-- Gallery section -->
	<!-- <section class="gallery-section spad">
		<div class="container">
			<div class="section-title text-center">
				<h3>Popular Places</h3>
				<p>We understand the value and importance of place</p>
			</div>
			<div class="gallery">
				<div class="grid-sizer"></div>
				<a href="#" class="gallery-item grid-long set-bg" data-setbg="<?= base_url('assets/img/gallery/1.jpg')?>">
					<div class="gi-info">
						<h3>New York</h3>
						<p>118 Properties</p>
					</div>
				</a>
				<a href="#" class="gallery-item grid-wide set-bg" data-setbg="<?= base_url('assets/img/gallery/2.jpg')?>">
					<div class="gi-info">
						<h3>Florida</h3>
						<p>112 Properties</p>
					</div>
				</a>
				<a href="#" class="gallery-item set-bg" data-setbg="<?= base_url('assets/img/gallery/3.jpg')?>">
					<div class="gi-info">
						<h3>San Jose</h3>
						<p>72 Properties</p>
					</div>
				</a>
				<a href="#" class="gallery-item set-bg" data-setbg="<?= base_url('assets/img/gallery/4.jpg')?>">
					<div class="gi-info">
						<h3>St Louis</h3>
						<p>50 Properties</p>
					</div>
				</a>
				
			</div>
		</div>
	</section> -->
	<!-- Gallery section end -->



	<!-- Review section -->
	<!-- <section class="review-section set-bg" data-setbg="<?= base_url('assets/img/review-bg.jpg')?>">
		<div class="container">
			<div class="review-slider owl-carousel">
				<div class="review-item text-white">
					<div class="rating">
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
					</div>
					<p>“Leramiz was quick to understand my needs and steer me in the right direction. Their professionalism and warmth made the process of finding a suitable home a lot less stressful than it could have been. Thanks, agent Tony Holland.”</p>
					<h5>Stacy Mc Neeley</h5>
					<span>CEP’s Director</span>
					<div class="clint-pic set-bg" data-setbg="<?= base_url('assets/img/review/1.jpg')?>"></div>
				</div>
				<div class="review-item text-white">
					<div class="rating">
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
					</div>
					<p>“Leramiz was quick to understand my needs and steer me in the right direction. Their professionalism and warmth made the process of finding a suitable home a lot less stressful than it could have been. Thanks, agent Tony Holland.”</p>
					<h5>Stacy Mc Neeley</h5>
					<span>CEP’s Director</span>
					<div class="clint-pic set-bg" data-setbg="<?= base_url('assets/img/review/1.jpg')?>"></div>
				</div>
				<div class="review-item text-white">
					<div class="rating">
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
					</div>
					<p>“Leramiz was quick to understand my needs and steer me in the right direction. Their professionalism and warmth made the process of finding a suitable home a lot less stressful than it could have been. Thanks, agent Tony Holland.”</p>
					<h5>Stacy Mc Neeley</h5>
					<span>CEP’s Director</span>
					<div class="clint-pic set-bg" data-setbg="<?= base_url('assets/img/review/1.jpg')?>"></div>
				</div>
			</div>
		</div>
	</section> -->
	<!-- Review section end-->


	<!-- Blog section -->
	<section class="blog-section spad">
		<div class="container">
			<div class="section-title text-center">
				<h3>LATEST NEWS</h3>
				<p>Real estate news headlines around the world.</p>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 blog-item">
					<img src="<?= base_url('assets/img/blog/1.jpg')?>" alt="">
					<h5><a href="single-blog.html">Housing confidence hits record high as prices skyrocket</a></h5>
					<div class="blog-meta">
						<span><i class="fa fa-user"></i>Amanda Seyfried</span>
						<span><i class="fa fa-clock-o"></i>25 Jun 201</span>
					</div>
					<p>Integer luctus diam ac scerisque consectetur. Vimus dotnetact euismod lacus sit amet. Aenean interdus mid vitae maximus...</p>
				</div>
				<div class="col-lg-4 col-md-6 blog-item">
					<img src="<?= base_url('assets/img/blog/2.jpg')?>" alt="">
					<h5><a href="single-blog.html">Taylor Swift is selling her $2.95 million Beverly Hills mansion</a></h5>
					<div class="blog-meta">
						<span><i class="fa fa-user"></i>Amanda Seyfried</span>
						<span><i class="fa fa-clock-o"></i>25 Jun 201</span>
					</div>
					<p>Integer luctus diam ac scerisque consectetur. Vimus dotnetact euismod lacus sit amet. Aenean interdus mid vitae maximus...</p>
				</div>
				<div class="col-lg-4 col-md-6 blog-item">
					<img src="<?= base_url('assets/img/blog/3.jpg')?>" alt="">
					<h5><a href="single-blog.html">NYC luxury housing market saturated with inventory, says celebrity realtor</a></h5>
					<div class="blog-meta">
						<span><i class="fa fa-user"></i>Amanda Seyfried</span>
						<span><i class="fa fa-clock-o"></i>25 Jun 201</span>
					</div>
					<p>Integer luctus diam ac scerisque consectetur. Vimus dotnetact euismod lacus sit amet. Aenean interdus mid vitae maximus...</p>
				</div>
			</div>
		</div>
	</section>
	<!-- Blog section end -->

	<?php 
		$this->load->view('page-part/common-foot', $pageData);
	?>

</body>
</html>