<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->

<!-- Modal -->
<div class="modal fade" id="daftar-proyek-filter-modal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Filter Daftar Proyek</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="">
						<div class="">
							<div class="row" style="z-index:9999;">
								<div class="col-lg-6 col-md-12 col-12" style="">
									<h6>Lokasi/Area</h6>
									<input id="daftar-proyek-filter-txt-searchfield-location" onkeyup="changeDropSearch();" onfocusout="$(this).parent().find('.dropdown-container').fadeOut();" onfocus="$(this).parent().find('.dropdown-container').fadeIn();" data-hidden='' type="text" style="" placeholder="Kota, Daerah, Alamat, Provinsi ... " class="w-100 mt-2" autocomplete="off">
									<div class="col-12 dropdown-container" style="position:absolute; left:0px; width:100%;  z-index:999; display:none;">
										<div class="dropdown-inner-container bg-white shadow" style="minimalistScrollbar max-height:60vh; overflow-y:auto;">
											<div class="dropdown-separator"><i class="fa fa-clock mr-2"></i> Pencarian Tersimpan</div>
											<div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" data-parent="#daftar-proyek-filter-txt-searchfield-location" onclick="insertDatatoParent(this);">
												<div>
													<div class="data-show">Kenjeran</div>
													<div class="data-hidden label">Surabaya, Jawa Timur</div>
												</div>
												<div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
											</div>
											<div class="dropdown-separator"><i class="fas mr-2 fa-map-marker-alt"></i> Wilayah</div>
											<div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" data-parent="#daftar-proyek-filter-txt-searchfield-location" onclick="insertDatatoParent(this);">
												<div>
													<div class="data-show">Kenjeran</div>
													<div class="data-hidden label">Surabaya, Jawa Timur</div>
												</div>
												<div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
											</div>
											<div class="dropdown-separator"><i class="fas mr-2 fa-user-tie"></i> Agen</div>
											<div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" data-parent="#daftar-proyek-filter-txt-searchfield-location" onclick="insertDatatoParent(this);">
												<div>
													<div class="data-show">Kenjeran</div>
													<div class="data-hidden label">Surabaya, Jawa Timur</div>
												</div>
												<div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
											</div>
											<div class="dropdown-separator"><i class="fas mr-2 fa-building"></i> Proyek</div>
											<div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" data-parent="#daftar-proyek-filter-txt-searchfield-location" onclick="insertDatatoParent(this);">
												<div>
													<div class="data-show">Kenjeran</div>
													<div class="data-hidden label">Surabaya, Jawa Timur</div>
												</div>
												<div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-12" style="">
									<h6>Judul Proyek</h6>
									<input id="daftar-proyek-filter-txt-searchfield-title" onkeyup="changeDropSearch();" onfocusout="$(this).parent().find('.dropdown-container').fadeOut();" onfocus="$(this).parent().find('.dropdown-container').fadeIn();" data-hidden='' type="text" style="" placeholder="Proyek Era " class="w-100 mt-2" autocomplete="off">
									<div class="col-12 dropdown-container" style="position:absolute; left:0px; width:100%;  z-index:999; display:none;">
										<div class="dropdown-inner-container bg-white shadow" style="minimalistScrollbar max-height:60vh; overflow-y:auto;">
											<div class="dropdown-separator"><i class="fa fa-clock mr-2"></i> Pencarian Tersimpan</div>
											<div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" data-parent="#daftar-proyek-filter-txt-searchfield-title" onclick="insertDatatoParent(this);">
												<div>
													<div class="data-show">Kenjeran</div>
													<div class="data-hidden label">Surabaya, Jawa Timur</div>
												</div>
												<div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
											</div>
											<div class="dropdown-separator"><i class="fas mr-2 fa-map-marker-alt"></i> Wilayah</div>
											<div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" data-parent="#daftar-proyek-filter-txt-searchfield-title" onclick="insertDatatoParent(this);">
												<div>
													<div class="data-show">Kenjeran</div>
													<div class="data-hidden label">Surabaya, Jawa Timur</div>
												</div>
												<div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
											</div>
											<div class="dropdown-separator"><i class="fas mr-2 fa-user-tie"></i> Agen</div>
											<div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" data-parent="#daftar-proyek-filter-txt-searchfield-title" onclick="insertDatatoParent(this);">
												<div>
													<div class="data-show">Kenjeran</div>
													<div class="data-hidden label">Surabaya, Jawa Timur</div>
												</div>
												<div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
											</div>
											<div class="dropdown-separator"><i class="fas mr-2 fa-building"></i> Proyek</div>
											<div class="dropdown-item flex flex-hor flex-separate flex-vertical-center" data-parent="#daftar-proyek-filter-txt-searchfield-title" onclick="insertDatatoParent(this);">
												<div>
													<div class="data-show">Kenjeran</div>
													<div class="data-hidden label">Surabaya, Jawa Timur</div>
												</div>
												<div class="badge badge-pill px-3 py-2" style="border: 1px solid rgba(0, 0, 0, 0.103);">Jalan</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-ver mt-2" style="z-index:1;">
								<div class="flex flex-hor flex-equal">
									<div class="flex flex-ver margin-col-xlg">
										<h6>Harga Min</h6>
										<input class="daftar-proyek-filter-priceInput min money" step:1000000 type="Number" onchange="cekPrice(this);" id="daftar-proyek-filter-minPrice">
										<small class="errPrice" class="text-danger" style="color:red;"></small> 
									</div>
									<div class="flex flex-ver">
										<h6>Harga Max</h6>
										<input class="daftar-proyek-filter-priceInput max money" step:1000000 type="Number" onchange="cekPrice(this);" id="daftar-proyek-filter-maxPrice">
										<small class="errPrice" class="text-danger" style="color:red;"></small> 
									</div>
								</div>        
								<div class="m-ion-range-slider">
									<input type="hidden" id="daftar-proyek-filter-priceSlider" />
								</div>
							</div>
							<div class="row mt-2" style="z-index:1;">
								<div class="col-lg-6 col-md-12 col-12 mb-2">
									<div class="flex flex-ver">
										<h6>Kamar Tidur</h6>
										<div class="flex flex-hor flex-equal selectrange">
											<select onchange="//cekKT(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="daftar-proyek-filter-minKT">
												<option value='-1'>No Min</option>    
												<option value='1'>1</option>
												<option value='2'>2</option>
												<option value='3'>3</option>
												<option value='4'>4</option>
												<option value='5'>5</option>
												<option value='6'>6</option>
												<option value='7'>7</option>
												<option value='8'>8</option>
												<option value='9'>9</option>
												<option value='10'>10+</option>
											</select>
											<select onchange="//cekKT(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="daftar-proyek-filter-maxKT">
												<option value='-1'>No Max</option>    
												<option value='1'>1</option>
												<option value='2'>2</option>
												<option value='3'>3</option>
												<option value='4'>4</option>
												<option value='5'>5</option>
												<option value='6'>6</option>
												<option value='7'>7</option>
												<option value='8'>8</option>
												<option value='9'>9</option>
												<option value='10'>10+</option>
											</select>
										</div>
										<small class="errKT" class="text-danger" style="color:red;"></small> 
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-12 mb-2">
									<div class="flex flex-ver ">
										<h6>Kamar Mandi</h6>
										<div class="flex flex-hor flex-equal selectrange">
											<select onchange="//cekKM(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="daftar-proyek-filter-minKM">
												<option value='-1'>No Min</option>    
												<option value='1'>1</option>
												<option value='2'>2</option>
												<option value='3'>3</option>
												<option value='4'>4</option>
												<option value='5'>5</option>
												<option value='6'>6</option>
												<option value='7'>7</option>
												<option value='8'>8</option>
												<option value='9'>9</option>
												<option value='10'>10+</option>
											</select>
											<select onchange="//cekKM(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="daftar-proyek-filter-maxKM">
												<option value='-1'>No Max</option>    
												<option value='1'>1</option>
												<option value='2'>2</option>
												<option value='3'>3</option>
												<option value='4'>4</option>
												<option value='5'>5</option>
												<option value='6'>6</option>
												<option value='7'>7</option>
												<option value='8'>8</option>
												<option value='9'>9</option>
												<option value='10'>10+</option>
											</select>
										</div>        
										<small class="errKM" class="text-danger" style="color:red;"></small>         
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-12 mb-2">
									<div class="flex flex-ver ">
										<h6>Luas Tanah</h6>
										<div class="flex flex-hor flex-equal selectrange">
											<select onchange="//cekLT(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="daftar-proyek-filter-minLT">
												<option value='-1'>No Min</option>    
												<option value='25'>25</option>
												<option value='50'>50</option>
												<option value='75'>75</option>
												<option value='500'>500</option>
												<option value='750'>750</option>
												<option value='1000'>1000</option>
												<option value='1250'>1250</option>
												<option value='1500'>1500</option>
												<option value='2000'>2000</option>
												<option value='4000'>4000</option>
												<option value='6000'>6000</option>
												<option value='8000'>8000</option>
												<option value='10000'>10000</option>
												<option value='20000'>20000</option>
												<option value='30000'>30000</option>
												<option value='40000'>40000</option>
												<option value='50000'>50000</option>
											</select>
											<select onchange="//cekLT(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="daftar-proyek-filter-maxLT">
												<option value='-1'>No Max</option>    
												<option value='25'>25</option>
												<option value='50'>50</option>
												<option value='75'>75</option>
												<option value='500'>500</option>
												<option value='750'>750</option>
												<option value='1000'>1000</option>
												<option value='1250'>1250</option>
												<option value='1500'>1500</option>
												<option value='2000'>2000</option>
												<option value='4000'>4000</option>
												<option value='6000'>6000</option>
												<option value='8000'>8000</option>
												<option value='10000'>10000</option>
												<option value='20000'>20000</option>
												<option value='30000'>30000</option>
												<option value='40000'>40000</option>
												<option value='50000'>50000</option>
											</select>
										</div>
										<small class="errLT" class="text-danger" style="color:red;"></small> 
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-12 mb-2">
									<div class="flex flex-ver ">
										<h6>Luas Bangunan</h6>
										<div class="flex flex-hor flex-equal selectrange">
											<select onchange="cekLB(this);" class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" id="daftar-proyek-filter-minLB">
												<option value='-1'>No Min</option>    
												<option value='25'>25</option>
												<option value='50'>50</option>
												<option value='75'>75</option>
												<option value='500'>500</option>
												<option value='750'>750</option>
												<option value='1000'>1000</option>
												<option value='1250'>1250</option>
												<option value='1500'>1500</option>
												<option value='2000'>2000</option>
												<option value='4000'>4000</option>
												<option value='6000'>6000</option>
												<option value='8000'>8000</option>
												<option value='10000'>10000</option>
												<option value='20000'>20000</option>
												<option value='30000'>30000</option>
												<option value='40000'>40000</option>
												<option value='50000'>50000</option>
											</select>
											<select onchange="cekLB(this);" class="form-control m-bootstrap-select m_selectpicker end" data-live-search="true" id="daftar-proyek-filter-maxLB">
												<option value='-1'>No Max</option>    
												<option value='25'>25</option>
												<option value='50'>50</option>
												<option value='75'>75</option>
												<option value='500'>500</option>
												<option value='750'>750</option>
												<option value='1000'>1000</option>
												<option value='1250'>1250</option>
												<option value='1500'>1500</option>
												<option value='2000'>2000</option>
												<option value='4000'>4000</option>
												<option value='6000'>6000</option>
												<option value='8000'>8000</option>
												<option value='10000'>10000</option>
												<option value='20000'>20000</option>
												<option value='30000'>30000</option>
												<option value='40000'>40000</option>
												<option value='50000'>50000</option>
											</select>
										</div>    
										<small class="errLB" class="text-danger" style="color:red;"></small>             
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer py-1 px-5">
				<div class="flex flex-hor w-100">
					<div class="flex flex-hor w-100 flex-separate adv-search-item flex-vertical-center">
						<!-- <div class="btn btn-reset bg-red txt-white">Reset</div> -->
						
						<a href="javascript:void(0)" style="font-weight:bold; cursor: pointer; font-size:1.25em;" onclick="resetFilter();">Reset</a>
						<div class="flex flex-hor-reverse flex-centered flex-vertical-center">
							<div class="btn txt-white bg-red" onclick="" data-dismiss="modal">Search</div>
							<div class="btn txt-white bg-red mr-3" onclick="" data-dismiss="modal">Save & Search</div>
							<div class="btn btn-info mr-3"  data-dismiss="modal">Cancel</div>
							<div class="label mr-3 " id="matchProyek" style="font-size:1em;"> XX Proyek </div>
						</div>
					</div>
				</div>
				<!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save</button> -->
			</div>
		</div>
	</div>
</div>

	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->


	<!-- Breadcrumb -->
	<!-- <div class="site-breadcrumb" style="margin-top: 50px;">
		<div class="container">
			<a href="/"><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i>Primary Market</span>
		</div>
	</div> -->
	<section class="my-container flex flex-ver" style="margin-top:100px;">
		<div class="container" style="margin:0px;width:100%; max-width:120000px; padding:0px">
			<div class="row w-100 m-0" style="width:100%;">
                <!--Map Section -->
                <div class="col-md-12 single-list-page hide-on-small-only" id="mapDiv" style="height:50vh; z-index:1;"></div>
                <!--End of map Section -->
            </div>
			<div class="listing-part-header-left d-flex justify-content-between w-100 row m-0 mt-2">
				<div class="d-flex align-items-center">
					<div class="listing-part-header-title">
						<h5>Menampilkan <span id="showedListing">0</span> dari <span id="totalListing">0</span> Primary Market</h5>
					</div>			
					<div class="listing-part-header-order" style="position:relative">
						<div onclick="$(this).parent().find('.sorter-tools').fadeToggle();" style="cursor:pointer;" class="hoverable">
							<i class="fa fa-sort" aria-hidden="true"></i>
							Urutkan
						</div>
						<div class="card flex flex-ver shadow sorter-tools" style="display:none; position:absolute; background-color:white; z-index:10;width:250%; transform:translateX(-50%);    padding-top: 15px;clip-path: polygon(0px 15px, 40% 15px, 50% 0px, 60% 15px, 100% 15px, 100% 100%, 0px 100%);">
							<div class="flex flex-hor flex-equal" style="" id="urutan">
								<div active-group="sortir-alphabet" onclick="urutkanProyekAZ('asc');" class="txt-center activeable-solo hoverable txt-bold animated active" data-value="asc">A-Z</div>
								<div active-group="sortir-alphabet" onclick="urutkanProyekAZ('desc');" class="txt-center activeable-solo hoverable txt-bold animated" data-value="desc">Z-A</div>
							</div>
							<div class="flex-ver pl-3">
								<!-- <div class="hoverable" onclick="sort('');">Recommended</div> -->
								<div class="hoverable" data-value="startlisting" id="sort-startlisting" onclick="urutkanProyek(this);">Terbaru <i class="fa fa-check"></i></div>
								<div class="hoverable" data-value="harga" id="sort-harga" onclick="urutkanProyek(this);">Harga</div>
								<div class="hoverable" data-value="kamartidur" id="sort-kamartidur" onclick="urutkanProyek(this);">Kamar Tidur</div>
								<div class="hoverable" data-value="kamarmandi" id="sort-kamarmandi" onclick="urutkanProyek(this);">Kamar Mandi</div>
								<div class="hoverable" data-value="luastanah" id="sort-luastanah" onclick="urutkanProyek(this);">Luas Tanah</div>
								<div class="hoverable" data-value="luasbangunan" id="sort-luasbangunan" onclick="urutkanProyek(this);">Luas Bangunan</div>
							</div>
						</div>
					</div>
				</div>
				<div style="position:relative" onclick="$(this).parent().find('.filter-tools').fadeToggle();" style="cursor:pointer;" class="hoverable">
					<div data-toggle="modal" data-target="#daftar-proyek-filter-modal">
						<i class="fa fa-filter" aria-hidden="true"></i>
						Filter
					</div>
					<div class="card flex flex-ver shadow filter-tools" style="position:absolute; background-color:white; z-index:10;width:250%; transform:translateX(-50%);    padding-top: 15px;clip-path: polygon(0px 15px, 40% 15px, 50% 0px, 60% 15px, 100% 15px, 100% 100%, 0px 100%);">
						
					</div>
				</div>
			</div>
            
			<div style="flex-wrap:wrap;" class="flex flex-hor mt-2 row w-100 m-0" id="listProyeks">
                <!-- Load Proyek -->
			</div>
			
			<div class="sensecode-paginate" style="margin-top:20px;" id="proyek-pagination"></div>
        </div>
	</section>
	<!-- Page -->
	<section class="page-section">
		<div class="container">

			<div class="row">

				<!-- sidebar -->

			</div>
		</div>
	</section>
	<!-- Page end -->

	<?php 
		$this->load->view('page-part/common-foot');
	?>
	<script src="https://cdn.rawgit.com/hayeswise/Leaflet.PointInPolygon/v1.0.0/wise-leaflet-pip.js"></script>
	<script>
		
	$('#exampleModal').on('show.bs.modal', event => {
		var button = $(event.relatedTarget);
		var modal = $(this);
		// Use above variables to manipulate the DOM
		
	});
</script>
</body>

</html>
