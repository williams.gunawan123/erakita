<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->

	<div class="modal fade" id="m_modal_contact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document" style="transform: translateX(8px);">
			<div class="modal-content" style="background-color:#fff0; box-shadow:none;">
				<div class="modal-header" style="padding:10px 5px; border:none;">
					<h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="background-color:white;">
					<div style="font-size:1.4em; text-align:center;">Bagaimana kami bisa menghubungi anda?</div><br>
					<div class="margin-row-sm"><span>Pesan</span></div>
					<input class="margin-row-md" type="text" placeholder="Masukkan Pesan Anda Disini ..." style="width:100%">
					<div class="margin-row-sm"><span>Telepon</span></div>
					<input class="margin-row-lg" type="text" placeholder="Masukkan Nomor Anda Disini ..." style="width:100%">
					<a data-href="https://wa.me/" data-phone="6287853265186" href="" class="btn btn-danger bg-red" style="width:100%;">Kirim Pesan</a>
					
				</div>
				<!--div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div -->
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="m_modal_6"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document" style="max-width:97%; transform: translateX(8px);">
			<div class="modal-content" style="background-color:#fff0; box-shadow:none;">
				<div class="modal-header" style="padding:10px 5px; border:none;">
					<h5 class="modal-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="background-color:white; padding:0px;">
					
					<div id="mapListing" style="position: relative;overflow: hidden;width: 100%;height: 88vh;"></div>
					
					<div class="m-ion-range-slider map-slider" style="width:30%; position:absolute; left:5px; bottom: 5px; background-color: #00000075; border-radius: 5px; padding: 10px;">
							<input type="hidden" id="map-slider" />
					</div>
					<div id="rightMap">
						<div class="flex flex-ver" style="height:100%;">
							<div class="flex flex-hor minimalistScrollbar" style="max-height:25%; overflow-x:auto;padding:10px;">
								<div class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable"><span>Sekitarnya</span></div>
								<div class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable"><span>Keluarga</span></div>
								<div class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable"><span>GayaHidup</span></div>
								<div class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable"><span>Sport</span></div>
								<div class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable"><span>Entertainment</span></div>
							</div>
							<div class="flex flex-ver minimalistScrollbar" style="background-color:#e4e4e4; height:100%; padding:0px 20px; overflow-y:auto;">
								<div class="mapNearPlace">
									
									<h6 class="mapNearCategory">Sekolah <span>x</span></h6>
									<div class="flex-flex-ver mapNearItemContainer minimalistScrollbar" style="max-height:200px; overflow-y:auto;">
										<?php for ($i=0; $i < 10; $i++) { ?>
										
										
										<div class="flex flex-hor flex-vertical-center mapItem">
											<div class="mapItemName">Sekolah Menengah Atas Negeri 9 Surabaya</div>
											<div class="flex flex-ver">
												<div class="map-time">3 Mins</div>
												<div class="map-far">230 m</div>
											</div>
										</div>
									
									<?php } ?>
									</div>
								</div>
								
								<div class="mapNearPlace">
									
									<h6 class="mapNearCategory">Rumah Sakit <span>x</span></h6>
									<div class="flex-flex-ver mapNearItemContainer minimalistScrollbar" style="max-height:200px; overflow-y:auto;">
										<?php for ($i=0; $i < 10; $i++) { ?>
										
										
										<div class="flex flex-hor flex-vertical-center mapItem">
											<div class="mapItemName">Rumah Sakit ke Lima Puluh Enam Surabaya</div>
											<div class="flex flex-ver">
												<div class="map-time">3 Mins</div>
												<div class="map-far">230 m</div>
											</div>
										</div>
									
									<?php } ?>
									</div>
								</div>


								
								<div class="mapNearPlace">
									
									<h6 class="mapNearCategory">Gym <span>x</span></h6>
									<div class="flex-flex-ver mapNearItemContainer minimalistScrollbar" style="max-height:200px; overflow-y:auto;">
										<?php for ($i=0; $i < 10; $i++) { ?>
										
										
										<div class="flex flex-hor flex-vertical-center mapItem">
											<div class="mapItemName">Regency 21 Surabaya</div>
											<div class="flex flex-ver">
												<div class="map-time">3 Mins</div>
												<div class="map-far">230 m</div>
											</div>
										</div>
									
									<?php } ?>
									</div>
								</div>



							</div>
						</div>
					</div>
				</div>
				<!--div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div-->
			</div>
		</div>
	</div>
	
	<?php 
		$this->load->view("/page-part/header-saya");
	?>

	<!-- Breadcrumb -->
	<div class="site-breadcrumb" style="margin-top: 100px;">
		<div class="my-container">
			<a href=""><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i>Wishlist</span>
		</div>
	</div>

    
	<section class="my-container flex flex-ver">
        <div class="flex flex-hor flex-separate margin-row-md">
            <h2>Wishlist Saya</h2>
            <div class="txt-white clickable bg-red txt-bold padding-m rounded-m"><span style="">Membuat Wishlist</span></div>
        </div>
        <!-- <table class="my-table">
            <thead>
                <th>
                    Label Wishlist
                </th>
                <th>
                    Harga
                </th>
                <th>
                    Luas Tanah
                </th>
                <th>
                    Luas Bangunan
				</th>
				<th>
                    Kamar Tidur
				</th>
				<th>
                    Kamar Mandi
				</th>
				<th>
                    Dibuat Oleh
                </th>
                <th>
                    Aksi
                </th>
            </thead>
            <tbody id="listWishlist">
                    <tr>
                        <td>
                            <a href="#">Krian Gudang 1ha utk expansi</a>
                            <div class="label">Krian, Trosobo, Sidoarjo</div>
                            <div class="label">Gudang sewa, >10,000 m<sup>2</sup></div>
                        </td>
                        <td>
                            <select class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" name="minKT">
                                <option value='-1'>Instant</option>    
                                <option value='1'>Approve</option>
                            </select>
                        </td>
                        <td>
                            <select class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" name="minKT">
                                <option value='-1'>Harian</option>    
                                <option value='1'>Mingguan</option>
                                <option value='2'>Bulanan</option>
                                <option value='3'>Tahunan</option>
                            </select>
                        </td>
                        <td>Anda?</td>
                        <td><i class="txt-md fas fa-pencil-alt margin-col-md"></i> <i class="txt-md fas fa-trash-alt"></i></td>
                    </tr>              
            </tbody>
		</table> -->
		<?php 
			foreach ($dataWishlist->data as $data) {
				$row = "";
				$row .= "<div class='m-accordion m-accordion--bordered section-to-hide section-to-clean' id='m_accordion_5' role='tablist'>";
				$row .= "	<!--begin::Item Jika Jenis transaksi = Beli -->";
				$row .= "	<div class='m-accordion__item m-accordion__item--solid'>";
				$row .= "		<div class='m-accordion__item-head activeable active bg-red borderless' active-group='detail-group5' role='tab' id='m_accordion_5_item_1_head' data-toggle='collapse' href='#m_accordion_5_item_1_body' aria-expanded='    true'>";
				$row .= "			<span class='m-accordion__item-icon'><i class='fa  flaticon-alert-2'></i></span>";
				$row .= "			<span class='m-accordion__item-title'>Listing Baru Saja Dilihat</span>";
				$row .= "			<span class='m-accordion__item-mode'></span>";
				$row .= "		</div>";
				$row .= "		<div style='overflow-y:auto' class='m-accordion__item-body collapse show' id='m_accordion_5_item_1_body' class=' ' role='tabpanel' aria-labelledby='m_accordion_5_item_1_head' data-parent='#m_accordion_5'>";
				$row .= "			<div class='m-accordion__item-content row'>";

				foreach ($data->detail as $recent) { 
					if ($recent->jenis_transaksi == "0"){
						$jenis = "Jual";
						$bgJenis = "jual";
						$harga = $recent->harga_jual;
					}elseif ($recent->jenis_transaksi == "1"){
						$jenis = "Sewa";
						$bgJenis = "sewa";
						$harga = $recent->harga_sewa . "/thn";
					}else{
						$jenis = "Jual Sewa";
						$bgJenis = "jual_sewa";
						$harga = $recent->harga_jual . " - " . $recent->harga_sewa . "/thn";;
					}
					$gambar = [];
					// for ($z=1; $z <= $recent->gambar; $z++){
					// 	$gambar[] = "/assets/img/listing/".$recent->id_listing."/".$z.".jpg";
					// }
					$gambar = json_encode($gambar, JSON_OBJECT_AS_ARRAY);

					$fiturDalam = "";
					if ($recent->kamar_tidur != null) {
						$fiturDalam .= "<div class='listing-detail-pills flex-4 txt-center'><i class='fas fa-bed'></i><br>".$recent->kamar_tidur."</div>";
					}
					if ($recent->kamar_mandi != null){
						$fiturDalam .= "<div class='listing-detail-pills flex-4 txt-center'><i class='fas fa-bath'></i><br>".$recent->kamar_mandi."</div>";
					}
					if ($recent->luas_tanah != null){
						$fiturDalam .= "<div class='listing-detail-pills flex-4 txt-center'><i class='fas fa-square'></i><br>".$recent->luas_tanah."m<sup>2</sup></div>";
					}
					if ($recent->luas_bangunan != null){
						$fiturDalam .= "<div class='listing-detail-pills flex-4 txt-center'><i class='fas fa-building'></i><br>".$recent->luas_bangunan."m<sup>2</sup></div>";
					}
					$favorite = "";
					if ($recent->favorite=="1"){
						$favorite = "favorite";
					}
					// My name is "$name". I am printing some $foo->foo.
					// Now, I am printing some {$foo->bar[1]}.
					// This should print a capital 'A': \x41
					$row.= "";
					$row.= "<div class='col-lg-6 col-sm-12 mb-3 px-2'>";
					$row.= "	<div class=' mb-0 listing-item m-portlet m-portlet--rounded' data-id='".$recent->id_listing."' data-telp='".$recent->telp_broker."' data-judul='".$recent->judul_listing."' data-lat='".$recent->lat_listing."' data-lng='".$recent->long_listing."' data-tipe='".$recent->nama_tipe."' data-jenis='".$jenis."'>";
					$row.= "		<div class='m-portlet__head sensecode-slider initiate' data-index='0' data-files='".$gambar."'>";
					$row.= "			<div class='listing-slider-badge jenis-".$bgJenis."'>".$jenis."</div>";
					$row.= "			<div class='listing-slider-icon jenis-".$bgJenis."'><i class='fas fa-home'></i></div>";
					$row.= "			<div class='listing-slider-counter' style='z-index:2;'>1 of 3</div>";
					$row.= "		</div>";
					$row.= "		<div class='m-portlet__body' style='padding:0px;'>";
					$row.= "			<div class='col-lg-10 col-md-10 col-sm-12' style='padding: 10px 5px 10px 10px;'>";
					$row.= "				<div class='listing-price listing-info'>".$harga."</div>";
					$row.= "				<div class='listing-title listing-info truncate hoverable'><b>".$recent->judul_listing."</b></div>";
					$row.= "				<div class='listing-jalan listing-info truncate'>".$recent->alamat_listing."</div>";
					$row.= "				<div class='listing-kota listing-info truncate'>".$recent->nama_kecamatan.", ".$recent->nama_kota."</div>";
					$row.= "				<div class='listing-detail'>";
					$row.= "					".$fiturDalam."";
					$row.= "				</div>";
					$row.= "			</div>";
					$row.= "			<div class='col-lg-2 col-md-2 col-sm-12 listing-content-right' style='padding:10px 10px 10px 5px'>";
					$row.= "				<div class='listing-content-icon' style='margin:auto;'><i onclick='sendWA(this);' class='fab fa-whatsapp fa-lg' data-skin='white' data-toggle='m-popover' data-placement='top' data-content='Pesan WhatsApp'></i></div>";
					$row.= "				<div class='listing-content-icon' style='margin:auto;'><i onclick='favorite(this);' class='fas fa-heart fa-lg ".$favorite."' data-skin='white' data-toggle='m-popover' data-placement='top' data-content='Favorit'></i></div>";
					$row.= "				<div class='listing-content-icon' style='margin:auto;'><i onclick='shareLink(this);' class='fas fa-share fa-lg' data-skin='white' data-toggle='m-popover' data-placement='top' data-content='Bagikan'></i></div>";
					$row.= "				<a href='/detaillisting/view/".$recent->id_listing."' style='margin:auto;' class='listing-content-icon txt-black'><i class='fas fa-ellipsis-h fa-lg' data-skin='white' data-toggle='m-popover' data-placement='top' data-content='Tampilkan Lebih Banyak'></i></a>";
					$row.= "			</div>";
					$row.= "		</div>";
					$row.= "	</div>";
					$row.= "</div>";
				} 
				$row.="			</div>";
				$row.="		</div>";
				$row.="	</div>";
				$row.="	<!--end::Item-->";
				$row.="</div>";
				echo $row;
			} 
			?>
		

	</section>
	<!-- Page -->
	<section class="page-section">
		<div class="container">
			
			<div class="row">
                
				<!-- sidebar -->
				
			</div>
		</div>
	</section>
	<!-- Page end -->

	<?php 
		$this->load->view('page-part/common-foot');
	?>

</body>
</html>