<!DOCTYPE html>
<html lang="en">

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="/assets/css/materialize.min.css" media="screen,projection" />

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EraKita | Register</title>
    <link rel="stylesheet" href="/assets/css/loginregister.css">
</head>

<body>
    <div class="background">
        <div class="layer"></div>
        <div class="form">
            <div class="form-content" style="padding:10px;">
                <h1 style="margin:10px; font-size:2em">Register</h1>
                <div class="row" style="margin-bottom:0px;">
                    <form class="col s12">
                            <div class="row" style="margin-bottom:0px">
                                <div class="input-field col s12" style="margin-top: 0px; margin-bottom: 0.4rem;">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input id="name" type="text" class="validate">
                                        <label for="name">Name</label>
                                        <span class="helper-text" data-error="Nama tidak sesuai" data-success="">Ex: Herman Budianto</span>
                                </div>
                            </div>
                        <div class="row" style="margin-bottom:0px">
                            <div class="input-field col s12" style="margin-top: 0px; margin-bottom: 0.4rem;">
                                    <i class="material-icons prefix">mail</i>
                                    <input id="email" type="email" class="validate">
                                    <label for="email">Email</label>
                                    <span class="helper-text" data-error="Email tidak sesuai" data-success="">Ex: XXX@#####.com 
                                        <!-- <span style=" float:right;color:red;">Sudah Terdaftar</span> -->
                                    </span>
                                    
                            </div>
                        </div>
                        <div class="row" style="margin-bottom:3px">
                            <div class="input-field col s12" style="margin-top: 0px; margin-bottom: 0.4rem;">
                                    <i class="material-icons prefix">home</i>
                                    <input id="address" type="text" class="validate">
                                    <label for="address">Address</label>
                                    <span class="helper-text" data-error="Alamat tidak sesuai" data-success="">Ex: Raya darmo,24 surabaya</span>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom:0px">
                            <div class="input-field col s12" style="margin-top: 0px; margin-bottom: 0.4rem;">
                                    <i class="material-icons prefix">phone</i>
                                    <input id="telephone" type="tel" class="validate" value="62">
                                    <label for="icon_telephone">Telephone</label>
                                    <span class="helper-text" data-error="Nomor telepon tidak sesuai" data-success="">Ex: +62 811 222 XXXXX
                                        <!-- <span style=" float:right;color:red;">Sudah Terdaftar</span> -->
                                    </span>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom:0px">
                            <div class="input-field col s12" style="margin-top: 0px; margin-bottom: 0.4rem;">
                                    <i class="material-icons prefix">vpn_key</i>
                                    <input id="password" type="password" class="validate">
                                    <label for="password">Password</label>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom:10px;">
                            <div class="input-field col s12" style="margin-top: 0px; margin-bottom: 0.4rem;">
                                    <i class="material-icons prefix">vpn_key</i>
                                    <input id="password" type="password" class="validate">
                                    <label for="password">Confirm Password</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <a class="waves-effect waves-light btn deep-orange darken-2" style="width:100%" id="btnRegister">Register</a>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom:10px">
                            <div class="col s12">
                                <a class="waves-effect waves-light btn deep-orange lighten-2" style="width:100%" href="/login">Login</a>
                            </div>
                        </div>

                    </form>
                </div>

            </div>

        </div>
    </div>
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="/assets/js/materialize.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/assets/js/main.js"></script>
</body>
</html>
<script>
    $(document).ready(function(){
        $("#btnRegister").click(function(){
            var sendData = { nama: $("#name").val(),
                email: $("#email").val(),
                alamat: $("#address").val(),
                telp: $("#telephone").val(),
                password: $("#password").val()
            }; 
            $.post({
                "url" : API_URL + "/api/post/customers/register?token=public",
                data: JSON.stringify(sendData),
                success : function(data){
                    data = JSON.parse(data);
                    if (data['key'] == 1){
                        window.location = "/home";
                    }
                }
            });
        });
    });
</script>