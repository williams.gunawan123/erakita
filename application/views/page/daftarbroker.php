<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->


    
	<section class="my-container flex flex-ver">		
		<!-- Breadcrumb -->
		<div class="site-breadcrumb pb-3" style="margin-top: 50px;">
			<div class="container">
				<a href="/"><i class="fa fa-home"></i>Home</a>
				<span><i class="fa fa-angle-right"></i>Broker</span>
			</div>
		</div>

        <div class="flex flex-hor " style="flex-wrap: wrap;">

            <div class="flex flex-hor flex-equal mb-3 w-100">
                <div class="ml-2 mr-2">
                    <label>Nama Broker</label>
                    <input type="text" id="srNamaBroker" class="form-control">
                </div>
                <div class="ml-2 mr-2">
                    <label>Cabang Broker</label>
					<!-- <input type="text" id="srCabangBroker" class="form-control"> -->
					<select data-live-search="true" id="srCabangBroker" class=" m-bootstrap-select m_selectpicker form-control" multiple></select>
                </div>
                <div class="ml-2 mr-2">
                    <label>Aksi</label>
                    <div class="flex flex-hor w-100 flex-equal" align="center">
                        <button class="btn w-100 bg-red txt-white" onclick="searchBroker();"><span class="fa fa-search">&nbsp;</span>Search</button>
                        <button class="btn w-100 btn-reset bg-blue txt-black clickable" onclick="reset();"><span class="fa fa-close">&nbsp;</span>Reset</button>
                    </div>
                </div>
			</div>
			<div class="listing-part-header-left">
				<div class="listing-part-header-title">
					<h5>Menampilkan <span id="showedBroker">0</span> dari <span id="totalBroker">0</span> Broker</h5>
				</div>			
				<div class="listing-part-header-order" style="position:relative">
					<div onclick="$(this).parent().find('.sorter-tools').fadeToggle();" style="cursor:pointer;" class="hoverable">
						<i class="fa fa-filter"></i>
						Urutkan
					</div>
					<div class="card flex flex-ver shadow sorter-tools" style="display:none; position:absolute; background-color:white; z-index:10;width:250%; transform:translateX(-50%);    padding-top: 15px;clip-path: polygon(0px 15px, 40% 15px, 50% 0px, 60% 15px, 100% 15px, 100% 100%, 0px 100%);">
						<div class="flex flex-hor flex-equal" style="" id="urutan">
							<div active-group="sortir-alphabet" onclick="urutkanBrokerAZ('asc');" class="txt-center activeable-solo hoverable txt-bold animated active" data-value="asc">A-Z</div>
							<div active-group="sortir-alphabet" onclick="urutkanBrokerAZ('desc');" class="txt-center activeable-solo hoverable txt-bold animated" data-value="desc">Z-A</div>
						</div>
						<div class="flex-ver pl-3">
							<!-- <div class="hoverable" onclick="sort('');">Recommended</div> -->
							<div class="hoverable" data-value="aktivitas" id="sort-aktivitas" onclick="urutkanBroker(this);">Aktivitas Terakhir <i class="fa fa-check"></i></div>
							<div class="hoverable" data-value="nama" id="sort-nama" onclick="urutkanBroker(this);">Nama</div>
						</div>
					</div>
				</div>
			</div>
            <div class="flex flex-hor w-100 row mx-0"  style="flex-wrap: wrap;" id="daftarBroker">
				<!-- Daftar Broker -->
				<div class="col-12 col-sm-12 col-md-6 p-2 col-lg-4">
					<div class="col-12 card">
						<div class="row">
							<div class="col-9 align-items-center txt-center d-flex flex-column align-self-center">
								<div class="hoverable" style="font-size:1.5em"><a href="/broker/detail/`+e.id_broker+`">` + e.nama_broker + `</a></div>
								<div class="hoverable label"><a href="/cabang/detail/`+e.id_cabang+`">` + e.nama_cabang + `</a></div>
								<div class="ratio11 square align-self-center circle col-6 p-0 div-image my-1" style="background-image:url('/assets/img/profile-broker/` + foto + `');"></div>
							</div>
							<div class="col-3 align-self-center">
								<button type="button" class="rounded-top btn col-12 btn-info"><span style="color: #FFFFFF; font-size: 1.3em;" class="fa fa-phone"></span></button>
								<button onclick="sendWA('` + e.nama_broker + `','` + e.telp_broker + `');" type="button" class="rounded-bottom btn col-12 btn-success"><span style="color: #FFFFFF; font-size: 1.3em;" class="fa fa-whatsapp"></span></button>
							</div>
						</div>
						<div class="col-12 px-0 flex-hor flex-wrap pb-4 pt-1 d-flex">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6 p-1">
								<div class="col-12 bg-red txt-white rounded py-2">
									<i class="fa fa-home fa-lg"></i>
									<div class="txt-bold">Mensharekan</div>
									<div style="font-size:0.9em;">`+e.mensharekan+` Properti</div>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6 p-1">
								<div class="col-12 bg-red txt-white rounded py-2">
									<i class="fa fa-home fa-lg"></i>
									<div class="txt-bold" >Mensharekan</div>
									<div style="font-size:0.9em;">`+e.mensharekan+` Properti</div>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6 p-1">
								<div class="col-12 bg-red txt-white rounded py-2">
									<i class="fa fa-home fa-lg"></i>
									<div class="txt-bold" >Mensharekan</div>
									<div style="font-size:0.9em;">`+e.mensharekan+` Properti</div>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6 p-1">
								<div class="col-12 bg-red txt-white rounded py-2">
									<i class="fa fa-home fa-lg"></i>
									<div class="txt-bold" >Mensharekan</div>
									<div style="font-size:0.9em;">`+e.mensharekan+` Properti</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				
				<div class="col-lg-4 col-sm-12 col-12 padding-s" style="margin-bottom:25px;">
                	<div style="background-color:gainsboro; height:250px; align-items:stretch;" >
                    	<div class="flex flex-ver" style="padding: 10px 0px; margin: auto;">
                        	<div class="flex flex-ver txt-bold  margin-col-md txt-center">
                            	<a class="txt-black hoverable" href="/broker/detail/`+e.id_broker+`"><span>` + e.nama_broker + `</span></a>
                            	<a class="txt-black hoverable" href="/cabang/detail/`+e.id_cabang+`"><span> ` + e.nama_cabang + `</span></a>
                        	</div>
                        	<div class="square" style="border-radius:50%; border: 2px solid white; background-image:url('/assets/img/profile-broker/` + foto + `');height:7vw; width:7vw; background-size:cover; background-repeat:no-repeat; background-position: center; margin:auto;">
                        	</div>
                        	<div class="flex flex-hor" style="flex-grow:1; position:relative;">
                            	<div class="flex flex-hor flex-separate padding-s flex-vertical-center" style=" font-size:1em; position:absolute; right: 5px; bottom:0px;">
                                	<div class="flex flex-hor flex-equal">
                                    	<div class="clickable padding-m txt-white" style="border-radius: 5px; margin: 2px; background-color: #2e7d32;"><span style="color: #FFFFFF; font-size: 1.3em;" class="fa fa-phone"><span></span></div>
                                    	<div onclick="sendWA('` + e.nama_broker + `','` + e.telp_broker + `');" class="clickable padding-m txt-white" style="border-radius: 5px; margin: 2px; background-color: #128C7E;"><span style="color: #FFFFFF; font-size: 1.3em;" class="fa fa-whatsapp"><span></span></div>
                                	</div>
                            	</div>
                            	<div class="flex flex-ver txt-bold padding-s flex-equal flex-1 txt-center">
                                	<div><span style="color: #2e7d32; font-size: 1.3em;" class="fa fa-phone"></span>&nbsp; <span style="color: #666F;" class=" margin-col-md">+` + e.telp_broker + `</span></div>
                                	<div><span style="color: #128C7E; font-size: 1.3em;" class="fa fa-whatsapp"></span>&nbsp; <span style="color: #666F;" class="margin-col-md">+` + e.telp_broker + `</span></div>
                            	</div>
                        	</div>
                    	</div>
                	</div>
                	<div style=" height:100px; margin-top:-1rem; margin-bottom:7rem; flex-wrap:wrap; width:98%; margin-left:auto; margin-right:auto;" class="flex flex-hor">
                    	<div class="flex-2 padding-s" style="">
                        	<div style="background-color:white; box-shadow:0px 0px 20px 1px #0000001c; height:100%; padding: 10px 0px;">
                            	<div class="flex flex-ver my-container">
                                	<div class="flex flex-hor txt-lg"> <i class="fa fa-home txt-white padding-s"></i><i class="fa fa-search txt-white padding-s"></i></div>
                                	<div class="txt-bold" >Menlistingkan</div>
                                	<div style="font-size:0.9em;">`+e.menlistingkan+` Properti</div>
                            	</div>
                        	</div>
                    	</div>
                    	<div class="flex-2 padding-s" style="">
                        	<div style="background-color:white; box-shadow:0px 0px 20px 1px #0000001c; height:100%; padding: 10px 0px;">
                            	<div class="flex flex-ver my-container">
                                	<div class="flex flex-hor txt-lg"> <i class="fa fa-home txt-white padding-s"></i><i class="fa fa-search txt-white padding-s"></i></div>
                                	<div class="txt-bold" >Mensharekan</div>
                                	<div style="font-size:0.9em;">`+e.mensharekan+` Properti</div>
                            	</div>
                        	</div>
                    	</div>
	
                    	<div class="flex-2 padding-s" style="">
                        	<div style="background-color:white; box-shadow:0px 0px 20px 1px #0000001c; height:100%; padding: 10px 0px;">
                            	<div class="flex flex-ver my-container">
                                	<div class="flex flex-hor txt-lg"> <i class="fa fa-home txt-white padding-s"></i><i class="fa fa-search txt-white padding-s"></i></div>
                                	<div class="txt-bold" >Men-Jual/Sewa-kan</div>
                                	<div style="font-size:0.9em;">24 Properti</div>
                            	</div>
                        	</div>
                    	</div>
	
                    	<div class="flex-2 padding-s" style="">
                        	<div style="background-color:white; box-shadow:0px 0px 20px 1px #0000001c; height:100%; padding: 10px 0px;">
                            	<div class="flex flex-ver my-container">
                                	<div class="flex flex-hor txt-lg" style=""> <i class="fa fa-home txt-white padding-s"></i><i class="fa fa-search txt-white padding-s"></i></div>
                                	<div class="txt-bold" >Transaksi CoBroke</div>
                                	<div style="font-size:0.9em;">24 Properti</div>
                            	</div>
                        	</div>
                    	</div>
	
                	</div>
            	</div>
			</div>
			
		</div>
		<div class="sensecode-paginate" style="margin-bottom: 20px;" id="broker-pagination"></div>
	</section>
	
	<?php 
		$this->load->view('page-part/common-foot');
	?>

</body>
</html>