<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view('page-part/common-head', $pageData);
?>
	<!-- Stylesheets -->
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css')?>"/>
	<link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css')?>"/>
	<link rel="stylesheet" href="<?= base_url('assets/css/animate.css')?>"/>
	<link rel="stylesheet" href="<?= base_url('assets/css/owl.carousel.css')?>"/>
	<link rel="stylesheet" href="<?= base_url('assets/css/header.css')?>"/>
	<link rel="stylesheet" href="<?= base_url('assets/css/listing-list.css')?>"/>
	<link rel="stylesheet" href="<?= base_url('assets/css/style.css')?>"/>

	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style>
	.lds-ellipsis {
		display: inline-block;
		position: relative;
		width: 64px;
		height: 64px;
	}
	.lds-ellipsis div {
		position: absolute;
		top: 27px;
		width: 11px;
		height: 11px;
		border-radius: 50%;
		background: #f44336;
		animation-timing-function: cubic-bezier(0, 1, 1, 0);
	}
	.lds-ellipsis div:nth-child(1) {
		left: 6px;
		animation: lds-ellipsis1 0.6s infinite;
	}
	.lds-ellipsis div:nth-child(2) {
		left: 6px;
		animation: lds-ellipsis2 0.6s infinite;
	}
	.lds-ellipsis div:nth-child(3) {
		left: 26px;
		animation: lds-ellipsis2 0.6s infinite;
	}
	.lds-ellipsis div:nth-child(4) {
		left: 45px;
		animation: lds-ellipsis3 0.6s infinite;
	}
	@keyframes lds-ellipsis1 {
		0% { transform: scale(0); }
		100% { transform: scale(1); }
	}
	@keyframes lds-ellipsis3 {
		0% { transform: scale(1); }
		100% { transform: scale(0); }
	}
	@keyframes lds-ellipsis2 {
		0% { transform: translate(0, 0); }
		100% { transform: translate(19px, 0); }
	}
	</style>
<body>	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>
	
	<!-- Header section -->
	<?php $this->load->view('page-part/header');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>" style="margin-bottom:20px">
		<div class="container text-white">
			<h2>Featured Listings</h2>
		</div>
	</section>
	<!--  Page top end -->

	<!-- 
	<div class="site-breadcrumb">
		<div class="container">
			<a href=""><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i>Listings</span>
		</div>
	</div>Breadcrumb -->


	<!-- page -->
	<section class="page-section categories-page">
		<div class="container">
			<div class="row">
				<div class="map-container col-lg-6 col-md-12" style="margin-bottom: 20px">
					<div class="pos-map" id="map-canvas" style="width:100%"></div>
				</div>
				<div class="listing-list-container col-lg-6 col-md-12">
					<div class="row filter-box filter-form" style="z-index:99;margin-bottom: 10px">
						asdasd
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<!-- feature -->
							<div class="feature-item">
								<div class="feature-pic set-bg" data-setbg="<?= base_url('assets/img/feature/1.jpg')?>">
									<div class="sale-notic">FOR SALE</div>
								</div>
								<div class="feature-text">
									<div class="text-center feature-title">
										<h5>1963 S Crescent Heights Blvd</h5>
										<p><i class="fa fa-map-marker"></i> Los Angeles, CA 90034</p>
									</div>
									<div class="room-info-warp">
										<div class="room-info">
											<div class="rf-left">
												<p><i class="fa fa-th-large"></i> 800</p>
												<p><i class="fa fa-bed"></i> 10</p>
											</div>
											<div class="rf-right">
												<p><i class="fa fa-car"></i> 2 </p>
												<p><i class="fa fa-bath"></i> 6 </p>
											</div>	
										</div>
										<div class="room-info">
											<div class="rf-left">
												<p><i class="fa fa-user"></i> Tony Holland</p>
											</div>
											<div class="rf-right">
												<p><i class="fa fa-clock-o"></i> 1 days ago</p>
											</div>	
										</div>
									</div>
									<a href="#" class="room-price">$1,200,000</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<!-- feature -->
							<div class="feature-item">
								<div class="feature-pic set-bg" data-setbg="<?= base_url('assets/img/feature/2.jpg')?>">
									<div class="sale-notic">FOR SALE</div>
								</div>
								<div class="feature-text">
									<div class="text-center feature-title">
										<h5>305 North Palm Drive</h5>
										<p><i class="fa fa-map-marker"></i> Beverly Hills, CA 90210</p>
									</div>
									<div class="room-info-warp">
										<div class="room-info">
											<div class="rf-left">
												<p><i class="fa fa-th-large"></i> 1500 Square foot</p>
												<p><i class="fa fa-bed"></i> 16 Bedrooms</p>
											</div>
											<div class="rf-right">
												<p><i class="fa fa-car"></i> 2 Garages</p>
												<p><i class="fa fa-bath"></i> 8 Bathrooms</p>
											</div>	
										</div>
										<div class="room-info">
											<div class="rf-left">
												<p><i class="fa fa-user"></i> Gina Wesley</p>
											</div>
											<div class="rf-right">
												<p><i class="fa fa-clock-o"></i> 1 days ago</p>
											</div>	
										</div>
									</div>
									<a href="#" class="room-price">$4,500,000</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<!-- feature -->
							<div class="feature-item">
								<div class="feature-pic set-bg" data-setbg="<?= base_url('assets/img/feature/3.jpg')?>">
									<div class="rent-notic">FOR Rent</div>
								</div>
								<div class="feature-text">
									<div class="text-center feature-title">
										<h5>305 North Palm Drive</h5>
										<p><i class="fa fa-map-marker"></i> Beverly Hills, CA 90210</p>
									</div>
									<div class="room-info-warp">
										<div class="room-info">
											<div class="rf-left">
												<p><i class="fa fa-th-large"></i> 1500 Square foot</p>
												<p><i class="fa fa-bed"></i> 16 Bedrooms</p>
											</div>
											<div class="rf-right">
												<p><i class="fa fa-car"></i> 2 Garages</p>
												<p><i class="fa fa-bath"></i> 8 Bathrooms</p>
											</div>	
										</div>
										<div class="room-info">
											<div class="rf-left">
												<p><i class="fa fa-user"></i> Gina Wesley</p>
											</div>
											<div class="rf-right">
												<p><i class="fa fa-clock-o"></i> 1 days ago</p>
											</div>	
										</div>
									</div>
									<a href="#" class="room-price">$2,500/month</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<!-- feature -->
							<div class="feature-item">
								<div class="feature-pic set-bg" data-setbg="<?= base_url('assets/img/feature/4.jpg')?>">
									<div class="sale-notic">FOR SALE</div>
								</div>
								<div class="feature-text">
									<div class="text-center feature-title">
										<h5>28 Quaker Ridge Road, Manhasset</h5>
										<p><i class="fa fa-map-marker"></i> 28 Quaker Ridge Road, Manhasset</p>
									</div>
									<div class="room-info-warp">
										<div class="room-info">
											<div class="rf-left">
												<p><i class="fa fa-th-large"></i> 1200 Square foot</p>
												<p><i class="fa fa-bed"></i> 12 Bedrooms</p>
											</div>
											<div class="rf-right">
												<p><i class="fa fa-car"></i> 3 Garages</p>
												<p><i class="fa fa-bath"></i> 8 Bathrooms</p>
											</div>	
										</div>
										<div class="room-info">
											<div class="rf-left">
												<p><i class="fa fa-user"></i> Sasha Gordon </p>
											</div>
											<div class="rf-right">
												<p><i class="fa fa-clock-o"></i> 8 days ago</p>
											</div>	
										</div>
									</div>
									<a href="#" class="room-price">$5,600,000</a>
								</div>
							</div>
						</div>
					</div>
					<div class="loading-container" style="display:flex">
						<div class="lds-ellipsis" style="margin:auto"><div></div><div></div><div></div><div></div></div>
					</div>
				</div>
			</div>		
		</div>
	</section>
	<!-- page end -->

<<<<<<< Updated upstream:application/views/page/categories.php

	<!-- Clients section -->
	<div class="clients-section">
		<div class="container">
			<div class="clients-slider owl-carousel">
				<a href="#">
					<img src="<?= base_url('assets/img/partner/1.png')?>" alt="">
				</a>
				<a href="#">
					<img src="<?= base_url('assets/img/partner/2.png')?>" alt="">
				</a>
				<a href="#">
					<img src="<?= base_url('assets/img/partner/3.png')?>" alt="">
				</a>
				<a href="#">
					<img src="<?= base_url('assets/img/partner/4.png')?>" alt="">
				</a>
				<a href="#">
					<img src="<?= base_url('assets/img/partner/5.png')?>" alt="">
				</a>
			</div>
		</div>
	</div>
	<!-- Clients section end -->

	<?php 
		$this->load->view('page-part/common-foot');
	?>

=======
	<!-- Footer section -->
	<?php $this->load->view('footer'); ?>
	<!-- Footer end -->
                                        
	<!--====== Javascripts & Jquery ======-->
	<script src="<?= base_url('assets/js/jquery-3.2.1.min.js')?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?= base_url('assets/js/owl.carousel.min.js')?>"></script>
	<script src="<?= base_url('assets/js/masonry.pkgd.min.js')?>"></script>
	<script src="<?= base_url('assets/js/magnific-popup.min.js')?>"></script>
	<script src="<?= base_url('assets/js/main.js')?>"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0YyDTa0qqOjIerob2VTIwo_XVMhrruxo"></script>
	<script src="<?= base_url('assets/js/map-2.js')?>"></script>
>>>>>>> Stashed changes:application/views/categories/index.php
</body>
<script>
	$(document).ready(function(){
		if($(document).width() >= 992){
			$(".pos-map").css("height",($(window).height() - 40) + "px");
		}else $(".pos-map").css("height", $(document).width() + "px");

		$(window).scroll(function(){
			let footerHeight = $("footer").height();
			let client = $(".clients-section").height();
			let scrollTop = $(window).scrollTop();
			let scrollHeight = (($(window).innerHeight() / $(document).height()) * 100) / 100 * $(document).height();
			if($(document).width() >= 992){
				if((scrollTop + scrollHeight) > ($(document).height() - client - footerHeight - 150)){
					let top = ($(document).height() - client - footerHeight - 500) - $(".pos-map").height();
					$(".pos-map").css("top", top + "px");
					$(".pos-map").css("position","relative");
					$(".filter-box").css("top", top + "px");
					$(".filter-box").css("position","relative");
				}else if(scrollTop >= 350){
					$(".pos-map").css("width", $(".pos-map").width() + "px");
					$(".pos-map").css("top", "20px");
					$(".pos-map").css("position","fixed");
					$(".filter-box").css("width", ($(".filter-box").width() + 62) + "px");
					$(".filter-box").css("top", "20px");
					$(".filter-box").css("position","fixed");
				}else if(scrollTop < 350){
					$(".pos-map").css("top", "0px");
					$(".pos-map").css("position","relative");
					$(".filter-box").css("top", "0px");
					$(".filter-box").css("position","relative");
				}
			}
		});
	});
</script>
</html>