<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->


    <!-- Breadcrumb -->
    <div class="site-breadcrumb" style="margin-top: 50px;">
		<div class="container">
            <a href="/"><i class="fa fa-home"></i>Home&nbsp;</a>
			<a href="/cabang"><i class="fa fa-angle-right"></i>Cabang</a>
			<span><i class="fa fa-angle-right"></i><?=$dataCabang->nama_cabang?></span>
		</div>
	</div>
	<section class="my-container flex flex-ver">
        <div class="flex flex-ver">
            <div style="background-color:gainsboro; height:250px; align-items:stretch; padding-top:20px;" >
                <div class="flex flex-hor" style="padding: 0px 5%; margin: auto;">
                    <div class="square" style="border-radius:50%; border: 2px solid white; background-image:url('/assets/img/erakita-logo-square.png');height:10vw; width:10vw; background-size:contain; background-repeat:no-repeat; background-position: center;">
                        
                    </div>
                    <div class="flex flex-ver" style="flex-grow:1; margin-top:30px; margin-left:20px;">
                        <div class="flex flex-hor flex-separate padding-s flex-vertical-center" style="font-size:1.5em;">
                            <div class="flex flex-hor">
                            <div class="flex flex-ver txt-bold  margin-col-md" style="margin:auto;">
                                    <span><?=$dataCabang->nama_cabang?></span>
                                </div>
                                <div class="flex txt-white" style="background-image:url('/assets/img/erakita-logo-square.png'); background-size:contain; background-repeat:no-repeat; height:50px; width:50px; margin-left:30px;"></div>
                            </div>
                            <div class="flex flex-hor flex-equal">
                                <div class="clickable padding-m txt-white" style="border-radius: 5px; margin: 2px; background-color: #c41c00;"><span class="fa fa-envelope" style="color: #FFFFFF; font-size: 1.3em;"><span></span></div>
                                <div class="clickable padding-m txt-white" style="border-radius: 5px; margin: 2px; background-color: #2e7d32;"><span class="fa fa-phone" style="color: #FFFFFF; font-size: 1.3em;"><span></span></div>
                                <div class="clickable padding-m txt-white" style="border-radius: 5px; margin: 2px; background-color: #128C7E;"><span class="fa fa-whatsapp" style="color: #FFFFFF; font-size: 1.3em;"><span></span></div>
                            </div>
                        </div>
                        <table style="margin-left: 7px;">
                            <tr>
                                <td><span style="color: #c41c00; font-size: 1.3em;" class="fa fa-envelope"></span></td>
                                <td>:</td>
                                <td><span style="color: #666F;" class="margin-col-md"><?=$dataCabang->alamat_cabang?></span></td>
                            </tr>
                            <tr>
                                <td><span style="color: #2e7d32; font-size: 1.3em;" class="fa fa-phone"></span></td>
                                <td>:</td>
                                <td><span style="color: #666F;" class=" margin-col-md">+<?=$dataCabang->telp_cabang?></span></td>
                            </tr>
                            <tr>
                                <td><span style="color: #128C7E; font-size: 1.3em;" class="fa fa-whatsapp"></span></td>
                                <td>:</td>
                                <td><span style="color: #666F;" class="margin-col-md">+<?=$dataCabang->telp_cabang?></span></td>
                            </tr>
                        </table>
                    </div>
                    
                </div>

            </div>
            <div style=" height:100px; margin-top:-2rem; margin-bottom:7rem; flex-wrap:wrap;" class="my-container flex flex-hor">
                <div class="flex-5 padding-s" style="">
                    <div style="box-shadow:0px 0px 20px 1px #0000001c; padding: 10px 0px;" class="bg-red clickable activeable-solo" active-group="click-group">
                        <div style="text-align:center; font-weight:bold; font-size: 1.5em;">Kota Spesialis</div>
                    </div>
                </div>
                
                <div class="flex-5 padding-s" style="">
                    <div style="box-shadow:0px 0px 20px 1px #0000001c; padding: 10px 0px;" class="bg-red clickable activeable-solo" active-group="click-group">
                        <div style="text-align:center; font-weight:bold; font-size: 1.5em;">Broker Agen</div>
                    </div>
                </div>
                
                <div class="flex-5 padding-s" style="">
                    <div style="box-shadow:0px 0px 20px 1px #0000001c; padding: 10px 0px;" class="bg-red clickable activeable-solo" active-group="click-group">
                        <div style="text-align:center; font-weight:bold; font-size: 1.5em;">Listing</div>
                    </div>
                </div>
                
                <div class="flex-5 padding-s" style="">
                    <div style="box-shadow:0px 0px 20px 1px #0000001c; padding: 10px 0px;" class="bg-red clickable activeable-solo" active-group="click-group">
                        <div style="text-align:center; font-weight:bold; font-size: 1.5em;">Transaksi</div>
                    </div>
                </div>
                
                <div class="flex-5 padding-s" style="">
                    <div style="box-shadow:0px 0px 20px 1px #0000001c; padding: 10px 0px;" class="bg-red clickable activeable-solo" active-group="click-group">
                        <div style="text-align:center; font-weight:bold; font-size: 1.5em;">Aksi Customer</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 flex flex-ver">
                    <div style="font-weight: bold; font-size: 1.5em;">Listing Spesialis</div>
                    <div>Top 3 kota dengan listing terbanyak</div>
                    <div class="row align-items-center">
                        <div class="col">
                            <div id="chart_listing_spesialis" class="m-widget14__chart" style="height: 160px">
                                <div class="m-widget14__stat" style="font-size: 2.4rem; font-weight: 600; position: absolute; top: 50%; left: 50%; -webkit-transform: translate(-50%, -50%); transform: translate(-50%, -50%);">45</div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="m-widget14__legends">
                                <div class="m-widget14__legend">
                                    <span class="m-widget14__legend-bullet m--bg-accent"></span>
                                    <span class="m-widget14__legend-text">37% Sport Tickets</span>
                                </div>
                                <div class="m-widget14__legend">
                                    <span class="m-widget14__legend-bullet m--bg-warning"></span>
                                    <span class="m-widget14__legend-text">47% Business Events</span>
                                </div>
                                <div class="m-widget14__legend">
                                    <span class="m-widget14__legend-bullet m--bg-brand"></span>
                                    <span class="m-widget14__legend-text">19% Others</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="font-weight: bold; font-size: 1.5em; text-align: right;">Transaksi Baru</div>
                    <div style="text-align: right;">Top 3 kota dengan transaksi baru terbanyak</div>
                    <div class="row align-items-center">
                        <div class="col">
                            <div id="chart_transaksi_baru" class="m-widget14__chart" style="height: 160px">
                                <div class="m-widget14__stat" style="font-size: 2.4rem; font-weight: 600; position: absolute; top: 50%; left: 50%; -webkit-transform: translate(-50%, -50%); transform: translate(-50%, -50%);">45</div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="m-widget14__legends">
                                <div class="m-widget14__legend">
                                    <span class="m-widget14__legend-bullet m--bg-accent"></span>
                                    <span class="m-widget14__legend-text">37% Sport Tickets</span>
                                </div>
                                <div class="m-widget14__legend">
                                    <span class="m-widget14__legend-bullet m--bg-warning"></span>
                                    <span class="m-widget14__legend-text">47% Business Events</span>
                                </div>
                                <div class="m-widget14__legend">
                                    <span class="m-widget14__legend-bullet m--bg-brand"></span>
                                    <span class="m-widget14__legend-text">19% Others</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 flex flex-ver">
                    <div style="font-weight: bold; font-size: 1.5em;">Harga Jual (Listing)</div>
                    <table style="text-align: center; width: 100%; margin: 10px 0px;">
                        <tr style="color: #888;">
                            <td width="50%">Minimum</td>
                            <td width="50%">Maximum</td>
                        </tr>
                        <tr style="font-weight: bold; font-size: 2.5em;">
                            <td width="50%">700jt</td>
                            <td width="50%">7M</td>
                        </tr>
                    </table>
                    <div style="font-weight: bold; font-size: 1.5em;">Harga Sewa (Listing)</div>
                    <table style="text-align: center; width: 100%; margin: 10px 0px;">
                        <tr style="color: #888;">
                            <td width="50%">Minimum</td>
                            <td width="50%">Maximum</td>
                        </tr>
                        <tr style="font-weight: bold; font-size: 2.5em;">
                            <td width="50%">25jt</td>
                            <td width="50%">275jt</td>
                        </tr>
                    </table>
                    <div style="font-weight: bold; font-size: 1.5em;">Harga Jual (Transaksi Baru)</div>
                    <table style="text-align: center; width: 100%; margin: 10px 0px;">
                        <tr style="color: #888;">
                            <td width="50%">Minimum</td>
                            <td width="50%">Maximum</td>
                        </tr>
                        <tr style="font-weight: bold; font-size: 2.5em;">
                            <td width="50%">700jt</td>
                            <td width="50%">7M</td>
                        </tr>
                    </table>
                    <div style="font-weight: bold; font-size: 1.5em;">Harga Sewa (Transaksi Baru)</div>
                    <table style="text-align: center; width: 100%; margin: 10px 0px;">
                        <tr style="color: #888;">
                            <td width="50%">Minimum</td>
                            <td width="50%">Maximum</td>
                        </tr>
                        <tr style="font-weight: bold; font-size: 2.5em;">
                            <td width="50%">25jt</td>
                            <td width="50%">275jt</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-4">
                    <div style="font-weight: bold; font-size: 1.8em;">Broker Agen (<span></span>)</div>
                    <div class="flex flex-hor flex-equal mt-2" style="font-weight: bold; font-size: 1.2em;">
                        <div><input type="text" placeholder="Search..."></div>
                        <div><i class="fa fa-fw fa-sort-amount-up">&nbsp;</i>Urutkan</div>
                    </div>
                    <div style="font-weight: bold; font-size: 1.8em; margin-bottom: 7px;">Menampilkan <span>0</span> broker agen</div>
                    <?php for ($i=0; $i<count($brokerCabang); $i++){ ?>
                        <a onclick="updateListing('<?=$brokerCabang[$i]->id_broker?>');" class="row" style="padding: 5px 0px; border-bottom: dashed #444 1px; border-top: dashed #444 1px;">
                            <div class="col-sm-3">
                                <img src="/assets/img/profile-broker/<?=$brokerCabang[$i]->foto_broker?>" alt="" style="display: inline-block; border-radius: 50%; object-fit: cover; border: dashed #aaaa 1.5px; padding: 3px;">
                            </div>
                            <div class="col-sm-9 flex flex-ver" style="justify-content: center;">
                                <div style="font-weight: bold; font-size: 1.4em;"><?=$brokerCabang[$i]->nama_broker?></div>
                                <div><?=$brokerCabang[$i]->nama_cabang?></div>
                            </div>
                        </a>
                    <?php } ?>
                </div>
                <div class="col-md-8">
                    <div style="font-weight: bold; font-size: 1.8em;">Listing oleh [<span></span>]</div>
                    <div class="flex flex-hor txt-bold flex-equal txt-center" style="border-bottom:solid gainsboro 1px; margin-top: 5px;">
                        <div active-group="detail-action"
                            class="flex-equal clickable bg-red activeable-solo active padding-m">
                            <span>Aktif</span>
                            <span class="m-badge m-badge--warning bg-red txt-white">24</span>
                        </div>
                        <div active-group="detail-action" class="flex-equal clickable bg-red activeable-solo padding-m">
                            <span>Terjual/Tersewa</span>
                            <span class="m-badge m-badge--warning bg-red txt-white">35</span>
                        </div>
                    </div>
                    <div class="flex flex-hor flex-equal mt-2" style="font-weight: bold; font-size: 1.2em; padding: 5px 0px;">
                        <div>Menampilkan <span>0</span> properti</div>
                        <div><i class="fa fa-fw fa-sort-amount-up">&nbsp;</i>Urutkan</div>
                        <div class="flex flex-hor txt-bold flex-equal txt-center" style="border-bottom:solid gainsboro 1px;">
                            <div active-group="option-action"
                                class="flex-equal clickable bg-red activeable-solo active padding-s">
                                <span>Foto</span>
                            </div>
                            <div active-group="option-action" class="flex-equal clickable bg-red activeable-solo padding-s">
                                <span>Tabel</span>
                            </div>
                        </div>
                    </div>
                    <!--Map Section -->
                    <div class="col-md-12 single-list-page hide-on-small-only" id="mapListing" style="height:30vh;"></div>
                    <!--End of map Section -->
                    <div style="margin-top:5vh; flex-wrap:wrap;" class="flex flex-hor" id="listListing">
                        <a href="#" class="flex-2 rounded" style="border: dotted #9999 0.5px; color: black;">
                            <div class="flex flex-ver" style="height:100%; padding: 10px;">
                                <div style="background-image:url('https://static.dezeen.com/uploads/2017/08/clifton-house-project-architecture_dezeen_hero-1.jpg'); background-size:cover; min-height: 200px; background-position: center;">
                                    <div style="background-color:seagreen; padding: 2px; min-width: 196px; width: 50%; margin-top: 20px; margin-left: 10px; color: white; font-weight: bold; border-radius: 20px; text-align: center;">[BadgeInfo]</div>
                                </div>
                                <div class="flex flex-hor" style="margin-top: 5px;">
                                    <div class="flex flex-ver flex-equal" style="width: 90%;">
                                        <div style="font-weight: bold; font-size: 1.5em;">500 jt</div>
                                        <div style="font-size: 1.2em;">Rumah Mewah Indah Mantap</div>
                                        <div>Jalan Kertajaya No. 181, Gubeng, Surabaya</div>
                                        <div class="flex flex-hor" style="width: 80%;">
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-bed"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-bath"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-square"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-building"></i><br>5</div>
                                        </div>
                                    </div>
                                    <div class="flex flex-ver" style="font-size: 1.8em; width: 10%;">
                                        <div><i class="fa fa-whatsapp"></i></div>
                                        <div><i class="fa fa-heart"></i></div>
                                        <div><i class="fa fa-share"></i></div>
                                        <div><i class="fa fa-ellipsis-h"></i></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="flex-2 rounded" style="border: dotted #9999 0.5px; color: black;">
                            <div class="flex flex-ver" style="height:100%; padding: 10px;">
                                <div style="background-image:url('https://static.dezeen.com/uploads/2017/08/clifton-house-project-architecture_dezeen_hero-1.jpg'); background-size:cover; min-height: 200px; background-position: center;">
                                    <div style="background-color:seagreen; padding: 2px; min-width: 196px; width: 50%; margin-top: 20px; margin-left: 10px; color: white; font-weight: bold; border-radius: 20px; text-align: center;">[BadgeInfo]</div>
                                </div>
                                <div class="flex flex-hor" style="margin-top: 5px;">
                                    <div class="flex flex-ver flex-equal" style="width: 90%;">
                                        <div style="font-weight: bold; font-size: 1.5em;">500 jt</div>
                                        <div style="font-size: 1.2em;">Rumah Mewah Indah Mantap</div>
                                        <div>Jalan Kertajaya No. 181, Gubeng, Surabaya</div>
                                        <div class="flex flex-hor" style="width: 80%;">
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-bed"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-bath"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-square"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-building"></i><br>5</div>
                                        </div>
                                    </div>
                                    <div class="flex flex-ver" style="font-size: 1.8em; width: 10%;">
                                        <div><i class="fa fa-whatsapp"></i></div>
                                        <div><i class="fa fa-heart"></i></div>
                                        <div><i class="fa fa-share"></i></div>
                                        <div><i class="fa fa-ellipsis-h"></i></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="flex-2 rounded" style="border: dotted #9999 0.5px; color: black;">
                            <div class="flex flex-ver" style="height:100%; padding: 10px;">
                                <div style="background-image:url('https://static.dezeen.com/uploads/2017/08/clifton-house-project-architecture_dezeen_hero-1.jpg'); background-size:cover; min-height: 200px; background-position: center;">
                                    <div style="background-color:seagreen; padding: 2px; min-width: 196px; width: 50%; margin-top: 20px; margin-left: 10px; color: white; font-weight: bold; border-radius: 20px; text-align: center;">[BadgeInfo]</div>
                                </div>
                                <div class="flex flex-hor" style="margin-top: 5px;">
                                    <div class="flex flex-ver flex-equal" style="width: 90%;">
                                        <div style="font-weight: bold; font-size: 1.5em;">500 jt</div>
                                        <div style="font-size: 1.2em;">Rumah Mewah Indah Mantap</div>
                                        <div>Jalan Kertajaya No. 181, Gubeng, Surabaya</div>
                                        <div class="flex flex-hor" style="width: 80%;">
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-bed"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-bath"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-square"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-building"></i><br>5</div>
                                        </div>
                                    </div>
                                    <div class="flex flex-ver" style="font-size: 1.8em; width: 10%;">
                                        <div><i class="fa fa-whatsapp"></i></div>
                                        <div><i class="fa fa-heart"></i></div>
                                        <div><i class="fa fa-share"></i></div>
                                        <div><i class="fa fa-ellipsis-h"></i></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="flex-2 rounded" style="border: dotted #9999 0.5px; color: black;">
                            <div class="flex flex-ver" style="height:100%; padding: 10px;">
                                <div style="background-image:url('https://static.dezeen.com/uploads/2017/08/clifton-house-project-architecture_dezeen_hero-1.jpg'); background-size:cover; min-height: 200px; background-position: center;">
                                    <div style="background-color:seagreen; padding: 2px; min-width: 196px; width: 50%; margin-top: 20px; margin-left: 10px; color: white; font-weight: bold; border-radius: 20px; text-align: center;">[BadgeInfo]</div>
                                </div>
                                <div class="flex flex-hor" style="margin-top: 5px;">
                                    <div class="flex flex-ver flex-equal" style="width: 90%;">
                                        <div style="font-weight: bold; font-size: 1.5em;">500 jt</div>
                                        <div style="font-size: 1.2em;">Rumah Mewah Indah Mantap</div>
                                        <div>Jalan Kertajaya No. 181, Gubeng, Surabaya</div>
                                        <div class="flex flex-hor" style="width: 80%;">
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-bed"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-bath"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-square"></i><br>5</div>
                                            <div class="flex-4 txt-center" style="background-color: #d32f2f; color: white; margin-right: 5px; border-radius: 3px; padding: 5px;"><i style="font-size: 1.3em;" class="fas fa-building"></i><br>5</div>
                                        </div>
                                    </div>
                                    <div class="flex flex-ver" style="font-size: 1.8em; width: 10%;">
                                        <div><i class="fa fa-whatsapp"></i></div>
                                        <div><i class="fa fa-heart"></i></div>
                                        <div><i class="fa fa-share"></i></div>
                                        <div><i class="fa fa-ellipsis-h"></i></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
	</section>
	<!-- Page -->
	<section class="page-section">
		<div class="container">

			<div class="row">

				<!-- sidebar -->

			</div>
		</div>
	</section>
	<!-- Page end -->


	<!-- Clients section -->
	<div class="clients-section">
		<div class="container">
			<div class="clients-slider owl-carousel">
				<a href="#">
					<img src="/assets/img/partner/1.png" alt="">
				</a>
				<a href="#">
					<img src='assets/img/partner/2.png' alt="">
				</a>
				<a href="#">
					<img src="/assets/img/partner/3.png" alt="">
				</a>
				<a href="#">
					<img src="/assets/img/partner/4.png" alt="">
				</a>
				<a href="#">
					<img src="/assets/img/partner/5.png" alt="">
				</a>
			</div>
		</div>
	</div>
	<!-- Clients section end -->

	<?php 
		$this->load->view('page-part/common-foot');
	?>
<!-- load for map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyJi7G0CbtkyT9qjITRHKHcg8tKCZ1tpk"></script>
<script src="/assets/js/map-2.js"></script>
<script type="text/javascript" src="/assets/json/data.json"></script>
<script type="text/javascript" src="/assets/js/markerclusterer.js"></script>

<script type="text/javascript">
    function initialize() {
    var center = new google.maps.LatLng(37.4419, -122.1419);

    var map = new google.maps.Map(document.getElementById('mapListing'), {
        zoom: 3,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
                fullscreenControl: false,
                mapTypeControl: false,
                streetViewControl: false
    });

            var markers = [];
    for (var i = 0; i < LatLongData.length; i++) {
            var dataPhoto = LatLongData[i];
        var latLng = new google.maps.LatLng(dataPhoto.Lat, dataPhoto.Long);
        var marker = new google.maps.Marker({
        position: latLng
        });
        markers.push(marker);
    }
    var markerCluster = new MarkerClusterer(map, markers, {
        averageCenter: true
    });

    google.maps.event.addListener(markerCluster, "click", function (c) {
        log("click: ");
        log("&mdash;Center of cluster: " + c.getCenter());
        log("&mdash;Number of managed markers in cluster: " + c.getSize());
        var m = c.getMarkers();
        var p = [];
        for (var i = 0; i < m.length; i++ ){
        p.push(m[i].getPosition());
        }
        log("&mdash;Locations of managed markers: " + p.join(", "));

    });
    google.maps.event.addListener(markerCluster, "mouseover", function (c) {
        log("mouseover: ");
        log("&mdash;Center of cluster: " + c.getCenter());
        log("&mdash;Number of managed markers in cluster: " + c.getSize());
    });
    google.maps.event.addListener(markerCluster, "mouseout", function (c) {
        log("mouseout: ");
        log("&mdash;Center of cluster: " + c.getCenter());
        log("&mdash;Number of managed markers in cluster: " + c.getSize());
    });

            var saveSearchDiv = document.createElement("div");
            var saveSearchInner = new generateSearch(saveSearchDiv, map);
            saveSearchDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(saveSearchDiv);
            let text = ["Remove Outline", "Draw", "Layers"];
            for(let i = 0;i < 3;i++){
                var customControlDiv = document.createElement("div");
                var customControl = new generateCustom(customControlDiv, text[i]);
                customControlDiv.index = 1;
                map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(customControlDiv);
            }
    }
    
    google.maps.event.addDomListener(window, 'load', initialize);
        function generateSearch(controlDiv, map){
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = '#d32f2f';
            controlUI.style.borderRadius = "5px";
            controlUI.style.cursor = 'pointer';
            controlUI.style.margin = "10px";
            controlUI.style.textAlign = 'center';
            controlDiv.append(controlUI);

            var controlText = document.createElement('div');
    controlText.style.color = 'white';
            controlText.style.fontWeight = "bold";
    controlText.style.fontSize = '16px';
            controlText.style.padding = "10px";
    controlText.innerHTML = 'Save Search';
    controlUI.appendChild(controlText);

            controlUI.addEventListener('click', function() {
        map.setCenter(chicago);
    });
        }

        function generateCustom(controlDiv, text){
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = 'white';
            controlUI.style.borderRadius = "5px";
            controlUI.style.cursor = 'pointer';
            controlUI.style.margin = "10px";
            controlUI.style.marginBottom = "20px";
            controlUI.style.textAlign = 'center';
            controlDiv.append(controlUI);

            var controlText = document.createElement('div');
            controlText.style.fontWeight = "bold";
    controlText.style.fontSize = '16px';
            controlText.style.padding = "10px";
    controlText.innerHTML = text;
    controlUI.appendChild(controlText);
        }
</script>

<script>
    var listingSpesialis = function() {
        if ($('#chart_listing_spesialis').length == 0) {
            return;
        }

        var chart = new Chartist.Pie('#chart_listing_spesialis', {
            series: [{
                    value: 32,
                    className: 'custom',
                    meta: {
                        color: mApp.getColor('brand')
                    }
                },
                {
                    value: 32,
                    className: 'custom',
                    meta: {
                        color: mApp.getColor('accent')
                    }
                },
                {
                    value: 36,
                    className: 'custom',
                    meta: {
                        color: mApp.getColor('warning')
                    }
                }
            ],
            labels: [1, 2, 3]
        }, {
            donut: true,
            donutWidth: 17,
            showLabel: false
        });

        chart.on('draw', function(data) {
            if (data.type === 'slice') {
                // Get the total path length in order to use for dash array animation
                var pathLength = data.element._node.getTotalLength();

                // Set a dasharray that matches the path length as prerequisite to animate dashoffset
                data.element.attr({
                    'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                });

                // Create animation definition while also assigning an ID to the animation for later sync usage
                var animationDefinition = {
                    'stroke-dashoffset': {
                        id: 'anim' + data.index,
                        dur: 1000,
                        from: -pathLength + 'px',
                        to: '0px',
                        easing: Chartist.Svg.Easing.easeOutQuint,
                        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                        fill: 'freeze',
                        'stroke': data.meta.color
                    }
                };

                // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
                if (data.index !== 0) {
                    animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
                }

                // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

                data.element.attr({
                    'stroke-dashoffset': -pathLength + 'px',
                    'stroke': data.meta.color
                });

                // We can't use guided mode as the animations need to rely on setting begin manually
                // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
                data.element.animate(animationDefinition, false);
            }
        });

        // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
        return;
        
        /*
        chart.on('created', function() {
            if (window.__anim21278907124) {
                clearTimeout(window.__anim21278907124);
                window.__anim21278907124 = null;
            }
            window.__anim21278907124 = setTimeout(chart.update.bind(chart), 15000);
        });
        */
    }
    listingSpesialis();
</script>

<script>
    var transaksiBaru = function() {
        if ($('#chart_transaksi_baru').length == 0) {
            return;
        }

        var chart = new Chartist.Pie('#chart_transaksi_baru', {
            series: [{
                    value: 32,
                    className: 'custom',
                    meta: {
                        color: mApp.getColor('brand')
                    }
                },
                {
                    value: 32,
                    className: 'custom',
                    meta: {
                        color: mApp.getColor('accent')
                    }
                },
                {
                    value: 36,
                    className: 'custom',
                    meta: {
                        color: mApp.getColor('warning')
                    }
                }
            ],
            labels: [1, 2, 3]
        }, {
            donut: true,
            donutWidth: 17,
            showLabel: false
        });

        chart.on('draw', function(data) {
            if (data.type === 'slice') {
                // Get the total path length in order to use for dash array animation
                var pathLength = data.element._node.getTotalLength();

                // Set a dasharray that matches the path length as prerequisite to animate dashoffset
                data.element.attr({
                    'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                });

                // Create animation definition while also assigning an ID to the animation for later sync usage
                var animationDefinition = {
                    'stroke-dashoffset': {
                        id: 'anim' + data.index,
                        dur: 1000,
                        from: -pathLength + 'px',
                        to: '0px',
                        easing: Chartist.Svg.Easing.easeOutQuint,
                        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                        fill: 'freeze',
                        'stroke': data.meta.color
                    }
                };

                // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
                if (data.index !== 0) {
                    animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
                }

                // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

                data.element.attr({
                    'stroke-dashoffset': -pathLength + 'px',
                    'stroke': data.meta.color
                });

                // We can't use guided mode as the animations need to rely on setting begin manually
                // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
                data.element.animate(animationDefinition, false);
            }
        });

        // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
        return;
        
        /*
        chart.on('created', function() {
            if (window.__anim21278907124) {
                clearTimeout(window.__anim21278907124);
                window.__anim21278907124 = null;
            }
            window.__anim21278907124 = setTimeout(chart.update.bind(chart), 15000);
        });
        */
    }
    transaksiBaru();
</script>

</body>

</html>
