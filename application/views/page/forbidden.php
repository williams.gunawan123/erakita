<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('page-part/common-head', $pageData);?>


<body>
    
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
    <!-- Header section end -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
	<main>
        <div style="height:calc(100vh - 75px); text-align:center; display:flex;">
            <div id="forbidden" style="margin:auto; justify-content:center; font-size:4em; font-family:nunito">
                403 Forbidden
            </div>
        </div>
    </main>
	<?php 
		$set['footer']=false;
		$this->load->view('page-part/common-foot', $set);
	?>
</body>

</html>
