<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->
	<!-- Header section -->
	<?php $this->load->view('page-part/header-saya');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->


	<div class="summary-preview flex flex-ver flex-separate my-container"
		style='margin-top:120px;border-bottom: 1px solid rgba(0, 0, 0, 0.13); padding-bottom:10px;'>
		<div class="rounded" style="background-color: white;">
			<div class="rounded flex flex-ver" style="overflow:hidden">
				<div class="flex flex-ver" style="flex-grow:1">
					<div class="txt-bold padding-s" style="border-bottom:solid gainsboro 1px">
						<div class="flex flex-hor flex-vertical-center flex-separate" style="margin-bottom:10px;">
							<h2>My Profile</h2>
							<a href="/createlisting" class="btn txt-white rounded bg-red ">Edit Profile</a>
						</div>
					</div>
					<div class="flex flex-ver" style="padding-top :20px;">
						
						<div href="#" class="user-photo-large clickable flex flex-hor flex-vertical-center" style="background-image:url('<?= (isset($_SESSION["loggedPhoto"])) ? "/assets/img/profile-customer/".$_SESSION["loggedPhoto"] : "/assets/img/profile-customer/default.png" ; ?>');">
							<span class="txt-white txt-bold txt-center" style="opacity:0; width:100%; display:block"><i class="fas fa-pencil-alt"></i></span>
						</div>
						<div class="flex flex-ver">
							<div class="txt-center txt-bold"><h2>Biodata</h2></div>
						</div>
                        <div class="flex flex-hor flex-vertical-center flex-wrap my-container">
							<?php
							for ($i=0; $i < 10; $i++) { ?>
							<div class="flex flex-ver flex-2 padding-m">
								<div style="">
									<div class="m-form__section m-form__section--first">
										<div class="m-form__heading form-group m-form__group">
											<h5 class="m-form__heading-title">* Judul</h5>
											<input type="text" name="judul" class="form-control m-input" placeholder="Judul" minlength=10 maxlength=100 required/>
										</div>
									</div>
								</div>
							</div>
							<?php
							}
							?>
						</div>
						<div class="my-container flex flex-hor-reverse" style="padding-right:10px">
							<div class="bg-red clickable padding-m txt-white">
								<span class="txt-md">Confirm</span>
							</div>
						</div>
						



					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Page -->
	<section class="page-section">
		<div class="container">

			<div class="row">

				<!-- sidebar -->

			</div>
		</div>
	</section>
	<!-- Page end -->

	<?php 
		$this->load->view('page-part/common-foot');
	?>

</body>

<script>
	var datatableLatestOrders = function () {
		if ($('#m_datatable_latest_orders').length === 0) {
			return;
		}

		var datatable = $('.m_datatable').mDatatable({
			data: {
				type: 'remote',
				source: {
					read: {
						url: 'http://localhost/testing.json'
					}
				},
				pageSize: 10,
				saveState: {
					cookie: false,
					webstorage: true
				},
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true
			},

			layout: {
				theme: 'default',
				class: '',
				scroll: true,
				height: 380,
				footer: false
			},

			sortable: true,

			filterable: false,

			pagination: true,

			columns: [{
				field: "RecordID",
				title: "#",
				sortable: false,
				width: 40,
				selector: {
					class: 'm-checkbox--solid m-checkbox--brand'
				},
				textAlign: 'center'
			}, {
				field: "Image",
				title: "Preview",
				sortable: 'asc',
				filterable: false,
				width: 150,
				template: '<img src="{{Image}}">'
			}, {
				field: "Judul",
				title: "Judul",
				width: 150,
				template: '<h3>{{Judul}}</h3><br/>{{Alamat}}<br/>{{KT}}&nbsp;{{KM}}&nbsp;{{LT}}&nbsp;{{LB}}'
			}, {
				field: "Daerah",
				title: "Daerah",
				width: 150,
				template: '{{Daerah}}'
			}, {
				field: "Kota",
				title: "Kota",
				width: 150,
				template: '{{Kota}}'
			}, {
				field: "Harga",
				title: "Harga",
				width: 150,
				template: '{{Daerah}}'
			}, {
				field: "Submit",
				title: "Submit",
				width: 150,
				template: '{{Submit}}'
			}, {
				field: "Approve",
				title: "Approve",
				width: 150,
				template: '{{Approve}}'
			}, {
				field: "Actions",
				width: 110,
				title: "Actions",
				sortable: false,
				overflow: 'visible',
				template: function (row, index, datatable) {
					var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
					return '\
                        <div class="dropdown ' + dropup + '">\
                            <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
                                <i class="la la-ellipsis-h"></i>\
                            </a>\
                            <div class="dropdown-menu dropdown-menu-right">\
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>\
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>\
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>\
                            </div>\
                        </div>\
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                            <i class="la la-edit"></i>\
                        </a>\
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\
                            <i class="la la-trash"></i>\
                        </a>\
                    ';
				}
			}]
		});
	}
	datatableLatestOrders();

</script>

</html>
