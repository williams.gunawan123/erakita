<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);


?>

<body>
	<?php
		if ($this->input->cookie('Erakita-Listing-Broker') != NULL){
			echo "<script> const cookieBroker = " .$this->input->cookie('Erakita-Listing-Broker').";</script>";
		}else{
			echo "<script> const cookieBroker = null;</script>";
		}
	?>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->

	<div class="modal fade" id="m_modal_contact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document" style="transform: translateX(8px);">
			<div class="modal-content" style="background-color:#fff0; box-shadow:none;">
				<div class="modal-header" style="padding:10px 5px; border:none;">
					<h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="background-color:white;">
					<div style="font-size:1.4em; text-align:center;">Bagaimana kami bisa menghubungi anda?</div><br>
					<div class="margin-row-sm"><span>Pesan</span></div>
					<input class="margin-row-md" type="text" placeholder="Masukkan Pesan Anda Disini ..." style="width:100%">
					<div class="margin-row-sm"><span>Telepon</span></div>
					<input class="margin-row-lg" type="text" placeholder="Masukkan Nomor Anda Disini ..." style="width:100%">
					<a data-href="https://wa.me/" data-phone="6287853265186" href="" class="btn btn-danger bg-red" style="width:100%;">Kirim Pesan</a>
					
				</div>
				<!--div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div -->
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="m_modal_6"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document" style="max-width:97%; transform: translateX(8px);">
			<div class="modal-content" style="background-color:#fff0; box-shadow:none;">
				<div class="modal-header" style="padding:10px 5px; border:none;">
					<h5 class="modal-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="background-color:white; padding:0px;">
					
					<div id="mapListing" style="position: relative;overflow: hidden;width: 75%;height: 88vh;"></div>
					
					<div class="m-ion-range-slider map-slider" style="width:30%; position:absolute; left:5px; bottom: 5px; background-color: #00000075; border-radius: 5px; padding: 10px;">
							<input type="hidden" id="map-slider" />
					</div>
					<div id="rightMap">
						<div class="flex flex-ver" style="height:100%;">
							<div class="flex flex-hor minimalistScrollbar" style="max-height:25%; overflow-x:auto;padding:10px;">
								<div onclick="updateMarker('1','-2');" class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable-solo clickable active" active-group="header-map"><span>Fasilitas Umum</span></div>
								<div onclick="updateMarker('0','0');" class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable-solo clickable" active-group="header-map"><span>Listing Sekitar</span></div>
							</div>
							<div class="flex flex-hor minimalistScrollbar" id="detailPublic" data-selected='-1' style="max-height:25%; overflow-x:auto;padding:10px;">
								<div onclick="updateMarker('1','0');" class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable-solo clickable active" active-group="detail-public-place"><span>Keluarga</span></div>
								<div onclick="updateMarker('1','1');" class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable-solo clickable" active-group="detail-public-place"><span>Transportasi</span></div>
								<div onclick="updateMarker('1','2');" class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable-solo clickable" active-group="detail-public-place"><span>Gaya Hidup</span></div>
							</div>
							<div class="flex flex-ver minimalistScrollbar" id='nearbyPlaceCard' style="background-color:#e4e4e4; height:100%; padding:0px 20px; overflow-y:auto;">
							</div>
						</div>
					</div>
				</div>
				<!--div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div-->
			</div>
		</div>
	</div>



	
	<section class="my-container flex flex-ver section-to-print">
		<!-- Breadcrumb -->
		<div class="site-breadcrumb section-to-print pb-3" style="margin-top: 50px;">
			<div class="container p-0">
				<a href="/home"><i class="fa fa-home"></i>Home</a>
				<span><i class="fa fa-angle-right"></i></span>
				<a href="/listing?&searchValue=Propinsi-<?= $dataListing->nama_provinsi ?>"><?= $dataListing->nama_provinsi ?></a>
				<span><i class="fa fa-angle-right"></i></span>
				<a href="/listing?&searchValue=Kota-<?= $dataListing->nama_kota ?>"><?= $dataListing->nama_kota ?></a>
				<span><i class="fa fa-angle-right"></i></span>
				<a href="/listing?&searchValue=Kecamatan-<?= $dataListing->nama_kecamatan ?>"><?= $dataListing->nama_kecamatan ?></a>
				<span><i class="fa fa-angle-right"></i></span>
				<a href="/listing?&searchValue=Kelurahan-<?= $dataListing->nama_kelurahan ?>"><?= $dataListing->nama_kelurahan ?></a>
				<!-- <span><i class="fa fa-angle-right"></i></span> -->
			</div>
		</div>
		<div id="asdf" class="sticky-header-detail bg-white summary-preview flex flex-hor flex-separate" style=''>
			<div class="flex flex-hor flex-vertical-center">
				<?php
					$icon = $dataListing->tipe_listing == "1" ? "home" : ($dataListing->tipe_listing == "2" ? "building-o" : ($dataListing->tipe_listing == "3" ? "archive" : ($dataListing->tipe_listing == "4" ? "industry" : ($dataListing->tipe_listing == "5" ? "road" : "industry"))));
				?>
				<div class="rounded-s txt-white txt-center margin-col-lg" style="padding:5px;background-color:var(--bg-<?=$dataListing->jenis_transaksi == "0" ? "jual" : "sewa"?>"><i class="fa fa-<?=$icon?>" style="font-size:3em;"></i></div>
				<div class="flex flex-ver">
					<div><b><?=$dataListing->judul_listing?></b></div>
					<div><?=$dataListing->alamat_listing?></div>
					<div><?=$dataListing->nama_kecamatan?>, <?=$dataListing->nama_kota?></div>
					<div>
						<?php
						/**
						 * Function to change number to another format
						 * 
						 * @param 	int x
						 * @return 	string 
						 */
							function shortNumber(int $x) : string{
								$x = $x / 1000000;
								if ($x >= 1000000){
									return round($x/1000000,2) ." T";
								}else if ($x >= 1000){
									return round($x/1000, 2) ." M";
								}else{
									return round(number_format($x), 2) ." jt";
								}
							}
							$pureHarga = 0;
							if ($dataListing->jenis_transaksi == 0){
								$jenis = "Dijual";
								echo $jenis;
								$harga = shortNumber(intval($dataListing->harga_jual));
								$pureHarga = $dataListing->harga_jual;
							}else{
								$jenis = "Disewakan";
								echo $jenis;
								$harga = shortNumber(intval($dataListing->harga_sewa)) ."/thn";
								$pureHarga = $dataListing->harga_sewa;
							}
						?>
					</div>
				</div>
			</div>
			<div class="flex flex-hor flex-vertical-center txt-center recursive">
				<b><div class="txt-lg margin-col-lg padding-m bg-red rounded-s txt-white">
					<?=$harga?>
				</div></b>
				<div class="flex flex-ver separator-left padding-m relative">
					<?php $dataListing->kamar_tidur = $dataListing->kamar_tidur == null ? "N/A" : $dataListing->kamar_tidur; ?>
					<b><div><?= $dataListing->kamar_tidur ?> </div></b>
					<div>Kamar<br>Tidur</div>
				</div>
				<div class="flex flex-ver separator-left padding-m relative">
					<?php $dataListing->kamar_mandi = $dataListing->kamar_mandi == null ? "N/A" : $dataListing->kamar_mandi; ?>
					<b><div><?=$dataListing->kamar_mandi?></div></b>
					<div>Kamar<br>Mandi</div>
				</div>
	
				<div class="flex flex-ver separator-left padding-m relative">
					<?php $dataListing->luas_tanah = $dataListing->luas_tanah == null ? "N/A" : $dataListing->luas_tanah; ?>
					<b><div><?=$dataListing->luas_tanah?> m<sup>2</sup></div></b>
					<div class="flex flex-hor">
						<div>Luas<br>Tanah</div>
					</div>
				</div>
				<div class="flex flex-ver separator-left padding-m relative">
					<?php $dataListing->luas_bangunan = $dataListing->luas_bangunan == null ? "N/A" : $dataListing->luas_bangunan; ?>
					<b><div><?=$dataListing->luas_bangunan?> m<sup>2</sup></div></b>
					<div class="flex flex-hor">
						<div>Luas<br>Bangunan</div>
					</div>
				</div>
	
			</div>
			<div class="flex flex-hor flex-vertical-center">
				<div class="flex flex-ver margin-col-md">
					<table>
						<input type="hidden" id="startListing" value="<?=$dataListing->start_listing?>"/>
						<tr id="lamaListing"></tr>
					</table>
				</div>
				<div class="flex flex-ver">
					<div><i class="fa fa-eye"></i> <?=$dataListing->total_view?> Views</div>
					<div><i class="fa fa-book"></i> <?=$dataListing->total_telp?> Kontak</div>
					<div><i class="fa fa-heart"></i> <?=$dataListing->total_favorite?> Favorites</div>
				</div>
			</div>
		</div>
		<div class="sticky-action flex flex-hor flex-separate section-to-hide section-to-clean">
			<div class="flex flex-hor">
				<div class='command-action'>Garis Besar</div>
				<div class='command-action' scrollTo="m_accordion_1_item_1_head" scrollOffset='-178'>Detail</div>
				<div class='command-action' scrollTo="m_accordion_2_item_1_head" scrollOffset='-178'>Deskripsi</div>
				<div class='command-action' scrollTo="m_accordion_3_item_1_head" scrollOffset='-178'>Video</div>
				<div class='command-action' scrollTo="m_accordion_4_item_1_head" scrollOffset='-178'>Simulasi KPR</div>
				<!-- <div class='command-action'>Sekitar</div> -->
			</div>
			<div  class='command-action'>
				KEMBALI KE ATAS <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
			</div>
		</div>
			<div class="flex flex-hor" id="horizontal-content">
				<div class="single-list-page">
                    
					<div class="single-list-slider owl-carousel" id="sl-slider">
						<?php for ($i=1; $i<=$dataListing->gambar; $i++){ ?>
							<div class="sl-item set-bg" data-setbg="/assets/img/listing/<?=$dataListing->id_listing ."/" .$i?>.jpg">
								<div class="sale-notic"><?=$jenis?></div>
							</div>
						<?php } ?>
					</div>
					<div class="owl-carousel sl-thumb-slider" id="sl-slider-thumb">
						<?php for ($i=1; $i<=$dataListing->gambar; $i++){ ?>
							<div class="sl-thumb set-bg" data-setbg="/assets/img/listing/<?=$dataListing->id_listing ."/" .$i?>.jpg"></div>
						<?php } ?>
					</div>
					
					
								<!--begin::Section-->
								<div class="m-accordion m-accordion--bordered" id="m_accordion_1" role="tablist">

									<!--begin::Item-->
									<div class="m-accordion__item m-accordion__item--solid">
										<div class="m-accordion__item-head activeable active bg-red borderless" active-group="detail-group1" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="    true">
											<span class="m-accordion__item-icon"><i class="fa flaticon-user-ok"></i></span>
											<span class="m-accordion__item-title">Detail</span>
											<span class="m-accordion__item-mode"></span>
										</div>
										<div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
											<div class="m-accordion__item-content">
												<div class="flex flex-hor flex-1 flex-wrap">
													<div class="detil-item flex-2">
														<p class="label">Tipe Properti</p>
														<div class="dash-bottom margin-row-sm txt-bold" style="margin-right:5px;">
															<?php
																switch ($dataListing->tipe_listing) {
																	case 1:
																		$tipe = "Rumah " .$jenis;
																		break;
																	case 2:
																		$tipe = "Apartemen " .$jenis;
																		break;
																	case 3:
																		$tipe = "Ruko " .$jenis;
																		break;
																	case 4:
																		$tipe = "Tanah " .$jenis;
																		break;
																	case 5:
																		$tipe = "Gudang " .$jenis;
																		break;
																	case 6:
																		$tipe = "Pabrik " .$jenis;
																		break;
																	default:
																		$tipe = "N/A";
																}
																echo $tipe;
															?>
														</div>
													</div>

													<div class="detil-item flex-2">
														<p class="label">&nbsp;</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-left:5px;">
															&nbsp;	
														</div>
													</div>

													<div class="detil-item flex-2">
														<p class="label">Panjang</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-right:5px;">
															<?php $dataListing->panjang_tanah = $dataListing->panjang_tanah == null ? "N/A" : $dataListing->panjang_tanah; ?>
															<?=$dataListing->panjang_tanah?> m
														</div>
													</div>

													<div class="detil-item flex-2">
														<p class="label">Lebar</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-left:5px;">
															<?php $dataListing->lebar_tanah = $dataListing->lebar_tanah == null ? "N/A" : $dataListing->lebar_tanah; ?>
															<?=$dataListing->lebar_tanah?> m
														</div>
													</div>

													
													<div class="detil-item flex-2">
														<p class="label">Rp</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-right:5px;">
															<?=$harga?>
														</div>
													</div>

													<div class="detil-item flex-2">
														<p class="label">Nego</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-left:5px;">
															<?php
																switch ($dataListing->nego_listing) {
																	case 0:
																		echo "Tidak";
																		break;
																	case 1:
																		echo "Ya";
																		break;
																	default:
																		echo "N/A";
																}
															?>
														</div>
													</div>

													
													<!-- Start Optional Details -->

													<div class="detil-item flex-2">
														<p class="label">Sertifikat</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-right:5px;">
															<?php
																switch ($dataListing->sertifikat_listing) {
																	case 1:
																		echo "Sertifikat";
																		break;
																	default:
																		echo "N/A";
																}
															?>
														</div>
													</div>

													<div class="detil-item flex-2">
														<p class="label">Hadap</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-left:5px;">
															<?php
																switch ($dataListing->hadap_listing) {
																	case "U":
																		echo "Utara";
																		break;
																	case "T":
																		echo "Timur";
																		break;
																	case "S":
																		echo "Selatan";
																		break;
																	case "B":
																		echo "Barat";
																		break;
																	default:
																		echo "N/A";
																}
															?>
														</div>
													</div>
													<div class="detil-item flex-2">
														<p class="label">Tahun Dibangun</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-right:5px;">
															<?php $dataListing->tahun_bangunan = $dataListing->tahun_bangunan == null ? "N/A" : $dataListing->tahun_bangunan; ?>
															<?=$dataListing->tahun_bangunan?>
														</div>
													</div>

													<div class="detil-item flex-2">
														<p class="label">Hoek</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-left:5px;">
															<?php
																switch ($dataListing->hoek_listing) {
																	case 1:
																		echo "Ya";
																		break;
																	case 0:
																		echo "Tidak";
																		break;
																	default:
																		echo "N/A";
																}
															?>
														</div>
													</div>

													
													
													
													<div class="detil-item flex-2">
														<p class="label">Lantai</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-right:5px;">
															<?php $dataListing->jml_lantai = $dataListing->jml_lantai == null ? "N/A" : $dataListing->jml_lantai; ?>
															<?=$dataListing->jml_lantai?>
														</div>
													</div>

													<div class="detil-item flex-2">
														<p class="label">Garasi</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-left:5px;">
															<?php $dataListing->jml_garasi = $dataListing->jml_garasi == null ? "N/A" : $dataListing->jml_garasi; ?>
															<?=$dataListing->jml_garasi?>
														</div>
													</div>
													
													<div class="detil-item flex-2">
														<p class="label">Interior</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-right:5px;">
															<?php
																switch ($dataListing->hoek_listing) {
																	case 1:
																		echo "Ya";
																		break;
																	case 0:
																		echo "Tidak";
																		break;
																	default:
																		echo "N/A";
																}
															?>
														</div>
													</div>

													<div class="detil-item flex-2">
														<p class="label">Listrik</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-left:5px;">
															<?php $dataListing->listrik = $dataListing->listrik == null ? "N/A" : $dataListing->listrik; ?>
															<?=$dataListing->listrik?>
														</div>
													</div>
													<!-- End Optional Details -->
													
													<div class="detil-item flex-2">
														<p class="label">Pengembang</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-right:5px;">
															<?php $dataListing->id_proyek = $dataListing->id_proyek == null ? "N/A" : $dataListing->id_proyek; ?>
															<?=$dataListing->id_proyek?>
														</div>
													</div>

													<div class="detil-item flex-2">
														<p class="label">Proyek</p>
														<div class="dash-bottom margin-row-md txt-bold" style="margin-left:5px;">
															<?php $dataListing->id_proyek = $dataListing->id_proyek == null ? "N/A" : $dataListing->id_proyek; ?>
															<?=$dataListing->id_proyek?>
														</div>
													</div>
												</div>



												<!-- <div class="row property-details-list">
													<div class="col-md-4 col-sm-6">
														<p><i class="fa fa-check-circle-o"></i> Air conditioning</p>
														<p><i class="fa fa-check-circle-o"></i> Telephone</p>
														<p><i class="fa fa-check-circle-o"></i> Laundry Room</p>
													</div>
													<div class="col-md-4 col-sm-6">
														<p><i class="fa fa-check-circle-o"></i> Central Heating</p>
														<p><i class="fa fa-check-circle-o"></i> Family Villa</p>
														<p><i class="fa fa-check-circle-o"></i> Metro Central</p>
													</div>
													<div class="col-md-4">
														<p><i class="fa fa-check-circle-o"></i> City views</p>
														<p><i class="fa fa-check-circle-o"></i> Internet</p>
														<p><i class="fa fa-check-circle-o"></i> Electric Range</p>
													</div>
												</div>
												<p>
												</p> -->
											</div>
										</div>
									</div>
									
								</div>
								<div class="m-accordion m-accordion--bordered" id="m_accordion_2" role="tablist">
									<!--end::Item-->

									<!--begin::Item-->
									<div class="m-accordion__item m-accordion__item--solid">
										<div class="m-accordion__item-head activeable active bg-red borderless" active-group="detail-group2" role="tab" id="m_accordion_2_item_1_head" data-toggle="collapse" href="#m_accordion_2_item_1_body" aria-expanded="    true">
											<span class="m-accordion__item-icon"><i class="fa  flaticon-placeholder"></i></span>
											<span class="m-accordion__item-title">Deskripsi</span>
											<span class="m-accordion__item-mode"></span>
										</div>
										<div class="m-accordion__item-body collapse show" id="m_accordion_2_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_2_item_1_head" data-parent="#m_accordion_2">
											<div class="m-accordion__item-content">
												<p>
													<?=$dataListing->deskripsi_listing?>
												</p>
											</div>
										</div>
									</div>

									<!--end::Item-->
								</div>
								<div class="m-accordion m-accordion--bordered" id="m_accordion_3" role="tablist">
								

									<!--begin::Item Jika Jenis Video != NULL -->
									<?php if ($dataListing->url_video != ""){ ?>
									<div class="m-accordion__item m-accordion__item--solid">
										<div class="m-accordion__item-head activeable active bg-red borderless" active-group="detail-group3" role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="    true">
											<span class="m-accordion__item-icon"><i class="fa  flaticon-placeholder"></i></span>
											<span class="m-accordion__item-title">Video</span>
											<span class="m-accordion__item-mode"></span>
										</div>
										<div class="m-accordion__item-body collapse show" id="m_accordion_3_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_3">
											<div class="m-accordion__item-content">
												<iframe width="100%" height="500px"
													src="<?=$dataListing->url_video?>">
												</iframe>
											</div>
										</div>
									</div>
									<?php } ?>
									<!--end::Item-->
								</div>
								<div class="m-accordion m-accordion--bordered" id="m_accordion_4" role="tablist">
								
									<!--begin::Item Jika Jenis transaksi = Beli -->
									<div class="m-accordion__item m-accordion__item--solid">
										<div class="m-accordion__item-head activeable active bg-red borderless" active-group="detail-group4" role="tab" id="m_accordion_4_item_1_head" data-toggle="collapse" href="#m_accordion_4_item_1_body" aria-expanded="    true">
											<span class="m-accordion__item-icon"><i class="fa  flaticon-alert-2"></i></span>
											<span class="m-accordion__item-title">Simulasi KPR</span>
											<span class="m-accordion__item-mode"></span>
										</div>
										<div class="m-accordion__item-body collapse show" id="m_accordion_4_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_4_item_1_head" data-parent="#m_accordion_4">
											<div class="m-accordion__item-content">
												<div class="row">
													<div class="col-lg-12 kpr-price-month txt-bold txt-md">Rp <span id="pmt">10.318.616</span> per bulan</div>
												</div>
												<div class="row">
													<div class="col-lg-12 kpr-price-month-detail mb-2" style="color:var(--gray)"><span class='percentRateLbl'>8.75</span>% Bunga Tahunan Fixed, selama <span class="longLoanLbl">5</span> Tahun</div>
												</div>
												
												<div class="row mb-2">
													<div class="flex flex-hor flex-vertical-center flex-1 col-lg-12">
														<div class="totalPokokPercent" style="background-color:teal;      width:50%; border-radius:10px 0px 0px 10px; height:10px;"></div>
														<div class="totalBungaPercent" style="background-color:goldenrod; width:50%; border-radius:0px 10px 10px 0px; height:10px;"></div>
													</div>
												</div>
												<div class="row mb-4">
													<div class="col-lg-3 flex flex-hor flex-vertical-center">
														<div class="mr-2" style="background-color:teal; border-radius:50%; height:10px; width:10px;"></div>
														 Total Pokok
													</div>
													<div class="col-lg-3 txt-bold">Rp. <span class="totalPokok">500.000.000</span></div>
													<div class="col-lg-3 flex flex-hor flex-vertical-center">
														<div class="mr-2" style="background-color:goldenrod; border-radius:50%; height:10px; width:10px;"></div>
														Total Bunga
													</div>
													<div class="col-lg-3 txt-bold">Rp. <span class="totalBunga">119.116.981</span></div>
												</div>

												<div class="row">
													<div class="col-lg-6">
														<label class="txt-bold" for="hargaProperti">Harga Properti</label>
														<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text">Rp.</span>
															</div>
															<input type="text" class="form-control" name="hargaProperti" id="hargaProperti" placeholder="" aria-label="">
														</div>
													</div>
													<div class="col-lg-6">
														<label class="txt-bold" for="hargaProperti">Uang Muka</label>
														<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text">Rp.</span>
															</div>
															<input disabled type="text" class="form-control" name="uangMuka" id="uangMuka" placeholder="" aria-label="">
															<div class="input-group-append">
																<input style="text-align:right;" type="text" class="form-control" name="uangMukaPercent" id="uangMukaPercent" placeholder="" aria-label="" size="1">
																<span class="input-group-text">%</span>
															</div>
														</div>
													</div>
												</div>

												<div class="row mt-2">
													<div class="col-lg-6">
														<div set-default="<?= intval($pureHarga) ?>" set-max="<?= intval($pureHarga)*1.5 ?>" set-min="<?= intval($pureHarga)*0.5 ?>" class="m-ion-range-slider" id="sliderHargaProperti">
															<input type="hidden" id="inputSliderHargaProperti" />
														</div>
													</div>
													<div class="col-lg-6">
														<div set-max="100" set-min="0" class="m-ion-range-slider" id="sliderUangMuka">
															<input type="hidden" id="inputSliderUangMuka" />
														</div>
													</div>
												</div>

												<div class="row mt-4">
													<div class="col lg-6">
														<label class="txt-bold" for="jangkaWaktuAngsuran">Jangka Waktu Angsuran</label>
														<div class="input-group">
															<input type="number" class="form-control" name="jangkaWaktuAngsuran" id="jangkaWaktuAngsuran" placeholder="" aria-label="">
															<div class="input-group-append">
																<span class="input-group-text">Thn</span>
															</div>
														</div>
													</div>
													<div class="col lg-6">
														<label class="txt-bold" for="bungaFixedTahunan">Bunga Fixed Tahunan</label>
														<div class="input-group">
															<input type="number" class="form-control" name="bungaFixedTahunan" id="bungaFixedTahunan" placeholder="" aria-label="">
															<div class="input-group-append">
																<span class="input-group-text">%</span>
															</div>
														</div>
													</div>
												</div>
												
												<div class="row mt-2">
													<div class="col-lg-6">
														<div set-max="30" set-min="0" class="m-ion-range-slider" id="sliderJangkaWaktuAngsuran">
															<input type="hidden" id="inputSliderJangkaWaktuAngsuran" />
														</div>
													</div>
													<div class="col-lg-6">
														<div set-max="50" set-min="0" class="m-ion-range-slider" id="sliderBungaFixedTahunan">
															<input type="hidden" id="inputBungaFixedTahunan" />
														</div>
													</div>
												</div>



											</div>
										</div>
									</div>

									<!--end::Item-->
								</div>
								<?php 
								if (isset($dataRecent) && count($dataRecent->data)>0) {
									$row = "";
									$row .= "<div class='m-accordion m-accordion--bordered section-to-hide section-to-clean' id='m_accordion_5' role='tablist'>";
									$row .= "	<!--begin::Item Jika Jenis transaksi = Beli -->";
									$row .= "	<div class='m-accordion__item m-accordion__item--solid'>";
									$row .= "		<div class='m-accordion__item-head activeable active bg-red borderless' active-group='detail-group5' role='tab' id='m_accordion_5_item_1_head' data-toggle='collapse' href='#m_accordion_5_item_1_body' aria-expanded='    true'>";
									$row .= "			<span class='m-accordion__item-icon'><i class='fa  flaticon-alert-2'></i></span>";
									$row .= "			<span class='m-accordion__item-title'>Listing Baru Saja Dilihat</span>";
									$row .= "			<span class='m-accordion__item-mode'></span>";
									$row .= "		</div>";
									$row .= "		<div class='m-accordion__item-body collapse show' id='m_accordion_5_item_1_body' class=' ' role='tabpanel' aria-labelledby='m_accordion_5_item_1_head' data-parent='#m_accordion_5'>";
									$row .= "			<div class='m-accordion__item-content row'>";
									foreach ($dataRecent->data as $key => $recent) { 
										if ($recent->jenis_transaksi == "0"){
											$jenis = "Jual";
											$bgJenis = "jual";
											$harga = $recent->harga_jual;
										}elseif ($recent->jenis_transaksi == "1"){
											$jenis = "Sewa";
											$bgJenis = "sewa";
											$harga = $recent->harga_sewa . "/thn";
										}else{
											$jenis = "Jual Sewa";
											$bgJenis = "jual_sewa";
											$harga = $recent->harga_jual . " - " . $recent->harga_sewa . "/thn";;
										}
										$gambar = [];
										for ($z=1; $z <= $recent->gambar; $z++){
											$gambar[] = "/assets/img/listing/".$recent->id_listing."/".$z.".jpg";
										}
										$gambar = json_encode($gambar, JSON_OBJECT_AS_ARRAY);

										$fiturDalam = "";
										if ($recent->kamar_tidur != null) {
											$fiturDalam .= "<div class='listing-detail-pills flex-4 txt-center'><i class='fas fa-bed'></i><br>".$recent->kamar_tidur."</div>";
										}
										if ($recent->kamar_mandi != null){
											$fiturDalam .= "<div class='listing-detail-pills flex-4 txt-center'><i class='fas fa-bath'></i><br>".$recent->kamar_mandi."</div>";
										}
										if ($recent->luas_tanah != null){
											$fiturDalam .= "<div class='listing-detail-pills flex-4 txt-center'><i class='fas fa-square'></i><br>".$recent->luas_tanah."m<sup>2</sup></div>";
										}
										if ($recent->luas_bangunan != null){
											$fiturDalam .= "<div class='listing-detail-pills flex-4 txt-center'><i class='fas fa-building'></i><br>".$recent->luas_bangunan."m<sup>2</sup></div>";
										}
										$favorite = "";
										if ($recent->favorite=="1"){
											$favorite = "favorite";
										}
										// My name is "$name". I am printing some $foo->foo.
										// Now, I am printing some {$foo->bar[1]}.
										// This should print a capital 'A': \x41
										$row.= "";
										$row.= "<div class='col-lg-6 col-sm-12 mb-3 px-2'>";
										$row.= "	<div class=' mb-0 listing-item m-portlet m-portlet--rounded' data-id='".$recent->id_listing."' data-telp='".$recent->telp_broker."' data-judul='".$recent->judul_listing."' data-lat='".$recent->lat_listing."' data-lng='".$recent->long_listing."' data-tipe='".$recent->nama_tipe."' data-jenis='".$jenis."'>";
										$row.= "		<div class='m-portlet__head sensecode-slider initiate' data-index='0' data-files='".$gambar."'>";
										$row.= "			<div class='listing-slider-badge jenis-".$bgJenis."'>".$jenis."</div>";
										$row.= "			<div class='listing-slider-icon jenis-".$bgJenis."'><i class='fas fa-home'></i></div>";
										$row.= "			<div class='listing-slider-counter' style='z-index:2;'>1 of 3</div>";
										$row.= "		</div>";
										$row.= "		<div class='m-portlet__body' style='padding:0px;'>";
										$row.= "			<div class='col-lg-10 col-md-10 col-sm-12' style='padding: 10px 5px 10px 10px;'>";
										$row.= "				<div class='listing-price listing-info'>".$harga."</div>";
										$row.= "				<div class='listing-title listing-info truncate hoverable'><b>".$recent->judul_listing."</b></div>";
										$row.= "				<div class='listing-jalan listing-info truncate'>".$recent->alamat_listing."</div>";
										$row.= "				<div class='listing-kota listing-info truncate'>".$recent->nama_kecamatan.", ".$recent->nama_kota."</div>";
										$row.= "				<div class='listing-detail'>";
										$row.= "					".$fiturDalam."";
										$row.= "				</div>";
										$row.= "			</div>";
										$row.= "			<div class='col-lg-2 col-md-2 col-sm-12 listing-content-right' style='padding:10px 10px 10px 5px'>";
										$row.= "				<div class='listing-content-icon' style='margin:auto;'><i onclick='sendWA(this);' class='fab fa-whatsapp fa-lg' data-skin='white' data-toggle='m-popover' data-placement='top' data-content='Pesan WhatsApp'></i></div>";
										$row.= "				<div class='listing-content-icon' style='margin:auto;'><i onclick='favorite(this);' class='fas fa-heart fa-lg ".$favorite."' data-skin='white' data-toggle='m-popover' data-placement='top' data-content='Favorit'></i></div>";
										$row.= "				<div class='listing-content-icon' style='margin:auto;'><i onclick='shareLink(this);' class='fas fa-share fa-lg' data-skin='white' data-toggle='m-popover' data-placement='top' data-content='Bagikan'></i></div>";
										$row.= "				<a href='/detaillisting/view/".$recent->id_listing."' style='margin:auto;' class='listing-content-icon txt-black'><i class='fas fa-ellipsis-h fa-lg' data-skin='white' data-toggle='m-popover' data-placement='top' data-content='Tampilkan Lebih Banyak'></i></a>";
										$row.= "			</div>";
										$row.= "		</div>";
										$row.= "	</div>";
										$row.= "</div>";
									} 
									$row.="			</div>";
									$row.="		</div>";
									$row.="	</div>";
									$row.="	<!--end::Item-->";
									$row.="</div>";
									echo $row;
								} 
								?>
								<!--end::Section-->
				</div>
				<div class="sidebar" id="data-listing" style="" data-id="<?=$dataListing->id_listing?>" data-judul="<?=$dataListing->judul_listing?>" data-broker="<?=$dataBroker->id_broker?>">
					
					<div class="booking-form section-to-hide" style="width:100%">
				
						<div data-toggle="modal" data-target="#m_modal_6" style="cursor:pointer;height:100px; margin: 0px 50px;"  class="map-banner section-to-clean"></div>
						
						<div class=" section-to-clean flex flex-hor flex-vertical-center flex-centered pt-2" style="width:50%; margin-left: auto; margin-right: auto;">
							<div class="flex flex-ver flex-centered txt-center" style="width:25%;">
								<?php
									$favorite = "";
									$xout = "";
									if ($dataListing->favorite==1){
										$favorite = "favorite";
									}
									$xout = "";
									if ($dataListing->xout==1){
										$xout = "xout";
									}
								?>
								<div onclick="favorite(this);" class="mini-btn squared <?=$favorite?>"><i class="fa fa-heart"></i></div>
								<div class="mini-txt">Favorite</div>
							</div>
							<div class="flex flex-ver flex-centered txt-center" style="width:25%;">
								<div onclick="xout(this);" class="mini-btn squared <?=$xout?>"><i class="fa fa-times"></i></div>
								<div class="mini-txt">X-Out</div>
							</div>
							<div class="flex flex-ver flex-centered txt-center" style="width:25%;">
								<div onclick="shareLink(this);" class="mini-btn squared"><i class="fa fa-share"></i></div>
								<div class="mini-txt">Bagi</div>
							</div>
							<div class="flex flex-ver flex-centered txt-center" style="width:25%;">
								<div onclick="window.printMe();" class="mini-btn squared"><i class="fa fa-print"></i></div>
								<div class="mini-txt">Print</div>
							</div>
							
						</div>
						<div class=" section-to-clean booking-form flex flex-ver flex-vertical-center txt-center">
							<h3>Lihat Property Ini</h3>
							<!-- <div id="actions"></div> -->
							<div class="flex flex-hor flex-vertical-center custom-date-picker" style="max-width:100%;">
								<i class="fa fa-chevron-left date-swipe-left" style="font-size:2.5em;"></i>
								<div class="flex flex-hor date-item-container">
									<div class="date-item flex flex-ver txt-center padding-m">
										<div>[Hari]</div>
										<div>[H]</div>
										<div>[MMM]</div>
									</div>
									<div class="date-item active flex flex-ver txt-center padding-m">
										<div>[Hari]</div>
										<div>[H]</div>
										<div>[MMM]</div>
									</div>
									<div class="date-item flex flex-ver txt-center padding-m">
										<div>[Hari]</div>
										<div>[H]</div>
										<div>[MMM]</div>
									</div>
								</div>
								<i class="fa fa-chevron-right date-swipe-right" style="font-size:2.5em;"></i>
							</div>
							
							
							<div class="flex flex-ver timepick" style="display:none">
								<div class="flex-equal flex-hor flex lastnorm">
									<div class="margin-col-sm rounded-s mini-btn activeable-solo bg-red active" active-group="time-booking">13:00</div>
									<div class="margin-col-sm rounded-s mini-btn activeable-solo bg-red" active-group="time-booking">14:00</div>
									<div class="margin-col-sm rounded-s mini-btn activeable-solo bg-red" active-group="time-booking">15:00</div>
									<div class="margin-col-sm rounded-s mini-btn activeable-solo bg-red" active-group="time-booking">16:00</div>
								</div>
								<div class="flex-12 flex-equal flex flex-hor">
									<div class="margin-col-sm rounded-s mini-btn activeable-solo bg-red" active-group="time-booking">17:00</div>
									<div class="margin-col-sm rounded-s mini-btn activeable-solo bg-red" active-group="time-booking">18:00</div>
									<div class="margin-col-sm rounded-s mini-btn activeable-solo bg-red" active-group="time-booking">19:00</div>
									<div class="margin-col-sm rounded-s mini-btn activeable-solo bg-red" active-group="time-booking">20:00</div>
								</div>
							</div>
							
							<div class="bookingbtn clickable" onclick="jadwalkan(this);">Jadwalkan</div>
						</div>
						<div class="author-card flex flex-hor section-to-show">
							<div class="author-img set-bg" style="" data-setbg="/assets/img/profile-broker/<?=$dataBroker->foto_broker?>"></div>
							<div class="author-contact" style="padding:0px; margin-left:10px;">
								<h5>
									<a href="javascript:void(0)"><?=$dataBroker->nama_broker?></a>
								</h5>
								<div>
									<a href="javascript:void(0)">Era Kita Cabang Pakuwon</a>
								</div>
								<div><a onclick="sendWA(this);" class="hoverable" style="color:lightgray"><i style="color: #128C7E; font-size: 1.3em;" class="fa fa-whatsapp"></i>+<?=$dataBroker->telp_broker?></a></div>
								<div><a onclick="sendWA(this);" class="hoverable" style="color:lightgray"><i style="color: #128C7E; font-size: 1.3em;" class="fa fa-phone"></i>+<?=$dataBroker->telp_broker?></a></div>
								<!-- <a href="mailto:williams.gunawan123@gmail.com" class="hoverable"><p style="width:215px;white-space: nowrap; text-overflow:ellipsis; overflow:hidden;"><i style="color: #c41c00; font-size: 1.3em;" class="fa fa-envelope"></i><?=$dataBroker->email_broker?></p></a> -->
							</div>
						</div>
					</div>
					
					<div class="related-properties" style="display:none;">
						<h2>Related Property</h2>
						<div class="rp-item">
							<div class="rp-pic set-bg" data-setbg="<?= base_url('assets/img/feature/1.jpg')?>">
								<div class="sale-notic">FOR SALE</div>
							</div>
							<div class="rp-info">
								<h5>1963 S Crescent Heights Blvd</h5>
								<p><i class="fa fa-map-marker"></i>Los Angeles, CA 90034</p>
							</div>
							<a href="#" class="rp-price">$1,200,000</a>
						</div>
						<div class="rp-item">
							<div class="rp-pic set-bg" data-setbg="<?= base_url('assets/img/feature/2.jpg')?>">
								<div class="rent-notic">FOR Rent</div>
							</div>
							<div class="rp-info">
								<h5>17 Sturges Road, Wokingham</h5>
								<p><i class="fa fa-map-marker"></i> Newtown, CT 06470</p>
							</div>
							<a href="#" class="rp-price">$2,500/month</a>
						</div>
						<div class="rp-item">
							<div class="rp-pic set-bg" data-setbg="/assets/img/feature/4.jpg">
								<div class="sale-notic">FOR SALE</div>
							</div>
							<div class="rp-info">
								<h5>28 Quaker Ridge Road, Manhasset</h5>
								<p><i class="fa fa-map-marker"></i>28 Quaker Ridge Road, Manhasset</p>
							</div>
							<a href="#" class="rp-price">$5,600,000</a>
						</div>
						<div class="rp-item">
							<div class="rp-pic set-bg" data-setbg="<?= base_url('assets/img/feature/5.jpg')?>">
								<div class="rent-notic">FOR Rent</div>
							</div>
							<div class="rp-info">
								<h5>Sofi Berryessa 750 N King Road</h5>
								<p><i class="fa fa-map-marker"></i>Sofi Berryessa 750 N King Road</p>
							</div>
							<a href="#" class="rp-price">$1,600/month</a>
						</div>
					</div>
				</div>
			</div>	
			<input type="hidden" id="lat" value="<?=$dataListing->lat_listing?>"/>
			<input type="hidden" id="lng" value="<?=$dataListing->long_listing?>"/>
	</section>
	<!-- Page -->
	<!-- <section class="page-section">
		<div class="container">
			
			<div class="row">
                
			
				
			</div>
		</div> -->
	</section>
	<!-- Page end -->

	<?php 
		$this->load->view('page-part/common-foot');
	?>

	<!-- load for map -->
 	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyJi7G0CbtkyT9qjITRHKHcg8tKCZ1tpk&v=3.exp&libraries=places"></script> 
	<script src="/assets/js/map-2.js"></script>

	<script type="text/javascript" src="/assets/json/data.json"></script>
	<script type="text/javascript" src="/assets/js/markerclusterer.js"></script>

	<script>
		var map;
		var infowindow;
		var center;
		var markers = [];
		var radius = 1000;
		function initialize() {
			center = new google.maps.LatLng($("#lat").val(), $("#lng").val());
			var myStyles =[
				{
					featureType: "poi",
					elementType: "labels",
					stylers: [
						{ visibility: "off" }
					]
				}
			];
			map = new google.maps.Map(document.getElementById('mapListing'), {
				center: center,
				zoom: 16,
				styles: myStyles,
				scrollwheel: true,
  				zoomControl: true
			});
			var marker = new google.maps.Marker({
				map: map,
				position: center
			});
			updateMarker('1','0');
		}

		function updateMarker(idxListing, indexPubPlace){
			if (idxListing == '1'){
				$("#detailPublic").removeAttr("hidden");
				if (indexPubPlace != $("#detailPublic").attr("data-selected")){
					if (indexPubPlace == '-2'){
						indexPubPlace = $("#detailPublic").attr("data-selected");
					}
					$("#nearbyPlaceCard").html("");
					for (var i=0; i<markers.length; i++){
						markers[i].setMap(null);
					}
					markers = [];
					if (indexPubPlace == '0'){
						requestNearby('school');
						requestNearby('atm');
						requestNearby('hospital');
						requestNearby('church');
						requestNearby('hindu_temple');
						requestNearby('mosque');
					}else if (indexPubPlace == '1'){
						requestNearby('airport');
						requestNearby('bus_station');
						requestNearby('train_station');
					}else if (indexPubPlace == '2'){
						requestNearby('cafe');
						requestNearby('clothing_store');
						requestNearby('convenience_store');
						requestNearby('department_store');
						requestNearby('gym');
						requestNearby('supermarket');
						requestNearby('shopping_mall');
					}
				}
				$("#detailPublic").attr("data-selected", indexPubPlace);
			}else{
				$("#nearbyPlaceCard").html("");
				for (var i=0; i<markers.length; i++){
					markers[i].setMap(null);
				}
				markers = [];
				$("#detailPublic").attr("hidden","hidden");
			}
		}

		function requestNearby(types){
			var request = {
				location: center,
				radius: radius,
				type: types // this is where you set the map to get the hospitals and health related places
			};
			infowindow = new google.maps.InfoWindow();
			var service = new google.maps.places.PlacesService(map);
			service.nearbySearch(request, callback);
		}

		function callback(results, status) {
			createMapNearbyCard(results);
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				for (var i = 0; i < results.length; i++) {
					createMarker(results[i]);
				}
			}
		}

		function createMarker(place) {
			var placeLoc = place.geometry.location;

			var icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(0, 0),
				scaledSize: new google.maps.Size(25, 25)
			};

			var marker = new google.maps.Marker({
				map: map,
				icon: icon,
				position: place.geometry.location
			});

			markers.push(marker);
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(place.name);
				infowindow.open(map, this);
			});
		}

		google.maps.event.addDomListener(window, 'load', initialize);

		function createMapNearbyCard(data){
			if (data[0] != undefined){
				console.log(data[0])
				console.log(data[0].geometry.location)
				var dom = '';
				dom += '<div class="mapNearPlace">';
				dom += '<div class="flex-flex-ver mapNearItemContainer">';
				dom += '<h6 class="mapNearCategory txt-lg" style="text-transform:uppercase;">' + data[0].types[0] + ' <span><i class="fa fa-times"></i></span></h6>';
				dom += `<div  style="max-height:200px; overflow-y:auto;" class='minimalistScrollbar'>`;

				data.forEach(e => {
					dom += '<div class="flex flex-hor flex-vertical-center mapItem flex-separate">';
					dom += '	<div class="mapItemName truncate">' + e.name + '</div>';
					dom += '		<div class="flex flex-hor flex-vertical-center">';
					dom += '			<i class="fa fa-clock" style="margin-right:10px; font-size:1.25em;"></i>';
					dom += '			<div class="flex flex-ver">';
					dom += '				<div class="map-time">3 Mins</div>';
					dom += '				<div class="map-far">230 m</div>';
					dom += '			</div>';
					dom += '		</div>';
					dom += '</div>';
				});

				dom += `</div>`;
				dom += '</div>';
				dom += '</div>';  

				$("#nearbyPlaceCard").append(dom);
			}
		}
  	</script> 
</body>
</html>