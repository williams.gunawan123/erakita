<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/loginregister2.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
</head>
<body>
    <div class="container" style="height:100vh">
        <div class="row" style="height:100%; position:relative;">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto" style="position:absolute; top:50%; left:50%; transform:translate(-50%, -50%);">
                <div class="card card-signin">
                    <div class="card-body">
                    <h5 class="card-title text-center">Sign Up</h5>
                    <form class="form-signin">

                        <div class="form-label-group">
                            <input type="text" id="regisName" class="form-control" style="" placeholder="Full Name" required autofocus>
                            <label for="regisName">Full Name</label>
                        </div>

                        <div class="form-label-group">
                            <input type="email" id="regisEmail" class="form-control" style="" placeholder="Email address" required autofocus>
                            <label for="regisEmail">Email address</label>
                        </div>


                        
                        <div class="form-label-group">
                            <input type="text" id="regisTelephone" class="form-control" placeholder="Telephone" required>
                            <label for="regisTelephone">Telephone</label>
                        </div>

                        <div class="form-label-group">
                            <input type="password" id="regisPassword" class="form-control" placeholder="Password" required>
                            <label for="regisPassword">Password</label>
                        </div>

                        
                        <div class="form-label-group">
                            <input type="password" id="regisConfirmPassword" class="form-control" placeholder="Confirm Password" required>
                            <label for="regisConfirmPassword">Confirm Password</label>
                        </div>


                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="button" onclick="register();">Sign up</button>
                        
                        <a href="/login" class="btn btn-lg btn-info btn-block text-uppercase" >Back to Login</a>
                    
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>