<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->

	<section class="my-container flex flex-ver">
    
        <!-- Breadcrumb -->
        <div class="site-breadcrumb pb-3 row" style="margin-top: 50px;">
            <div class="">
                <a href="/"><i class="fa fa-home"></i>Home&nbsp;</a>
                <a href="/proyek"><i class="fa fa-angle-right"></i>Proyek</a>
                <span><i class="fa fa-angle-right"></i><?=$dataProyek->nama_proyek?></span>
            </div>
        </div>
		<div class="row" id="dataproyek" data-id="<?=$dataProyek->id_proyek?>" style="justify-content: left; align-content: flex-end;">
            <div class="col-sm-12" style="background-image: url('/assets/img/proyek/<?=$dataProyek->photo_proyek?>'); background-repeat: no-repeat; background-position: center; background-size: cover; height: 60vh; padding: 0px;" align="center">
                <div style="background: linear-gradient(0deg, rgba(0,0,0,1) 0%, rgba(0,0,0,0) 100%); width: 100%; height: 27vh; position: absolute; bottom: 0px;"></div>
                <div style="position: absolute; color: white; padding: 20px; width: 25%; bottom: 10px;">
                    <div style="border-bottom: dotted #aaa 1px; padding: 10px;">
                        <span style="font-weight: bold; font-size: 1.8em;"></span><?=$dataProyek->nama_proyek?></span><br/>
                        <span>By <?=$dataProyek->nama_developer?></span><div style="font-weight: bold;margin-top:5px;">
                        <span class="fa fa-map-marker">&nbsp;</span><?=$dataProyek->nama_kecamatan?>, <?=$dataProyek->nama_kota?></div>
                    </div>
                    <div class="flex flex-ver" style="padding: 10px;">  
                        <!-- <table>
                        <?php
                            if ($dataProyek->min_tidur == $dataProyek->max_tidur){
                                $tidur = $dataProyek->min_tidur;
                            }else{
                                $tidur = $dataProyek->min_tidur." - ".$dataProyek->max_tidur;
                            }
                            if ($dataProyek->min_bangunan == $dataProyek->max_bangunan){
                                $bangunan = $dataProyek->min_bangunan ."m<sup>2</sup>";
                            }else{
                                $bangunan = $dataProyek->min_bangunan." - ".$dataProyek->max_bangunan ."m<sup>2</sup>";
                            }
                        ?> -->
                            <!-- <tr><td>Tipe Properti</td><td>:</td><td style="font-weight: bold; font-size: 1.2em;">Rumah</td></tr> -->
                            <!-- <tr><td>Luas</td><td>:</td><td style="font-weight: bold; font-size: 1.2em;"><?=$bangunan?></td></tr> -->
                            <!-- <tr><td>Bedroom</td><td>:</td><td style="font-weight: bold; font-size: 1.2em;"><?=$tidur?></td></tr> -->
                            <tr><td>Starting from</td><td>:</td><td style="font-weight: bold; font-size: 1.2em;">Rp <?=number_format($dataProyek->min_jual)?></td></tr>
                        </table>
                    </div>
                </div>
			</div>
			
        </div>
        <div class="row">
            <div class="col-md-7 px-0">
                <div class="col-md-12 mt-2 px-0">
                    <h5>Deskripsi</h5><br>
                    <p><?=$dataProyek->deskripsi_proyek?></p> 
                </div>
                <hr class="mb-1">
                <div class="col-md-12 px-0">
                    <div class="listing-part-header-left">
                        <div class="listing-part-header-title">
                            <h5>Menampilkan <span id="showedListing">0</span> dari <span id="totalListing">0</span> jenis</h5>
                        </div>			
                        <div class="listing-part-header-order" style="position:relative">
                            <div onclick="$(this).parent().find('.sorter-tools').fadeToggle();" style="cursor:pointer;" class="hoverable">
                                <i class="fa fa-filter"></i>
                                Urutkan
                            </div>
                            <div class="card flex flex-ver shadow sorter-tools" style="display:none; position:absolute; background-color:white; z-index:10;width:250%; transform:translateX(-50%);    padding-top: 15px;clip-path: polygon(0px 15px, 40% 15px, 50% 0px, 60% 15px, 100% 15px, 100% 100%, 0px 100%);">
                                <div class="flex flex-hor flex-equal" style="" id="urutan">
                                    <div active-group="sortir-alphabet" onclick="urutkanUnitAZ('asc');" class="txt-center activeable-solo hoverable txt-bold animated active" data-value="asc">A-Z</div>
                                    <div active-group="sortir-alphabet" onclick="urutkanUnitAZ('desc');" class="txt-center activeable-solo hoverable txt-bold animated" data-value="desc">Z-A</div>
                                </div>
                                <div class="flex-ver pl-3">
                                    <!-- <div class="hoverable" onclick="sort('');">Recommended</div> -->
                                    <div class="hoverable" data-value="startlisting" id="sort-startlisting" onclick="urutkanUnit(this);">Terbaru <i class="fa fa-check"></i></div>
                                    <div class="hoverable" data-value="harga" id="sort-harga" onclick="urutkanUnit(this);">Harga</div>
                                    <div class="hoverable" data-value="kamartidur" id="sort-kamartidur" onclick="urutkanUnit(this);">Kamar Tidur</div>
                                    <div class="hoverable" data-value="kamarmandi" id="sort-kamarmandi" onclick="urutkanUnit(this);">Kamar Mandi</div>
                                    <div class="hoverable" data-value="luastanah" id="sort-luastanah" onclick="urutkanUnit(this);">Luas Tanah</div>
                                    <div class="hoverable" data-value="luasbangunan" id="sort-luasbangunan" onclick="urutkanUnit(this);">Luas Bangunan</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 px-0">
                    <div style="flex-wrap:wrap;" class="flex flex-hor" id="listUnit">
                        <!-- data proyek -->
                        
                    	<!-- <div class="col-lg-6 col-sm-12 mb-3 px-2">
                    		<div onclick='selectListingCard(this);' class=" mb-0 listing-item m-portlet m-portlet--rounded" data-idx='`+el.indexMarker+`' data-id='`+e.id_listing+`' data-telp='`+e.telp_broker+`' data-judul='`+e.judul_listing+`' data-lat='`+e.lat_listing+`' data-lng='`+e.long_listing+`' data-tipe='`+e.nama_tipe+`' data-jenis='`+jenis+`'>
                    			<div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='`+gambar+`'>
                    				<div class="listing-slider-badge jenis-`+bgJenis+`">` + jenis + `</div>
                    				<div class="listing-slider-icon jenis-`+bgJenis+`"><i class="fas fa-`+icon+`"></i></div>
                    				<div class="listing-slider-counter" style="z-index:2;">1 of 3</div>
                    			</div>
                    			<div class="m-portlet__body" style="padding:0px;">
                    				<div class="col-lg-10 col-md-10 col-sm-12" style="padding: 10px 5px 10px 10px;">
                    					<div class="listing-price listing-info">` + harga + `</div>
                    					<div class="listing-title listing-info truncate hoverable" onclick="viewDetail(this);"><b>` + e.judul_listing + `</b></div>
                    					<div class="listing-detail">
                    						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>`+e.kamar_tidur+`</div>		
                    						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>`+e.kamar_mandi+`</div>
                    						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>`+e.luas_tanah + `m<sup>2</sup></div>
                    						<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>`+e.luas_bangunan + `m<sup>2</sup></div>
                    					</div>
                    				</div>
                    				<div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding:10px 10px 10px 5px">
                    					<div class="listing-content-icon" style="margin:auto;"><i onclick='favorite(this);' class="fas fa-heart fa-lg ` + favorite + `" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Favorit"></i></div>
                    					<div class="listing-content-icon" style="margin:auto;"><i onclick='shareLink(this);' class="fas fa-share fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Bagikan"></i></div>
                    					<a href="/detaillisting/view/`+e.id_listing+`" style="margin:auto;" class="listing-content-icon txt-black"><i class="fas fa-ellipsis-h fa-lg" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Tampilkan Lebih Banyak"></i></a>
                    				</div>
                    			</div>
                    		</div>
                    	</div> -->

                    </div>
                    <div class="sensecode-paginate" style="margin-top:20px;" id="listing-pagination"></div>
                </div>
            </div>
            <div class="col-md-5 align-self-start px-0" style="position:sticky; top:70px; left:0px;">
                <!--Map Section -->
				<div class="col-md-12 single-list-page hide-on-small-only" id="mapDiv" style="height:30vh;"></div>
				<!--End of map Section -->
				<div class="author-card flex flex-hor" id="databroker" data-id="<?=$dataBroker->id_broker?>">
                    <div class="author-img set-bg" style="" data-setbg="/assets/img/profile-broker/<?=$dataBroker->foto_broker?>"></div>
                    <div class="author-contact" style="padding:0px; margin-left:10px;">
                        <h5>
                            <a href="/broker/detail/<?=$dataBroker->id_broker?>"><?=$dataBroker->nama_broker?></a>
                        </h5>
                        <div>
                            <a href="javascript:void(0)">Era Kita Cabang Pakuwon</a>
                        </div>
                        <div><a onclick="sendWA(this);" class="hoverable" style="color:lightgray"><i style="color: #128C7E; font-size: 1.3em;" class="fa fa-whatsapp"></i>+<?=$dataBroker->telp_broker?></a></div>
                        <div><a onclick="sendWA(this);" class="hoverable" style="color:lightgray"><i style="color: #128C7E; font-size: 1.3em;" class="fa fa-phone"></i>+<?=$dataBroker->telp_broker?></a></div>

                        <!-- <a href="/broker/detail/<?=$dataBroker->id_broker?>" class="txt-black hoverable"><h5><?=$dataBroker->nama_broker?></h5></a>
                        <a onclick="sendWA(this);"><p><i style="color: #128C7E; font-size: 1.3em;" class="fa fa-whatsapp"></i>+<?=$dataBroker->telp_broker?></p></a>
                        <a href="mailto:williams.gunawan123@gmail.com"><p style="width:215px;white-space: nowrap; text-overflow:ellipsis; overflow:hidden;"><i style="color: #c41c00; font-size: 1.3em;" class="fa fa-envelope"></i><?=$dataBroker->email_broker?></p></a> -->
                    </div>
                </div>
            </div>
        </div>
        
	</section>
	<!-- Page -->
	<section class="page-section">
		<div class="container">

			<div class="row">

				<!-- sidebar -->

			</div>
		</div>
	</section>
	<!-- Page end -->

	<?php 
		$this->load->view('page-part/common-foot');
	?>
    <script src="https://cdn.rawgit.com/hayeswise/Leaflet.PointInPolygon/v1.0.0/wise-leaflet-pip.js"></script>
</body>

</html>
