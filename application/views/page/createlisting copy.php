<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->
	<!-- Header section -->
	<?php $this->load->view('page-part/header-saya');?>
	<!-- Header section end -->
	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->
	<div class="m-content">
		<div class="row">
		<div class="col-xl-2"></div>
		<div class="col-xl-8" style="padding-top: 110px;">

			<!--Begin::Main Portlet-->
			<div class="m-portlet">

				<!--begin: Portlet Head-->
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Create Listing
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="#" data-toggle="m-tooltip" class="m-portlet__nav-link m-portlet__nav-link--icon" data-direction="left" data-width="auto" title="" data-original-title="Get help with filling up this form">
									<i class="flaticon-info m--icon-font-size-lg3"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>

				<!--end: Portlet Head-->

				<!--begin: Form Wizard-->
				<div class="m-wizard m-wizard--1 m-wizard--success m-wizard--step-first" id="m_wizard">

					<!--begin: Message container -->
					<div class="m-portlet__padding-x">

						<!-- Here you can put a message or alert -->
					</div>

					<!--end: Message container -->

					<!--begin: Form Wizard Head -->
					<div class="m-wizard__head m-portlet__padding-x">

						<!--begin: Form Wizard Progress -->
						<div class="m-wizard__progress">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: calc(0% + 26px);"></div>
							</div>
						</div>

						<!--end: Form Wizard Progress -->

						<!--begin: Form Wizard Nav -->
						<div class="m-wizard__nav">
							<div class="m-wizard__steps">
								<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
									<div class="m-wizard__step-info">
										<a href="#" class="m-wizard__step-number">
											<span><span>1</span></span>
										</a>
										<div class="m-wizard__step-line">
											<span></span>
										</div>
										<div class="m-wizard__step-label">
											Deskripsi
										</div>
									</div>
								</div>
								<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
									<div class="m-wizard__step-info">
										<a href="#" class="m-wizard__step-number">
											<span><span>2</span></span>
										</a>
										<div class="m-wizard__step-line">
											<span></span>
										</div>
										<div class="m-wizard__step-label">
											Lokasi
										</div>
									</div>
								</div>
								<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
									<div class="m-wizard__step-info">
										<a href="#" class="m-wizard__step-number">
											<span><span>3</span></span>
										</a>
										<div class="m-wizard__step-line">
											<span></span>
										</div>
										<div class="m-wizard__step-label">
											Detail
										</div>
									</div>
								</div>
								<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_4">
									<div class="m-wizard__step-info">
										<a href="#" class="m-wizard__step-number">
											<span><span>4</span></span>
										</a>
										<div class="m-wizard__step-line">
											<span></span>
										</div>
										<div class="m-wizard__step-label">
											Media
										</div>
									</div>
								</div>
								<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_4">
									<div class="m-wizard__step-info">
										<a href="#" class="m-wizard__step-number">
											<span><span>5</span></span>
										</a>
										<div class="m-wizard__step-line">
											<span></span>
										</div>
										<div class="m-wizard__step-label">
											Konfirmasi
										</div>
									</div>
								</div>
							</div>
						</div>

						<!--end: Form Wizard Nav -->
					</div>

					<!--end: Form Wizard Head -->

					<!--begin: Form Wizard Form-->
					<div class="m-wizard__form">

						<!--
	1) Use m-form--label-align-left class to alight the form input lables to the right
	2) Use m-form--state class to highlight input control borders on form validation
-->
						<form action="/test" class="m-form m-form--label-align-left- m-form--state-" id="m_form" novalidate="novalidate" enctype="multipart/form-data" method="post">
							<label class="col-xl-3 col-lg-3 col-form-label">* wajib diisi</label>
							<!--begin: Form Body -->
							<div class="m-portlet__body">

								<!--begin: Form Wizard Step 1-->
								<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
									<div class="row">
										<div class="col-xl-4">
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">Property Deskripsi</h3>
													<label class="col-form-label">Mencakup judul dan deskripsi singkat properti anda</label>
												</div>		
											</div>
										</div>
										<div class="col-xl-8">	
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading form-group m-form__group row">
													<h3 class="m-form__heading-title">* Judul</h3>
													<input type="text" name="judul" class="form-control m-input" placeholder="Judul" minlength=10 maxlength=100 required/>
												</div>
												<div class="m-form__heading form-group m-form__group row">
													<h3 class="m-form__heading-title">* Deskripsi</h3>
												</div>
												<div class="m-form__heading form-group m-form__group row">
													<textarea name="deskripsi" class="summernote" id="m_summernote_1" required></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="m-separator m-separator--dashed m-separator--lg"></div>
									<div class="row">
										<div class="col-xl-4">
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">Tipe Properti & Jenis Transaksi</h3>
													<label class="col-form-label">Tipe Properti mencakup rumah, apartemen/condo, ruko, tanah, gudang, pabrik, dan lainnya. Sedangkan jenis transaksi adalah jual beli atau sewa</label>
												</div>		
											</div>
										</div>
										<div class="col-xl-4">	
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading form-group m-form__group row">
													<h3 class="m-form__heading-title">* Tipe Properti</h3>
													<select name="tipe" class="form-control m-bootstrap-select m_selectpicker" id="sec-tipe" onchange="updateTipe(this);">
													</select>
												</div>
											</div>
										</div>
										<div class="col-xl-4">	
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading form-group m-form__group row">
													<h3 class="m-form__heading-title">* Jenis Transaksi</h3>
													<select name="jenis" class="form-control m-bootstrap-select m_selectpicker" onchange="updateJenis(this);">
														<option value="0">Jual</option>
														<option value="1">Sewa</option>
														<option value="2">Jual - Sewa</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="m-separator m-separator--dashed m-separator--lg"></div>
									<div class="row">
										<div class="col-xl-4">
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">Harga</h3>
												</div>		
											</div>
										</div>
										<div class="col-xl-8">
											<div class="row" id="sec-harga">
												<div class="col-xl-4">	
													<div class="m-form__section m-form__section--first">
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Harga Jual</h3>
															<input type='text' name="harga_jual" class="form-control" id="m_inputmask_7"/>
														</div>
													</div>
												</div>
												<div class="col-xl-4" hidden>	
													<div class="m-form__section m-form__section--first">
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Harga Sewa</h3>
															<input type='text' name="harga_sewa" class="form-control" id="m_inputmask_8"/>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xl-4">	
													<div class="m-form__section m-form__section--first">
														<div class="m-form__heading">
															<h3 class="m-form__heading-title">* Nego</h3>
															<span class="m-bootstrap-switch m-bootstrap-switch--pill">
																<input data-switch="true" name="nego" type="checkbox" checked="checked" data-on-text="Yes" data-off-text="No">
															</span>
														</div>
													</div>
												</div>
												<div class="col-xl-4" id="sec-minim-sewa" hidden>	
													<div class="m-form__section m-form__section--first">
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Minim Sewa</h3>
															<input type='number' name="minim_sewa" class="form-control"/>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<!--end: Form Wizard Step 1-->

								<!--begin: Form Wizard Step 2-->
								<div class="m-wizard__form-step m-wizard__form-step" id="m_wizard_form_step_2">
									<div class="row">
										<div class="col-xl-4">
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">Lokasi Properti</h3>
													<label class="col-form-label">Masukkan lokasi properti anda yang ingin dijual	 </label>
												</div>		
											</div>
										</div>
										<div class="col-xl-8">	
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading form-group m-form__group row">
													<h3 class="m-form__heading-title">* Alamat</h3>
													<input type="text" name="jalan" id="jalan" class="form-control m-input" placeholder="Alamat" required/>
												</div>
												<div class="row">
													<div class="col-xl-4">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">Blok</h3>
															<input type="text" name="blok" id="blok" class="form-control m-input" placeholder="Blok"/>
														</div>
													</div>
													<div class="col-xl-4">	
														<div class="m-form__heading">
															<h3 class="m-form__heading-title">Nomor</h3>
															<input type="text" name="nomor" id="nomor" class="form-control m-input" placeholder="Nomor"/>
														</div>
													</div>
													<div class="col-xl-4">	
														<table><tr>
															<td style="vertical-align:top;">** </td>
															<td>Informasi Blok dan Nomor Rumah Tidak Akan Ditampilkan Pada Halaman Customer</td>
														</tr></table>
													</div>
												</div>
												<div class="row">
													<div class="col-xl-6">	
															<div class="m-form__heading form-group m-form__group row">
																<h3 class="m-form__heading-title">* Propinsi</h3>
																<input type="text" name="provinsi" id="provinsi" class="form-control m-input" placeholder="Propinsi" required/>
															</div>
													</div>
													<div class="col-xl-6">	
														<div class="m-form__heading">
															<h3 class="m-form__heading-title">* Kota</h3>
															<input type="text" name="kota" id="kota" class="form-control m-input" placeholder="Kota" required/>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xl-6">	
															<div class="m-form__heading form-group m-form__group row">
																<h3 class="m-form__heading-title">* Kecamatan</h3>
																<input type="text" name="kecamatan" id="kecamatan" class="form-control m-input" placeholder="Kecamatan" required/>
															</div>
													</div>
													<div class="col-xl-6">	
														<div class="m-form__heading">
															<h3 class="m-form__heading-title">* Kelurahan</h3>
															<input type="text" name="kelurahan" id="kelurahan" class="form-control m-input" placeholder="Kelurahan" required/>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xl-12">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Kode Pos</h3>
															<input type='text' name="kodepos" id="kodepos" class="form-control" maxlength=5 required/>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xl-12">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">Area</h3>
															<select class="form-control m-bootstrap-select m_selectpicker" multiple="multiple" name="area[]" id="sec-area">
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<input type="hidden" id="lat" name="lat"/>
													<input type="hidden" id="lng" name="lng"/>
													<div class="col-xl-12" style="height:400px">	
														<div id="map"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end: Form Wizard Step 2-->
								<!--begin: Form Wizard Step 3-->
								<div class="m-wizard__form-step m-wizard__form-step" id="m_wizard_form_step_3">
									<div class="row">
										<div class="col-xl-4">
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">Detail Properti</h3>
													<label class="col-form-label">Masukkan detail informasi mengenai rumah anda yang ingin dijual</label>
												</div>		
											</div>
										</div>
										<div class="col-xl-8">	
											<div class="m-form__section m-form__section--first">
												<div class="row">
													<div class="col-xl-6">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Panjang Tanah</h3>
															<div class="input-group">
																<input type="number" class="form-control m-input" placeholder="Panjang Tanah" aria-describedby="basic-addon2" name="panjang_tanah" id="panjang_tanah" onchange="updateLuas();" required>
																<div class="input-group-append"><span class="input-group-text" id="basic-addon2">m&nbsp;</span></div>
															</div>
														</div>
													</div>
													<div class="col-xl-6">	
														<div class="m-form__heading">
															<h3 class="m-form__heading-title">* Lebar Tanah</h3>
															<div class="input-group">
																<input type="number" class="form-control m-input" placeholder="Lebar Tanah" aria-describedby="basic-addon2" name="lebar_tanah" id="lebar_tanah" onchange="updateLuas();" required>
																<div class="input-group-append"><span class="input-group-text" id="basic-addon2">m&nbsp;</span></div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xl-6">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Luas Tanah</h3>
															<div class="input-group">
																<input type="number" class="form-control m-input" placeholder="Luas Tanah" aria-describedby="basic-addon2" name="luas_tanah" id="luas_tanah" required>
																<div class="input-group-append"><span class="input-group-text" id="basic-addon2">m<sup>2</sup></span></div>
															</div>
														</div>
													</div>
													<div class="col-xl-6">	
														<div class="m-form__heading">
															<h3 class="m-form__heading-title">* Luas Bangunan</h3>
															<div class="input-group">
																<input type="number" class="form-control m-input" placeholder="Luas Bangunan" aria-describedby="basic-addon2" name="luas_bangunan">
																<div class="input-group-append"><span class="input-group-text" id="basic-addon2">m<sup>2</sup></span></div>
															</div>
														</div>
													</div>
												</div>
												<div class="row non-tanah">
													<div class="col-xl-6">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Kamar Tidur</h3>
															<input type="number" class="form-control m-input" placeholder="Kamar Tidur" aria-describedby="basic-addon2" name="kamar_tidur">
														</div>
													</div>
													<div class="col-xl-6">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Kamar Mandi</h3>
															<input type="number" class="form-control m-input" placeholder="Kamar Mandi" aria-describedby="basic-addon2" name="kamar_mandi">
														</div>
													</div>
												</div>
												<div class="row non-tanah">
													<div class="col-xl-6">	
														<div class="m-form__heading">
															<h3 class="m-form__heading-title">* Tahun Dibangun</h3>
															<input type="number" class="form-control m-input" placeholder="Tahun Dibangun" aria-describedby="basic-addon2" name="tahun_dibangun">
														</div>
													</div>
													<div class="col-xl-6">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Orientasi Bangunan</h3>
															<select class="form-control m-bootstrap-select m_selectpicker" name="hadap">
																<option value="U">Utara</option>
																<option value="T">Timur</option>
																<option value="S">Selatan</option>
																<option value="B">Barat</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xl-6">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Sertifikat</h3>
															<select class="form-control m-bootstrap-select m_selectpicker" name="sertifikat" required>
																<option value="Sertifikat Hak Milik (SHM)">Sertifikat Hak Milik (SHM)</option>
																<option value="Hak Guna Bangunan (HGB)">Hak Guna Bangunan (HGB)</option>
															</select>
														</div>
													</div>
													<div class="col-xl-6">	
														<div class="m-form__heading">
															<h3 class="m-form__heading-title">* Hoek</h3>
															<span class="m-bootstrap-switch m-bootstrap-switch--pill">
																<input data-switch="true" type="checkbox" checked="checked" data-on-text="Yes" data-off-text="No" name="hoek">
															</span>
														</div>
													</div>
												</div>
												<div class="row non-tanah">
													<div class="col-xl-6">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Jumlah Lantai</h3>
															<input type="number" class="form-control m-input" placeholder="Jumlah Lantai" aria-describedby="basic-addon2" name="jumlah_lantai">
														</div>
													</div>
													<div class="col-xl-6">	
														<div class="m-form__heading">
															<h3 class="m-form__heading-title">* Garasi</h3>
															<input type="number" class="form-control m-input" placeholder="Garasi" aria-describedby="basic-addon2" name="jumlah_garasi">
														</div>
													</div>
												</div>
												<div class="row non-tanah">
													<div class="col-xl-6">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">* Interior</h3>
															<input type="text" class="form-control m-input" placeholder="Interior" name="interior"/>
														</div>
													</div>
													<div class="col-xl-6">	
														<div class="m-form__heading">
															<h3 class="m-form__heading-title">* Listrik</h3>
															<input type="number" class="form-control m-input" placeholder="Listrik" aria-describedby="basic-addon2" name="listrik">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<!--end: Form Wizard Step 3-->

								<!--begin: Form Wizard Step 4-->
								<div class="m-wizard__form-step m-wizard__form-step" id="m_wizard_form_step_4">
									<div class="row">
										<div class="col-xl-4">
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">Gambar Properti</h3>
												</div>		
											</div>
										</div>
										<div class="col-xl-8">	
											<div class="m-form__section m-form__section--first flex flex-ver" style="align-items:stretch;">
											<div class="form-group" style="margin-bottom:10px;">
												<div ondragleave="dropRelease(this,event)" ondragenter="dropHere(this,event)" ondragover="dropHere(this,event)" style="position: relative; overflow: hidden; width: 100%; height:100%; text-align: center; border: 1px dashed #d32f2f; padding:20px;" class="clickable">
													<span class="normal" style="width: 100%; text-align: center; height: 100%; vertical-align: middle;">Click Here to Upload or Drag Your photos Here</span>
													<span class="parentActive" style="width: 100%; text-align: center; height: 100%; vertical-align: middle;">Drop Your Files</span>
													<input class="inputGambar custom-file-input" onchange="loadFile(this,event)" multiple accept="image/*" type="file" id="exampleInputFile" name="images[]" required>
												</div>
											

											<!-- <p class="help-block">Example block-level help text here.</p> -->
											</div>

											<div id="preview" class="" style="display:none;border: 1px solid gainsboro;">
												<!-- Start Carousel -->
												<div id="carousel-preview" style="display:inline-block; background-color:Gainsboro; width:100%;height:250px; position:relative; transition:all .2s ease-in-out;    background-size: contain;
													background-position: center;
													background-repeat: no-repeat;">
												<!-- Controls -->
												<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"  onclick="next(-1);" >
													<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
													<span class="sr-only">Previous</span>
												</a>
												<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"  onclick="next(1);" >
													<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
													<span class="sr-only">Next</span>
												</a>
												</div>
												<!-- End Carousel -->
												<div class="minimalistScrollbar flex flex-hor" id="thumbnail-preview" style="width:100%; display:flex; overflow-x:scroll;">
												<img style="object-fit: cover;" src="<?= base_url()?>/assets/img/author.jpg" label="robby" alt="" class="marked" height="100px" width="100px">
												<img style="object-fit: cover;" src="<?= base_url()?>/assets/img/service.jpg" label="delvin" alt="" class="marked"  height="100px" width="100px">
												</div>
											</div>  
											</div>
										</div>
									</div>	
									<div class="m-separator m-separator--dashed m-separator--lg"></div>
									<div class="row">
										<div class="col-xl-4">
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">Video Properti</h3>
												</div>		
											</div>
										</div>
										<div class="col-xl-8">	
											<div class="m-form__section m-form__section--first">
												<div class="row">
													<div class="col-xl-6">	
														<div class="m-form__heading form-group m-form__group row">
															<h3 class="m-form__heading-title">Video dari</h3>
															<select class="form-control m-bootstrap-select m_selectpicker" name="source_video">
																<option value="youtube">Youtube</option>
															</select>
														</div>
													</div>
													<div class="col-xl-6">	
														<div class="m-form__heading">
															<h3 class="m-form__heading-title">Embed Video ID</h3>
															<input type="text" class="form-control m-input" placeholder="" aria-describedby="basic-addon2" name="url_video" onchange="updateVideo(this);">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end: Form Wizard Step 4-->
							</div>

							<!--end: Form Body -->

							<!--begin: Form Actions -->
							<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
								<div class="m-form__actions m-form__actions">
									<div class="row">
										<div class="col-lg-2"></div>
										<div class="col-lg-4 m--align-left">
											<button class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
												<span>
													<i class="la la-arrow-left"></i>&nbsp;&nbsp;
													<span>Back</span>
												</span>
											</button>
										</div>
										<div class="col-lg-4 m--align-right">

											<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
												<span>
													<i class="la la-check"></i>&nbsp;&nbsp;
													<span>Submit</span>
												</span>
											</button>
											<button class="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
												<span>
													<span>Save &amp; Continue</span>&nbsp;&nbsp;
													<i class="la la-arrow-right"></i>
												</span>
											</button>
										</div>
										<div class="col-lg-2"></div>
									</div>
								</div>
							</div>

							<!--end: Form Actions -->
						</form>
					</div>

					<!--end: Form Wizard Form-->
				</div>

				<!--end: Form Wizard-->
			</div>

			<!--End::Main Portlet-->
		</div>
	</div>
</div>

<script>
	// This example adds a search box to a map, using the Google Place Autocomplete
	// feature. People can enter geographical searches. The search box will return a
	// pick list containing a mix of places and predicted search terms.

	// This example requires the Places library. Include the libraries=places
	// parameter when you first load the API. For example:
	// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

	function initAutocomplete() {
	var map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -33.8688, lng: 151.2195},
		zoom: 13,
		mapTypeId: 'roadmap'
	});

	// Create the search box and link it to the UI element.
	var input = document.getElementById('jalan');
	var searchBox = new google.maps.places.SearchBox(input);
	// map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
	});

	var markers = [];
	// Listen for the event fired when the user selects a prediction and retrieve
	// more details for that place.
	searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
		return;
		}

		// Clear out the old markers.
		markers.forEach(function(marker) {
		marker.setMap(null);
		});
		markers = [];

		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
		if (!place.geometry) {
			console.log("Returned place contains no geometry");
			return;
		}

		var icon = {
			url: place.icon,
			size: new google.maps.Size(71, 71),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(17, 34),
			scaledSize: new google.maps.Size(25, 25)
		};

		// Create a marker for each place.
		markers.push(new google.maps.Marker({
			map: map,
			icon: icon,
			title: place.name,
			position: place.geometry.location
		}));

		if (place.geometry.viewport) {
			// Only geocodes have viewport.
			bounds.union(place.geometry.viewport);
		} else {
			bounds.extend(place.geometry.location);
		}
		});
		map.fitBounds(bounds);
		var sendData = {"jalan" : $("#jalan").val()};

		$.post({
			"url" : API_URL + "/api/get/detaillocation/data?token=public",
			data: JSON.stringify(sendData),
			success : function(data){
				data = JSON.parse(data);
				console.log(data)
				$("#provinsi").val(data['administrative_area_level_1']);
				$("#kota").val(data['administrative_area_level_2']);
				$("#kecamatan").val(data['administrative_area_level_3']);
				$("#kelurahan").val(data['administrative_area_level_4']);
				$("#kodepos").val(data['postal_code']);
				$("#lat").val(data['latitude']);
				$("#lng").val(data['longitude']);
			}
		});
	});
	}

</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyJi7G0CbtkyT9qjITRHKHcg8tKCZ1tpk&libraries=places&callback=initAutocomplete"
		async defer></script>
	<?php 
		$this->load->view('page-part/common-foot');
	?>
</body>
</html>