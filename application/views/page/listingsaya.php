<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->
	<!-- Header section -->
	<?php $this->load->view('page-part/header-saya');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->


	<div class="summary-preview flex flex-ver flex-separate my-container"
		style='margin-top:120px;border-bottom: 1px solid rgba(0, 0, 0, 0.13); padding-bottom:10px;'>
		<div class="rounded" style="background-color: white;">
			<div class="rounded flex flex-ver" style="overflow:hidden">
				<div class="flex flex-ver" style="flex-grow:1">
					<div class="txt-bold padding-s" style="border-bottom:solid gainsboro 1px">
						<div class="flex flex-hor flex-vertical-center flex-separate" style="margin-bottom:10px;">
							<h2></h2>
							<a href="/createlisting" class="btn txt-white rounded bg-red ">Membuat Listing</a>
						</div>
					</div>
					<div class="flex flex-hor txt-bold flex-equal txt-center"
						style="border-bottom:solid gainsboro 1px ;">
						<div active-group="detail-action"
							class="flex-equal clickable bg-red activeable-solo active padding-m">
							<span>Aktif</span>
							<span class="m-badge m-badge--warning bg-red txt-white">5</span>
						</div>
						<div active-group="detail-action" class="flex-equal clickable bg-red activeable-solo padding-m" onclick="loadListing(0);">
							<span>Waiting For Approval</span>
							<span class="m-badge m-badge--warning bg-red txt-white">5</span>
						</div>
						<div active-group="detail-action" class="flex-equal clickable bg-red activeable-solo padding-m">
							<span>Non-Aktif</span>
							<span class="m-badge m-badge--warning bg-red txt-white">5</span>
						</div>
						<div active-group="detail-action" class="flex-equal clickable bg-red activeable-solo padding-m">
							<span>Terjual/Tersewa</span>
							<span class="m-badge m-badge--warning bg-red txt-white">5</span>
						</div>
						<div active-group="detail-action" class="flex-equal clickable bg-red activeable-solo padding-m">
							<span>Dishare</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="my-container flex flex-ver">
		<!-- <div class="flex flex-ver padding-s">
			<div class="flex flex-hor">
				<div class="square"
					style="border-radius:50%; background-image:url('/assets/img/blog/1.jpg');height:100px; width:100px; background-size:cover; background-repeat:no-repeat; background-position: center;">

				</div>
				<div class="flex flex-ver" style="flex-grow:1">
					<div class="flex flex-hor flex-separate padding-s">
						<div class="flex flex-ver txt-bold">
							<a class="" href="#"><span>Lisa Pangestu</span></a>
							<a class="" href="#"><span> ERA KITA Cabang Pakuwon</span></a>
						</div>
						<div class="flex flex-hor flex-equal">
							<div class="clickable padding-m txt-white" style="background-color:darkRed"><span
									class="fa fa-envelope"><span></span></div>
							<div class="clickable padding-m txt-white" style="background-color:green"><span
									class="fa fa-phone"><span></span></div>
							<div class="clickable padding-m txt-white" style="background-color:blue"><span
									class="fa fa-whatsapp"><span></span></div>
						</div>
					</div>
					<div class="flex flex-hor txt-bold padding-s flex-equal flex-1">
						<span class="fa fa-envelope"></span>&nbsp;<span
							style="color: rgb(175, 175, 175);">lisapangestu@gmail.com</span> &nbsp; &nbsp;
						<span class="fa fa-phone"></span>&nbsp; <span style="color: rgb(175, 175, 175);">+62 811 888
							888</span> &nbsp; &nbsp;
						<span class="fa fa-whatsapp"></span>&nbsp; <span style="color: rgb(175, 175, 175);">+62 811 888
							888</span> &nbsp; &nbsp;
					</div>
					<div class="txt-bold">
						Dimanage Sejak : 12 Mar 2019
					</div>
				</div>
			</div>
		</div> -->
		<div class="rounded" style="background-color: white;">
			<div class="flex flex-ver padding-m">
				<!--
				<div class="flex flex-hor">
					<div class="flex flex-hor txt-bold padding-s flex-equal flex-1">
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-eye">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">View</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_view" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-whatsapp">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">Whatsapp</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_wa" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-phone">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">Telepon</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_telp" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-heart">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">Favorit</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_fav" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-th-list">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">Wishlist</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_wishlist" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-share">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">Share</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_share" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-close">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">X-Out</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_xout" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>-->
						<!-- <div class="flex flex-hor padding-s">
							<div class="padding-m" style="background-color: lightgreen;">
								Untuk gambar Orang"
							</div>
						</div> -->
					<!--</div>
				</div>
				-->
			</div>
		</div>
		<div class="flex flex-hor">
			<div class="flex flex-ver flex-equal" style="flex-grow:1;">
				<div class="ml-2 mr-2">
					<label>Tipe</label>
					<div class="dropdown bootstrap-select form-control m-bootstrap-select m_ start">
						<select class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" tabindex="-98">
							<option value="">Select</option>
						</select>
						<div class="dropdown-menu " role="combobox">
							<div class="bs-searchbox">
								<input type="text" class="form-control" autocomplete="off" role="textbox" aria-label="Search">
							</div>
							<div class="inner show" role="listbox" aria-expanded="false" tabindex="-1">
								<ul class="dropdown-menu inner show"></ul>
							</div>
						</div>
					</div>
				</div>
				<div class="ml-2 mr-2">
					<label>Jenis</label>
					<div class="dropdown bootstrap-select form-control m-bootstrap-select m_ start">
						<select class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" tabindex="-98">
							<option value="">Select</option>
						</select>
						<div class="dropdown-menu " role="combobox">
							<div class="bs-searchbox">
								<input type="text" class="form-control" autocomplete="off" role="textbox" aria-label="Search">
							</div>
							<div class="inner show" role="listbox" aria-expanded="false" tabindex="-1">
								<ul class="dropdown-menu inner show"></ul>
							</div>
						</div>
					</div>
				</div>
				<div class="ml-2 mr-2">
					<label>Properti</label>
					<input type="text" class="form-control">
				</div>
				<div class="ml-2 mr-2">
					<label>Daerah</label>
					<div class="dropdown bootstrap-select form-control m-bootstrap-select m_ start">
						<select class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" tabindex="-98">
							<option value="">Select</option>
						</select>
						<div class="dropdown-menu " role="combobox">
							<div class="bs-searchbox">
								<input type="text" class="form-control" autocomplete="off" role="textbox" aria-label="Search">
							</div>
							<div class="inner show" role="listbox" aria-expanded="false" tabindex="-1">
								<ul class="dropdown-menu inner show"></ul>
							</div>
						</div>
					</div>
				</div>
				<div class="ml-2 mr-2">
					<label>Kota</label>
					<div class="dropdown bootstrap-select form-control m-bootstrap-select m_ start">
						<select class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" tabindex="-98">
							<option value="">Select</option>
						</select>
						<div class="dropdown-menu " role="combobox">
							<div class="bs-searchbox">
								<input type="text" class="form-control" autocomplete="off" role="textbox" aria-label="Search">
							</div>
							<div class="inner show" role="listbox" aria-expanded="false" tabindex="-1">
								<ul class="dropdown-menu inner show"></ul>
							</div>
						</div>
					</div>
				</div>
				<div class="ml-2 mr-2">
					<label>Harga</label>
					<input type="number" class="form-control">
				</div>
				<div class="ml-2 mr-2">
					<label>Tanggal Submit</label>
					<div class="flex flex-ver" align="center">
						<input type="date" class="form-control">
						<input type="date" class="form-control">
					</div>
				</div>
				<div class="ml-2 mr-2">
					<label>Tanggal Approve</label>
					<div class="flex flex-ver" align="center">
						<input type="date" class="form-control">
						<input type="date" class="form-control">
					</div>
				</div>
				<div class="ml-2 mr-2">
					<label>Aksi</label>
					<div class="flex flex-ver" align="center">
						<button class="btn bg-red txt-white"><span class="fa fa-search">&nbsp;</span>Search</button>
						<button class="btn btn-reset bg-blue txt-black"><span class="fa fa-close">&nbsp;</span>Reset</button>
					</div>
				</div>
			</div>
			<!--Separator-->
			<!--begin: Datatable -->
			<!-- <div class="m_datatable mt-3" id="m_datatable_latest_orders"></div> -->
			<div class="flex flex-ver" style="width:80%;" id="listListing">
				<div class="listing-list row" style='border: 1px solid #00000012;'>
					<div class="flex flex-hor" data-slidein="x1">
						<div class="listing-list-photo col-sm-12 col-md-2" style="background-image:url('/assets/img/listing/1/1.jpg');"></div>
						<div class="listing-list-contain col-sm-12 col-md-10 row" style="margin-left:0;padding:10px 0px;">
							<div class="col-lg-3 col-sm-12 col-md-3 listing-list-contain-part part-main">
								<a class="main-part-title truncate hoverable txt-black">Rumah murah Ngagel Jaya Tengah</a>
								<div class="main-part-content">
									<div class="main-part-address">Jalan Ngagel Jaya Tengah No 73-77</div>
									<div class="main-part-information">
										<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bed"></i><br>2</div>
										<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-bath"></i><br>2</div>
										<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-square"></i><br>2m<sup>2</sup></div>
										<div class="listing-detail-pills flex-4 txt-center"><i class="fas fa-building"></i><br>2m<sup>2</sup></div>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-sm-12 col-md-2 listing-list-contain-part text-center">
								Serpong CBD, Parung, ...
							</div>
							<div class="col-lg-1 col-sm-12 col-md-1 listing-list-contain-part">
								Jakarta Selatan
							</div>
							<div class="col-lg-1 col-sm-12 col-md-1 listing-list-contain-part">
								470jt
							</div>
							<div class="col-lg-2 col-sm-12 col-md-2 listing-list-contain-part text-center">
								3 Dec 2018
							</div>
							<div class="col-lg-2 col-sm-12 col-md-2 listing-list-contain-part text-center">
								15 Dec 2018
							</div>
							<div class="col-lg-1 listing-list-contain-part part-dropdown col-sm-12 col-md-1">
								<div class="dropdown">	
									<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"> 
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(-186px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">						    	
										<a class="dropdown-item" href="#"><i class="la la-edit"></i> Preview</a>
										<a class="dropdown-item" href="#"><i class="fab fa-whatsapp"></i> Whatsapp</a>
										<a class="dropdown-item" href="#"><i class="la la-phone"></i> Telepon</a>
									</div>						
								</div>
							</div>
						</div>
					</div>
					<!-- Start Statistic Content -->
					<div style="display:none;" id='x1' class="minimalistScrollbar flex flex-hor txt-bold padding-s flex-equal flex-1" style="overflow-x:auto;">
						<div class="flex flex-hor padding-s" >
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-eye">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">View</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_view" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-whatsapp">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">Whatsapp</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_wa" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-phone">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">Telepon</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_telp" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-heart">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">Favorit</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_fav" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<!--
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-th-list">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">Wishlist</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_wishlist" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-share">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">Share</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_share" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						<div class="flex flex-hor padding-s">
							<div class="flex flex-ver padding-m" style="font-size: 2.5em; margin-top: 15px;">
								<span class="fa fa-close">
							</div>
							<div class="flex flex-hor flex-equal">
								<div class="flex flex-ver" style="flex-grow:1" align="center">
									<div style="color: rgb(175, 175, 175);">X-Out</div>
									<div class="txt-bold" style="font-size: 1.5em;">1678</div>
									<div><a href="#">(43 baru)</a></div>
									<div class="padding-m"> 
										<div class="m-widget11__chart" style="height:50px; width: 100px">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
												</div>
											</div>
											<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
											<canvas id="m_chart_xout" style="display: block; width: 100px; height: 50px;" width="100" height="50" class="chartjs-render-monitor"></canvas>
										</div>									
									</div>
								</div>
							</div>
						</div>
						-->
					</div>
					
					<!-- End Statistic Content-->

				</div>
				<!--end: Datatable -->
			</div>



		</div>

		
	</section>
	<!-- Page -->
	<section class="page-section">
		<div class="container">

			<div class="row">

				<!-- sidebar -->

			</div>
		</div>
	</section>
	<!-- Page end -->

	<?php 
		$this->load->view('page-part/common-foot');
	?>

</body>

<!-- <script>
	var datatableLatestOrders = function () {
		if ($('#m_datatable_latest_orders').length === 0) {
			return;
		}

		var datatable = $('.m_datatable').mDatatable({
			data: {
				type: 'remote',
				source: {
					read: {
						url: 'http://localhost/testing.json'
					}
				},
				pageSize: 10,
				saveState: {
					cookie: false,
					webstorage: true
				},
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true
			},

			layout: {
				theme: 'default',
				class: '',
				scroll: true,
				height: 380,
				footer: false
			},

			sortable: true,

			filterable: false,

			pagination: true,

			columns: [{
				field: "RecordID",
				title: "#",
				sortable: false,
				width: 40,
				selector: {
					class: 'm-checkbox--solid m-checkbox--brand'
				},
				textAlign: 'center'
			}, {
				field: "Image",
				title: "Preview",
				sortable: 'asc',
				filterable: false,
				width: 150,
				template: '<img src="{{Image}}">'
			}, {
				field: "Judul",
				title: "Judul",
				width: 150,
				template: '<h3>{{Judul}}</h3><br/>{{Alamat}}<br/>{{KT}}&nbsp;{{KM}}&nbsp;{{LT}}&nbsp;{{LB}}'
			}, {
				field: "Daerah",
				title: "Daerah",
				width: 150,
				template: '{{Daerah}}'
			}, {
				field: "Kota",
				title: "Kota",
				width: 150,
				template: '{{Kota}}'
			}, {
				field: "Harga",
				title: "Harga",
				width: 150,
				template: '{{Daerah}}'
			}, {
				field: "Submit",
				title: "Submit",
				width: 150,
				template: '{{Submit}}'
			}, {
				field: "Approve",
				title: "Approve",
				width: 150,
				template: '{{Approve}}'
			}, {
				field: "Actions",
				width: 110,
				title: "Actions",
				sortable: false,
				overflow: 'visible',
				template: function (row, index, datatable) {
					var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
					return '\
                        <div class="dropdown ' + dropup + '">\
                            <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
                                <i class="la la-ellipsis-h"></i>\
                            </a>\
                            <div class="dropdown-menu dropdown-menu-right">\
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>\
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>\
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>\
                            </div>\
                        </div>\
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                            <i class="la la-edit"></i>\
                        </a>\
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\
                            <i class="la la-trash"></i>\
                        </a>\
                    ';
				}
			}]
		});
	}
	datatableLatestOrders();

</script> -->

</html>
