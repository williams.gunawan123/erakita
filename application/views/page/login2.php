<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/assets/img/favicon.ico" rel="shortcut icon"/>
    <title>EraKita | Login Customer</title>
    
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/loginregister2.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
</head>
<body>
    <div class="container">
        <div class="row" style="height:100vh; position:relative;">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto" style="position:absolute; top:50%; left:50%; transform:translate(-50%, -50%);">
                <div class="card card-signin">
                    <div class="card-body">
                    <h5 class="card-title text-center">Sign In</h5>
                    <form class="form-signin">
                        <div class="form-label-group">
                            <input type="text" id="inputPhone" class="form-control" style="" placeholder="Phone Number" required autofocus>
                            <label for="inputPhone">Phone Number</label>
                        </div>
                        <div class="form-label-group">
                            <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                            <label for="inputPassword">Password</label>
                            <span style="position: absolute;top: 16px;right: 20px;" toggle="#inputPassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            <small id="errMsg" class="text-danger ml-4" style="display:none">
                                No. Telp/Password Tidak Sesuai
                            </small>
                        </div>

                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label" for="customCheck1">Remember password</label>
                        </div>
                        <div class="mb-2">
                            <a href="/forgot">Forgot Password?</a>
                            <a href="/register" style="float:right">Register here</a>
                        </div>
                        <button type="button" onclick="login()" class="btn btn-lg btn-primary btn-block text-uppercase">Sign in</button>
                        <hr class="my-4">
                        <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i class="fa fa-google mr-2"></i> Sign in with Google</button>
                        <!-- <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i class="fa fa-facebook-f mr-2"></i> Sign in with Facebook</button> -->
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/assets/js/jquery-3.2.1.min.js"></script>
</body>
<script>
    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    function login(){
        var sendData = {"telp":$("#inputPhone").val(), "password":$("#inputPassword").val(), "rememberMe" : $("#customCheck1").val()};
        if(getUrlVars()["last_action"] !== undefined){
            sendData.last_action = getUrlVars()["last_action"];
        }

        $.post({
            "url" : "/auth/login",
            "data" : sendData,
            success : function(data){
                data = JSON.parse(data);
                if (data.key != 1 || data.userData.id_customer == null){
                    $('#errMsg').slideDown().delay(1500).slideUp();
                } else {
                    if(getUrlVars()["last_url"] !== undefined){
                        window.location = getUrlVars()["last_url"];
                    } else {
                        window.location = "/";
                    }
                }
            }
        });    
    }
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
</html>