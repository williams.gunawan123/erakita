<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->


	<!-- Breadcrumb -->
	<div class="site-breadcrumb" style="margin-top: 50px;">
		<div class="container">
			<a href="/"><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i>Cabang</span>
		</div>
	</div>
	<section class="my-container flex flex-ver">
		<h4>Menampilkan <span id="showedCabang">0</span> dari <span id="totalCabang">0</span> Cabang</h4>
		<div class="row pt-5">
			<div class="col-sm-8">
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-inverse">
						<thead>
							<tr>
								<th width="40%">Cabang Agen</th>
								<th width="10%">Listing Total</th>
								<th width="15%">Per Bulan</th>
								<th width="10%">Transaksi Total</th>
								<th width="15%">Per Bulan</th>
								<th width="10%">Broker Agen</th>
							</tr>
						</thead>
						<tbody id="listCabang">
							<!-- Load Cabang -->
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="m-portlet m-portlet--head-overlay m-portlet--full-height  m-portlet--rounded-force">
					<div class="m-portlet__head m-portlet__head--fit-">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text m--font-light" id="side-alamat">
									Lontar 1, Surabaya, Jawa Timur
								</h3>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
									m-dropdown-toggle="hover">
									<div class="flex flex-hor flex-equal">
										<div class="clickable padding-m txt-white" style="border-radius: 5px; margin: 2px; background-color: #2e7d32;">
											<span class="fa fa-phone" style="color: white; font-size: 1.3em;"></span>
										</div>
										<div class="clickable padding-m txt-white" style="border-radius: 5px; margin: 2px; background-color: #128C7E;">
											<span class="fa fa-whatsapp" style="color: #FFFFFF; font-size: 1.3em;"></span>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						<div class="m-widget27 m-portlet-fit--sides">
							<div class="m-widget27__pic" style='padding: 15vh 0px;'>
								<!-- <img src="assets/app/media/img//bg/bg-4.jpg" alt=""> -->
								<h3 class="m-widget27__title m--font-light" style="top:50%;">
									<img class="padding-l" src="/assets/img/erakita-logo-square.png" alt="" style="padding:0px 40px;">
								</h3>
								<div class="m-widget27__btn">
									<div class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--bolder" id="side-nama">
                                        ERA KITA <br>
                                        Cabang Pakuwon
                                    </div>
								</div>
							</div>
							<div class="m-widget27__container">

								<!-- begin::Nav pills -->
								<ul class="m-widget27__nav-items nav nav-pills nav-fill" style='table-layout:auto;' role="tablist">
									<li class="m-widget27__nav-item nav-item">
										<a class="nav-link active" data-toggle="pill" href="#m_kota_spesialis">Kota Spesialis</a>
									</li>
									<li class="m-widget27__nav-item nav-item">
										<a class="nav-link" data-toggle="pill" href="#m_broker_agen">Broker Agen</a>
									</li>
									<li class="m-widget27__nav-item nav-item">
										<a class="nav-link" data-toggle="pill" href="#m_listing">Listing</a>
									</li>
									<li class="m-widget27__nav-item nav-item">
										<a class="nav-link" data-toggle="pill" href="#m_transaksi">Transaksi</a>
                                    </li>
                                    <li class="m-widget27__nav-item nav-item">
										<a class="nav-link" data-toggle="pill" href="#m_aksi_customer">Aksi Customer</a>
									</li>
								</ul>

								<!-- end::Nav pills -->

								<!-- begin::Tab Content -->
								<div class="m-widget27__tab tab-content m-widget27--no-padding">
									<div id="m_kota_spesialis" class="tab-pane active">
                                        <h5>Listing Spesialis</h5>
                                        <p>Top 3 kota dengan listing terbanyak</p>
										<div class="row  align-items-center">
											<div class="col">
												<div id="m_chart_personal_income_quater_1" class="m-widget27__chart"
													style="height: 160px">
													<div class="m-widget27__stat">37</div>
												</div>
											</div>
											<div class="col">
												<div class="m-widget27__legends">
													<div class="m-widget27__legend">
														<span class="m-widget27__legend-bullet m--bg-accent"></span>
														<span class="m-widget27__legend-text">12 Jakarta Selatan</span>
													</div>
													<div class="m-widget27__legend">
														<span class="m-widget27__legend-bullet m--bg-warning"></span>
														<span class="m-widget27__legend-text">6 Surabaya</span>
													</div>
													<div class="m-widget27__legend">
														<span class="m-widget27__legend-bullet m--bg-brand"></span>
														<span class="m-widget27__legend-text">4 Gresik</span>
                                                    </div>
                                                    <div class="m-widget27__legend">
														<span class="m-widget27__legend-bullet m--bg-info"></span>
														<span class="m-widget27__legend-text">2 lainnya</span>
													</div>
												</div>
											</div>
                                        </div>
                                        <h5 class="mt-3">Harga Jual (Listing)</h5>
                                        <div class="row align-items-center">
                                            <div class="col">
                                                <span>Minimum</span><br/>
                                                <h5>700jt</h5>
                                            </div>
                                            <div class="col">
                                                <span>Maximum</span><br/>
                                                <h5>7M</h5>
                                            </div>
                                        </div>
                                        <h5 class="mt-3">Harga Sewa (Listing)</h5>
                                        <div class="row align-items-center">
                                            <div class="col">
                                                <span>Minimum</span><br/>
                                                <h5>25jt</h5>
                                            </div>
                                            <div class="col">
                                                <span>Maximum</span><br/>
                                                <h5>275jt</h5>
                                            </div>
                                        </div>
                                        <h5 class="mt-3" align="right">Transaksi Baru</h5>
                                        <p align="right">Top 3 kota dengan transaksi baru terbanyak</p>
                                        <div class="row align-items-center">
                                            <div class="col">
												<div class="m-widget27__legends">
													<div class="m-widget27__legend">
														<span class="m-widget27__legend-bullet m--bg-accent"></span>
														<span class="m-widget27__legend-text">12 Jakarta Selatan</span>
													</div>
													<div class="m-widget27__legend">
														<span class="m-widget27__legend-bullet m--bg-warning"></span>
														<span class="m-widget27__legend-text">6 Surabaya</span>
													</div>
													<div class="m-widget27__legend">
														<span class="m-widget27__legend-bullet m--bg-brand"></span>
														<span class="m-widget27__legend-text">4 Gresik</span>
                                                    </div>
                                                    <div class="m-widget27__legend">
														<span class="m-widget27__legend-bullet m--bg-info"></span>
														<span class="m-widget27__legend-text">2 lainnya</span>
													</div>
												</div>
											</div>
											<div class="col">
												<div id="m_chart_personal_income_quater_1" class="m-widget27__chart"
													style="height: 160px">
													<div class="m-widget27__stat">37</div>
												</div>
											</div>
                                        </div>
									</div>
								</div>

								<!-- end::Tab Content -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Page -->
	<section class="page-section">
		<div class="container">

			<div class="row">

				<!-- sidebar -->

			</div>
		</div>
	</section>
	<!-- Page end -->

	<?php 
		$this->load->view('page-part/common-foot');
	?>

</body>

</html>
