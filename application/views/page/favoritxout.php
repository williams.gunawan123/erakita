<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('page-part/common-head', $pageData);?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->
	<!-- Header section -->
	<?php $this->load->view('page-part/header-saya');?>
	<!-- Header section end -->
	<main>
		<!-- Page -->
		<div id="my-chevron" class="chevron chevron-flip" data-show="hide"></div>
		<section class="page-section sticky" style="padding-bottom:0px;">

			<div class="container" style="margin:0px;width:100%; max-width:120000px; padding:0px">
				<div class="row" style="width:100%;">

					<div class="col-lg-6 single-list-page" id="detailListing">
						<div>
							<div class="single-list-slider owl-carousel" id="sl-slider">
								<div class="sl-item set-bg" data-setbg="<?= base_url('assets/img/single-list-slider/1.jpg')?>">
									<div class="sale-notic">FOR SALE</div>
								</div>
								<div class="sl-item set-bg" data-setbg="<?= base_url('assets/img/single-list-slider/2.jpg')?>">
									<div class="rent-notic">FOR Rent</div>
								</div>
								<div class="sl-item set-bg" data-setbg="<?= base_url('assets/img/single-list-slider/3.jpg')?>">
									<div class="sale-notic">FOR SALE</div>
								</div>
								<div class="sl-item set-bg" data-setbg="<?= base_url('assets/img/single-list-slider/4.jpg')?>">
									<div class="rent-notic">FOR Rent</div>
								</div>
								<div class="sl-item set-bg" data-setbg="<?= base_url('assets/img/single-list-slider/5.jpg')?>">
									<div class="sale-notic">FOR SALE</div>
								</div>
							</div>
							<div class="owl-carousel sl-thumb-slider" id="sl-slider-thumb">
								<div class="sl-thumb set-bg" data-setbg="<?= base_url('assets/img/single-list-slider/1.jpg')?>"></div>
								<div class="sl-thumb set-bg" data-setbg="<?= base_url('assets/img/single-list-slider/2.jpg')?>"></div>
								<div class="sl-thumb set-bg" data-setbg="<?= base_url('assets/img/single-list-slider/3.jpg')?>"></div>
								<div class="sl-thumb set-bg" data-setbg="<?= base_url('assets/img/single-list-slider/4.jpg')?>"></div>
								<div class="sl-thumb set-bg" data-setbg="<?= base_url('assets/img/single-list-slider/5.jpg')?>"></div>
							</div>
							<div class="single-list-content">
								<div class="row">
									<div class="col-xl-8 sl-title">
										<h2 id="detailJudul">305 North Palm Drive</h2>
										<p id="detailAlamat"><i class="fa fa-map-marker"></i>Beverly Hills, CA 90210</p>
									</div>
									<div class="col-xl-4">
										<a href="#" class="price-btn" id="detailHarga">$4,500,000</a>
									</div>
								</div>
								<h3 class="sl-sp-title">Property Details</h3>
								<div class="row property-details-list">
									<div class="col-md-4 col-sm-6">
										<p id="detailLuas"><i class="fa fa-th-large"></i> 1500 Square foot</p>
										<p id="detailKTidur"><i class="fa fa-bed"></i> 16 Bedrooms</p>
										<p><i class="fa fa-user"></i> Gina Wesley</p>
									</div>
									<div class="col-md-4 col-sm-6">
										<p id="detailGarasi"><i class="fa fa-car"></i> 2 Garages</p>
										<p><i class="fa fa-building-o"></i> Family Villa</p>
										<p><i class="fa fa-clock-o"></i> 1 days ago</p>
									</div>
									<div class="col-md-4">
										<p id="detailKMandi"><i class="fa fa-bath"></i> 8 Bathrooms</p>
										<p id="detailUmur"><i class="fa fa-trophy"></i> 5 years age</p>
									</div>
								</div>
								<h3 class="sl-sp-title">Description</h3>
								<div class="description">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus egestas fermentum ornareste. Donec index
										lorem. Vestibulum aliquet odio, eget tempor libero. Cras congue cursus tincidunt. Nullam venenatis dui id orci
										egestas tincidunt id elit. Nullam ut vuputate justo. Integer lacnia pharetra pretium. Casan ante ipsum primis
										in faucibus orci luctus et ultrice.</p>
								</div>
								<h3 class="sl-sp-title">Property Details</h3>
								<div class="row property-details-list">
									<div class="col-md-4 col-sm-6">
										<p><i class="fa fa-check-circle-o"></i> Air conditioning</p>
										<p><i class="fa fa-check-circle-o"></i> Telephone</p>
										<p><i class="fa fa-check-circle-o"></i> Laundry Room</p>
									</div>
									<div class="col-md-4 col-sm-6">
										<p><i class="fa fa-check-circle-o"></i> Central Heating</p>
										<p><i class="fa fa-check-circle-o"></i> Family Villa</p>
										<p><i class="fa fa-check-circle-o"></i> Metro Central</p>
									</div>
									<div class="col-md-4">
										<p><i class="fa fa-check-circle-o"></i> City views</p>
										<p><i class="fa fa-check-circle-o"></i> Internet</p>
										<p><i class="fa fa-check-circle-o"></i> Electric Range</p>
									</div>
								</div>
								<h3 class="sl-sp-title bd-no">Floorplans</h3>
								<div id="accordion" class="plan-accordion">
									<div class="panel">
										<div class="panel-header" id="headingOne">
											<button class="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="false"
											aria-controls="collapse1">First Floor: <span>660 sq ft</span> <i class="fa fa-angle-down"></i></button>
										</div>
										<div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
											<div class="panel-body">
												<img src="<?= base_url('assets/img/plan-sketch.jpg')?>" alt="">
											</div>
										</div>
									</div>
									<div class="panel">
										<div class="panel-header" id="headingTwo">
											<button class="panel-link" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">Second
												Floor:<span>610 sq ft.</span> <i class="fa fa-angle-down"></i>
											</button>
										</div>
										<div id="collapse2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
											<div class="panel-body">
												<img src="<?= base_url('assets/img/plan-sketch.jpg')?>" alt="">
											</div>
										</div>
									</div>
									<div class="panel">
										<div class="panel-header" id="headingThree">
											<button class="panel-link" data-toggle="collapse" data-target="#collapse3" aria-expanded="false"
											aria-controls="collapse3">Third Floor :<span>580 sq ft</span> <i class="fa fa-angle-down"></i>
											</button>
										</div>
										<div id="collapse3" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
											<div class="panel-body">
												<img src="<?= base_url('assets/img/plan-sketch.jpg')?>" alt="">
											</div>
										</div>
									</div>
								</div>

								<h3 class="sl-sp-title bd-no">Video</h3>
								<div class="perview-video">
									<img src="<?= base_url('assets/img/video.jpg')?>" alt="">
									<a href="https://www.youtube.com/watch?v=v13nSVp6m5I" class="video-link"><img src="<?= base_url('assets/img/video-btn.png')?>"
										alt=""></a>
								</div>
								<h3 class="sl-sp-title bd-no">Location</h3>
								<div class="pos-map" id="map-canvas"></div>
							</div>
						</div>
					</div>

					<!--Map Section -->
					<div class="col-lg-6 col-md-6 single-list-page hide-on-small-only" id="mapListing"></div>
					<!--End of map Section -->
					
					<div class="listing-part col-lg-6 col-sm-12 col-md-6">
						<div class="listing-part-header">
							<div class="listing-part-header-left">
								<div class="listing-part-header-title">
									Menampilkan <span class="listing-part-header-title-text" id="showedListing">XXXX</span> dari <span class="listing-part-header-title-text" id="totalListing">YYYY</span> properti
								</div>
								<!--<div class="listing-part-header-order">
									Urutkan
								</div>-->
							</div>
							<div class="listing-part-header-right">
								<div class="listing-part-header-tabs">
									<ul class="nav nav-pills" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#tabs-foto">Foto</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#tabs-tabel">Tabel</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#tabs-statistik">Statistik</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-content listing-part-content">
							<div class="tab-pane active" id="tabs-foto" role="tabpanel">
								<!-- Daftar Listing -->
							</div>
							<div class="tab-pane" id="tabs-tabel">
								<div class="row flex flex-hor"  style="align-content: stretch; align-items: stretch; margin-bottom: 10px;">
									<div class="listing-table-detail-listing col-lg-6 col-sm-12" style="height:100%;">
										<div class="m-portlet m-portlet--rounded" style="margin:0px;">
											<?php $gambar = json_encode(["/assets/img/single-list-slider/1.jpg","/assets/img/single-list-slider/2.jpg","/assets/img/single-list-slider/3.jpg"], JSON_OBJECT_AS_ARRAY) ?>
											<div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='<?= $gambar ?>'>
												<div class="listing-slider-badge">Open House</div>
												<div class="listing-slider-icon"><i class="fas fa-home"></i></div>
												<div class="listing-slider-counter">1 of 3</div>
											</div>
											<div class="m-portlet__body">
												<div class="col-lg-6 col-md-6 col-sm-12" style="padding-left: 0;padding-right:0">
													<div class="listing-price listing-info">500 Jt</div>
													<div class="listing-jalan listing-info">Jalan</div>
													<div class="listing-kota listing-info">Daerah, Kota</div>
												</div>
												<div class="col-lg-6 col-md-6 col-sm-12" style="padding-left: 0;padding-right:0">
													<div class="listing-detail">
														<div class="listing-detail-pills text-center"><i class="fas fa-bed"></i> 2</div>
														<div class="listing-detail-pills text-center"><i class="fas fa-bath"></i> 3</div>
														<div class="listing-detail-pills text-center"><span class="text-info">LT</span> 200m<sup>2</sup></div>
														<div class="listing-detail-pills text-center"><span class="text-info">LB</span> 500m<sup>2</sup></div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="listing-table-detail-agent col-lg-6 col-sm-12">
										<div class="flex m-portlet__body" style="align-content:stretch;height: 100%;">
											<div class="flex flex-ver flex-separate col-lg-10 col-md-10 col-sm-12" style="padding-left: 0;padding-right:0;align-content:stretch;height: 100%;">
												<div class="listing-title listing-info">Dilist oleh</div>
												<div class="listing-title listing-info">Delvin Limanto</div>
												<div class="listing-title listing-info">ERA Kita Cabang Pakuwon</div>
												<div class="row">
													<div class="col-lg-5 col-md-5 col-sm-5">Panjang</div>
													<div class="col-lg-7 col-md-7 col-sm-7 text-right">20 m</div>
												</div>
												<div class="row">
													<div class="col-lg-5 col-md-5 col-sm-5">Lebar</div>
													<div class="col-lg-7 col-md-7 col-sm-7 text-right">7 m</div>
												</div>
												<div class="row">
													<div class="col-lg-5 col-md-5 col-sm-5">Sertifikat</div>
													<div class="col-lg-7 col-md-7 col-sm-7 text-right">HGB</div>
												</div>
												<div class="row">
													<div class="col-lg-5 col-md-5 col-sm-5">Hadap</div>
													<div class="col-lg-7 col-md-7 col-sm-7 text-right">Timur</div>
												</div>
												<div class="row">
													<div class="col-lg-5 col-md-5 col-sm-5">Pengembang</div>
													<div class="col-lg-7 col-md-7 col-sm-7 text-right">Pakuwon</div>
												</div>
												<div class="row">
													<div class="col-lg-5 col-md-5 col-sm-5">Proyek</div>
													<div class="col-lg-7 col-md-7 col-sm-7 text-right">Grand Island</div>
												</div>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding-left: 0;padding-right:0">
												<div class="listing-content-icon"><i style="font-size: 1.3em;" class="fab fa-whatsapp"></i></div>
												<div class="listing-content-icon"><i style="font-size: 1.3em;" class="fas fa-heart heart-animated"></i></div>
												<div class="listing-content-icon"><i style="font-size: 1.3em;" class="fas fa-share"></i></div>
												<div class="listing-content-icon"><i style="font-size: 1.3em;" class="fas fa-ellipsis-h"></i></div>
											</div>
										</div>
									</div>
								</div>
								<div class="listing-table-detail">
									<div class="m-portlet m-portlet--mobile">
										<div class="m-portlet__body">

											<!--begin: Datatable -->
											<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
												<thead>
													<tr>
														<th>Alamat</th>
														<th>Daerah</th>
														<th>Kota</th>
														<th>Harga</th>
														<th>Kamar Tidur</th>
														<th>Kamar Mandi</th>
														<th>Luas Bangunan</th>
														<th>Luas Tanah</th>
														<th>Rp/m2</th>
														<th>Hadap</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<?php for ($i=0; $i<20; $i++){ ?>
														<tr>
															<td>Jalan Ngagel</td>
															<td>Gubeng</td>
															<td>Surabaya</td>
															<td>1.5 M</td>
															<td>3</td>
															<td>2</td>
															<td>270 m2</td>
															<td>300 m2</td>
															<td>10 jt</td>
															<td>Timur</td>
															<td></td>
														</tr>
													<?php } ?>
												</tbody>
												<tfoot>
													<tr>
														<th>Alamat</th>
														<th>Daerah</th>
														<th>Kota</th>
														<th>Harga</th>
														<th>Kamar Tidur</th>
														<th>Kamar Mandi</th>
														<th>Luas Bangunan</th>
														<th>Luas Tanah</th>
														<th>Rp/m2</th>
														<th>Hadap</th>
														<th></th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>				
							</div>
							<div class="tab-pane" id="tabs-statistik">Coming Soon...</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Page end -->
	</main>

	<?php 
		$set['footer']=false;
		$this->load->view('page-part/common-foot', $set);
	?>

	<!-- load for map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyJi7G0CbtkyT9qjITRHKHcg8tKCZ1tpk"></script>
	<script src="/assets/js/map-2.js"></script>
	<script type="text/javascript" src="/assets/json/data.json"></script>
	<script type="text/javascript" src="/assets/js/markerclusterer.js"></script>

    <script type="text/javascript">
      function initialize() {
        var center = new google.maps.LatLng(37.4419, -122.1419);

        var map = new google.maps.Map(document.getElementById('mapListing'), {
          zoom: 3,
          center: center,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
					fullscreenControl: false,
					mapTypeControl: false,
					streetViewControl: false
        });

				var markers = [];
        for (var i = 0; i < LatLongData.length; i++) {
		  		var dataPhoto = LatLongData[i];
          var latLng = new google.maps.LatLng(dataPhoto.Lat, dataPhoto.Long);
          var marker = new google.maps.Marker({
          	position: latLng
          });
          markers.push(marker);
        }
        var markerCluster = new MarkerClusterer(map, markers, {
        	averageCenter: true
        });

        google.maps.event.addListener(markerCluster, "click", function (c) {
        	log("click: ");
          log("&mdash;Center of cluster: " + c.getCenter());
          log("&mdash;Number of managed markers in cluster: " + c.getSize());
          var m = c.getMarkers();
          var p = [];
          for (var i = 0; i < m.length; i++ ){
            p.push(m[i].getPosition());
          }
          log("&mdash;Locations of managed markers: " + p.join(", "));

        });
        google.maps.event.addListener(markerCluster, "mouseover", function (c) {
          log("mouseover: ");
          log("&mdash;Center of cluster: " + c.getCenter());
          log("&mdash;Number of managed markers in cluster: " + c.getSize());
        });
        google.maps.event.addListener(markerCluster, "mouseout", function (c) {
          log("mouseout: ");
          log("&mdash;Center of cluster: " + c.getCenter());
          log("&mdash;Number of managed markers in cluster: " + c.getSize());
        });

				var saveSearchDiv = document.createElement("div");
				var saveSearchInner = new generateSearch(saveSearchDiv, map);
				saveSearchDiv.index = 1;
				map.controls[google.maps.ControlPosition.TOP_RIGHT].push(saveSearchDiv);
				let text = ["Remove Outline", "Draw", "Layers"];
				for(let i = 0;i < 3;i++){
					var customControlDiv = document.createElement("div");
					var customControl = new generateCustom(customControlDiv, text[i]);
					customControlDiv.index = 1;
					map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(customControlDiv);
				}
      }
	 
    	google.maps.event.addDomListener(window, 'load', initialize);

			function generateSearch(controlDiv, map){
				var controlUI = document.createElement('div');
				controlUI.style.backgroundColor = '#d32f2f';
				controlUI.style.borderRadius = "5px";
				controlUI.style.cursor = 'pointer';
				controlUI.style.margin = "10px";
				controlUI.style.textAlign = 'center';
				controlDiv.append(controlUI);

				var controlText = document.createElement('div');
        controlText.style.color = 'white';
				controlText.style.fontWeight = "bold";
        controlText.style.fontSize = '16px';
				controlText.style.padding = "10px";
        controlText.innerHTML = 'Save Search';
        controlUI.appendChild(controlText);

				controlUI.addEventListener('click', function() {
          map.setCenter(chicago);
        });
			}

			function generateCustom(controlDiv, text){
				var controlUI = document.createElement('div');
				controlUI.style.backgroundColor = 'white';
				controlUI.style.borderRadius = "5px";
				controlUI.style.cursor = 'pointer';
				controlUI.style.margin = "10px";
				controlUI.style.marginBottom = "20px";
				controlUI.style.textAlign = 'center';
				controlDiv.append(controlUI);

				var controlText = document.createElement('div');
				controlText.style.fontWeight = "bold";
        controlText.style.fontSize = '16px';
				controlText.style.padding = "10px";
        controlText.innerHTML = text;
        controlUI.appendChild(controlText);
			}
    </script>
</body>

</html>
