<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->


    <!-- Breadcrumb -->
    <div class="site-breadcrumb" style="margin-top: 50px;">
		<div class="container">
            <a href="/"><i class="fa fa-home"></i>Home&nbsp;</a>
			<a href="/developer"><i class="fa fa-angle-right"></i>Developer</a>
			<span><i class="fa fa-angle-right"></i><?=$dataDeveloper->nama_developer?></span>
		</div>
	</div>
	<section class="my-container flex flex-ver">
		<div class="row flex flex-hor flex-vertical-center" style="justify-content: center; align-content: center;">
            <div class="col-sm-12" style="background-image: url('http://getwallpapers.com/wallpaper/full/f/2/a/37562.jpg'); background-repeat: no-repeat; background-position: center; background-size: cover; height: 40vh; padding: 0px;" align="center">
                <div style="background-color: #888; opacity: 0.4; width: 100%; height: 100%; position: absolute;"></div>
            </div>
            <img src="/assets/img/erakita-logo-square.png" style="max-height: 25vh; position: absolute;" alt="">
        </div>
        <div class="row mt-3">
            <div class="col-sm-12">
                <h3>Deskripsi</h3><br>
                <p><?=$dataDeveloper->deskripsi_developer?></p> <hr>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-7">
                <h3>Proyek <span><?=$proyekDeveloper[0]->nama_proyek?></span></h3>
                <div class="flex flex-hor flex-equal mt-2" style="font-weight: bold; font-size: 1.2em;">
                    <div>Menampilkan 0 Proyek</div>
                    <div><i class="fa fa-fw fa-sort-amount-up">&nbsp;</i>Urutkan</div>
                    <div>Filter</div>
                </div>
                <!--Map Section -->
                <div class="col-md-12 single-list-page hide-on-small-only" id="mapListing" style="height:30vh;"></div>
                <!--End of map Section -->
                <div style="margin-top:5vh; flex-wrap:wrap;" class="flex flex-hor">
                    <?php for ($i=0; $i < count($proyekDeveloper); $i++){?>
                        <a href="#" onclick="updateListing('<?=$proyekDeveloper[$i]->id_proyek?>');" class="box-developer flex-2" style="border: dotted #eeeeee 0.5px;">
                            <div class="flex flex-ver" style="height:100%; padding: 10px;">
                                <div style="background-image:url('/assets/img/proyek/<?=$proyekDeveloper[$i]->photo_proyek?>'); background-size:cover; min-height: 200px; background-position: center;">
                                    <div style="background-color:royalblue; padding: 5px; min-width: 196px; width: 50%; margin-top: 20px; color: white; font-weight: bold; font-size: 1.15em;"><span class="fa fa-check-circle">&nbsp;</span>READY FOR OCCUPANCY</div>
                                </div>
                                <div style="font-weight: bold; border-bottom: dashed black 1px; padding: 3px 0px;">
                                    <span style="font-size: 1.8em;"><?=$proyekDeveloper[$i]->nama_proyek?></span><br/>
                                    <span>By <?=$dataDeveloper->nama_developer?></span>
                                </div>
                                <div style="font-weight: bold; font-size: 1.15em; color: black;"><span class="fa fa-map-marker">&nbsp;</span><?=$proyekDeveloper[$i]->nama_kecamatan?>, <?=$proyekDeveloper[$i]->nama_kota?></div>
                                <div class="flex flex-hor flex-equal description">
                                    <div>
                                        <table>
                                            <?php
                                                if ($proyekDeveloper[$i]->min_tidur == $proyekDeveloper[$i]->max_tidur){
                                                    $tidur = $proyekDeveloper[$i]->min_tidur;
                                                }else{
                                                    $tidur = $proyekDeveloper[$i]->min_tidur." - ".$proyekDeveloper[$i]->max_tidur;
                                                }
                                                if ($proyekDeveloper[$i]->min_bangunan == $proyekDeveloper[$i]->max_bangunan){
                                                    $bangunan = $proyekDeveloper[$i]->min_bangunan ."m<sup>2</sup>";
                                                }else{
                                                    $bangunan = $proyekDeveloper[$i]->min_bangunan." - ".$proyekDeveloper[$i]->max_bangunan ."m<sup>2</sup>";
                                                }
                                                if ($proyekDeveloper[$i]->min_mandi == $proyekDeveloper[$i]->max_mandi){
                                                    $mandi = $proyekDeveloper[$i]->min_mandi;
                                                }else{
                                                    $mandi = $proyekDeveloper[$i]->min_mandi." - ".$proyekDeveloper[$i]->max_mandi;
                                                }
                                                if ($proyekDeveloper[$i]->min_tanah == $proyekDeveloper[$i]->max_tanah){
                                                    $tanah = $proyekDeveloper[$i]->min_tanah ."m<sup>2</sup>";
                                                }else{
                                                    $tanah = $proyekDeveloper[$i]->min_tanah." - ".$proyekDeveloper[$i]->max_tanah ."m<sup>2</sup>";
                                                }
                                            ?>
                                            <tr><td>Kamar Tidur</td><td>:</td><td style="font-weight: bold; font-size: 1.15em; color: black;"><?=$tidur?></td></tr>
                                            <tr><td>L. Tanah</td><td>:</td><td style="font-weight: bold; font-size: 1.15em; color: black;"><?=$tanah?></td></tr>
                                            <tr><td>Harga Dari</td><td>:</td><td style="font-weight: bold; font-size: 1.15em; color: black;">Rp <?=number_format($proyekDeveloper[$i]->min_jual)?></td></tr>
                                        </table>
                                    </div>
                                    <div>
                                        <table>
                                            <tr><td>Kamar Mandi</td><td>:</td><td style="font-weight: bold; font-size: 1.15em; color: black;"><?=$mandi?></td></tr>
                                            <tr><td>L. Bangunan</td><td>:</td><td style="font-weight: bold; font-size: 1.15em; color: black;"><?=$bangunan?></td></tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-5">
                <h3>Jenis Unit (<span></span>)</h3>
                <div class="flex flex-hor flex-equal mt-2" style="font-weight: bold; font-size: 1.2em;">
                    <div>Menampilkan 0 Jenis</div>
                    <div><i class="fa fa-fw fa-sort-amount-up">&nbsp;</i>Urutkan</div>
                </div>
                <div style="margin-top:5vh; flex-wrap:wrap;" class="flex flex-hor" id="listUnit">
                    <!-- <a href="#" class="flex-1" style="border: dotted #eeeeee 0.5px;">
                        <div class="flex flex-hor" style="height:100%;">
                            <div style="background-image: url('https://pmcvariety.files.wordpress.com/2018/07/bradybunchhouse_sc11.jpg'); background-repeat: no-repeat; background-position: center; background-size: cover; min-height: 150px; font-size: 0.8em; color: white; width: 50%;">
                                <div style="background-color: lightseagreen; margin-top: 15px; padding: 1px; width: 64px;"><i class="fa fa-star"> &nbsp;</i>PREMIUM</div>
                                <div style="background-color: darkorange; margin-top: 5px; padding: 1px; width: 86px;"><i class="fa fa-home"> &nbsp;</i>DEVELOPMENT</div>
                            </div>
                            <div class="flex flex-ver" style="width: 50%; padding: 10px;">
                                <div style="font-weight:bold; font-size: 1.3em; height: 20%;">Pondok Indah Residence Man</div>
                                <div style="font-weight: bold; font-size: 2em; padding: 15px 10px; color: black; height: 60%;">Rp 4.001.528.549</div>
                                <div class="row" style="height: 20%; color: #888; font-size: 0.8em;">
                                    <div class="col-sm-4"><span style="font-weight: bold; font-size: 1.2em; color: black;">80m2</span>  Building Size</div>
                                    <div class="col-sm-4"><span style="font-weight: bold; font-size: 1.2em; color: black;">1</span> Bedroom</div>
                                    <div class="col-sm-4"><span style="font-weight: bold; font-size: 1.2em; color: black;">1</span>  Bathroom</div>
                                </div>
                            </div>
                        </div>
                    </a> -->
                </div>
            </div>
        </div>
	</section>
	<!-- Page -->
	<section class="page-section">
		<div class="container">

			<div class="row">

				<!-- sidebar -->

			</div>
		</div>
	</section>
	<!-- Page end -->

	<?php 
		$this->load->view('page-part/common-foot');
    ?>
    
    <!-- load for map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyJi7G0CbtkyT9qjITRHKHcg8tKCZ1tpk"></script>
<script src="/assets/js/map-2.js"></script>
<script type="text/javascript" src="/assets/json/data.json"></script>
<script type="text/javascript" src="/assets/js/markerclusterer.js"></script>

<script type="text/javascript">
    function initialize() {
    var center = new google.maps.LatLng(37.4419, -122.1419);

    var map = new google.maps.Map(document.getElementById('mapListing'), {
        zoom: 3,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
                fullscreenControl: false,
                mapTypeControl: false,
                streetViewControl: false
    });

            var markers = [];
    for (var i = 0; i < LatLongData.length; i++) {
            var dataPhoto = LatLongData[i];
        var latLng = new google.maps.LatLng(dataPhoto.Lat, dataPhoto.Long);
        var marker = new google.maps.Marker({
        position: latLng
        });
        markers.push(marker);
    }
    var markerCluster = new MarkerClusterer(map, markers, {
        averageCenter: true
    });

    google.maps.event.addListener(markerCluster, "click", function (c) {
        log("click: ");
        log("&mdash;Center of cluster: " + c.getCenter());
        log("&mdash;Number of managed markers in cluster: " + c.getSize());
        var m = c.getMarkers();
        var p = [];
        for (var i = 0; i < m.length; i++ ){
        p.push(m[i].getPosition());
        }
        log("&mdash;Locations of managed markers: " + p.join(", "));

    });
    google.maps.event.addListener(markerCluster, "mouseover", function (c) {
        log("mouseover: ");
        log("&mdash;Center of cluster: " + c.getCenter());
        log("&mdash;Number of managed markers in cluster: " + c.getSize());
    });
    google.maps.event.addListener(markerCluster, "mouseout", function (c) {
        log("mouseout: ");
        log("&mdash;Center of cluster: " + c.getCenter());
        log("&mdash;Number of managed markers in cluster: " + c.getSize());
    });

            var saveSearchDiv = document.createElement("div");
            var saveSearchInner = new generateSearch(saveSearchDiv, map);
            saveSearchDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(saveSearchDiv);
            let text = ["Remove Outline", "Draw", "Layers"];
            for(let i = 0;i < 3;i++){
                var customControlDiv = document.createElement("div");
                var customControl = new generateCustom(customControlDiv, text[i]);
                customControlDiv.index = 1;
                map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(customControlDiv);
            }
    }
    
    google.maps.event.addDomListener(window, 'load', initialize);
        function generateSearch(controlDiv, map){
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = '#d32f2f';
            controlUI.style.borderRadius = "5px";
            controlUI.style.cursor = 'pointer';
            controlUI.style.margin = "10px";
            controlUI.style.textAlign = 'center';
            controlDiv.append(controlUI);

            var controlText = document.createElement('div');
    controlText.style.color = 'white';
            controlText.style.fontWeight = "bold";
    controlText.style.fontSize = '16px';
            controlText.style.padding = "10px";
    controlText.innerHTML = 'Save Search';
    controlUI.appendChild(controlText);

            controlUI.addEventListener('click', function() {
        map.setCenter(chicago);
    });
        }

        function generateCustom(controlDiv, text){
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = 'white';
            controlUI.style.borderRadius = "5px";
            controlUI.style.cursor = 'pointer';
            controlUI.style.margin = "10px";
            controlUI.style.marginBottom = "20px";
            controlUI.style.textAlign = 'center';
            controlDiv.append(controlUI);

            var controlText = document.createElement('div');
            controlText.style.fontWeight = "bold";
    controlText.style.fontSize = '16px';
            controlText.style.padding = "10px";
    controlText.innerHTML = text;
    controlUI.appendChild(controlText);
        }
</script>

</body>

</html>
