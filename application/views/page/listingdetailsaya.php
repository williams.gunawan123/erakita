<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->
    <!-- Header section -->
    <?php $this->load->view('page-part/header-saya');?>
	<!-- Header section end -->

	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->

	
    <section class="summary-preview flex flex-ver flex-separate my-container" style='margin-top:120px;border-bottom: 1px solid rgba(0, 0, 0, 0.13); padding-bottom:10px;'>
        <input type="hidden" id="id-listing" value="<?=$dataListing->id_listing?>">
        <div class="flex flex-hor flex-vertical-center flex-separate" style="margin-bottom:10px;">
            <h2><?=$dataListing->judul_listing?></h2>
            <div class="btn txt-white rounded bg-red ">Membuat Listing</div>
        </div>
        
        <div class="flex flex-ver flex-separate padding-s rounded">
            <div class="padding-s flex flex-hor flex-equal row">
                <div class="col-lg-8 col-sm-12 mb-3">
                    <div class="rounded shadow-s" style="height:100%; background-color: white; margin-bottom:10px;">
                        <div class="rounded flex flex-hor flex-separate" style="overflow:hidden; height:100%;">
                            <div class="flex flex-ver" style="background-image:url('/assets/img/blog/1.jpg'); width:220px ; background-size: cover; background-repeat:no-repeat; background-position:center;background-color:gainsboro">
                                <!-- Ini Gambar  -->
                            </div>
                            <div class="flex flex-ver" style="flex-grow:1;">
                                <div class="flex flex-hor txt-bold flex-equal txt-center" style="border-bottom:solid gainsboro 1px ;">
                                    <div active-group="detail-category" class="flex-equal hoverable activeable-solo active padding-m"><span>Profile</span></div>
                                    <div active-group="detail-category" class="flex-equal hoverable activeable-solo padding-m"><span>Harga</span></div>
                                </div>
                                <div class="flex flex-ver padding-s">
                                    <div>Alamat : <?=$dataListing->alamat_listing?></div>
                                    <div>Daerah : </div>
                                    <div>Kecamatan / Kota : <?=$dataListing->nama_kecamatan?> / <?=$dataListing->nama_kota?></div>
                                    <div>Status : </div>
                                    <div>Tgl Submit : 2 Bulan Lalu (<?=$dataListing->start_listing?>)</div>
                                    <div>Tgl Aktif   : 2 Bulan Lalu (<?=$dataListing->start_listing?>)</div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 mb-3">
                    <div class="flex flex-ver shadow-s rounded" style="background-color:white;">
                        <div class="flex flex-hor flex-equal" style="border-bottom:solid gainsboro 1px ;">
                            <div active-group="view-grafik" class="txt-center txt-bold padding-m hoverable activeable-solo active">View</div>
                            <div active-group="view-grafik" class="txt-center txt-bold padding-m hoverable activeable-solo">Whatsapp</div>
                            <div active-group="view-grafik" class="txt-center txt-bold padding-m hoverable activeable-solo">Telpon</div>
                        </div>
                        <div class="p-s" style="height:200px; width 100%;">
                            <div class="m-widget4__chart m-portlet-fit--sides m--margin-top-10 m--margin-top-20" style="height:260px;">
                                <canvas id="m_chart_trends_stats"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="padding-s flex flex-hor row">
                <div class="col-sm-12 col-12 col-lg-6 mb-3">
                    <div class="rounded shadow-s" style="background-color: white;">
                        <div class="rounded flex flex-ver" style="overflow:hidden">
                            <div class="flex flex-ver" style="flex-grow:1">
                                <div class="txt-bold p-3 padding-s" style="border-bottom:solid gainsboro 1px">
                                    <h5>Aksi</h5>
                                </div>
                                <div class="flex flex-hor txt-bold flex-equal txt-center" style="border-bottom:solid gainsboro 1px ;">
                                    <div onclick="$('.detail-action-tab').fadeOut(); $('.detail-action-tab.container-1').fadeIn();" active-group="detail-action" class="flex-equal hoverable activeable-solo active padding-m"><span>Broker</span> <span class="m-badge m-badge--warning bg-red txt-white" id="badge-broker" hidden></span></div>
                                    <div onclick="$('.detail-action-tab').fadeOut(); $('.detail-action-tab.container-2').fadeIn(); readPostMedia();" active-group="detail-action" class="flex-equal hoverable activeable-solo padding-m"><span>Admin</span> <span class="m-badge m-badge--warning bg-red txt-white" id="badge-admin" hidden></span></div>
                                    <div onclick="$('.detail-action-tab').fadeOut(); $('.detail-action-tab.container-3').fadeIn();" active-group="detail-action" class="flex-equal hoverable activeable-solo padding-m"><span>Operator</span> <span class="m-badge m-badge--warning bg-red txt-white" id="badge-operator" hidden></span></div>
                                </div>
                                <div class="flex flex-ver padding-s detail-action-tab container-1">
                                    <div class="flex flex-hor row mx-0 flex-vertical-center">
                                        <div class="square col-2 ratio11 px-0" style="border-radius:50%; background-image:url('/assets/img/profile-broker/<?=$dataListing->foto_broker?>'); background-size:cover; background-repeat:no-repeat; background-position: center;">
                                            
                                        </div>
                                        <div class="flex flex-ver" style="flex-grow:1">
                                            <div class="flex flex-hor flex-separate padding-s">
                                                <div class="flex flex-ver txt-bold">
                                                    <a class="txt-bold" href="/broker/<?=$dataListing->id_broker?>"><span><?=$dataListing->nama_broker?></span></a>
                                                    <a class="txt-bold" href="/cabang/<?=$dataListing->id_cabang?>"><span> <?=$dataListing->nama_cabang?></span></a>
                                                </div>
                                                <div class="flex flex-hor flex-equal">
                                                    <div class="clickable padding-m txt-white" style="background-color:darkRed"><span class="fa fa-envelope"><span></span></div>
                                                    <div class="clickable padding-m txt-white" style="background-color:green"><span class="fa fa-phone"><span></span></div>
                                                    <div class="clickable padding-m txt-white" style="background-color:blue"><span class="fa fa-whatsapp"><span></span></div>
                                                </div>
                                            </div>
                                            <div class="flex flex-hor txt-bold padding-s flex-equal flex-1">
                                                <div><span class="fa fa-envelope"></span>&nbsp;<span style="color: rgb(175, 175, 175);" class="margin-col-md"><?=$dataListing->email_broker?></span></div>
                                                <div><span class="fa fa-phone"></span>&nbsp; <span style="color: rgb(175, 175, 175);" class=" margin-col-md">+<?=$dataListing->telp_broker?></span></div>
                                                <div><span class="fa fa-whatsapp"></span>&nbsp; <span style="color: rgb(175, 175, 175);" class="margin-col-md">+<?=$dataListing->telp_broker?></span></div>
                                            </div>
                                            <div class="txt-bold">
                                                &nbsp;&nbsp;Dimanage Sejak : <?=$dataListing->start_listing?>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div class="flex flex-hor flex-vertical-center mt-3 justify-content-start pl-4 row mx-0 pb-3">
                                        <i class="fas fa-users fa-lg col-2 " style="text-align:right;"></i>
                                        <div class="users-images flex flex-hor flex-vertical-center">
                                                <div class="div-image user-img shadow-s circle ratio11" style="width:35px; background-image:url('/assets/img/1.jpg')"></div>
                                                <div class="div-image user-img shadow-s circle ratio11" style="width:35px; background-image:url('/assets/img/1.jpg')"></div>
                                                <div class="div-image user-img shadow-s circle ratio11" style="width:35px; background-image:url('/assets/img/1.jpg')"></div>
                                                <div class="div-image user-img shadow-s circle ratio11" style="width:35px; background-image:url('/assets/img/1.jpg')"></div>
                                                <div class="div-image user-img shadow-s circle ratio11" style="width:35px; background-image:url('/assets/img/1.jpg')"></div>
                                                <div class="div-image user-img shadow-s circle ratio11" style="width:35px; background-image:url('/assets/img/1.jpg')"></div>
                                                <div class="div-image user-img shadow-s circle ratio11" style="width:35px; background-image:url('/assets/img/1.jpg')"></div>
                                                <div class="div-image user-img shadow-s circle ratio11" style="width:35px; background-image:url('/assets/img/1.jpg')"></div>
                                                <div class="div-image user-img shadow-s circle ratio11" style="width:35px; background-image:url('/assets/img/1.jpg')"></div>
                                                <div class="div-image user-img shadow-s circle ratio11" style="width:35px; background-image:url('/assets/img/1.jpg')"></div>
                                                <div class="div-image user-img shadow-s circle ratio11" style="width:35px; background-image:url('/assets/img/1.jpg')"></div>
                                                <div class="div-image user-more circle ratio11" style="background-color:gainsboro; width:30px; margin-left:-10px; z-index:2;"><span class="center-text">+8</span></div>
                                        </div>
                                    </div>
                                    <!-- 
                                    <div class="flex flex-ver minimalistScrollbar" style="flex-grow:1; max-height:500px; overflow-y:auto;">
                        
                                        <?php 
                                        for ($i=0; $i < 5; $i++) { 
                                        ?>
                                        
                                        <div class="flex flex-ver">
                                            <div class="flex flex-ver  padding-s" style=" border-bottom:solid gainsboro 1px">
                                                <div class="flex flex-hor">
                                                    <div class="square" style="border-radius:50%; background-image:url('/assets/img/blog/1.jpg');height:50px; width:50px; background-size:cover; background-repeat:no-repeat; background-position: center;">
                                                    </div>
                                                    <div class="flex flex-ver" style="flex-grow:1;">
                                                        <div class="flex flex-hor flex-separate padding-s flex-vertical-center" >
                                                            <div class="flex flex-ver">
                                                                <div><a href="javascript:void(0)" class="txt-bold">Meliana Trump</a> mengirim pesan ke <a href="javascript:void(0)" class="txt-bold">Saya</a></div>
                                                                <div style="color: rgb(175, 175, 175);">2 Days Ago | 22 Mei 2018 : 18.22:01</div>
                                                            </div>
                                                            <div><i class="fa fa-file-audio-o txt-lg" aria-hidden="true"></i></div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div style="color:rgb(175, 175, 175); text-align:justify; margin-left:50px;" class="padding-s border">
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imperdiet et lacus ac convallis. Integer sed metus viverra, lacinia mauris sed, volutpat orci. Praesent nisi purus, dictum non viverra non, rhoncus non dui. Aenean ac velit id nulla aliquet tempus. Morbi dignissim urna neque, ut tempor tellus hendrerit id. Integer efficitur accumsan ante nec mattis. Ut sit amet purus maximus, ultrices nisl ac, posuere diam.
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                        }
                                        ?>
                                    </div>
                                     -->
                                </div>
                                <div class="flex flex-ver padding-s detail-action-tab container-2" style="display:none;">
                                    <div class="col-12 col-sm-12 col-lg-12 px-2" id="listMedia">
                                        <div class="py-2 px-2 mx-0 row border-bottom">
                                            <div class="col-1 squared div-image" style="width:50px; background-image:url('/assets/img/1.jpg')">
                                                <!-- <a href="javascript:void(0)" class="show-on-hoverParent"><i class="fa fa-pencil w-100 txt-center"></i></a> -->
                                            </div>
                                            <div class="col-9 px-1">
                                                <div>
                                                    Nama Website 
                                                    <!-- <i class="fa fa-pencil" aria-hidden="true"></i> -->
                                                </div> 
                                                <div>
                                                    <a href="http://google.com">Link to Sosmed</a> 
                                                    <!-- <i class="fa fa-pencil" aria-hidden="true"></i> -->
                                                </div>
                                            </div>
                                            <div class="col-2 text-right label px-1 align-self-center">10 hari lalu</div>
                                        </div>
                                        <div class="py-2 px-2 mx-0 row border-bottom">
                                            <div class="col-1 squared div-image" style="width:50px; background-image:url('/assets/img/1.jpg')">
                                                <!-- <a href="javascript:void(0)" class="show-on-hoverParent"><i class="fa fa-pencil w-100 txt-center"></i></a> -->
                                            </div>
                                            <div class="col-9 px-1">
                                                <div>
                                                    Nama Website 
                                                    <!-- <i class="fa fa-pencil" aria-hidden="true"></i> -->
                                                </div> 
                                                <div>
                                                    <a href="http://google.com">Link to Sosmed</a> 
                                                    <!-- <i class="fa fa-pencil" aria-hidden="true"></i> -->
                                                </div>
                                            </div>
                                            <div class="col-2 text-right label px-1 align-self-center">10 hari lalu</div>
                                        </div>    
                                    </div>
                                    <div class="paginate pt-2 pb-2"></div>
                                </div>
                                <div class="flex flex-ver padding-s detail-action-tab container-3" style="display:none;">
                                    
                                    <div class="row mx-0" id="listBanner">
                                        <?php $dummyGambar = json_encode(["/assets/img/1.jpg", "/assets/img/bg.jpg"], JSON_OBJECT_AS_ARRAY); ?>
                                        <?php for ($i=0; $i < 2; $i++) { ?>
                                            <div class="col-12 mb-2 px-2 mt-2">
                                                <div class="card rounded">
                                                    <div class="div-image squared-69 sensecode-slider initiate" style="background-image:url('/assets/img/1.jpg'); width:100%;" data-index="0" data-files='<?= $dummyGambar ?>'>
                                                        <div class="listing-slider-counter">1 of 1</div>
                                                        
                                                    </div>
    
                                                    <div class="px-2 mt-2 row mx-0">
                                                        <div class="p-0 col-10 pr-2">Banner/Plang semula dicabut Banner/Plang semula Banner/Plang semula orang, memasang kembali yang baru, dan terdapat banner/plang brighton manyar. terdapat juga spanduk besar pemilik sendiri</div>
                                                        <div class="p-0 col-2 align-items-center flex flex-column txt-center justify-content-center" style="font-size:0.75em">
                                                            <div class="startDate pb-2">
                                                                <div>Check Date</div>
                                                                <div>7 Oct 2019</div>
                                                            </div>
                                                            <hr class="w-100 m-0">
                                                            <div class="endDate pt-2">
                                                                <div>Actual Date</div>
                                                                <div>12 Oct 2019</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="paginate mt-2"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-12 col-lg-6 mb-3">
                    <div class="flex flex-ver shadow-s rounded" style="background-color:white;">
                        
                        <div class="txt-bold padding-s p-3" style="border-bottom:solid gainsboro 1px">
                            <h5>Aksi Customer</h5>
                        </div>
                    
                        <div class="flex flex-ver minimalistScrollbar" style="flex-grow:1; max-height:500px; overflow-y:auto;">
                        
                            <?php 
                            for ($i=0; $i < 5; $i++) { 
                            ?>
                            
                            <div class="flex flex-ver">
                                <div class="flex flex-ver  padding-s" style=" border-bottom:solid gainsboro 1px">
                                    <div class="flex flex-hor">
                                        <div class="square" style="border-radius:50%; background-image:url('/assets/img/blog/1.jpg');height:50px; width:50px; background-size:cover; background-repeat:no-repeat; background-position: center;">
                                        </div>
                                        <div class="flex flex-ver" style="flex-grow:1;">
                                            <div class="flex flex-hor flex-separate padding-s flex-vertical-center" >
                                                <div class="flex flex-ver txt-bold">
                                                    <div><a href="javascript:void(0)" class="txt-bold">Meliana Trump</a></div>
                                                    <div style="color: rgb(175, 175, 175);">2 Days Ago</div>
                                                </div>
                                                <div>Pending</div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div style="color:rgb(175, 175, 175); text-align:justify" class="padding-s">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imperdiet et lacus ac convallis. Integer sed metus viverra, lacinia mauris sed, volutpat orci. Praesent nisi purus, dictum non viverra non, rhoncus non dui. Aenean ac velit id nulla aliquet tempus. Morbi dignissim urna neque, ut tempor tellus hendrerit id. Integer efficitur accumsan ante nec mattis. Ut sit amet purus maximus, ultrices nisl ac, posuere diam.
                                    </div>
                                </div>
                            </div>
    
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
	<!-- Page -->
	<section class="page-section">
		<div class="container">
			
			<div class="row">
                
				<!-- sidebar -->
				
			</div>
		</div>
	</section>
	<!-- Page end -->
    
	<?php 
		$this->load->view('page-part/common-foot');
	?>

	<!-- load for map -->
 	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyJi7G0CbtkyT9qjITRHKHcg8tKCZ1tpk&v=3.exp&libraries=places"></script> 
	<script src="/assets/js/map-2.js"></script>

	<script type="text/javascript" src="/assets/json/data.json"></script>
	<script type="text/javascript" src="/assets/js/markerclusterer.js"></script>

	 <script>
		var map;
		var infowindow;
		var center;
		var nearbyPlaces = [];
		function initialize() {
			center = new google.maps.LatLng(-7.24917, 112.75083); // sample location to start with: Mumbai, India

			map = new google.maps.Map(document.getElementById('mapListing'), {
				center: center,
				zoom: 15
			});

			requestNearby('gym');
			requestNearby('school');


		}

		function requestNearby(types){
			var request = {
				location: center,
				radius: 1000,
				type: types // this is where you set the map to get the hospitals and health related places
			};
			infowindow = new google.maps.InfoWindow();
			var service = new google.maps.places.PlacesService(map);
			service.nearbySearch(request, callback);
		}

		function callback(results, status) {
			nearbyPlaces.push(results);
			createMapNearbyCard('School', results);
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				for (var i = 0; i < results.length; i++) {
					createMarker(results[i]);
					console.log(results[i]);
				}
			}
		}

		function createMarker(place) {
			var placeLoc = place.geometry.location;
			var marker = new google.maps.Marker({
				map: map,
				position: place.geometry.location
			});

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(place.name);
				infowindow.open(map, this);
			});
		}

		google.maps.event.addDomListener(window, 'load', initialize);

		function createMapNearbyCard(titleCategory, data){
			var dom = '';
			dom += '<div class="mapNearPlace">';
			dom += '<div class="flex-flex-ver mapNearItemContainer minimalistScrollbar" style="max-height:200px; overflow-y:auto;">';
			dom += '<h6 class="mapNearCategory">' + data[0].types[0] + ' <span>x</span></h6>';

			data.forEach(e => {
				dom +='<div class="flex flex-hor flex-vertical-center mapItem">';
				dom +='	<div class="mapItemName">' + e.name + '</div>';
				dom +='		<div class="flex flex-ver">';
				dom +='				<div class="map-time">3 Mins</div>';
				dom +='				<div class="map-far">230 m</div>';
				dom +='		</div>';
				dom +='</div>';
			});
			dom +='</div>';
			dom +='</div>';  

			$("#nearbyPlaceCard").append(dom);
		}
  </script> 
</body>
</html>