<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('page-part/common-head', $pageData);?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->
	<main>
		<!-- Page -->
		<div id="my-chevron" class="chevron chevron-flip" data-show="hide"></div>
		<section class="page-section sticky" style="padding-bottom:0px;">

			<div class="container" style="margin:0px;width:100%; max-width:120000px; padding:0px">
				<div class="row" style="width:100%;">

					<div class="col-lg-7 single-list-page" id="detailListing">
						<div>
							<diV id="my-slider">
								<div class="single-list-slider owl-carousel" id="sl-slider">
									<div class="sl-item set-bg" data-setbg="/assets/img/listing/2/1.jpg">
										<div class="sale-notic">FOR SALE</div>
									</div>
									<div class="sl-item set-bg" data-setbg="/assets/img/listing/2/2.jpg">
										<div class="sale-notic">FOR SALE</div>
									</div>
									<div class="sl-item set-bg" data-setbg="/assets/img/listing/2/3.jpg">
										<div class="sale-notic">FOR SALE</div>
									</div>
									<div class="sl-item set-bg" data-setbg="/assets/img/listing/2/4.jpg">
										<div class="sale-notic">FOR SALE</div>
									</div>
								</div>
								<div class="owl-carousel sl-thumb-slider" id="sl-slider-thumb">
									<div class="sl-thumb set-bg" data-setbg="/assets/img/listing/2/1.jpg"></div>
									<div class="sl-thumb set-bg" data-setbg="/assets/img/listing/2/2.jpg"></div>
									<div class="sl-thumb set-bg" data-setbg="/assets/img/listing/2/3.jpg"></div>
									<div class="sl-thumb set-bg" data-setbg="/assets/img/listing/2/4.jpg"></div>
								</div>
							</diV>
							<div class="single-list-content">
								<div class="row">
									<div class="col-xl-8 sl-title">
										<h2 id="detailJudul"></h2>
										<p id="detailAlamat"></p>
									</div>
									<div class="col-xl-4">
										<a href="#" class="price-btn" id="detailHarga"></a>
									</div>
								</div>
								<h3 class="sl-sp-title">Property Details</h3>
								<div class="row property-details-list">
									<div class="col-md-4 col-sm-6">
										<p id="detailLuas"></p>
										<p id="detailKTidur"></p>
										<p id="detailBroker"></p>
									</div>
									<div class="col-md-4 col-sm-6">
										<p id="detailGarasi"></p>
										<p id="detailLantai"></p>
										<p><i class="fa fa-clock-o"></i> 1 days ago</p>
									</div>
									<div class="col-md-4">
										<p id="detailKMandi"></p>
										<p id="detailUmur"></p>
									</div>
								</div>
								<h3 class="sl-sp-title">Description</h3>
								<div class="description">
								</div>
								<h3 class="sl-sp-title bd-no">Video</h3>
								<div class="perview-video">
									No Video Preview
								</div>
							</div>
						</div>
					</div>

					<!--Map Section -->
					<div class="col-lg-7 col-md-7 single-list-page hide-on-small-only" style="z-index:1;" id="mapDiv"></div>
					<!--End of map Section -->
					
					<div class="listing-part col-lg-5 col-sm-12 col-md-5">
						<div class="listing-part-header" style="font-size:1em">
							<div class="listing-part-header-left">
								<div class="listing-part-header-title">
									Menampilkan <span class="listing-part-header-title-text" id="showedListing">0</span> dari <span class="listing-part-header-title-text" id="totalListing">0</span> properti
								</div>
								
								<div class="listing-part-header-order" style="position:relative">
									<div onclick="$(this).parent().find('.sorter-tools').fadeToggle();" style="cursor:pointer;" class="hoverable">
										<i class="fa fa-sort-alpha-asc"></i>
										Urutkan - <span>Terbaru</span>
									</div>
									<div class="card flex flex-ver shadow sorter-tools" style="display:none; position:absolute; background-color:white; z-index:10;width:250%; transform:translateX(-50%);    padding-top: 15px;clip-path: polygon(0px 15px, 40% 15px, 50% 0px, 60% 15px, 100% 15px, 100% 100%, 0px 100%);">
										<div class="flex flex-hor flex-equal" style="" id="urutan">
											<div active-group="sortir-alphabet" onclick="urutkanAZ(this);" class="txt-center activeable-solo hoverable txt-bold animated active" data-value="asc">A-Z</div>
											<div active-group="sortir-alphabet" onclick="urutkanAZ(this);" class="txt-center activeable-solo hoverable txt-bold animated" data-value="desc">Z-A</div>
										</div>
										<div class="flex-ver pl-3">
											<!-- <div class="hoverable" onclick="sort('');">Recommended</div> -->
											<div class="hoverable" data-value="startlisting" id="sort-startlisting" onclick="urutkan(this);">Terbaru <i class="fa fa-check"></i></div>
											<div class="hoverable" data-value="harga" id="sort-harga" onclick="urutkan(this);">Harga</div>
											<div class="hoverable" data-value="kamartidur" id="sort-kamartidur" onclick="urutkan(this);">Kamar Tidur</div>
											<div class="hoverable" data-value="kamarmandi" id="sort-kamarmandi" onclick="urutkan(this);">Kamar Mandi</div>
											<div class="hoverable" data-value="luastanah" id="sort-luastanah" onclick="urutkan(this);">Luas Tanah</div>
											<div class="hoverable" data-value="luasbangunan" id="sort-luasbangunan" onclick="urutkan(this);">Luas Bangunan</div>
											<div class="hoverable" data-value="hargam" id="sort-hargam" onclick="urutkan(this);">Harga/m<sup>2</sup></div>
										</div>
									</div>
								</div>
							</div>
							<div class="listing-part-header-right">
								<div class="listing-part-header-tabs">
									<ul class="nav nav-pills" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#tabs-foto" onclick="setTimeout(updateTableScroll,100);">Foto</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#tabs-tabel">Tabel</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-content listing-part-content">
							<div class="tab-pane active" role="tabpanel" id="tabs-foto" style="position:relative;">
								<div class="tab-pane mb-4 row m-0 w-100 content" style="height: calc(100vh - 175px); "></div>
								
							</div>
							<div class="tab-pane" id="tabs-tabel">
								<div class="tab-pane mb-4 w-100" style="height: calc(100vh - 175px); margin-right:10px;">
									<div class="row flex flex-hor" class="tableDetail"  style="align-content: stretch; align-items: stretch; margin-bottom: 10px;">
										<div class="listing-table-detail-listing col-lg-6 col-sm-12" style="height:100%;">
											<div class="m-portlet m-portlet--rounded" style="margin:0px;">
												<?php $gambar = json_encode(["/assets/img/single-list-slider/1.jpg","/assets/img/single-list-slider/2.jpg","/assets/img/single-list-slider/3.jpg"], JSON_OBJECT_AS_ARRAY) ?>
												<div class="m-portlet__head sensecode-slider initiate" data-index="0" data-files='<?= $gambar ?>'>
													<div class="listing-slider-badge">Open House</div>
													<div class="listing-slider-icon"><i class="fas fa-home"></i></div>
													<div class="listing-slider-counter">1 of 3</div>
												</div>
												<div class="m-portlet__body flex flex-ver" style="padding:10px;">
													<div class="col-sm-12" style="padding-left: 0;padding-right:0">
														<div class="listing-price listing-info" id="tablePrice">500 Jt - 900 Jt</div>
														<div class="listing-jalan listing-info" id="tableAlamat">Jalan Kesuma Bangsa XXXi belebeb</div>
														<div class="listing-kota listing-info" id="tableKota">Daerah, Kota</div>
													</div>
													<div class="col-sm-12" style="padding-left: 0;padding-right:0">
														<div class="listing-detail flex flex-hor flex-vertical-center flex-equal">
															<div class="listing-detail-pills flex-4 txt-center" id="tableBed"><i class="fas fa-bed"></i><br>2</div>
															<div class="listing-detail-pills flex-4 txt-center" id="tableBath"><i class="fas fa-bath"></i><br>3</div>
															<div class="listing-detail-pills flex-4 txt-center" id="tableLT"><i class="fas fa-square"></i><br>200m<sup>2</sup></div>
															<div class="listing-detail-pills flex-4 txt-center" id="tableLB"><i class="fas fa-building"></i><br>500m<sup>2</sup></div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="listing-table-detail-agent col-lg-6 col-sm-12" style="padding-left:0px;">
											<div class="flex flex-ver m-portlet m-portlet--rounded" style="align-content:stretch;height: 100%; padding:10px;">
												<div class="flex flex-hor flex-separate flex-vertical-center">
													<div class="flex flex-hor" style='padding-top: 10px; padding-bottom: 10px; margin-right:10px; margin-left:10px'>
														<a href="#" class="user-photo-mini squared" id="tablePhotoBroker" style="margin:0px;background-image:url('<?= (isset($_SESSION["loggedPhoto"])) ? "/assets/img/profile-customer/".$_SESSION["loggedPhoto"] : "/assets/img/profile-customer/default.png" ; ?>');"></a>
													</div>
													<div class="flex flex-ver flex-1" style=" margin:auto">
														<!-- <div class="listing-title listing-info">Dilist oleh</div> -->
														<div class="listing-title listing-info hoverable txt-bold" style="font-size:1.5em;" id="tableBroker">Delvin Limanto</div>
														<div class="listing-title listing-info label" style="font-size:0.9em;" id="tableCabang">ERA Kita Cabang Pakuwon</div>
													</div>
													
												</div>
												<div class="flex flex-hor" style="height:100%; padding:10px">
													<div class="flex flex-ver flex-separate col-lg-10 col-md-10 col-sm-12" style="padding:0px 0px; align-content:stretch;height: 100%; ">
														<div class="row">
															<div class="col-lg-5 col-md-5 col-sm-5">Panjang</div>
															<div class="col-lg-7 col-md-7 col-sm-7 text-right" id="tablePanjang">20 m</div>
														</div>
														<div class="row">
															<div class="col-lg-5 col-md-5 col-sm-5">Lebar</div>
															<div class="col-lg-7 col-md-7 col-sm-7 text-right" id="tableLebar">7 m</div>
														</div>
														<div class="row">
															<div class="col-lg-5 col-md-5 col-sm-5">Sertifikat</div>
															<div class="col-lg-7 col-md-7 col-sm-7 text-right" id="tableSertifikat">HGB</div>
														</div>
														<div class="row">
															<div class="col-lg-5 col-md-5 col-sm-5">Hadap</div>
															<div class="col-lg-7 col-md-7 col-sm-7 text-right" id="tableHadap">Timur</div>
														</div>
														<div class="row">
															<div class="col-lg-5 col-md-5 col-sm-5">Pengembang</div>
															<div class="col-lg-7 col-md-7 col-sm-7 text-right" id="tableDeveloper">Pakuwon</div>
														</div>
														<div class="row">
															<div class="col-lg-5 col-md-5 col-sm-5">Proyek</div>
															<div class="col-lg-7 col-md-7 col-sm-7 text-right" id="tableProyek">Grand Island</div>
														</div>
													</div>
													<div class="col-lg-2 col-md-2 col-sm-12 listing-content-right" style="padding-left: 0;padding-right:0">
														<div class="listing-content-icon"><i style="font-size: 1.3em;" class="fab fa-whatsapp"></i></div>
														<div class="listing-content-icon"><i style="font-size: 1.3em;" class="fas fa-heart heart-animated"></i></div>
														<div class="listing-content-icon"><i style="font-size: 1.3em;" class="fas fa-share"></i></div>
														<div class="listing-content-icon"><i style="font-size: 1.3em;" class="fas fa-ellipsis-h"></i></div>
													</div>

												</div>
												
											</div>
										</div>
									</div>
									<div class="listing-table-detail">
										<div class="m-portlet m-portlet--mobile">
											<div class="m-portlet__body p-0 minimalistScrollbar" style="overflow-x:auto;">
												<!-- 
													<div>
														<table id="data-app" class="sense-table" border="0" bordered="true" stripe="true">
															<thead>
																<th>
																	<div class="flex flex-vertical-center flex-hor" style="margin:auto; justify-content:center;">
																		<div style="display:inline-block" class="mr-2">
																			Listing
																		</div>
																		<div style="display:inline-block" class="">
																			<i style="display:block" class="fas sort fa-sm fa-caret-up"></i>
																			<i style="display:block" class="fas sort fa-sm fa-caret-down "></i>
																		</div>
																	</div>
																</th>
																
																<th>
																	<div class="flex flex-vertical-center flex-hor" style="margin:auto; justify-content:center;">
																		<div style="display:inline-block" class="mr-2">
																			Alamat
																		</div>
																		<div style="display:inline-block" class="">
																			<i style="display:block" class="fas sort fa-sm fa-caret-up"></i>
																			<i style="display:block" class="fas sort fa-sm fa-caret-down "></i>
																		</div>
																	</div>
																</th>
																<th>
																	<div class="flex flex-vertical-center flex-hor" style="margin:auto; justify-content:center;">
																		<div style="display:inline-block" class="mr-2">
																			Jadwal
																		</div>
																		<div style="display:inline-block" class="">
																			<i style="display:block" class="fas sort fa-sm fa-caret-up"></i>
																			<i style="display:block" class="fas sort fa-sm fa-caret-down "></i>
																		</div>
																	</div>
																</th>
																<th>
																	<div class="flex flex-vertical-center flex-hor" style="margin:auto; justify-content:center;">
																		<div style="display:inline-block" class="mr-2">
																			Customer
																		</div>
																		<div style="display:inline-block" class="">
																			<i style="display:block" class="fas sort fa-sm fa-caret-up"></i>
																			<i style="display:block" class="fas sort fa-sm fa-caret-down "></i>
																		</div>
																	</div>
																</th>
																<th>
																	<div class="flex flex-vertical-center flex-hor" style="margin:auto; justify-content:center;">
																		<div style="display:inline-block" class="mr-2">
																			Status
																		</div>
																		<div style="display:inline-block" class="">
																			<i style="display:block" class="fas sort fa-sm fa-caret-up"></i>
																			<i style="display:block" class="fas sort fa-sm fa-caret-down "></i>
																		</div>
																	</div>
																</th>
																<th>
																	<div class="flex flex-vertical-center flex-hor" style="margin:auto; justify-content:center;">
																		<div style="display:inline-block" class="mr-2">
																			Action
																		</div>
																		<div style="display:inline-block" class="">
																			<i style="display:block" class="fas sort fa-sm fa-caret-up"></i>
																			<i style="display:block" class="fas sort fa-sm fa-caret-down "></i>
																		</div>
																	</div>
																</th>
															</thead>
															<tbody id="content-body-appointment">
																<tr>
																	<td class="td-judul">
																		<div class="truncate" style="cursor:pointer;" data-container="body" data-toggle="m-popover" data-placement="top" data-content="Dijual Rumah Bagus Di Rumah Ku" data-original-title="" title="">Dijual Rumah Bagus Di Rumah Ku</div>
																	</td>
																	<td class="td-alamat">
																		<div class="truncate" style="cursor:pointer;" data-container="body" data-toggle="m-popover" data-placement="top" data-content="alamat rumahe legit pul gasih" data-original-title="" title="">alamat rumahe legit pul gasih</div>
																	</td>
																	<td style="text-align:center">Kamis, 17 Sep 2019<br><span>18:00</span></td>
																	<td>
																		<div class="flex flex-hor flex-vertical-center" style="position:relative;">
																			<a href="#" class="user-photo-mini" style="width:50px; height:50px;margin:5px 5px; background-image:url('<?= (isset($_SESSION["loggedPhoto"])) ? "/assets/img/profile-customer/".$_SESSION["loggedPhoto"] : "/assets/img/profile-customer/default.png" ; ?>');"></a>
																			<div class="flex flex-ver" style='justify-content: center;'>
																				<div><b>Willams Gunawan</b></div>
																				<div class="flex flex-hor flex-vertical-center">
																					<div class="" style="margin:auto;"><i onclick='sendWA(this);' style="font-size: 1.3em;" class="padding-s fab fa-whatsapp" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Pesan WhatsApp"></i></div>
																					<div class="" style="margin:auto;"><i onclick='sendWA(this);' style="font-size: 1.3em;" class="padding-s fas fa-phone" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Call Dial"></i></div>
																				</div>
																			</div>
		
																		</div>
																	</td>
																	<td align="center">
																		<span class="m-badge m-badge--brand pr-2 pl-2 pt-1 pb-1 w-100">Pending</span>
																	</td>
																	<td>
																		<div class="flex flex-hor flex-vertical-center flex-wrap flex-equal">
																			<button type="button" data-toggle="modal" data-target="#m_modal_updateAppointments" class="btn btn-info mb-1 w-100">Handle</button>
																		</div>
																	</td>
																</tr>
															</tbody>
														</table>
													</div> 
												-->
												<!--begin: Datatable -->
												<div>
													<table minitable class="sense-table mb-0" border="0" bordered="true" stripe="true">
														<thead>
															<tr>
																<th></th>
																<th>Alamat</th>
																<th>Kota</th>
																<th>Harga</th>
																<th>KT</th>
																<th>KM</th>
																<th>LB</th>
																<th>LT</th>
																<th>Harga/m<sup>2</sup></th>
															</tr>
														</thead>
														<tbody id="table-data">
														</tbody>
														<tfoot>
															<tr>
																<th></th>
																<th>Alamat</th>
																<th>Kota</th>
																<th>Harga</th>
																<th>KT</th>
																<th>KM</th>
																<th>LB</th>
																<th>LT</th>
																<th>Harga/m<sup>2</sup></th>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>				
							</div>
							<div class="sensecode-paginate" id="listing-pagination" style="padding-bottom:10px"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Page end -->
	</main>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>

	<!-- load for map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyJi7G0CbtkyT9qjITRHKHcg8tKCZ1tpk"></script>
	<!-- <script src="/assets/js/map-2.js"></script> -->
	<script type="text/javascript" src="/assets/json/data.json"></script>
	<script type="text/javascript" src="/assets/js/markerclusterer.js"></script>
	<script>

	</script>
	<?php 
		$set['footer']=false;
		$this->load->view('page-part/common-foot', $set);
	?>
	<script src="https://cdn.rawgit.com/hayeswise/Leaflet.PointInPolygon/v1.0.0/wise-leaflet-pip.js"></script>
</body>

</html>
