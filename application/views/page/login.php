<!DOCTYPE html>
<html lang="en">

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="/assets/css/materialize.min.css" media="screen,projection" />

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EraKita | Login</title>
    <link rel="stylesheet" href="/assets/css/loginregister.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <style>
    .field-icon {
  float: right;
  margin-left: -25px;
  margin-top: -35px;
  position: relative;
  z-index: 2;
}
 /* label focus color */
 .input-field input[type=text]:focus + label {
    color: #e64a19 !important;
}
/* label underline focus color */
  .input-field input[type=text]:focus {
    border-bottom: 1px solid #e64a19 !important;
    box-shadow: 0 1px 0 0 #e64a19 !important;
  }
    </style>
    
</head>

<body>
    <div class="background">
        <div class="layer"></div>
        <div class="form">
            <div class="form-content">
                <h1 style="margin:0px">LOGIN</h1>
                <div class="row">
                    <form class="col s12">
                        <div class="row" style="margin-bottom:0px">
                            <div class="input-field col s12 ">
                                    <i class="material-icons prefix">phone</i>
                                    <input id="telephone" type="tel" class="validate" value="62" maxlength=16>
                                    <label for="telephone">Telephone</label>
                                    <span class="helper-text" data-error="" data-success="">Ex: +62 811 222 XXXXX</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                    <i class="material-icons prefix">vpn_key</i>
                                    <input id="password" type="password" class="validate">
                                    <label for="password">Password</label>
                                    <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                    <a class="waves-effect waves-light btn deep-orange darken-2" style="width:100%" id="btnLogin">Login</a>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom:0px">
                            <div class="col s12">
                                    <a class="waves-effect waves-light btn  deep-orange lighten-2" style="width:100%" href="/register">Register</a>
                            </div>
                        </div>

                    </form>
                </div>

            </div>

        </div>
    </div>
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="/assets/js/materialize.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/assets/js/main.js"></script>
</body>

</html>
<script>
    $(document).ready(function(){
        $("#btnLogin").click(function(){
            var sendData = { telp: $("#telephone").val(),
                password: $("#password").val()
            }; 
            $.post({
                "url" : "/auth/login",
                data: JSON.stringify(sendData),
                success : function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    if (data['key'] == 1){
                        if (data['role']=="CU"){
                            window.location = "/";
                        }else{
                            window.location = ADMIN_URL;
                        }
                    }
                }
            });
        });
    });
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>