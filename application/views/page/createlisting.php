<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->
	<!-- Header section -->
	<?php $this->load->view('page-part/header-saya');?>
	<!-- Header section end -->
	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->
	<div class="m-content">
		<div class="row">
		<div class="col-xl-2"></div>
		<div class="col-xl-8" style="padding-top: 110px;">

			<!--Begin::Main Portlet-->
			<div class="m-portlet m-portlet--full-height">
				<!--begin: Portlet Head-->
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 id="titleWizardListing" class="m-portlet__head-text">
								New Listing
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								
							</li>
						</ul>
					</div>
				</div>
				<!--end: Portlet Head-->
				<!--begin: Form Wizard-->
				<div class="m-wizard m-wizard--2 m-wizard--success" id="m_wizard1">
					<!--begin: Message container -->
					<div class="m-portlet__padding-x">
						<!-- Here you can put a message or alert -->
					</div>
					<!--end: Message container -->
					<!--begin: Form Wizard Head -->
					<div class="m-wizard__head m-portlet__padding-x">
						<!--begin: Form Wizard Progress -->
						<div class="m-wizard__progress">
							<div class="progress">
								<div class="progress-bar" role="progressbar"  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						<!--end: Form Wizard Progress -->  
						<!--begin: Form Wizard Nav -->
						<div class="m-wizard__nav">
							<div class="m-wizard__steps">
								<div class="m-wizard__step m-wizard__step--current"  m-wizard-target="m_wizard1_form_step_1">
									<a href="#"  class="m-wizard__step-number">
										<span>
											<i class="fa  flaticon-placeholder"></i>
										</span>
									</a>
									<div class="m-wizard__step-info">
										<div class="m-wizard__step-title">
											1. Deskripsi Listing
										</div>
										<div class="m-wizard__step-desc">
											Berisikan informasi
											<br>
											deskripsi listing
										</div>
									</div>
								</div>
								<div class="m-wizard__step" m-wizard-target="m_wizard1_form_step_2">
									<a href="#" class="m-wizard__step-number">
										<span>
											<i class="fa  flaticon-layers"></i>
										</span>
									</a>
									<div class="m-wizard__step-info">
										<div class="m-wizard__step-title">
											2. Lokasi Listing
										</div>
										<div class="m-wizard__step-desc">
											Berisikan informasi
											<br>
											lokasi listing
										</div>
									</div>
								</div>
								<div class="m-wizard__step" m-wizard-target="m_wizard1_form_step_3">
									<a href="#" class="m-wizard__step-number">
										<span>
											<i class="fa  flaticon-layers"></i>
										</span>
									</a>
									<div class="m-wizard__step-info">
										<div class="m-wizard__step-title">
											3. Detail Listing
										</div>
										<div class="m-wizard__step-desc">
											Berisikan informasi
											<br>
											detail listing
										</div>
									</div>
								</div>
								<div class="m-wizard__step" m-wizard-target="m_wizard1_form_step_4">
									<a href="#" class="m-wizard__step-number">
										<span>
											<i class="fa  flaticon-layers"></i>
										</span>
									</a>
									<div class="m-wizard__step-info">
										<div class="m-wizard__step-title">
											4. Konfirmasi Data
										</div>
										<div class="m-wizard__step-desc">
											Silahkan cek data
											<br>
											sebelum disimpan
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--end: Form Wizard Nav -->
					</div>
					<!--end: Form Wizard Head -->  
					<!--begin: Form Wizard Form-->
					<div class="m-wizard__form">
						<form class="m-form m-form--label-align-left m-form--state" id="m_form1">
							<!--begin: Form Body -->
							<div class="m-portlet__body">
								<!--begin: Form Wizard Step 1-->
								<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard1_form_step_1">
									<div class="row">
										<div class="col-xl-8 offset-xl-2">
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">
														Deskripsi Listing
													</h3>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														* Judul:
													</label>
													<div class="col-xl-9 col-lg-9">
														<input type="text" name="judulListing" id="judulListing" class="form-control m-input" placeholder="Judul" minlength=10 maxlength=100>
														<span class="m-form__help">
															Please enter listing name
														</span>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														* Deskripsi:
													</label>
													<div class="col-xl-9 col-lg-9">
														<textarea class="summernote" name="deskripsiListing" id="deskripsiListing"></textarea>
														<span class="m-form__help">
															(Optional) Decription about listing
														</span>
													</div>
												</div>
											</div>
											<div class="m-separator m-separator--dashed m-separator--lg"></div>
											<div class="m-form__section">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">
														Tipe & Jenis Transaksi
														<i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Some help text goes here"></i>
													</h3>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														Tipe Listing:
													</label>
													<div class="col-xl-9 col-lg-9">
														<select id="tipeListing" name="tipeListing" class="form-control m-bootstrap-select m_selectpicker" data-size="4">
															<option data-icon="la la-home" value="1">Rumah</option>
															<option data-icon="la la-building" value="2">Apartement</option>
															<option data-icon="la la-archive" value="3">Gudang</option>
															<option data-icon="la la-industry" value="4">Ruko</option>
															<option data-icon="la la-road" value="5">Tanah</option>
															<option data-icon="la la-industry" value="6">Pabrik</option>
														</select>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														Tipe Transaksi:
													</label>
													<div class="col-xl-9 col-lg-9">
														<select name="transaksiListing" id="transaksiListing" class="form-control m-bootstrap-select m_selectpicker">
															<option value="0">Jual</option>
															<option value="1">Sewa</option>
														</select>
													</div>
												</div>
												<div class="form-group m-form__group row" id="jualStat">
													<label class="col-xl-3 col-lg-3 col-form-label">
														Harga Jual:
													</label>
													<div class="col-xl-9 col-lg-9">
														<input type='text' class="form-control" id="hargaJualListing" type="text"/>
													</div>
												</div>
												<div class="form-group m-form__group row" id="sewaStat" style="display:none">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															* Harga Sewa:
														</label>
														<input type='text' class="form-control" id="hargaSewaListing" type="text"/>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															* Minim Sewa:
														</label>
														<input type='text' class="form-control" id="minimSewaListing" type="text"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														* Nego:
													</label>
													<div class="col-xl-9 col-lg-9">
														<input data-switch="true" type="checkbox" id="negoListing" checked="checked" data-on-text="Nego" data-handle-width="50" data-off-text="Tidak" data-on-color="success">
													</div>
												</div>
											</div>
											<div class="m-separator m-separator--dashed m-separator--lg"></div>
											<div class="m-form__section">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">
														Media
														<i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Some help text goes here"></i>
													</h3>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														Photo:
													</label>
													<div class="col-xl-9 col-lg-9">
														<div class="m-dropzone dropzone m-dropzone--success" action="/test/proses_upload" id="m-dropzone-three">
															<div class="m-dropzone__msg dz-message needsclick">
																<h3 class="m-dropzone__msg-title">
																	Drop files here or click to upload.
																</h3>
																<span class="m-dropzone__msg-desc">
																	Only image, pdf and psd files are allowed for upload
																</span>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														URL:
													</label>
													<div class="col-xl-9 col-lg-9">
														<input type="text" name="urlListing" id="urlListing" class="form-control m-input">
														<span class="m-form__help">
															Please enter video URL
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end: Form Wizard Step 1-->
								<!--begin: Form Wizard Step 2-->
								<div class="m-wizard__form-step" id="m_wizard1_form_step_2">
									<div class="row">
										<div class="col-xl-8 offset-xl-2">
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">
														Location Details
													</h3>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-12">
														<label class="form-control-label">
															* Alamat:
														</label>
														<input type="text" name="alamatListing" id="alamatListing" class="form-control m-input" placeholder="Alamat"/>
														<input type="hidden" name="latListing" id="latListing"/>
														<input type="hidden" name="lngListing" id="lngListing"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															Blok:
														</label>
														<input type="text" name="blokListing" id="blokListing" class="form-control m-input" placeholder="Blok"/>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															Nomor:
														</label>
														<input type="text" name="nomorListing" id="nomorListing" class="form-control m-input" placeholder="Nomor"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															* Propinsi:
														</label>
														<input type="text" name="provinsiListing" id="provinsiListing" class="form-control m-input" placeholder="Propinsi"/>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															* Kota:
														</label>
														<input type="text" name="kotaListing" id="kotaListing" class="form-control m-input" placeholder="Kota"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															* Kecamatan:
														</label>
														<input type="text" name="kecamatanListing" id="kecamatanListing" class="form-control m-input" placeholder="Kecamatan"/>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															* Kelurahan:
														</label>
														<input type="text" name="kelurahanListing" id="kelurahanListing" class="form-control m-input" placeholder="Kelurahan"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-12">
														<label class="form-control-label">
															* Kode Pos:
														</label>
														<input type="text" name="kodeposListing" id="kodeposListing" class="form-control m-input" placeholder="Kode Pos" maxlength=5/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														* Area:
													</label>
													<div class="col-xl-9 col-lg-9">
														<select class="form-control m-select2" id="areaListing" name="param" multiple>
															<option value="1">Krembangan</option>
															<option value="HI">Hawaii</option>
															<option value="AK">Alaska</option>
															<option value="HI">Hawaii</option>
														</select>
													</div>
												</div>
											</div>
											<div class="m-separator m-separator--dashed m-separator--lg"></div>
											<div class="m-form__section">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">
														Maps
													</h3>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-xl-12" style="padding: 0px 30px;">	
														<div id="mapListing" style="height:300px;"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end: Form Wizard Step 2--> 
								<!--begin: Form Wizard Step 3-->
								<div class="m-wizard__form-step" id="m_wizard1_form_step_3">
									<div class="row">
										<div class="col-xl-8 offset-xl-2">
											<div class="m-form__section m-form__section--first">
												<div class="m-form__heading">
													<h3 class="m-form__heading-title">
														Detail Listing
													</h3>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															* Panjang Tanah(m):
														</label>
														<input type="number" name="ptanahListing" id="ptanahListing" class="form-control m-input" placeholder="Panjang Tanah"/>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															* Lebar Tanah(m):
														</label>
														<input type="number" name="ltanahListing" id="ltanahListing" class="form-control m-input" placeholder="Lebar Tanah"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															* Luas Tanah(m<sup>2</sup>):
														</label>
														<input type="number" name="lutanahListing" id="lutanahListing" class="form-control m-input" placeholder="Luas Tanah"/>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															Luas Bangunan(m<sup>2</sup>):
														</label>
														<input type="number" name="lubangunanListing" id="lubangunanListing" class="form-control m-input" placeholder="Luas Bangunan"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															Kamar Tidur:
														</label>
														<input type="number" name="ktidurListing" id="ktidurListing" class="form-control m-input" placeholder="Jumlah Kamar Tidur"/>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															Kamar Mandi:
														</label>
														<input type="number" name="kmandiListing" id="kmandiListing" class="form-control m-input" placeholder="Jumlah Kamar Mandi"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															Garasi:
														</label>
														<input type="number" name="garasiListing" id="garasiListing" class="form-control m-input" placeholder="Jumlah Garasi"/>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															Jumlah Lantai:
														</label>
														<input type="number" name="lantaiListing" id="lantaiListing" class="form-control m-input" placeholder="Jumlah Lantai"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															Listrik:
														</label>
														<input type="number" name="listrikListing" id="listrikListing" class="form-control m-input" placeholder="Total Listrik"/>
														<span class="m-form__help">
															Dalam Satuan W
														</span>
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">
															Tahun Dibangun:
														</label>
														<input type="text" class="form-control" id="tahunListing" readonly placeholder="Select Years"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														* Orientasi Bangunan:
													</label>
													<div class="col-xl-9 col-lg-9">
														<select id="orientasiListing" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true">
															<option value="0">Utara</option>
															<option value="1">Timur</option>
															<option value="2">Selatan</option>
															<option value="3">Barat</option>
														</select>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														* Sertifikat:
													</label>
													<div class="col-xl-9 col-lg-9">
														<select id="sertifikatListing" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true">
															<option value="0">Jual</option>
															<option value="1">Sewa</option>
															<option value="2">Jual Sewa</option>
														</select>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														* Hoek:
													</label>
													<div class="col-xl-9 col-lg-9">
														<input data-switch="true" type="checkbox" id="hoekListing" checked="checked" data-on-text="Ya" data-handle-width="50" data-off-text="Tidak" data-on-color="success">
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-xl-3 col-lg-3 col-form-label">
														* Termasuk Interior:
													</label>
													<div class="col-xl-9 col-lg-9">
														<input data-switch="true" type="checkbox" id="interiorListing" checked="checked" data-on-text="Ya" data-handle-width="50" data-off-text="Tidak" data-on-color="success">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end: Form Wizard Step 3--> 
								<!--begin: Form Wizard Step 4-->
								<div class="m-wizard__form-step" id="m_wizard1_form_step_4">
									<div class="row">
										<div class="col-xl-8 offset-xl-2">
											<!--begin::Section-->
											<ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--danger" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_form1_confirm_1" role="tab">
														1. Deskripsi Listing
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_form1_confirm_2" role="tab">
														2. Lokasi Listing
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_form1_confirm_3" role="tab">
														3. Detail Listing
													</a>
												</li>
											</ul>
											<div class="tab-content m--margin-top-40">
												<div class="tab-pane active" id="m_form1_confirm_1" role="tabpanel">
													<div class="m-form__section m-form__section--first">
														<div class="m-form__heading">
															<h4 class="m-form__heading-title">
																Deskripsi Listing
															</h4>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Judul
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="judulListing_preview" class="m-form__control-static">
																	Nick Stone
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Deskripsi
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="deskripsiListing_preview" class="m-form__control-static">
																	+206-78-55034890
																</span>
															</div>
														</div>
													</div>
													<div class="m-separator m-separator--dashed m-separator--lg"></div>
													<div class="m-form__section">
														<div class="m-form__heading">
															<h4 class="m-form__heading-title">
																Tipe & Jenis Transaksi
																<i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Some help text goes here"></i>
															</h4>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Tipe Listing
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="tipeListing_preview" class="m-form__control-static">
																	Headquarters 1120 N Street Sacramento 916-654-5266
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Tipe Transaksi
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="transaksiListing_preview" class="m-form__control-static">
																	Headquarters 1120 N Street Sacramento 916-654-5266
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row" id="jualStat_Preview">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Harga Jual
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="hargajualListing_preview" class="m-form__control-static">
																	Headquarters 1120 N Street Sacramento 916-654-5266
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row" id="sewaStat_Preview">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Harga Sewa:
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="hargasewaListing_preview" class="m-form__control-static">
																	Headquarters 1120 N Street Sacramento 916-654-5266
																</span>
															</div>
															<label class="col-xl-3 col-lg-3 col-form-label">
																Minim Sewa:
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="minimsewaListing_preview" class="m-form__control-static">
																	Headquarters 1120 N Street Sacramento 916-654-5266
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Nego
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="negoListing_preview" class="m-form__control-static">
																	Headquarters 1120 N Street Sacramento 916-654-5266
																</span>
															</div>
														</div>
													</div>
													<div class="m-separator m-separator--dashed m-separator--lg"></div>
													<div class="m-form__section">
														<div class="m-form__heading">
															<h4 class="m-form__heading-title">
																Media
																<i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Some help text goes here"></i>
															</h4>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Video
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="videoListing_preview" class="m-form__control-static">
																	Headquarters 1120 N Street Sacramento 916-654-5266
																</span>
															</div>
														</div>
													</div>
												</div>
												<div class="tab-pane" id="m_form1_confirm_2" role="tabpanel">
													<div class="m-form__section m-form__section--first">
														<div class="m-form__heading">
															<h4 class="m-form__heading-title">
																Location Details
															</h4>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Alamat
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="alamatListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Blok
															</label>
															<div class="col-xl-3 col-lg-3">
																<span id="blokListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
															<label class="col-xl-3 col-lg-3 col-form-label">
																Nomor
															</label>
															<div class="col-xl-3 col-lg-3">
																<span id="nomorListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Provinsi
															</label>
															<div class="col-xl-3 col-lg-3">
																<span id="provinsiListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
															<label class="col-xl-3 col-lg-3 col-form-label">
																Kota
															</label>
															<div class="col-xl-3 col-lg-3">
																<span id="kotaListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Kecamatan
															</label>
															<div class="col-xl-3 col-lg-3">
																<span id="kecamatanListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
															<label class="col-xl-3 col-lg-3 col-form-label">
																Kelurahan
															</label>
															<div class="col-xl-3 col-lg-3">
																<span id="kelurahanListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Kode Pos
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="kodeposListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Area
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="areaListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
													</div>
												</div>
												<div class="tab-pane" id="m_form1_confirm_3" role="tabpanel">
													<div class="m-form__section m-form__section--first">
														<div class="m-form__heading">
															<h4 class="m-form__heading-title">
																Detail Listing
															</h4>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Panjang Tanah(m)
															</label>
															<div class="col-xl-3 col-lg-3">
																<span id="ptanahListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
															<label class="col-xl-4 col-lg-4 col-form-label">
																Lebar Tanah(m)
															</label>
															<div class="col-xl-2 col-lg-2">
																<span id="ltanahListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Luas Tanah(m<sup>2</sup>)
															</label>
															<div class="col-xl-3 col-lg-3">
																<span id="lutanahListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
															<label class="col-xl-4 col-lg-4 col-form-label">
																Luas Bangunan(m<sup>2</sup>)
															</label>
															<div class="col-xl-2 col-lg-2">
																<span id="lubangunanListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Kamar Tidur
															</label>
															<div class="col-xl-3 col-lg-3">
																<span id="ktidurListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
															<label class="col-xl-4 col-lg-4 col-form-label">
																Kamar Mandi
															</label>
															<div class="col-xl-2 col-lg-2">
																<span id="kmandiListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Garasi
															</label>
															<div class="col-xl-3 col-lg-3">
																<span id="garasiListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
															<label class="col-xl-4 col-lg-4 col-form-label">
																Jumlah Lantai
															</label>
															<div class="col-xl-2 col-lg-2">
																<span id="lantaiListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Listrik
															</label>
															<div class="col-xl-3 col-lg-3">
																<span id="listrikListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
															<label class="col-xl-4 col-lg-4 col-form-label">
																Tahun Dibangun
															</label>
															<div class="col-xl-2 col-lg-2">
																<span id="tahunListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Orientasi
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="orientasiListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Sertifikat
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="sertifikatListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Termasuk Interior
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="interiorListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
														<div class="form-group m-form__group m-form__group--sm row">
															<label class="col-xl-3 col-lg-3 col-form-label">
																Termasuk Interior
															</label>
															<div class="col-xl-9 col-lg-9">
																<span id="interiorListing_preview" class="m-form__control-static">
																	sinortech.vertoffice.com
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!--end::Section-->
											<!--end::Section-->
										</div>
									</div>
								</div>
								<!--end: Form Wizard Step 4-->
							</div>
							<!--end: Form Body -->
							<!--begin: Form Actions -->
							<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
								<div class="m-form__actions">
									<div class="row">
										<div class="col-lg-2"></div>
										<div class="col-lg-4 m--align-left">
											<a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
												<span>
													<i class="la la-arrow-left"></i>
													&nbsp;&nbsp;
													<span>
														Back
													</span>
												</span>
											</a>
										</div>
										<div class="col-lg-4 m--align-right">
											<a id="actionListing" href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
												<span>
													<i class="la la-check"></i>
													&nbsp;&nbsp;
													<span id="ketSubmitListing">
														Submit
													</span>
												</span>
											</a>
											<a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" id="saveAsDraft" style="display:none">
												<span>
													<i class="la la-check"></i>
													&nbsp;&nbsp;
													<span>
														Save As Draft
													</span>
												</span>
											</a>
											<a href="#" class="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
												<span>
													<span>
														Continue
													</span>
													&nbsp;&nbsp;
													<i class="la la-arrow-right"></i>
												</span>
											</a>
										</div>
										<div class="col-lg-2"></div>
									</div>
								</div>
							</div>
							<!--end: Form Actions -->
						</form>
					</div>
					<!--end: Form Wizard Form-->
				</div>
				<!--end: Form Wizard-->
			</div>
			<!--End::Main Portlet-->
		</div>
	</div>
</div>

<script>
	// This example adds a search box to a map, using the Google Place Autocomplete
	// feature. People can enter geographical searches. The search box will return a
	// pick list containing a mix of places and predicted search terms.

	// This example requires the Places library. Include the libraries=places
	// parameter when you first load the API. For example:
	// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

	function initAutocomplete() {
		var map = new google.maps.Map(document.getElementById('mapListing'), {
			center: {lat: -7.24917, lng: 112.75083},
			zoom: 13,
			mapTypeId: 'roadmap'
		});

		// Create the search box and link it to the UI element.
		var input = document.getElementById('alamatListing');
		var searchBox = new google.maps.places.SearchBox(input);
		// map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

		// Bias the SearchBox results towards current map's viewport.
		map.addListener('bounds_changed', function() {
			searchBox.setBounds(map.getBounds());
		});
		var marker = new google.maps.Marker({
			position: {lat: -7.24917, lng: 112.75083},
			map: map
		});
		google.maps.event.addListener(map, 'dblclick', function(e) {
			var positionDoubleclick = e.latLng;
			var sendData = {"lat" : positionDoubleclick.lat(), "long" : positionDoubleclick.lng()};
			$.post(API_URL + "/api/get/detaillocation/data?token=junior", JSON.stringify(sendData), function (data, status) {
				$("#alamatListing").val(data['formatted_address']);
				$("#provinsiListing").val(data['administrative_area_level_1']);
				$("#kotaListing").val(data['administrative_area_level_2']);
				$("#kecamatanListing").val(data['administrative_area_level_3']);
				$("#kelurahanListing").val(data['administrative_area_level_4']);
				$("#kodeposListing").val(data['postal_code']);
				$("#latListing").val(data['latitude']);
				$("#lngListing").val(data['longitude']);
			}, 'json');
			marker.setPosition(positionDoubleclick);
			// if you don't do this, the map will zoom in
			e.stop();
			e.cancelBubble = true;
			if (e.stopPropagation) {
				e.stopPropagation();
			}
			if (e.preventDefault) {
				e.preventDefault(); 
			} else {
				e.returnValue = false;  
			}
		});

		var markers = [];
		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener('places_changed', function() {
			var places = searchBox.getPlaces();

			if (places.length == 0) {
			return;
			}

			// Clear out the old markers.
			markers.forEach(function(marker) {
				marker.setMap(null);
			});
			markers = [];

			// For each place, get the icon, name and location.
			var bounds = new google.maps.LatLngBounds();
			places.forEach(function(place) {
				if (!place.geometry) {
					console.log("Returned place contains no geometry");
					return;
				}

				var icon = {
					url: place.icon,
					size: new google.maps.Size(71, 71),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(17, 34),
					scaledSize: new google.maps.Size(25, 25)
				};

				// Create a marker for each place.
				markers.push(new google.maps.Marker({
					map: map,
					icon: icon,
					title: place.name,
					position: place.geometry.location
				}));

				if (place.geometry.viewport) {
					// Only geocodes have viewport.
					bounds.union(place.geometry.viewport);
				} else {
					bounds.extend(place.geometry.location);
				}
			});
			map.fitBounds(bounds);
			var sendData = {"jalan" : $("#alamatListing").val()};

			$.post(API_URL + "/api/get/detaillocation/data?token=junior", JSON.stringify(sendData), function (data, status) {
				$("#provinsiListing").val(data['administrative_area_level_1']);
				$("#kotaListing").val(data['administrative_area_level_2']);
				$("#kecamatanListing").val(data['administrative_area_level_3']);
				$("#kelurahanListing").val(data['administrative_area_level_4']);
				$("#kodeposListing").val(data['postal_code']);
				$("#latListing").val(data['latitude']);
				$("#lngListing").val(data['longitude']);
			}, 'json');
		});
	}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyJi7G0CbtkyT9qjITRHKHcg8tKCZ1tpk&libraries=places&callback=initAutocomplete" async defer></script>
	<?php 
		$this->load->view('page-part/common-foot');
	?>
</body>
</html>