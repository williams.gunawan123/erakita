<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->

	<div class="modal fade" id="m_modal_contact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document" style="transform: translateX(8px);">
			<div class="modal-content" style="background-color:#fff0; box-shadow:none;">
				<div class="modal-header" style="padding:10px 5px; border:none;">
					<h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="background-color:white;">
					<div style="font-size:1.4em; text-align:center;">Bagaimana kami bisa menghubungi anda?</div><br>
					<div class="margin-row-sm"><span>Pesan</span></div>
					<input class="margin-row-md" type="text" placeholder="Masukkan Pesan Anda Disini ..." style="width:100%">
					<div class="margin-row-sm"><span>Telepon</span></div>
					<input class="margin-row-lg" type="text" placeholder="Masukkan Nomor Anda Disini ..." style="width:100%">
					<a data-href="https://wa.me/" data-phone="6287853265186" href="" class="btn btn-danger bg-red" style="width:100%;">Kirim Pesan</a>
					
				</div>
				<!--div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div -->
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="m_modal_6"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document" style="max-width:97%; transform: translateX(8px);">
			<div class="modal-content" style="background-color:#fff0; box-shadow:none;">
				<div class="modal-header" style="padding:10px 5px; border:none;">
					<h5 class="modal-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="background-color:white; padding:0px;">
					
					<div id="mapListing" style="position: relative;overflow: hidden;width: 100%;height: 88vh;"></div>
					
					<div class="m-ion-range-slider map-slider" style="width:30%; position:absolute; left:5px; bottom: 5px; background-color: #00000075; border-radius: 5px; padding: 10px;">
							<input type="hidden" id="map-slider" />
					</div>
					<div id="rightMap">
						<div class="flex flex-ver" style="height:100%;">
							<div class="flex flex-hor minimalistScrollbar" style="max-height:25%; overflow-x:auto;padding:10px;">
								<div class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable"><span>Sekitarnya</span></div>
								<div class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable"><span>Keluarga</span></div>
								<div class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable"><span>GayaHidup</span></div>
								<div class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable"><span>Sport</span></div>
								<div class="mapCategoryFilter flex-vertical-center flex bg-red rounded-s margin-col-sm activeable"><span>Entertainment</span></div>
							</div>
							<div class="flex flex-ver minimalistScrollbar" style="background-color:#e4e4e4; height:100%; padding:0px 20px; overflow-y:auto;">
								<div class="mapNearPlace">
									
									<h6 class="mapNearCategory">Sekolah <span>x</span></h6>
									<div class="flex-flex-ver mapNearItemContainer minimalistScrollbar" style="max-height:200px; overflow-y:auto;">
										<?php for ($i=0; $i < 10; $i++) { ?>
										
										
										<div class="flex flex-hor flex-vertical-center mapItem">
											<div class="mapItemName">Sekolah Menengah Atas Negeri 9 Surabaya</div>
											<div class="flex flex-ver">
												<div class="map-time">3 Mins</div>
												<div class="map-far">230 m</div>
											</div>
										</div>
									
									<?php } ?>
									</div>
								</div>
								
								<div class="mapNearPlace">
									
									<h6 class="mapNearCategory">Rumah Sakit <span>x</span></h6>
									<div class="flex-flex-ver mapNearItemContainer minimalistScrollbar" style="max-height:200px; overflow-y:auto;">
										<?php for ($i=0; $i < 10; $i++) { ?>
										
										
										<div class="flex flex-hor flex-vertical-center mapItem">
											<div class="mapItemName">Rumah Sakit ke Lima Puluh Enam Surabaya</div>
											<div class="flex flex-ver">
												<div class="map-time">3 Mins</div>
												<div class="map-far">230 m</div>
											</div>
										</div>
									
									<?php } ?>
									</div>
								</div>


								
								<div class="mapNearPlace">
									
									<h6 class="mapNearCategory">Gym <span>x</span></h6>
									<div class="flex-flex-ver mapNearItemContainer minimalistScrollbar" style="max-height:200px; overflow-y:auto;">
										<?php for ($i=0; $i < 10; $i++) { ?>
										
										
										<div class="flex flex-hor flex-vertical-center mapItem">
											<div class="mapItemName">Regency 21 Surabaya</div>
											<div class="flex flex-ver">
												<div class="map-time">3 Mins</div>
												<div class="map-far">230 m</div>
											</div>
										</div>
									
									<?php } ?>
									</div>
								</div>



							</div>
						</div>
					</div>
				</div>
				<!--div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div-->
			</div>
		</div>
	</div>
	
	<?php 
		$this->load->view("/page-part/header-saya");
	?>

	<!-- Breadcrumb -->
	<div class="site-breadcrumb" style="margin-top: 100px;">
		<div class="my-container">
			<a href=""><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i>Jadwal</span>
		</div>
	</div>

    
	<section class="my-container flex flex-ver">
        <div class="flex flex-hor flex-separate margin-row-md">
            <h2>Jadwal Saya</h2>
            <!--div class="txt-white clickable bg-red txt-bold padding-m rounded-m"><span style="">Membuat Wishlist</span></div-->
        </div>
        <table class="my-table">
            <thead>
                <th>
                    Listing
                </th>
                <th>
                    Hari
                </th>
                <th>
					Waktu & Tanggal
                </th>
                <th>
                    Broker
                </th>
                <th>
                    Status
                </th>
                <th>
                    Action
                </th>
            </thead>
            <tbody>
                <?php 
                    for ($i=0; $i < 0; $i++) { 
                ?>
                    <tr>
                        <td>
                            <a href="#">Krian Gudang 1ha utk expansi</a>
                            <div class="label">Krian, Trosobo, Sidoarjo</div>
                            <div class="label">Gudang sewa, >10,000 m<sup>2</sup></div>
                        </td>
                        <td>
							Senin
                            <!-- <select class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" name="minKT">
                                <option value='-1'>Instant</option>    
                                <option value='1'>Approve</option>
                            </select> -->
                        </td>
                        <td>
							18:00 / 24 Jan 2019
                            <!-- <select class="form-control m-bootstrap-select m_selectpicker start" data-live-search="true" name="minKT">
                                <option value='-1'>Harian</option>    
                                <option value='1'>Mingguan</option>
                                <option value='2'>Bulanan</option>
                                <option value='3'>Tahunan</option>
                            </select> -->
                        </td>
                        <td>
							<div class="flex flex-hor" style="position:relative;">
								<a href="#" class="user-photo-mini" style="margin:5px 5px; background-image:url('<?= (isset($_SESSION["loggedPhoto"])) ? "/assets/img/profile-customer/".$_SESSION["loggedPhoto"] : "/assets/img/profile-customer/default.png" ; ?>');"></a>
								<div class="flex flex-ver" style='justify-content: center;'>
									<div><b>Willams Gunawan</b></div>
									<div>+62 81 123 123 123</div>
									<div>williams.gunawan123@gmail.com</div>
								</div>
								<div class="flex flex-ver" style="">
									<div class="listing-content-icon" style="margin:auto;"><i onclick='sendWA(this);' style="margin:10px; font-size: 1.3em;" class="txt-white bg-red rounded-s padding-s fab fa-whatsapp" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Pesan WhatsApp"></i></div>
									<div class="listing-content-icon" style="margin:auto;"><i onclick='sendWA(this);' style="margin:10px; font-size: 1.3em;" class="txt-white bg-red rounded-s padding-s fa fa-phone" data-skin="white" data-toggle="m-popover" data-placement="top" data-content="Call Dial"></i></div>
									
								</div>
							</div>
						</td>
						<td><div class="txt-bold" style="">Pending</div></td>
                        <td><!--<i class="txt-md fas fa-pencil-alt margin-col-md"></i>--> <i class="hoverable txt-md fa fa-times"></i></td>
                    </tr>
                <?php
                    }
                ?>
                
            </tbody>
        </table>


	</section>
	<!-- Page -->
	<section class="page-section">
		<div class="container">
			
			<div class="row">
                
				<!-- sidebar -->
				
			</div>
		</div>
	</section>
	<!-- Page end -->

	<?php 
		$this->load->view('page-part/common-foot');
	?>

</body>
</html>