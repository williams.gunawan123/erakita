<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php 
	$this->load->view('page-part/common-head', $pageData);
?>

<body>
	<!-- Header section -->
	<?php $this->load->view('page-part/main-header');?>
	<!-- Header section end -->


	<!-- Page top section -->
	<!-- <section class="page-top-section set-bg" data-setbg="<?= base_url('assets/img/page-top-bg.jpg')?>">
		<div class="container text-white">
			<h2>SINGLE LISTING</h2>
		</div>
	</section> -->
	<!--  Page top end -->


    <!-- Breadcrumb -->
    <div class="site-breadcrumb" style="margin-top: 50px;">
		<div class="container">
			<a href="/"><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i>Developer</span>
		</div>
	</div>
	<section class="my-container flex flex-ver">
        <div class="container" style="margin:0px;width:100%; max-width:120000px; padding:0px">
            <div style="margin-top:5vh; flex-wrap:wrap;" class="flex flex-hor" id="listDeveloper">
 
            </div>
        </div>
	</section>
	<!-- Page -->
	<section class="page-section">
		<div class="container">

			<div class="row">

				<!-- sidebar -->

			</div>
		</div>
	</section>
	<!-- Page end -->

	<?php 
		$this->load->view('page-part/common-foot');
	?>
</body>

</html>
