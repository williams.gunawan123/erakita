<?php 
class Assets 
{
    private $js, $css, $custom;

    function __construct($admin = false)
    {
        if ($admin) {
            $this -> js = [
                "owl.carousel.min", "masonry.pkgd.min", "magnific-popup.min", "main"
            ];
            $this -> css = [
                "font-awesome.min", "animate", "owl.carousel", "style"
            ];
            // Add default js & css For ADMIN
            $this -> custom = array(
                "css" => [],
                "js" => []
            );
            $this->addcss("page/shared");
            
            $this->addCustom("js",["vendors/popper.js/dist/umd/popper"]);
            $this->addCustom("js",["demo/custom/crud/forms/widgets/bootstrap-select", "js/shared"]);
    
        }
        else{
            // Add default js & css for page
            $this -> js = [
                "jquery-3.2.1.min", "bootstrap.min", "owl.carousel.min", "masonry.pkgd.min", "magnific-popup.min", "main", "shared"
            ];
            $this -> css = [
                "bootstrap.min", "font-awesome.min", "animate", "owl.carousel", "style"
            ];
            // Add default js & css For ADMIN
            $this -> custom = array(
                "css" => [],
                "js" => []
            );
            $this->addcss("page/shared");
    
        }
    }

    function addcss($newCss){
        if (is_array($newCss)) {
            foreach ($newCss as $key => $value) {
                $this -> css[] = $value;
            }
        }
        else{
            $this -> css[] = $newCss;
        }
    }
    function addjs($newJs){
        foreach ($newJs as $key => $value) {
            $this -> js[] = $value;
        }
    }
    function addCustom( $type ,$newCustom){
        foreach ($newCustom as $key => $value) {
            $this -> custom[$type][] = $value;
        }
    }

    function stringCustom($type){
        $string = '';
        if ($type=="js") {
            if (isset($this->custom[$type])) {
                foreach ($this->custom[$type] as $key => $value) {
                    $string .= "<script src='/assets/$value.$type'></script>";
                }
            }
        }
        elseif($type=="css"){
            if (isset($this->custom[$type])) {
                foreach ($this->custom[$type] as $key => $value) {
                    $string .= "<link rel='stylesheet' href='/assets/$value.$type'/>";
                }
            }
        } elseif ($type=="jsLink") {
            if (isset($this->custom[$type])) {
                foreach ($this->custom[$type] as $key => $value) {
                    $string .= "<script src='$value'></script>";
                }
            }
        } elseif ($type=="cssLink") {
            if (isset($this->custom[$type])) {
                foreach ($this->custom[$type] as $key => $value) {
                    $string .= "<link rel='stylesheet' href='$value'/>";
                }
            }
        }
        return $string;
    }

    
    function removeCss( $targetCss){
        $this -> css = array_diff($this -> css, $targetCss);
    }

    function removeJs( $targetJs){
        $this -> js = array_diff($this -> js, $targetJs);
    }

    function stringJs() {
        if (isset($this->js) && count($this->js)>0) {
            $string = '';
            foreach ($this->js as $key => $value) {
                $string .= '<script src="/assets/js/'.$value.'.js"'.'></script>';
            }
            return $string;    
        }
        else{
            return;
        }
    }
    
    function stringCss() {
        if (isset($this->css) && count($this->css)>0) {
            $string = '';
            foreach ($this->css as $key => $value) {
                $string .= "<link rel='stylesheet' href='/assets/css/$value.css' />";
            }
            return $string;    
        }
        else{
            return;
        }
    }
}

?>