<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DetailListing extends CI_Controller {
    function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "Detail Listing";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets(true);
		$assets->addcss("team/sensecode-slider");
		$assets->addcss("page/".strtolower(str_replace(' ','', $pageTitle)));
		
		$assets->addCustom("js",['js/team/public',"js/page/".strtolower(str_replace(' ','', $pageTitle))]);
		$assets->addjs(["team/sensecode-slider"]);
		
		$assets->addCustom("jsLink",["https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"]);
		$assets->addCustom("jsLink",["https://maps.googleapis.com/maps/api/js?key=AIzaSyAznNMid4ZdqySeGTfk0Ebhu_aXuBmb2bc&callback=initMap"]);

		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS

		


		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets,
		);
	}


	function view($id)
	{
		if (isset($_SESSION['loggedUser'])){
			$dataListing = file_get_contents(API_URL .'/api/get/listings/getById/'.$id.'?token=public&id_cust='.$_SESSION['loggedUser']);
		}else{
			$dataListing = file_get_contents(API_URL .'/api/get/listings/getById/'.$id.'?token=public');
		}
		if (isset($_GET['broker'])){
			$dataBroker = file_get_contents(API_URL .'/api/get/brokers/getById/'.$_GET['broker'].'?token=public');
			if (!isset($_GET['rand'])){
				$cookie= array(
					'name'   => 'Erakita-Listing-Broker',
					'value'  => $_GET['broker'],
					'expire' => '86400',
				);
				$this->input->set_cookie($cookie);
			}
		}else{
			if ($this->input->cookie('Erakita-Listing-Broker') != NULL){
				$dataBroker = file_get_contents(API_URL .'/api/get/brokers/getById/'.$this->input->cookie('Erakita-Listing-Broker').'?token=public');
			}else{
				$dataBroker = file_get_contents(API_URL .'/api/get/brokers/getById/'.json_decode($dataListing)->id_broker.'?token=public');
			}
		}
		$this->AllData['dataBroker'] = json_decode($dataBroker);
		$this->AllData['dataListing'] = json_decode($dataListing);
		

		$dataRecent = "";
		if (isset($_SESSION["loggedUser"]) && is_numeric($_SESSION["loggedUser"])) {
			// if (isset($_GET['broker'])){
			// 	$url = API_URL ."/api/get/customers/logView?token=customer&id_cust=" . $_SESSION["loggedUser"] ."&id_listing=" . $id ."&broker=" .$_GET['broker'];
			// }else{
			// 	$url = API_URL ."/api/get/customers/logView?token=customer&id_cust=" . $_SESSION["loggedUser"] ."&id_listing=" . $id;
			// }

			// // Init CURL
			// $curl = curl_init();
			// /* Start Setting CURL Options */
			// curl_setopt($curl, CURLOPT_URL, $url);
			// curl_setopt($curl, CURLOPT_POST, true);
			// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			// /* End Setting CURL Options */
			// // Execute CURL
			// curl_exec($curl);
			// // Close CURL
			// curl_close($curl);
			$dataRecent = file_get_contents(API_URL .'/api/get/customers/getRecentView?token=customer&cust='.$_SESSION["loggedUser"]);
			$this->AllData['dataRecent'] = json_decode($dataRecent);
		}
		$this->load->view('page/detaillisting', $this->AllData);
    }
}
