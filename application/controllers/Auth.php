<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	private $AllData;


	public function login()
	{
        if(isset($_COOKIE["Era-BrokerLogin"])) {
			$customer = json_decode(file_get_contents(API_URL .'/api/get/customers/getById?cusstomer='.$_COOKIE['Era-CustLogin'].'&token=public'));

            $_SESSION['loggedUser'] =  $customer->data->id_customer;
            $_SESSION['loggedName'] =  $customer->data->nama_customer;
            $_SESSION['loggedPhoto'] = $customer->data->photo_customer;

            $cookie_name = "Era-CustLogin";
            $cookie_value = $customer->data->id_customer;
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");

            $ret['key'] = 1;
            $ret['userData'] = $customer->data;

            $ret = json_encode($ret);
		}else{
            $data = ["telp" => $_POST['telp'], "password" => $_POST['password']];
            $data = json_encode($data);
            $url = API_URL ."/api/post/customers/login?token=public";
            // Init CURL
            $curl = curl_init();
            /* Start Setting CURL Options */
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            /* End Setting CURL Options */
            // Execute CURL
            $ret = curl_exec($curl);
            // Close CURL
            curl_close($curl);
            if (isset(json_decode($ret)->userData->id_customer)){
                $_SESSION['loggedUser'] = json_decode($ret)->userData->id_customer;
                $_SESSION['loggedName'] = json_decode($ret)->userData->nama_customer;
                $_SESSION['loggedPhoto'] = json_decode($ret)->userData->photo_customer;
    
                if ($_POST['rememberMe']==1){
                    $cookie_name = "Era-CustLogin";
                    $cookie_value = json_decode($ret)->userData->id_customer;
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
                }
            }
        }
        if(isset($_POST['last_action'])) {
            $lastActionData = explode('#', $_POST['last_action']);
            $this->doLastAction($lastActionData);
        }

		echo $ret;
    }

    public function doLastAction($lastActionData) {
        $userID = $_SESSION['loggedUser'];
        $lastActionMethod = $lastActionData[0];
        $lastActionMethodData = $lastActionData[1];
        $data = ["telp" => $_POST['telp'], "password" => $_POST['password']];
        $data = json_encode($data);
        $url = "";
        if ($lastActionMethod == "like") {
            $url = API_URL ."/api/get/customers/logFavorite?token=customer&id_cust=" . $userID ."&id_listing=" . $lastActionMethodData;
            // Init CURL
            $curl = curl_init();
            /* Start Setting CURL Options */
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            /* End Setting CURL Options */
            // Execute CURL
            curl_exec($curl);

            // Close CURL
            curl_close($curl);

            $url = API_URL ."/api/get/customers/updateFavorite?token=customer&id_cust=" . $userID ."&id_listing=" . $lastActionMethodData;
        } else if ($lastActionMethod == "xout") {
            $url = API_URL ."/api/get/customers/updateXout?token=customer&id_cust=" . $userID ."&id_listing=" . $lastActionMethodData;
        }
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_exec($curl);
        curl_close($curl);
    }
    
    public function logout()
	{
        unset($_SESSION['loggedUser']);
        unset($_SESSION['loggedName']);
        unset($_SESSION['loggedPhoto']);
        unset($_SESSION['activeOneSignal']);
        unset($_COOKIE['Era-CustLogin']);
    }
    
    public function register(){
        $data = ["nama" => $_POST['nama'],
            "email" => $_POST['email'],
            "telp" => $_POST['telp'],
            "password" => $_POST['password']
        ];
        $data = json_encode($data);
        $url = API_URL ."/api/post/customers/register?token=public";
        // Init CURL
        $curl = curl_init();
        /* Start Setting CURL Options */
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        /* End Setting CURL Options */
        // Execute CURL
        $ret = curl_exec($curl);
        // Close CURL
        curl_close($curl);
        echo $ret;
    }
}
