<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyek extends CI_Controller {
    function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "Daftar Proyek";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets(true);
		$assets->addjs(["team/sensecode-paginate", "team/sensecode-map"]); 
		$assets->addCustom("css" ,["vendors/Leaflet.markercluster-1.4.1/dist/leaflet", "vendors/Leaflet.markercluster-1.4.1/dist/MarkerCluster", "vendors/Leaflet.markercluster-1.4.1/dist/MarkerCluster.Default", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.draw", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.rrose"]);
		$assets->addCustom("js" ,["vendors/Leaflet.markercluster-1.4.1/dist/leaflet-src", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.markercluster-src", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.draw", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.rrose-src"]);


		$assets->addCss(["page/". str_replace(" ","",strtolower($pageTitle)), "sensecode-paginate", "team/sensecode-map"] /*["..."] add More if needed*/);



		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS

		


		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets,
			"_currentPage" => ["proyek" => 1] 
		);
	}

	function index()
	{
		$this->AllData["pageData"]["assets"]->addCustom("js",["js/page/daftarproyek"]);
		$this->load->view('page/daftarproyek', $this->AllData);
    }
    
    function detail($id)
    {
		$this->AllData["pageData"]['assets']->addCustom("css",["css/page/detailproyek"]);
		$this->AllData["pageData"]['assets']->addCss(["team/sensecode-slider"]);
		$this->AllData["pageData"]['assets']->addjs(["team/sensecode-slider"]);
		
		$dataProyek = file_get_contents(API_URL .'/api/get/proyeks/getById/'.$id.'?token=public');
		$this->AllData['dataProyek'] = json_decode($dataProyek);
		$dataBroker = file_get_contents(API_URL .'/api/get/brokers/getRandom?token=public');
		$this->AllData['dataBroker'] = json_decode($dataBroker);
		$this->AllData["pageData"]["assets"]->addCustom("js",["js/page/detailproyek"]);
        $this->load->view('page/detailproyek', $this->AllData);
    }
}
