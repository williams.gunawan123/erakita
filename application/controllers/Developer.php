<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Developer extends CI_Controller {
    function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "Daftar Developer";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets(true);
		$assets->addcss("page/".strtolower(str_replace(' ','', $pageTitle)));
		$assets->addCustom("js",["js/page/".strtolower(str_replace(' ','', $pageTitle))]);
		
		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS

		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets,
			"_currentPage" => ["developer" => 1] 
		);
	}


	function index()
	{
		$this->load->view('page/daftardeveloper', $this->AllData);
	}
	
	function detail($id)
	{
		$this->AllData["pageData"]["assets"]->addCustom("js",["js/page/detaildeveloper"]);
		$dataDeveloper = file_get_contents(API_URL .'/api/get/developers/getById/'.$id.'?token=public');
		$this->AllData['dataDeveloper'] = json_decode($dataDeveloper);
		$proyekDeveloper = file_get_contents(API_URL .'/api/get/proyeks/getActive?token=public&id_dev=' .$id);
		$this->AllData['proyekDeveloper'] = json_decode($proyekDeveloper);
		$this->load->view('page/detaildeveloper', $this->AllData);
    }
}
