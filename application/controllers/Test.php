<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {
	public function index()
	{
                $_POST['harga_jual'] = $this->updateInputMask($_POST['harga_jual']);
                $_POST['harga_sewa'] = $this->updateInputMask($_POST['harga_sewa']);
                $_POST['kota'] = str_replace("Kota ","",$_POST['kota']);
                $_POST['kecamatan'] = str_replace("Kecamatan ","",$_POST['kecamatan']);
                $_POST['kelurahan'] = str_replace("Kelurahan ","",$_POST['kelurahan']);
                $_POST['customer'] = $_SESSION['loggedUser'];
                $_POST['broker'] = 1;
                $_POST['proyek'] = 1;
                $_POST['gambar'] = count($_FILES['images']['name']);

                $url = API_URL ."/api/post/listings/addListing?token=customer";
                // Init CURL
                $curl = curl_init();
                /* Start Setting CURL Options */
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($_POST));
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                /* End Setting CURL Options */
                // Execute CURL
                $ret = curl_exec($curl);
                // Close CURL
                curl_close($curl);
                $ret = json_decode($ret);
                if ($ret->stat){
                        $filesCount = count($_FILES['images']['name']);
                        $this->load->library('upload');
                        mkdir('./assets/img/listing/' .$ret->id, 0777, TRUE);
                        for ($i = 0;$i < $filesCount; $i++){
                                $_FILES['file']['name']     = $_FILES['images']['name'][$i];
                                $_FILES['file']['type']     = $_FILES['images']['type'][$i];
                                $_FILES['file']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
                                $_FILES['file']['error']    = $_FILES['images']['error'][$i];
                                $_FILES['file']['size']     = $_FILES['images']['size'][$i];

                                $config['upload_path']      = "./assets/img/listing/" .$ret->id ."/";
                                $config['allowed_types']    = 'jpg|png|jpeg';
                                $config['file_name']        = ($i+1) .'.jpg';
                                
                                $this->upload->initialize($config);

                                echo $config['upload_path'];
                                echo $config['file_name'];
                                if(!$this->upload->do_upload('file')){
                                        echo $this->upload->display_errors();
                                }
                        }
                }
                echo json_encode($ret);
        }
        
        function updateInputMask($input){
                $input = str_replace("Rp ", "", $input);
                $input = str_replace("_", "", $input);
                $input = str_replace(".", "", $input);
                return $input;
        }

        function testkonfirmasi(){
                $this->load->view("page/testkonfirmasi");
        }

        function proses_upload(){
                $idx = $this->input->post("idx");
                $id = $this->input->post("id");

                $ret["name"] = "";
                $ret["error"] = false;
                $ret["errMsg"] = "";

                if (!is_dir('assets/img/listing/' .$id)) {
                        mkdir('./assets/img/listing/' .$id, 0777, TRUE);
                }

                $config['upload_path']      = "./assets/img/listing/" .$id ."/";
                $config['allowed_types']    = 'jpg|png|jpeg';
                $config['file_name']        = $idx .'.jpg';

                $this->load->library('upload',$config);

                if($this->upload->do_upload('file')){
                        $ret["name"] = $this->upload->data('file_name');
                } else {
                        $ret["error"] = true;
                        $ret["errMsg"] = $this->upload->display_errors();
                }

                return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(200)
                        ->set_output(json_encode($ret));
        }
}
