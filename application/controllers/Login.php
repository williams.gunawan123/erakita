<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "Login";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets();

		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS
		$assets->addjs(["page/".strtolower($pageTitle)] /*["..."] add More if needed*/);
		//$assets->addCss(["loginregister2","bootstrap.min"] /*["..."] add More if needed*/);
		
		
		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets
		);
	}

	public function index()
	{
		$this->load->view('page/login2', $this->AllData);
	}
}
