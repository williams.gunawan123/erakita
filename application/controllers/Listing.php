<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing extends CI_Controller {
    function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "Listing";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets(true);

		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS
		$assets->addcss("team/sensecode-slider");
		
		$assets->addCss(["team/sensecode-map", "team/sensecode-table", "page/". str_replace(" ","",strtolower($pageTitle)), "sensecode-paginate"] /*["..."] add More if needed*/);
		// by Default add Ready Page JS
		$assets->addjs(["team/sensecode-paginate", "team/sensecode-map"]);   
	
		$assets->addCustom("css" ,["vendors/Leaflet.markercluster-1.4.1/dist/leaflet", "vendors/Leaflet.markercluster-1.4.1/dist/MarkerCluster", "vendors/Leaflet.markercluster-1.4.1/dist/MarkerCluster.Default", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.draw", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.rrose"]);
		$assets->addCustom("js" ,["vendors/Leaflet.markercluster-1.4.1/dist/leaflet-src", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.markercluster-src", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.draw", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.rrose-src", "js/page/".strtolower($pageTitle)]);

		$assets->addjs([/*"CustomGoogleMapMarker",*/ "team/sensecode-slider"] /*["..."] add More if needed*/);

		// $assets->addCustom("jsLink",["https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"]);
		// $assets->addCustom("jsLink",["https://maps.googleapis.com/maps/api/js?key=AIzaSyAznNMid4ZdqySeGTfk0Ebhu_aXuBmb2bc&callback=initMap"]);
		$assets->addCustom("js",["vendors/custom/datatables/datatables.bundle", "demo/custom/crud/datatables/basic/paginations"]);
		
		$assets->addCustom("css",["vendors/custom/datatables/datatables.bundle"]);
		
		

		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets,
			"_currentPage" => ["listing" => 1] 
		);
	}

	//Load all listing
	function index()
	{
		$this->load->view('page/listing', $this->AllData);
		//$this->load->view('page/forbidden', $this->AllData);
    }
    
    function detail()
    {
        $this->load->view('singlelist/index');
	}
	
	//Load search listing
	function search(){
		
		$this->load->view('page/listing', $this->AllData);
	}
}
