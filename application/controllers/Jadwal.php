<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller {
    function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "Jadwal";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets(true);
		$assets->addcss("page/".strtolower(str_replace(' ','', $pageTitle)));
		$assets->addCustom("js",["js/page/".strtolower(str_replace(' ','', $pageTitle))]);
		
		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS

		


		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets,
			"_currentPage" => ["jadwal" => 1] 
		);
	}


	function index()
	{
		// Ambil Wisth List Bruh
		// $dataListing = file_get_contents(api_url() .'/api/get/listings/getById/'.$id.'?token=public');
		// $this->AllData['dataListing'] = json_decode($dataListing);
		$this->load->view('page/jadwal', $this->AllData);
    }
    
    function detail()
    {
        $this->load->view('singlelist/index');
    }
}
