<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Broker extends CI_Controller {
    function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "DaftarBroker";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets(true);
		$assets->addjs(["team/sensecode-paginate","team/sensecode-map"]); 
		$assets->addCss(["team/sensecode-map", "page/". str_replace(" ","",strtolower($pageTitle)), "sensecode-paginate"] /*["..."] add More if needed*/);
		$assets->addCustom("js",["js/page/".strtolower(str_replace(' ','', $pageTitle))]);
		
		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS
		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets,
			"_currentPage" => ["broker" => 1] 
		);
	}


	function index()
	{
		$this->load->view('page/daftarbroker', $this->AllData);
	}
	
    function detail($id)
	{
		$this->AllData["pageData"]["assets"]->addcss("page/listing");
		$this->AllData["pageData"]["assets"]->addcss("team/sensecode-slider");
		$this->AllData["pageData"]["assets"]->addCustom("css" ,["vendors/Leaflet.markercluster-1.4.1/dist/leaflet", "vendors/Leaflet.markercluster-1.4.1/dist/MarkerCluster", "vendors/Leaflet.markercluster-1.4.1/dist/MarkerCluster.Default", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.draw", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.rrose"]);
		$this->AllData["pageData"]["assets"]->addCustom("js" ,["vendors/Leaflet.markercluster-1.4.1/dist/leaflet-src", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.markercluster-src", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.draw", "vendors/Leaflet.markercluster-1.4.1/dist/leaflet.rrose-src", "js/page/detailbroker"]);
		$this->AllData["pageData"]["assets"]->addCustom("js",["js/team/sensecode-slider"]);

		$dataBroker = file_get_contents(API_URL .'/api/get/brokers/getById/'.$id.'?token=public');
		$this->AllData['dataBroker'] = json_decode($dataBroker);
		$this->load->view('page/detailbroker', $this->AllData);
    }
}
