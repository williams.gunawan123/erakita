<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Createlisting extends CI_Controller {
    function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "Create Listing";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets(true);
		$assets->addcss("page/".strtolower(str_replace(' ','', $pageTitle)));
		$assets->addjs(["page/".strtolower(str_replace(' ','', $pageTitle))]);
		$assets->addCustom("js",['js/team/public']);
		
		$assets->addCustom("jsLink",["https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"]);
		$assets->addCustom("jsLink",["https://maps.googleapis.com/maps/api/js?key=AIzaSyAznNMid4ZdqySeGTfk0Ebhu_aXuBmb2bc&callback=initMap"]);


		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS
		//$assets->addCustom("js", ["demo/custom/crud/wizard/wizard"]);
		$assets->addCustom("js", ["demo/custom/crud/forms/widgets/summernote"]);
		$assets->addCustom("js", ["demo/custom/crud/forms/widgets/bootstrap-select"]);
		$assets->addCustom("js", ["demo/custom/crud/forms/widgets/input-mask"]);
		$assets->addCustom("js", ["demo/custom/crud/forms/widgets/bootstrap-switch"]);
	

		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets,
			"_currentPage" => ["createlisting" => 1] 
		);
	}


	public function index()
	{
		if (!isset($_SESSION["loggedUser"])) {
			redirect("/login?last_url=https://dev.erakita.co.id/createlisting");
		} else {
			$this->load->view('page/createlisting', $this->AllData);
		}
	}
}
