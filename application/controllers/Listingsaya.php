<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListingSaya extends CI_Controller {
    function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "Listing Saya";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets(true);
		$assets->addcss("page/".strtolower(str_replace(' ','', $pageTitle)));
		$assets->addCustom("js",["js/page/".strtolower(str_replace(' ','', $pageTitle))]);
		$assets->addCustom("js",["js/page/dashboard"]);
		

		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS
		$assets->addCustom("jsLink",["https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"]);
		$assets->addCustom("jsLink",["https://maps.googleapis.com/maps/api/js?key=AIzaSyAznNMid4ZdqySeGTfk0Ebhu_aXuBmb2bc&callback=initMap"]);
		


		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets,
			"_currentPage" => ["listingsaya" => 1] 
		);
	}


	function index()
	{
		//$dataListing = file_get_contents(api_url() .'/api/get/listings/getById/'.$id.'?token=public');
		//$this->AllData['dataListing'] = json_decode($dataListing);
		$this->load->view('page/listingsaya', $this->AllData);
    }
    
    function detail($id=null)
    {
		$this->AllData["pageData"]["assets"]->addcss(["sensecode-paginate", "team/sensecode-slider"]);
		$this->AllData["pageData"]["assets"]->addjs(["team/sensecode-paginate", "team/sensecode-slider"]);
		$this->AllData["pageData"]["assets"]->addCustom("js",["js/page/listingdetailsaya"]);
		$dataListing = file_get_contents(API_URL .'/api/get/listings/getById/'.$id.'?token=public');
		$this->AllData['dataListing'] = json_decode($dataListing);
        $this->load->view('page/listingdetailsaya', $this->AllData);
    }
}
