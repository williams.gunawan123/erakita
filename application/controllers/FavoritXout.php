<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FavoritXout extends CI_Controller {
    function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "Favorit - X-out";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets(true);

		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS
		$assets->addjs(["page/favoritxout", "team/sensecode-slider", "../demo/custom/crud/datatables/extensions/select",
			"../vendors/custom/datatables/datatables.bundle"] /*["..."] add More if needed*/);
		$assets->addcss("page/favoritxout");
		$assets->addcss("team/sensecode-slider");
		
		$assets->addCustom("jsLink",["https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"]);
		$assets->addCustom("jsLink",["https://maps.googleapis.com/maps/api/js?key=AIzaSyAznNMid4ZdqySeGTfk0Ebhu_aXuBmb2bc&callback=initMap"]);

		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets,
			"_currentPage" => ["favoritxout" => 1] 
		);
	}

	//Load all listing
	function index()
	{
		$this->load->view('page/favoritxout', $this->AllData);
    }
}
