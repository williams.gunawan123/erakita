<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabang extends CI_Controller {
    function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "Daftar Cabang";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets(true);
		$assets->addcss("page/".strtolower(str_replace(' ','', $pageTitle)));
		$assets->addCustom("js",["js/page/".strtolower(str_replace(' ','', $pageTitle))]);
		$assets->addCustom("js",["js/page/dashboard"]);
		

		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS

		


		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets,
			"_currentPage" => ["cabang" => 1] 
		);
	}

	function index()
	{
		$this->load->view('page/daftarcabang', $this->AllData);
    }
    
    function detail($id)
	{
		$this->AllData["pageData"]["assets"]->addCustom("js",["js/page/detailcabang"]);
		$dataCabang = file_get_contents(API_URL .'/api/get/cabangs/getById/'.$id.'?token=public');
		$this->AllData['dataCabang'] = json_decode($dataCabang);
		$brokerCabang = file_get_contents(API_URL .'/api/get/brokers/getByCabang/'.$id.'?token=public');
		$this->AllData['brokerCabang'] = json_decode($brokerCabang);
		$this->load->view('page/detailcabang', $this->AllData);
    }
}
