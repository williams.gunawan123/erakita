<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist extends CI_Controller {
    function __construct()
	{
		// Call Parent Constructor
		parent::__construct();
		
		// Set PageData
		$webTitle = "EraKita";
		$pageTitle = "Wishlist";
		
		// Call Assets helper class
		$this->load->helper("assets");

		// Load All Default CSS & JS
		$assets = new Assets(true);
		$assets->addcss("page/".strtolower(str_replace(' ','', $pageTitle)));
		$assets->addCustom("js",["js/page/".strtolower(str_replace(' ','', $pageTitle))]);
		
		// Remove Unwanted Assets or Add Custom Assets now
		// Example: param must be in array, and it support multiple value
		// $assets->removeCss(["font-awesome.min", "..." , "..."]); 
		// $assets->addCss(["font-awesome.min", "..." , "..."]); 

		// by Default add Ready Page JS

		


		// Set Current Page Data
		$this->AllData['pageData'] = array(
			"webTitle"  => $webTitle,
			"pageTitle" => $pageTitle,
			"assets" => $assets,
			"_currentPage" => ["wishlist" => 1] 
		);
	}


	function index()
	{
		// Ambil Wisth List Bruh
		$dataWishlist = file_get_contents(API_URL .'/api/get/wishlists/getByCustomer?token=customer&customer=' .$_SESSION['loggedUser']);
		$this->AllData['dataWishlist'] = json_decode($dataWishlist);
		$this->load->view('page/wishlist', $this->AllData);
    }
    
    function detail()
    {
        $this->load->view('singlelist/index');
    }
}
